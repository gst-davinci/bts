/*
 *  Copyright 2006 by Texas Instruments Incorporated.
 *
 *  All rights reserved. Property of Texas Instruments Incorporated.
 *  Restricted rights to use, duplicate or disclose this code are
 *  granted through contract.
 *
 */

/*
 *  ======== std.h ========
 *  This file is an "alias" header used to make client code "look cleaner".
 *  Client code can #include "<xdc/std.h>" rather than "<xdc/rts/std.h>"
 */
#include "rts/std.h"
/*
 *  @(#) xdc 1, 1, 0, 0,132 1-2-2006 xdc-m52
*/

