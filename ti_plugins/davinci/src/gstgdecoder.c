/*
 * Plugin Name : gdecoder
 * Description : A Generic (DSP based) Video Decoder for for TI Davinci DM644x
 *
 * Copyright (C) 2007 Texas Instruments, Inc.
 * Copyright (C) 2008 Behind The Set, LLC
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gstgdecoder.h"
#include "gstcontigbuffer.h"

#define FIX_LINE_LENGTH 2176
#define FIX_LINE_LENGTH_BY_2 2176/2

GST_DEBUG_CATEGORY_STATIC(gstgdecoder_debug);
#define GST_CAT_DEFAULT (gstgdecoder_debug)

/* Codecs Type Enumeration */
typedef enum {
    ST_VIDEO_H264,
    ST_VIDEO_MPEG4,
    ST_VIDEO_WMV3,
    ST_VIDEO_WVC1
} CODECS;

/* Properties Type Enumeration */
typedef enum {
    PROP_0,                 //prop_id should be greater than 0.
    ARG_ENGINE_NAME,
    ARG_ENGINE_OPEN,
    ARG_CODEC_NAME,
    ARG_ENABLE_PROCESSING
} PROPERTIES;

/* Engine Type Enumeration */
typedef enum {
    H264ENGINE,
    MPEG4ENGINE,
    WMENGINE
} ENGINE;

/* Declarations */
static void gst_gdecoder_base_init(GstGDecoderClass * klass);
static void gst_gdecoder_class_init(GstGDecoderClass * klass);
static void gst_gdecoder_init(GstGDecoder * gdecoder);
static GstFlowReturn gst_gdecoder_chain(GstPad * pad,GstBuffer * buffer);
static GstStateChangeReturn gst_gdecoder_change_state(GstElement *
        element,
        GstStateChange
        transition);

static gboolean gst_gdecoder_decoder_initialize(GstGDecoder *
        gdecoder);
static GstCaps *gst_gdecoder_sink_getcaps(GstPad * pad);
static gboolean gst_gdecoder_sink_setcaps(GstPad * pad,
        GstCaps * caps);

static gboolean gst_gdecoder_src_event(GstPad * pad, GstEvent * event);
static gboolean gst_gdecoder_sink_event(GstPad * pad,
                                        GstEvent * event);

static gboolean gst_gdecoder_sink_activate(GstPad * sinkpad);
static gboolean gst_gdecoder_sink_activate_pull(GstPad * sinkpad,
        gboolean active);
static void gst_gdecoder_loop(GstPad * pad);
static void gst_gdecoder_set_property(GObject * object, guint prop_id,
                                      const GValue * value,
                                      GParamSpec * pspec);
static gboolean gst_gdecoder_open_engine(GstGDecoder * gdecoder);


/* elementfactory information */
static GstElementDetails gdecoder_details = {
    "A Generic Video Decoder",
    "Codec/Decoder/Video",
    "Decodes Various Video Formats using the Codec Engine",
    "Rishi Bhattacharya <rishi@gstreamer.net>"
    "                Pratheesh Gangadhar <pratheesh@ti.com>"
};

static GstStaticPadTemplate sink_template = GST_STATIC_PAD_TEMPLATE("sink",
        GST_PAD_SINK,
        GST_PAD_ALWAYS,
        GST_STATIC_CAPS
        ("video/mpeg, "
         "mpegversion = (int) { 4}, "
         "systemstream = (boolean) FALSE; "
         "video/x-h264;"
         "video/x-vc1;"
         "video/x-vc9,"
         "wmvversion = (int) { 3}"));

static GstStaticPadTemplate src_template = GST_STATIC_PAD_TEMPLATE("src",
        GST_PAD_SRC,
        GST_PAD_ALWAYS,
        GST_STATIC_CAPS
        ("video/x-raw-yuv, "
         "format = (fourcc) UYVY , "
         "framerate = (fraction) [ 0, MAX ], "
         "width = (int) [ 1, MAX ], "
         "height = (int) [ 1, MAX ];"
         "video/x-raw-yuv, "
         "format = (fourcc) YUY2 , "
         "framerate = (fraction) [ 0, MAX ], "
         "width = (int) [ 1, MAX ], "
         "height = (int) [ 1, MAX ];"
         "video/x-raw-yuv, "
         "format = (fourcc) YVYU , "
         "framerate = (fraction) [ 0, MAX ], "
         "width = (int) [ 1, MAX ], "
         "height = (int) [ 1, MAX ]"));


static GstElementClass *parent_class = NULL;


#if 0
static GType gst_gdecoder_get_engine_type(void)
{
    static GType gdecoder_engine_type = 0;

    GST_LOG("gdecoder: gst_gdecoder_get_engine_type Begin");
    if (!gdecoder_engine_type) {

        static GEnumValue engine_types[] = {
            {H264ENGINE, "H264Engine","When running H264 or standalone AAC"},
            {MPEG4ENGINE, "MPEG4Engine", "When running MPEG4"},
            {WMENGINE, "WMEngine", "When running WMV or standalone WMA"},
            {0, NULL, NULL}
        };

        gdecoder_engine_type =
            g_enum_register_static("GdecoderEngineName", engine_types);
    }

    GST_LOG("gdecoder: gst_gdecoder_get_engine_type End");
    return gdecoder_engine_type;
}
#endif

static GType gst_gdecoder_get_codec_type(void)
{
    static GType gdecoder_codec_type = 0;

    GST_LOG("gdecoder: gst_gdecoder_get_codec_type Begin");

    if (!gdecoder_codec_type) {
        static GEnumValue codec_types[] = {
            {ST_VIDEO_H264, "H264", "H264 raw bit stream"},
            {ST_VIDEO_MPEG4, "MPEG4", "MPEG4  raw bit stream"},
            {ST_VIDEO_WMV3, "WMV3", "WMV3 (RCV)raw bit stream"},
            {ST_VIDEO_WVC1, "WVC1", "VC1  raw bit stream"},
            {0, NULL, NULL}
        };

        gdecoder_codec_type =
            g_enum_register_static("GdecoderCodecName", codec_types);
    }
    GST_LOG("gdecoder: gst_gdecoder_get_codec_type End");

    return gdecoder_codec_type;
}


GType gst_gdecoder_get_type(void)
{
    static GType gdecoder_type = 0;

    GST_LOG("gdecoder: gst_gdecoder_get_type Begin");

    if (!gdecoder_type) {
        static const GTypeInfo gdecoder_info = {
            sizeof(GstGDecoderClass),
            (GBaseInitFunc) gst_gdecoder_base_init,
            NULL,    /* base_finalize function is NULL */
            (GClassInitFunc) gst_gdecoder_class_init,
            NULL,    /* class_finalize is NULL */
            NULL,    /* class_data is NULL */
            sizeof(GstGDecoder), /* Instance type */
            0,                   /* Number of pre-allocated Instances */
            (GInstanceInitFunc) gst_gdecoder_init,
        };

        gdecoder_type =
            g_type_register_static(GST_TYPE_ELEMENT, "GstGDecoder",
                                   &gdecoder_info, 0);

        GST_DEBUG_CATEGORY_INIT(gstgdecoder_debug,
                                "gdecoder",
                                0,
                                "Generic Video Decoder Element for the ARM");
    }

    GST_LOG("gdecoder: gst_gdecoder_get_type End");

    return gdecoder_type;
}


static GstCaps *gst_gdecoder_sink_getcaps(GstPad * pad)
{
    GstGDecoder *gdecoder;

    GST_LOG("gdecoder: gst_gdecoder_sink_getcaps invoked and call gst_caps_copy");

    gdecoder = GST_GDECODER(GST_PAD_PARENT(pad));
    return gst_caps_copy(gst_pad_get_pad_template_caps(pad));
}

static gboolean gst_gdecoder_sink_setcaps(GstPad * pad, GstCaps * caps)
{
    GstGDecoder *gdecoder;
    GstStructure *structure;
    GstCaps *intersection;
    const gchar *mime;

    GST_LOG("gdecoder: Inside gst_gdecoder_sink_setcaps BEGIN...");

    gdecoder = GST_GDECODER(GST_PAD_PARENT(pad));

    intersection =
        gst_caps_intersect(gst_pad_get_pad_template_caps(pad), caps);
    GST_DEBUG_OBJECT(gdecoder, "Intersection Returned %" GST_PTR_FORMAT,
                     intersection);

    if (gst_caps_is_empty(intersection)) {
        GST_ERROR("gdecoder:  Caps empty error...");
        return FALSE;
    }
    gst_caps_unref(intersection);

    structure = gst_caps_get_structure(caps, 0);
    mime = gst_structure_get_name(structure);

    if (!strcmp("video/x-h264", mime)) {
        gdecoder->codec = ST_VIDEO_H264;
        GST_INFO("gdecoder: H.264");
    }
    else if (!strcmp("video/mpeg", mime)) {
        int version = 0;
        gst_structure_get_int(structure, "mpegversion", &version);

        if (version == 4) {
            gdecoder->codec = ST_VIDEO_MPEG4;
            GST_INFO("gdecoder: MPEG4");
        }
        else
            return FALSE;
    }
    else if (!strcmp("video/x-vc9", mime)) {
        gdecoder->codec = ST_VIDEO_WMV3;
        GST_INFO("gdecoder: WMV3");
    }
    else if (!strcmp("video/x-vc1", mime)) {
        gdecoder->codec = ST_VIDEO_WVC1;
        GST_INFO("gdecoder: VC1");
    }
    else {
        GST_WARNING("gdecoder:  Unknown mime type...");
        return FALSE;
    }
    gdecoder->isCodecPropSet = TRUE;

    GST_LOG("gdecoder: Inside gst_gdecoder_sink_setcaps END...");

    return TRUE;
}

static void gst_gdecoder_base_init(GstGDecoderClass * klass)
{
    GstElementClass *element_class = GST_ELEMENT_CLASS(klass);

    GST_LOG("gdecoder: gst_gdecoder_base_init BEGIN...");

    klass->sink_template = gst_static_pad_template_get(&sink_template);
    klass->src_template = gst_static_pad_template_get(&src_template);

    gst_element_class_add_pad_template(element_class, klass->src_template);
    gst_element_class_add_pad_template(element_class,
                                       klass->sink_template);

    gst_element_class_set_details(element_class, &gdecoder_details);

    GST_LOG("gdecoder: gst_gdecoder_base_init END...");

}

static void gst_gdecoder_class_init(GstGDecoderClass * klass)
{
    GstElementClass *gstelement_class;
    GObjectClass *object_class = (GObjectClass *) (klass);

    GST_LOG("gdecoder: gst_gdecoder_class_init BEGIN...");

    parent_class = g_type_class_ref(GST_TYPE_ELEMENT);
    gstelement_class = (GstElementClass *) klass;
    gstelement_class->change_state = gst_gdecoder_change_state;
    object_class->set_property = gst_gdecoder_set_property;

    g_object_class_install_property(object_class,ARG_ENGINE_OPEN,g_param_spec_string ("EngineOverride","EngineOverride","Force a specific engine to open"
                                    ,"",G_PARAM_WRITABLE));

    g_object_class_install_property(object_class, ARG_CODEC_NAME,
                                    g_param_spec_enum("Codec", "Codec",
                                                      "Name of Codec to run",
                                                      GST_TYPE_GDECODER_CODEC,
                                                      0,
                                                      G_PARAM_WRITABLE));

    g_object_class_install_property(object_class,ARG_ENABLE_PROCESSING,
                                    g_param_spec_boolean ("Enable_Processing",
                                                          "Enable_Processing",
                                                          "Used during dsp on/off"
                                                          ,TRUE,
                                                          G_PARAM_WRITABLE));

    GST_LOG("gdecoder: gst_gdecoder_class_init END...");

}

static void gst_gdecoder_set_property(GObject * object, guint prop_id,
                                      const GValue * value,
                                      GParamSpec * pspec)
{
    GstGDecoder *gdecoder = GST_GDECODER(object);

    GST_LOG("gdecoder: gst_gdecoder_set_property BEGIN...%d..",prop_id);

    switch (prop_id) {

    case ARG_ENGINE_OPEN:
        if(gdecoder->engineName)
            free(gdecoder->engineName);
        gdecoder->engineName = g_value_dup_string(value);
        GST_LOG("Engine name set to value %s",gdecoder->engineName);
        break;

    case ARG_CODEC_NAME:

        gdecoder->codec = g_value_get_enum(value);

        gdecoder->isCodecPropSet = TRUE;

        GST_LOG("gdecoder: set ARG_CODEC_NAME property");
        GST_LOG("gdecoder: Codec set to enum value %d", gdecoder->codec);


        break;

    case ARG_ENABLE_PROCESSING:
        GST_LOG("gdecoder: set ARG_ENABLE_PROCESSING property");

        if (gdecoder->enable_processing != g_value_get_boolean(value)) {

            gdecoder->enable_processing = g_value_get_boolean(value);

            if (gdecoder->enable_processing == TRUE) {

                GST_LOG("gdecoder: Enable_processing");

                g_mutex_unlock(gdecoder->process_lock);
            }

            else {
                GST_LOG("gdecoder: Disable_processing");

                //pthread_mutex_lock(adecoder->start_decode);

            }
        }

        break;

    default:
        GST_WARNING("gstgdecoder: set_property -- Invalid property ID....");
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);

        break;

    }

    GST_LOG("gdecoder: gst_gdecoder_set_property END...");

}

gboolean gst_gdecoder_query(GstElement * element, GstQuery * query)
{
    GstStructure *vidp;
    GValue bytesConsumed = { 0, };
    GValue dspLoad = { 0, };
    GstGDecoder *gdecoder = GST_GDECODER(element);

    GST_LOG("gdecoder: gst_gdecoder_query BEGIN..");

    g_value_init(&bytesConsumed, G_TYPE_UINT64);
    g_value_set_uint64(&bytesConsumed, gdecoder->bytesConsumed);

    g_value_init(&dspLoad, G_TYPE_INT);
    g_value_set_int(&dspLoad, Engine_getCpuLoad(gdecoder->ce));

    vidp = gst_query_get_structure(query);
    gst_structure_set_value(vidp, "bytesConsumed", &bytesConsumed);
    gst_structure_set_value(vidp, "dspCpuLoad", &dspLoad);
    g_value_unset(&bytesConsumed);
    g_value_unset(&dspLoad);

    GST_LOG("gdecoder: gst_gdecoder_query END..");

    return TRUE;
}

/* Instance initialization of GstGDecoder type */

static void gst_gdecoder_init(GstGDecoder * gdecoder)
{
    GstGDecoderClass *klass = GST_GDECODER_GET_CLASS(gdecoder);
    GstElementClass *element_class = GST_ELEMENT_CLASS(klass);


    GST_LOG("gdecoder: gst_gdecoder_init BEGIN..");

    gdecoder->sinkpad =
        gst_pad_new_from_template(klass->sink_template, "sink");
    gst_pad_set_activate_function(gdecoder->sinkpad,
                                  GST_DEBUG_FUNCPTR
                                  (gst_gdecoder_sink_activate));
    gst_pad_set_activatepull_function(gdecoder->sinkpad,
                                      GST_DEBUG_FUNCPTR
                                      (gst_gdecoder_sink_activate_pull));

    gst_element_add_pad(GST_ELEMENT(gdecoder), gdecoder->sinkpad);
    gst_pad_set_event_function(gdecoder->sinkpad,
                               GST_DEBUG_FUNCPTR(gst_gdecoder_sink_event));

    gst_pad_set_chain_function(gdecoder->sinkpad, gst_gdecoder_chain);
    gst_pad_set_getcaps_function(gdecoder->sinkpad,
                                 gst_gdecoder_sink_getcaps);
    gst_pad_set_setcaps_function(gdecoder->sinkpad,
                                 gst_gdecoder_sink_setcaps);

    gdecoder->srcpad =
        gst_pad_new_from_template(klass->src_template, "src");
    gst_element_add_pad(GST_ELEMENT(gdecoder), gdecoder->srcpad);
    gst_pad_set_event_function(gdecoder->srcpad,
                               GST_DEBUG_FUNCPTR(gst_gdecoder_src_event));
    element_class->query = gst_gdecoder_query;
    gdecoder->bytesConsumed = 0;
    gst_gdecoder_reset(gdecoder);

    GST_LOG("gdecoder: gst_gdecoder_init END..");
}

static gboolean gst_gdecoder_sink_activate(GstPad * sinkpad)
{
    GstGDecoder *gdecoder = GST_GDECODER(gst_pad_get_parent(sinkpad));

    GST_LOG("gdecoder: gst_gdecoder_sink_activate invoked");
    if (gst_pad_check_pull_range(sinkpad)) {
        gdecoder->streaming = FALSE;
        GST_LOG("gdecoder: STREAMING IS FALSE");
        gst_object_unref(gdecoder);
        return gst_pad_activate_pull(sinkpad, TRUE);
    }
    else {
        //GST_LOG("gdecoder: pull_range not supported on sinkpad");
        gdecoder->streaming = TRUE;
        GST_LOG("gdecoder: STREAMING IS TRUE");
        gst_object_unref(gdecoder);
        return gst_pad_activate_push(sinkpad, TRUE);
    }
    GST_LOG("gdecoder: gst_gdecoder_sink_activate END");

}

static gboolean
gst_gdecoder_sink_activate_pull(GstPad * sinkpad, gboolean active)
{
    GstGDecoder *gdecoder = GST_GDECODER(gst_pad_get_parent(sinkpad));

    GST_LOG("gdecoder: gst_gdecoder_sink_activate_pull BEGIN..");

    if (active) {
        /* if we have a scheduler we can start the task */
        gst_pad_start_task(sinkpad, (GstTaskFunction) gst_gdecoder_loop,
                           sinkpad);
    }
    else {
        gst_pad_stop_task(sinkpad);
    }
    gst_object_unref(gdecoder);

    GST_LOG("gdecoder: gst_gdecoder_sink_activate_pull END...");

    return TRUE;
};

/* gst_gdecoder_decode accepts output buffer to fill as argument and updates it
   with decoded output and time stamp to display. If no output  is avaiable, will set
   GST_CLOCK_TIME_NONE as time stamp
*/

static gint gst_gdecoder_decode(GstGDecoder * gdecoder, GstBuffer ** outbuf)
{
    //    IVIDDEC_DynamicParams dynamicParams;
    IVIDDEC_DynamicParams dynamicParams;
    IVIDDEC_Status status;
    IVIDDEC_InArgs vidDecInArgs;
    IVIDDEC_OutArgs vidDecOutArgs;

    XDAS_Int32 inBufSize, bufferSize, outBufSize, ret;
    XDAS_Int8 *outData, *inData;
    guint64 pts, timestamp;
    guint64 duration = FRAME_DURATION;
    GstFlowReturn res;
    GstBuffer *buf = NULL;      //Used in pad pull range

    GST_LOG("gdecoder: gst_gdecoder_decode BEGIN..");

    //Calculate number of bytes avaialable in the input buffer
    if (!gdecoder->streaming) {
        inBufSize = GST_BUFFER_SIZE(gdecoder->inbuf) - gdecoder->curInBufStartOffset;
        //Update input buffer start address
        inData =
            (XDAS_Int8 *) ((guint32) GST_BUFFER_DATA(gdecoder->inbuf) +
                           gdecoder->curInBufStartOffset);
        GST_LOG("gdecoder_decode: No streaming.. inBufSize = %d, inData  = %p ",inBufSize, inData);
    }
    else {
        if (gdecoder->inbuf_pool == NULL) {
            GST_ERROR("gdecoder_decode: inbuf_pool == NULL");
            gdecoder->isEof = 1;
            return -2;
        }
        g_mutex_lock(gdecoder->inbuf_lock);
        buf = (GstBuffer *) (gdecoder->inbuf_pool->data);
        inBufSize = GST_BUFFER_SIZE(buf);
        timestamp = GST_BUFFER_TIMESTAMP(buf);
        duration = GST_BUFFER_DURATION(buf);
        gdecoder->inbuf_pool =
            g_slist_delete_link(gdecoder->inbuf_pool,
                                gdecoder->inbuf_pool);
        g_mutex_unlock(gdecoder->inbuf_lock);
        //GST_LOG("gdecoder: buffer size = %d", inBufSize);
        memcpy(GST_BUFFER_DATA(gdecoder->inbuf), GST_BUFFER_DATA(buf), inBufSize);
        gst_buffer_unref(buf);
        inData = (XDAS_Int8 *) GST_BUFFER_DATA(gdecoder->inbuf);
        GST_LOG("gdecoder_decode: Streaming ON.. inBufSize = %d, inData  = %p ",inBufSize, inData);
    }
    //RCV1 header parsing
rcv1:
    if ((gdecoder->codec == ST_VIDEO_WMV3) && gdecoder->isRcv) {
        if (gdecoder->ptsNoDemux == PREROLL_TIME) {
            inBufSize = 20;
        }
        else {
            inBufSize =
                (guint8) inData[0] | ((guint8) inData[1] << 8) | ((guint8)
                        inData[2]
                        << 16) |
                ((guint8) inData[3] << 24);


            inData += 4;
            gdecoder->curInBufStartOffset += 4;
        }

    }
    GST_LOG("gdecoder: inbuf size = %d , ptr = %p", inBufSize, inData);

    //Fill in codec engine input parameters before process call
    gdecoder->inBufDesc.bufSizes = &inBufSize;
    gdecoder->inBufDesc.numBufs = 1;
    gdecoder->inBufDesc.bufs = &inData;
    outBufSize = GST_BUFFER_SIZE(*outbuf);
    outData = (XDAS_Int8 *) GST_BUFFER_DATA(*outbuf);

    GST_LOG("gdecoder: outbuf size = %d , ptr = %p", outBufSize, outData);

    gdecoder->outBufDesc.bufSizes = &outBufSize;
    gdecoder->outBufDesc.bufs = &outData;
    gdecoder->outBufDesc.numBufs = 1;

    //We use presentation time stamp (pts) as input ID
    if (gdecoder->streaming) {
        pts = timestamp;
    }
    else {
        pts = gdecoder->ptsNoDemux;
    }

    // Handle time stamp over flow, since inputID is 32-bita
    GST_BUFFER_TIMESTAMP(*outbuf) = pts;
    GST_BUFFER_DURATION(*outbuf) = duration;
    vidDecInArgs.inputID = (XDAS_Int32) *outbuf;
    *outbuf = NULL;

    //    dynamicParams.size = sizeof(IVIDDEC_DynamicParams);
    dynamicParams.size = sizeof(IVIDDEC_DynamicParams);

    //    dynamicParams.decodeHeader = 0;
    //    dynamicParams.displayWidth = 0;

    dynamicParams.decodeHeader = XDM_DECODE_AU;
    dynamicParams.displayWidth = XDM_DEFAULT; //FIX_LINE_LENGTH_BY_2;

    dynamicParams.frameSkipMode = IVIDEO_NO_SKIP;

    status.size = sizeof(IVIDDEC_Status);
    vidDecInArgs.size = sizeof(IVIDDEC_InArgs);
    vidDecInArgs.numBytes = inBufSize;
    vidDecOutArgs.size = sizeof(IVIDDEC_OutArgs);
    vidDecOutArgs.outputID = 0;

    ret =
        VIDDEC_process((VIDDEC_Handle) gdecoder->decoder,
                       &(gdecoder->inBufDesc), &(gdecoder->outBufDesc),
                       &vidDecInArgs, &vidDecOutArgs);
    //	    FILE *fid = fopen("vc1dsptrace.txt","w");
    //Engine_fwriteTrace(gdecoder->ce,"DSP-",fid);

    if (ret == VIDDEC_EOK) {
        ret = GST_FLOW_OK;
        GST_DEBUG("VIDDEC_process() called successfully");
        GST_DEBUG("calling VIDDEC_control() for getting XDMSTATUS");
        ret =
            VIDDEC_control((VIDDEC_Handle) gdecoder->decoder,
                           XDM_GETSTATUS, &dynamicParams, &status);
        if (ret != VIDDEC_EOK) {
            GST_WARNING("gdecoder: VIDDEC_control() returned failure for XDMGETSTATUS");
            return -1;
        }
        /* Update bytes consumed for video bitrate query */
        gdecoder->bytesConsumed += vidDecOutArgs.bytesConsumed;

        if (gdecoder->ptsNoDemux == PREROLL_TIME) {
            /* Update width and height for vidoesink settings and output buffer size
               computation */
            gdecoder->width = status.outputWidth;
            gdecoder->height = status.outputHeight;
            GST_LOG("gdecoder: resolution = %dx%d", gdecoder->width,
                    gdecoder->height);
        }
        gdecoder->ptsNoDemux += FRAME_DURATION;
        //If buffer can't be displayed, set time stamp to GST_CLOCK_TIME_NONE
        if (vidDecOutArgs.displayBufs.numBufs == 0) {
            GST_LOG("gdecoder: vidDecOutArlayBufs.numBufs == 0");
            GST_BUFFER_TIMESTAMP(outbuf) = GST_CLOCK_TIME_NONE;
            if (gdecoder->ptsNoDemux != (PREROLL_TIME + FRAME_DURATION))
                gdecoder->ptsNoDemux -= FRAME_DURATION;
        }
        else {
            *outbuf = (GstBuffer *)vidDecOutArgs.outputID;
        }

        //Update input buffer pointer
        gdecoder->curInBufStartOffset += vidDecOutArgs.bytesConsumed;

        if (gdecoder->streaming) {
            if ((gdecoder->codec == ST_VIDEO_WMV3) && gdecoder->isRcv &&
                    (gdecoder->ptsNoDemux == PREROLL_TIME + FRAME_DURATION)
                    && !gdecoder->onlyonce) {
                inData += 20;
                gdecoder->onlyonce = TRUE;
                goto rcv1;

            }
            //if (gdecoder->curInBufStartOffset < (gdecoder->inBufSize) >> 1) {
//            GST_LOG("gdecoder: Bytes consumed by decoder = %ld",
//                   vidDecOutArgs.bytesConsumed);
        }
        else {
            if ((GST_BUFFER_SIZE(gdecoder->inbuf) - gdecoder->curInBufStartOffset) >= (GST_BUFFER_SIZE(gdecoder->inbuf) >> 1)) {  //
                //  GST_LOG("gdecoder: Bytes consumed by decoder = %ld",
                //         vidDecOutArgs.bytesConsumed);
            } else {
                //GST_LOG("gdecoder:  Move %ld bytes to start of buffer ", (gdecoder->inBufSize-gdecoder->curInBufStartOffset));
                memmove(GST_BUFFER_DATA(gdecoder->inbuf),
                        (guint8 *) ((guint32) GST_BUFFER_DATA(gdecoder->inbuf) +
                                    gdecoder->curInBufStartOffset),
                        (GST_BUFFER_SIZE(gdecoder->inbuf) -
                         gdecoder->curInBufStartOffset));
                if ((res =
                            gst_pad_pull_range(gdecoder->sinkpad,
                                               gdecoder->readOffset,
                                               gdecoder->curInBufStartOffset,
                                               &buf)) != GST_FLOW_OK) {
                    gdecoder->isEof = TRUE;
                    GST_LOG("gdecoder: EOF reached");
                    //gst_pad_push_event (gdecoder->srcpad, gst_event_new_eos ());
                    return -2;
                }
                gdecoder->readOffset += gdecoder->curInBufStartOffset;
                memcpy((guint8 *) ((guint32) GST_BUFFER_DATA(gdecoder->inbuf) +
                                   (GST_BUFFER_SIZE(gdecoder->inbuf) -
                                    gdecoder->curInBufStartOffset)),
                       GST_BUFFER_DATA(buf), GST_BUFFER_SIZE(buf));
                gdecoder->curInBufStartOffset = 0;
                gst_buffer_unref(buf);
            }
            ret =
                VIDDEC_control((VIDDEC_Handle) gdecoder->decoder,
                               XDM_GETBUFINFO, &dynamicParams, &status);
            if (ret != VIDDEC_EOK) {
                GST_WARNING
                ("VIDDEC_control() returned failure for XDM_GETBUFINFO");
                return -1;
            }
            bufferSize =
                ((status.bufInfo.minInBufSize[0] << 1) -
                 GST_BUFFER_SIZE(gdecoder->inbuf));

            if (bufferSize > 0) {
                GstBuffer *tmp;
                GST_LOG("gdecoder: Re-allocating to new size = %ld",
                        status.bufInfo.minInBufSize[0]);
                tmp = gdecoder->inbuf;
                gdecoder->inbuf = gst_contigbuffer_new(status.bufInfo.minInBufSize[0] * 2);
                if (gdecoder->inbuf == NULL)
                    GST_WARNING("gdecoder: Error during realloc");
                memcpy(GST_BUFFER_DATA(gdecoder->inbuf), GST_BUFFER_DATA(tmp), GST_BUFFER_SIZE(tmp));
                if (tmp != NULL)
                    gst_buffer_unref(tmp);
                if ((res =  gst_pad_pull_range(gdecoder->sinkpad,
                                               gdecoder->readOffset, bufferSize,
                                               &buf)) != GST_FLOW_OK) {
                    gdecoder->isEof = TRUE;
                    GST_LOG("gdecoder: EOF reached");
                    //gst_pad_push_event (gdecoder->srcpad, gst_event_new_eos ());
                    return -2;
                }
                gdecoder->readOffset += bufferSize;
                memcpy((guint8 *) ((guint32) GST_BUFFER_DATA(gdecoder->inbuf) +
                                   GST_BUFFER_SIZE(gdecoder->inbuf)),
                       GST_BUFFER_DATA(buf), bufferSize);
                gst_buffer_unref(buf);
            }
        }
    }

    else {
        GST_DEBUG("VIDDEC_process() returned failure");
        //gst_pad_push_event(gdecoder->srcpad, gst_event_new_eos());
        return -1;
    }

    GST_LOG("gdecoder: gst_gdecoder_decode END..");

    return 0;
}

gint GCompare(gconstpointer a, gconstpointer b)
{
    guint64 ts_a, ts_b;
    gint ret;

    GST_LOG("gdecoder: GCompare BEGIN..");

    ts_a = GST_BUFFER_TIMESTAMP((GstBuffer *) a);
    ts_b = GST_BUFFER_TIMESTAMP((GstBuffer *) b);
    ret = (gint) (ts_a - ts_b);

    GST_LOG("gdecoder: GCompare END..");

    return ret;
}

void GPrint(gpointer data, gpointer user_data)
{
    GST_LOG("gdecoder: < %llu >", GST_BUFFER_TIMESTAMP((GstBuffer *) data));
}


void gst_gdecoder_insert_displaybufq(GstGDecoder * gdecoder,
                                     GstBuffer * buf)
{

    GST_LOG("gdecoder: gst_gdecoder_insert_displaybufq BEGIN..");

    g_mutex_lock(gdecoder->pool_lock);
    gdecoder->displaybuf_pool =
        g_slist_insert_sorted(gdecoder->displaybuf_pool, buf, &GCompare);
    g_mutex_unlock(gdecoder->pool_lock);
    //GST_LOG("gdecoder: displayq = ");
    //g_slist_foreach(gdecoder->displaybuf_pool, &GPrint, NULL);
    //GST_LOG("gdecoder: ");

    GST_LOG("gdecoder: gst_gdecoder_insert_displaybufq END..");

}

void gst_gdecoder_remove_displaybufq(GstGDecoder * gdecoder)
{
    GSList *temp;
    g_mutex_lock(gdecoder->pool_lock);

    GST_LOG("gdecoder: gst_gdecoder_remove_displaybufq BEGIN..");


    temp = gdecoder->displaybuf_pool;
    gdecoder->displaybuf_pool =
        g_slist_delete_link(gdecoder->displaybuf_pool,
                            gdecoder->displaybuf_pool);
    g_mutex_unlock(gdecoder->pool_lock);
    //g_slist_foreach (gdecoder->displaybuf_pool, &GPrint , NULL);
    // GST_LOG("gdecoder: ");

    GST_LOG("gdecoder: gst_gdecoder_remove_displaybufq END..");

}

//Read timestamp of  display buffer in the front of the queue
guint64 gst_gdecoder_read_displaybuf_ts(GstGDecoder * gdecoder)
{
    GstBuffer *buffer;
    guint64 ret;

    GST_LOG("gdecoder: gst_gdecoder_read_displaybuf_ts BEGIN..");

    if (gdecoder->displaybuf_pool == NULL)
        return -1;

    buffer = (GstBuffer *) gdecoder->displaybuf_pool->data;
    if (buffer)
        ret = GST_BUFFER_TIMESTAMP(buffer);
    else
        ret = -1;

    GST_LOG("gdecoder: gst_gdecoder_read_displaybuf_ts END..");

    return ret;
}

gint gdecoder_display(GstGDecoder * gdecoder)
{
    gint count;
    guint64 temp;
    GstBuffer *decoutbuf;
    GstCaps *caps;

    GST_LOG("gdecoder: gdecoder_display BEGIN..");

    count = g_slist_length(gdecoder->displaybuf_pool);
    while (count > 0) {
        count--;
        temp = gst_gdecoder_read_displaybuf_ts(gdecoder);
        if (temp == -1 || (gdecoder->lastdispts == temp)) {
            return -1;
        }
        if (!gdecoder->streaming && gdecoder->lastdispts
                && (temp - gdecoder->lastdispts > 60000000)) {
            break;
        }

        decoutbuf = (GstBuffer *) (gdecoder->displaybuf_pool->data);
        gst_gdecoder_remove_displaybufq(gdecoder);
        GST_LOG("gdecoder: pushing buffer to sink, pts = %llu,ptr:%x", temp,
                GST_BUFFER_DATA(decoutbuf));

        //FIXME:  This is here because the way the first buffer is created, we never set caps.
        //        It would be better to handle that one, then always check for null.
        caps = gst_buffer_get_caps(decoutbuf);
        if(caps) {
            gst_caps_unref(caps);
        }
        else {
            gst_buffer_set_caps(decoutbuf, GST_PAD_CAPS(gdecoder->srcpad));
        }

        if (gst_pad_push(gdecoder->srcpad, decoutbuf) != GST_FLOW_OK) {
            GST_LOG("gdecoder: Pad push failed");
        }

        gdecoder->lastdispts = temp;

    }

    GST_LOG("gdecoder: gdecoder_display END..");

    return 0;

}
static gint gst_gdecoder_process(GstGDecoder * gdecoder)
{
    XDAS_Int32 ret, outBufSize;
    IVIDDEC_DynamicParams dynamicParams;
    IVIDDEC_Status status;
    GstFlowReturn res;
    GstBuffer *buf, *decoutbuf;
    //guint8 *data;
    GstCaps *othercaps;
    gint count;
    guint32 inBufSize;
    //guint64 temp;


    GST_LOG("gdecoder: gst_gdecoder_process BEGIN....%d...",gdecoder->state);


    switch (gdecoder->state) {

    case GDECODER_INIT_STATE:
        //INIT STATE
        if (!gdecoder->decReady) {

            if (gst_gdecoder_decoder_initialize(gdecoder))
                gdecoder->decReady = TRUE;
            else {
                GST_ELEMENT_ERROR(gdecoder, RESOURCE, WRITE, (NULL),
                                  ("Could Not Initialize Decoder"));
                return -2;
            }
            dynamicParams.size = sizeof(IVIDDEC_DynamicParams);
            dynamicParams.decodeHeader = 0;
            dynamicParams.displayWidth = 0; // FIX_LINE_LENGTH_BY_2;
            dynamicParams.frameSkipMode = IVIDEO_NO_SKIP;
            status.size = sizeof(IVIDDEC_Status);

            ret =
                VIDDEC_control((VIDDEC_Handle) gdecoder->decoder,
                               XDM_GETBUFINFO, &dynamicParams, &status);
            if (ret != VIDDEC_EOK) {
                GST_WARNING
                ("VIDDEC_control() returned failure for XDM_GETBUFINFO");
                return -1;
            }

            if (gdecoder->streaming)
                inBufSize = status.bufInfo.minInBufSize[0];
            else
                inBufSize = (status.bufInfo.minInBufSize[0] << 1);
            GST_LOG("gdecoder: Actual Input Buffer Size is = %ld", inBufSize);
            GST_LOG("gdecoder: Codec engine reported minimum input buffer size = %ld",
                    status.bufInfo.minInBufSize[0]);
            gdecoder->inbuf = gst_contigbuffer_new(inBufSize);

            if (gdecoder->inbuf == NULL) {
                GST_WARNING("gdecoder: Failed to allocate inbuf");
                return -1;
            }
            if (!gdecoder->streaming) {
                outBufSize = MAX_FRAME_WIDTH * (MAX_FRAME_HEIGHT << 1);
                decoutbuf = gst_contigbuffer_new(outBufSize);
                if (decoutbuf == NULL) {
                    GST_WARNING("gdecoder: Failed to allocate outbuf");
                    return -1;
                }
            }
        }
        //Fill input buffer
        if (!gdecoder->streaming) {
            gdecoder->readOffset = 0;
            if ((res = gst_pad_pull_range(gdecoder->sinkpad,
                                          gdecoder->readOffset,
                                          GST_BUFFER_SIZE(gdecoder->inbuf),
                                          &buf)) != GST_FLOW_OK) {
                gdecoder->isEof = TRUE;
                GST_LOG("gdecoder: EOF reached");
                //gst_pad_push_event (gdecoder->srcpad, gst_event_new_eos ());
                return -2;
            }
            gdecoder->readOffset += GST_BUFFER_SIZE(gdecoder->inbuf);
            memcpy(GST_BUFFER_DATA(gdecoder->inbuf), GST_BUFFER_DATA(buf),
                   GST_BUFFER_SIZE(buf));
            gst_buffer_unref(buf);
            gdecoder->state = GDECODER_PROCESS_STATE;
        }
        else {
            outBufSize = MAX_FRAME_WIDTH * (MAX_FRAME_HEIGHT << 1);
            decoutbuf = gst_contigbuffer_new(outBufSize);
            if (decoutbuf == NULL) {
                GST_WARNING("gdecoder: Failed to allocate outbuf");
                return -1;
            }
            gdecoder->state = GDECODER_PROCESS_STATE;
        }

        //GST_LOG("gdecoder: calling decode");
        gst_gdecoder_decode(gdecoder, &decoutbuf);


        gdecoder->lastdispts = -1;
        //gdecoder->ptsNoDemux = 0;
        othercaps =
            gst_caps_new_simple("video/x-raw-yuv", "format",
                                GST_TYPE_FOURCC, GST_MAKE_FOURCC('U',
                                                                 'Y',
                                                                 'V',
                                                                 'Y'),
                                "framerate", GST_TYPE_FRACTION, 30000,
                                1001, "width", G_TYPE_INT,
                                gdecoder->width, "height", G_TYPE_INT,
                                gdecoder->height, NULL);

        gst_pad_set_caps(gdecoder->srcpad, othercaps);
        gst_pad_use_fixed_caps(gdecoder->srcpad);
        gst_caps_unref(othercaps);
        if (decoutbuf != NULL) {
            if (GST_BUFFER_TIMESTAMP(decoutbuf) != GST_CLOCK_TIME_NONE) {
                gdecoder->srcbuf = gst_contigbuffer_new(gdecoder->width * (gdecoder->height << 1));
                gst_buffer_set_caps(gdecoder->srcbuf, GST_PAD_CAPS(gdecoder->srcpad));

                memcpy(GST_BUFFER_DATA(gdecoder->srcbuf),
                       GST_BUFFER_DATA(decoutbuf),
                       GST_BUFFER_SIZE(gdecoder->srcbuf));
                GST_BUFFER_TIMESTAMP(gdecoder->srcbuf) =
                    GST_BUFFER_TIMESTAMP(decoutbuf);
                gst_buffer_unref(decoutbuf);
                decoutbuf = gdecoder->srcbuf;

                gst_gdecoder_insert_displaybufq(gdecoder, decoutbuf);
                gdecoder->srcbuf = NULL;

            }
            else {
                gst_buffer_unref(decoutbuf);
                decoutbuf = NULL;

            }
        }

        if (!gdecoder->streaming) {
            gst_segment_init(&gdecoder->segment, GST_FORMAT_TIME);
            gst_pad_push_event(gdecoder->srcpad,
                               gst_event_new_new_segment(FALSE,
                                                         gdecoder->segment.
                                                         rate,
                                                         gdecoder->segment.
                                                         format,
                                                         gdecoder->segment.
                                                         start,
                                                         gdecoder->segment.
                                                         duration,
                                                         gdecoder->segment.
                                                         start));
        }
        break;
    case GDECODER_PROCESS_STATE:
        ret = gdecoder_display(gdecoder);
        if (ret == -1)
            return -1;

        if (gdecoder->enable_processing == FALSE) {
            //GST_LOG("gdecoder: Waiting on lock");

            g_mutex_lock(gdecoder->process_lock);

            //GST_LOG("gdecoder: Lock succeeded");

            if (gdecoder->codec == ST_VIDEO_H264) {
                //XDAS_Int32 ret;
                IVIDDEC_DynamicParams dynamicParams;
                IVIDDEC_Status status;
                dynamicParams.size = sizeof(IVIDDEC_DynamicParams);
                status.size = sizeof(IVIDDEC_Status);
                //            ret =
                //	VIDDEC_control((VIDDEC_Handle) gdecoder->decoder,
                //		IMCOP_RESTORE,&dynamicParams,&status);

                //          ret =
                //VIDDEC_control((VIDDEC_Handle) gdecoder->decoder,
                //		XDM_RESET,&dynamicParams,&status);

            }
        }

        if (gdecoder->streaming) {
            count = 1;          //g_slist_length(gdecoder->inbuf_pool) ;
        }
        while ((gdecoder->streaming && (count > 0))
                || (!gdecoder->streaming
                    && (g_slist_length(gdecoder->displaybuf_pool) <= 3))) {
            if (gdecoder->streaming)
                count--;

            //decoutbuf = gst_buffer_new();
            if (gdecoder->srcbuf == NULL) {
                decoutbuf = gst_contigbuffer_new(gdecoder->width * (gdecoder->height << 1));
                if(decoutbuf == NULL) {
                    GST_WARNING_OBJECT(gdecoder,
                                       "failed when allocating a %d bytes buffer",
                                       gdecoder->width *
                                       (gdecoder->height << 1));
                    break;
                }
                gst_buffer_set_caps(decoutbuf, GST_PAD_CAPS(gdecoder->srcpad));

                //GST_LOG("gdecoder: calling decode");
                if (gst_gdecoder_decode(gdecoder, &decoutbuf) < 0) {
                    ret = gdecoder_display(gdecoder);
                    //if (ret  == -1)
                    //    return -1;
                    if(decoutbuf)
                        gst_buffer_unref(decoutbuf);
                    GST_WARNING("gdecoder: decode error");
                    return -2;
                }
                if(decoutbuf != NULL) {
                    if (GST_BUFFER_TIMESTAMP(decoutbuf) == GST_CLOCK_TIME_NONE) {
                        //GST_LOG("gdecoder: Saving %p as srcbuf", decoutbuf);
                        gdecoder->srcbuf = decoutbuf;
                    }
                    else {
                        gdecoder->srcbuf = NULL;
                        gst_gdecoder_insert_displaybufq(gdecoder, decoutbuf);

                    }
                }
            }
            else {
                //GST_LOG("gdecoder: calling decode using %p", gdecoder->srcbuf);
                if (gst_gdecoder_decode(gdecoder, &gdecoder->srcbuf) < 0) {
                    ret = gdecoder_display(gdecoder);
                    //if (ret  == -1)
                    //  return -1;
                    GST_WARNING("gdecoder: decode error");
                    return -2;
                }
                if(gdecoder->srcbuf != NULL) {
                    if (GST_BUFFER_TIMESTAMP(gdecoder->srcbuf) !=
                            GST_CLOCK_TIME_NONE) {

                        gst_gdecoder_insert_displaybufq(gdecoder,
                                                        gdecoder->srcbuf);
                        gdecoder->srcbuf = NULL;
                    }
                }
            }


        }
        break;

    default:
        GST_WARNING("gdecoder: UNKNOWN STATE");
        return -2;
    }

    GST_LOG("gdecoder: gst_gdecoder_process END..");

    return 0;
}

static void gst_gdecoder_loop(GstPad * pad)
{
    XDAS_Int32 ret;
    GstGDecoder *gdecoder = GST_GDECODER(gst_pad_get_parent(pad));

    GST_LOG("gdecoder: gst_gdecoder_loop BEGIN..");

    ret = gst_gdecoder_process(gdecoder);
    if (ret == -2)
        gst_pad_push_event(gdecoder->srcpad, gst_event_new_eos());

    GST_LOG("gdecoder: gst_gdecoder_loop END..");

}

static GstFlowReturn gst_gdecoder_chain(GstPad * pad, GstBuffer * buffer)
{
    GstGDecoder *gdecoder = GST_GDECODER(gst_pad_get_parent(pad));
    gint ret, inBufSize;
    //guint64 *timestamp;


    GST_LOG("gdecoder: gst_gdecoder_chain BEGIN..");

    if (gdecoder->inbuf == NULL)
        inBufSize = 0;
    else if (gdecoder->codec != ST_VIDEO_H264)
        inBufSize = (GST_BUFFER_SIZE(gdecoder->inbuf) << 1);
    else
        inBufSize = GST_BUFFER_SIZE(gdecoder->inbuf);
    if (buffer) {

        GST_LOG("gdecoder: Pushing %llu to time stamp queue, buffer size= %d",
                GST_BUFFER_TIMESTAMP(buffer), GST_BUFFER_SIZE(buffer));
        g_mutex_lock(gdecoder->inbuf_lock);
        gdecoder->inbuf_pool =
            g_slist_append(gdecoder->inbuf_pool, buffer);
        g_mutex_unlock(gdecoder->inbuf_lock);
        gdecoder->preroll_buflen += GST_BUFFER_SIZE(buffer);
    }

    if (gdecoder->preroll_buflen < inBufSize)
        goto end;
    ret = gst_gdecoder_process(gdecoder);
    if (ret == -2) {
        gdecoder_display(gdecoder);
        GST_LOG("gdecoder: displayq length = %d",
                g_slist_length(gdecoder->displaybuf_pool));
        //gst_pad_push_event(gdecoder->srcpad, gst_event_new_eos());
    }


end:
    gst_object_unref(gdecoder);


    GST_LOG("gdecoder: gst_gdecoder_chain END..");

    return GST_FLOW_OK;
}

static gboolean gst_gdecoder_open_engine(GstGDecoder * gdecoder)
{

    GST_LOG("gdecoder: gst_gdecoder_open_engine BEGIN..");

    CERuntime_init();

    if (gdecoder->engineName) {
        //Engine override is set
        GST_LOG ("Opening %s Engine ...", gdecoder->engineName);
        gdecoder->ce = Engine_open(gdecoder->engineName, NULL, NULL);
    }
    else if (gdecoder->isCodecPropSet == TRUE) {
        gdecoder->isRcv = FALSE;
        switch (gdecoder->codec) {
        case ST_VIDEO_H264:
            GST_LOG("gdecoder: Opening H264 Engine...");

            gdecoder->ce = Engine_open("H264Engine", NULL, NULL);
            //            Engine_setTrace(gdecoder->ce,"*=01234567");
            if (gdecoder->ce == NULL) {

                GST_LOG("gdecoder: Engine_open failed");

                return FALSE;
            }

            break;

        case ST_VIDEO_WMV3:
            gdecoder->isRcv = TRUE;

            GST_LOG("gdecoder: Opening WM Engine...");

            gdecoder->ce = Engine_open("WMEngine", NULL, NULL);
            //	    Engine_setTrace(gdecoder->ce,"*=01234567");
            break;

        case ST_VIDEO_WVC1:

            GST_LOG("gdecoder: Opening WM Engine...");

            gdecoder->ce = Engine_open("WMEngine", NULL, NULL);
            //	    Engine_setTrace(gdecoder->ce,"*=01234567");
            break;

        case ST_VIDEO_MPEG4:

            GST_LOG("gdecoder: Opening MPEG4 Engine...");

            gdecoder->ce = Engine_open("MPEG4Engine", NULL, NULL);
            //	    Engine_setTrace(gdecoder->ce,"*=01234567");
            if (gdecoder->ce == NULL) {

                GST_LOG("gdecoder: Engine_open failed");

            }
            break;
        }
    }
    else {                      //both properties not set......return failure.
        GST_LOG("gdecoder: Error:Both properties not set......");

        gdecoder->ce = Engine_open("H264Engine", NULL, NULL);
    }

    if (gdecoder->ce == NULL) {
        GST_LOG("gdecoder: Engine_open failed");

        return FALSE;
    }

    GST_LOG("gdecoder: gst_gdecoder_open_engine END..");

    return TRUE;
}

static gboolean gst_gdecoder_decoder_initialize(GstGDecoder * gdecoder)
{
    VIDDEC_Params params;
    IVIDDEC_DynamicParams dynamicParams;
    IVIDDEC_Status status;
    XDAS_Int32 ret;
    VIDDEC_Handle dec;

    GST_LOG("gdecoder: gst_gdecoder_decoder_initialize BEGIN..");

    params.size = sizeof(VIDDEC_Params);
    params.maxHeight = 576;
    params.maxWidth = 720;
    params.maxFrameRate = 0;
    params.maxBitRate = 0;
    params.dataEndianness = XDM_BYTE;
    params.forceChromaFormat = XDM_YUV_422ILE;

    dynamicParams.size = sizeof(IVIDDEC_DynamicParams);
    dynamicParams.decodeHeader = XDM_DECODE_AU;
    dynamicParams.displayWidth = XDM_DEFAULT; //FIX_LINE_LENGTH_BY_2;
    dynamicParams.frameSkipMode = IVIDEO_NO_SKIP;
    status.size = sizeof(IVIDDEC_Status);


    if (gst_gdecoder_open_engine(gdecoder) == FALSE) {
        GST_WARNING("gdecoder: Fatal error, Open_Engine failed");
        return FALSE;
    }

    switch (gdecoder->codec) {
    case ST_VIDEO_MPEG4: {
        dec = VIDDEC_create(gdecoder->ce, "mpeg4dec", &params);


        if (dec == NULL) {
            GST_LOG("gdecoder: CAN NOT create MPEG4 decoder");
            return FALSE;
        }
        GST_LOG("gdecoder: Created MPEG4 decoder");

        gdecoder->decoder = (void *) dec;
        ret =
            VIDDEC_control((VIDDEC_Handle) gdecoder->decoder,
                           XDM_RESET, &dynamicParams, &status);
        if (ret != VIDDEC_EOK) {

            GST_DEBUG
            ("VIDDEC_control() returned failure for XDM_RESET");
            return FALSE;
        }
        ret =
            VIDDEC_control((VIDDEC_Handle) gdecoder->decoder,
                           XDM_SETPARAMS, &dynamicParams, &status);
        if (ret != VIDDEC_EOK) {
            GST_DEBUG
            ("VIDDEC_control() returned failure for XDM_SETPARAMS");
            //	   	    FILE *fid = fopen("mpeg4dsptrace.txt","w");
            //Engine_fwriteTrace(gdecoder->ce,"DSP-",fid);
            return FALSE;
        }

    }
    break;
    case ST_VIDEO_WMV3:
    case ST_VIDEO_WVC1: {
        IVC1DEC_DynamicParams VC1dynamicParams;
        VC1dynamicParams.viddecDynamicParams.decodeHeader =
            XDM_DECODE_AU;
        VC1dynamicParams.viddecDynamicParams.displayWidth = XDM_DEFAULT; // Previously 0
        VC1dynamicParams.viddecDynamicParams.frameSkipMode =
            IVIDEO_NO_SKIP;
        VC1dynamicParams.viddecDynamicParams.size =
            sizeof(IVC1DEC_DynamicParams);
        if (gdecoder->isRcv) {
            VC1dynamicParams.bIsElementaryStream = 2;       //RCV1 bitstream
        }
        else {
            VC1dynamicParams.bIsElementaryStream = 1;       //VC1 bitstream
        }

        dec = VIDDEC_create(gdecoder->ce, "vc1dec", &params);
        GST_LOG("gdecoder: Created VC1 decoder");

        if (dec == NULL)
            return FALSE;

        gdecoder->decoder = (void *) dec;

        //#if 0 /* Commented by PB - Sept 30 2007 */
        ret =
            VIDDEC_control((VIDDEC_Handle) gdecoder->decoder,
                           XDM_RESET,(IVIDDEC_DynamicParams *)&VC1dynamicParams, &status);
        if (ret != VIDDEC_EOK) {
            GST_DEBUG
            ("VIDDEC_control() returned failure for XDM_RESET");
            return FALSE;
        }
        //#endif
        ret =
            VIDDEC_control((VIDDEC_Handle) gdecoder->decoder,
                           XDM_SETPARAMS,(IVIDDEC_DynamicParams *)&VC1dynamicParams, &status);
        if (ret != VIDDEC_EOK) {
            GST_DEBUG
            ("VIDDEC_control() returned failure for XDM_SETPARAMS");
            return FALSE;
        }
        GST_LOG("gdecoder: VC1 Decoder Control SETPARAMS done successfully");

    }
    break;
    case ST_VIDEO_H264: {
        dec = VIDDEC_create(gdecoder->ce, "h264mpdec", &params);
        //            FILE *fid = fopen("dsptrace.txt","w");
        //Engine_fwriteTrace(gdecoder->ce,"DSP-",fid);
        if (dec == NULL) {
            GST_ERROR("gdecoder: Error in creating H.264 decoder");
            return FALSE;
        }
        GST_LOG("gdecoder: Created H.264 decoder");

        gdecoder->decoder = (void *) dec;

        ret =
            VIDDEC_control((VIDDEC_Handle) gdecoder->decoder,
                           XDM_RESET, &dynamicParams, &status);
        if (ret != VIDDEC_EOK) {
            GST_WARNING
            ("VIDDEC_control() returned failure for XDM_RESET");
            return FALSE;
        }

        ret =
            VIDDEC_control((VIDDEC_Handle) gdecoder->decoder,
                           XDM_SETPARAMS, &dynamicParams, &status);
        if (ret != VIDDEC_EOK) {
            GST_WARNING
            ("VIDDEC_control() returned failure for XDM_SETPARAMS");
            return FALSE;
        }
        //            ret =
        // 	VIDDEC_control((VIDDEC_Handle) gdecoder->decoder,
        //					IMCOP_SAVE,&dynamicParams,&status);
        // if (ret != VIDDEC_EOK) {
        //   GST_WARNING
        //       ("VIDDEC_control() returned failure for IMCOP_SAVE");
        //   return FALSE;
        // }
    }
    break;
    }

    GST_LOG("gdecoder: gst_gdecoder_decoder_initialize END..");

    return TRUE;
}



static gboolean gst_gdecoder_sink_event(GstPad * pad, GstEvent * event)
{

    GstGDecoder *gdecoder = GST_GDECODER(GST_PAD_PARENT(pad));
    gboolean ret = TRUE;
    //gint retval;
    guint length;
    GST_DEBUG_OBJECT(gdecoder, "Got %s event on sink pad",
                     GST_EVENT_TYPE_NAME(event));

    GST_LOG("gdecoder: gst_gdecoder_sink_event BEGIN..");

    GST_LOG("gdecoder: Got %s event on sink pad", GST_EVENT_TYPE_NAME(event));
    switch (GST_EVENT_TYPE(event)) {
    case GST_EVENT_NEWSEGMENT: {
        ret = gst_pad_event_default(pad, event);
        break;
    }
    case GST_EVENT_FLUSH_START:
        ret = gst_pad_event_default(pad, event);
        break;
    case GST_EVENT_FLUSH_STOP:
        ret = gst_pad_event_default(pad, event);
        break;
    case GST_EVENT_EOS:
        length = g_slist_length(gdecoder->inbuf_pool);
        while (length > 0) {
            length--;
            gst_gdecoder_process(gdecoder);
            //GST_WARNING("gdecoder: length = %d", length);
        }
        gdecoder_display(gdecoder);
        ret = gst_pad_event_default(pad, event);
        break;
    default:
        ret = gst_pad_event_default(pad, event);
        break;
    }

    GST_LOG("gdecoder: gst_gdecoder_sink_event END..");

    return ret;
}

static gboolean gst_gdecoder_src_event(GstPad * pad, GstEvent * event)
{
    gboolean res;
    GstGDecoder *gdecoder;

    GST_LOG("gdecoder: gst_gdecoder_src_event BEGIN..");

    gdecoder = GST_GDECODER(GST_PAD_PARENT(pad));

    if (gdecoder->decoder == NULL)
        goto no_decoder;

    res = gst_pad_push_event(gdecoder->sinkpad, event);
    return res;

no_decoder: {
        GST_DEBUG_OBJECT(gdecoder, "no decoder, cannot handle event");
        gst_event_unref(event);
        return FALSE;
    }

    GST_LOG("gdecoder: gst_gdecoder_src_event END..");

}
void gst_gdecoder_reset(GstGDecoder * gdecoder)
{

    GST_LOG("gdecoder: gst_gdecoder_reset BEGIN..");

    gdecoder->decReady = FALSE;
    gdecoder->state = GDECODER_INIT_STATE;
    gdecoder->srcbuf = NULL;
    gdecoder->width = MAX_FRAME_WIDTH;
    gdecoder->height = MAX_FRAME_HEIGHT;
    if (gdecoder->inbuf)
      gst_buffer_unref(gdecoder->inbuf);
    gdecoder->inbuf = NULL;
    gdecoder->curInBufStartOffset = 0;
    gdecoder->ptsNoDemux = PREROLL_TIME;
    gdecoder->readOffset = 0;
    gdecoder->isEof = FALSE;
    gdecoder->pool_lock = g_mutex_new();
    gdecoder->inbuf_lock = g_mutex_new();
    gdecoder->onlyonce = FALSE;
    gdecoder->preroll_buflen = 0;
    gdecoder->isCodecPropSet = FALSE;
    gdecoder->enable_processing = TRUE;
    gdecoder->process_lock = g_mutex_new();
    g_mutex_lock(gdecoder->process_lock);


    GST_LOG("gdecoder: gst_gdecoder_reset END..");

}

void gst_gdecoder_cleanup(GstGDecoder * gdecoder)
{
    GstBuffer *buf;
    gint length;

    GST_LOG("gdecoder: gst_gdecoder_cleanup BEGIN..");

    g_mutex_lock(gdecoder->inbuf_lock);
    length = g_slist_length(gdecoder->inbuf_pool);
    while (length > 0) {
        length--;
        //GST_LOG("gdecoder: length = %d", g_slist_length (gdecoder->inbuf_pool));
        buf = (GstBuffer *) (gdecoder->inbuf_pool->data);
        gst_buffer_unref(buf);
        gdecoder->inbuf_pool =
            g_slist_delete_link(gdecoder->inbuf_pool,
                                gdecoder->inbuf_pool);
    }
    g_mutex_unlock(gdecoder->inbuf_lock);
    if (gdecoder->decoder)
        VIDDEC_delete((VIDDEC_Handle) gdecoder->decoder);
    if (gdecoder->ce)
        Engine_close(gdecoder->ce);
    g_mutex_free(gdecoder->pool_lock);
    g_mutex_free(gdecoder->inbuf_lock);

    g_mutex_unlock(gdecoder->process_lock);//FIXME:check if two unlocks are needed
    g_mutex_free(gdecoder->process_lock);

    if (gdecoder->inbuf != NULL)
        gst_buffer_unref(gdecoder->inbuf);
    gdecoder->inbuf = NULL;

    //GST_LOG("gdecoder: cleanup done");

    GST_LOG("gdecoder: gst_gdecoder_cleanup END..");

}

static GstStateChangeReturn
gst_gdecoder_change_state(GstElement * element, GstStateChange transition)
{
    GstStateChangeReturn result;
    GstGDecoder *gdecoder = GST_GDECODER(element);

    GST_LOG("gdecoder: gst_gdecoder_change_state BEGIN..");

    switch (transition) {
    case GST_STATE_CHANGE_NULL_TO_READY:
        GST_DEBUG_OBJECT(gdecoder, "State Changed from NULL_TO_READY");
        //gst_gdecoder_reset(gdecoder);
        break;
    case GST_STATE_CHANGE_READY_TO_PAUSED:
        GST_DEBUG_OBJECT(gdecoder, "State Changed from READY_TO_PAUSED");
        break;
    default:
        break;
    }

    result =
        GST_ELEMENT_CLASS(parent_class)->change_state(element, transition);

    switch (transition) {
    case GST_STATE_CHANGE_PAUSED_TO_READY:
        //GST_LOG ("gdecoder: paused to ready");
        GST_DEBUG_OBJECT(gdecoder, "State Changed from PAUSED_TO_READY");
        //GST_LOG("gdecoder: displayq length = %d", g_slist_length(gdecoder->displaybuf_pool) );

        break;
    case GST_STATE_CHANGE_READY_TO_NULL:
        gst_gdecoder_cleanup(gdecoder);
        gst_gdecoder_reset(gdecoder);
        GST_DEBUG_OBJECT(gdecoder, "State Changed from READY_TO_NULL");
        //GST_LOG ("gdecoder: ready to null");
        break;
    default:
        break;
    }

    GST_LOG("gdecoder: gst_gdecoder_change_state END..");

    return result;
}
