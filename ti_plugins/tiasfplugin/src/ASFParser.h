/*
 * ASFParser.h
 *
 * Header file for data Types in ASF Demuxer/Parser plugin 
 * for Gstreamer implementation on DM644x EVM
 *
 * Copyright (C) 2007 Texas Instruments, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#ifndef _ASF_PARSER_H_
#define _ASF_PARSER_H_

#include <dataTypes.h>
#include <ASFParserError.h>
#include <ASFParserInternal.h>


/*
 * Metadata supported by the ASF Parser
 */
/*
 * TITLE field.
 */
#define TITLE                               0xF001

/*
 * ALBUM field.
 */
#define ALBUM                               0xF002

/*
 * ARTIST field.
 */
#define ARTIST                              0xF003

/*
 * GENRE field.
 */
#define GENRE                               0xF004
        
/*
 * ALBUMARTIST field.
 */                             
#define ALBUMARTIST                         0xF005

/*
 * DESCRIPTION field.
 */
#define DESCRIPTION                         0xF006

/*
 * Rating field.
 */
#define RATING                              0xF007
            
/*
 * TOTALPLAYTIME field.
 */            
#define TOTALPLAYTIME                       0xF008

/*
 * PREROLL field.
 */
#define PREROLL                             0xF009
    
/*
 * AUDIO CODEC type field.
 */    
#define AUDIOCODEC                          0xF00A

/*
 * VIDEO CODEC type field.
 */
#define VIDEOCODEC                          0xF00B



typedef ASFPARSERPBCONTEXTINTERNAL          ASFPARSERPBCONTEXT;
typedef ASFPARSERPBCONTEXT*                 ASFPARSERPBCONTEXTHANDLE;


/*
 * Type of the video codec used to encode the video stream
 */ 
typedef enum 
{
    INVALID_VID = 0,
    WMV1_VID,
    WMV2_VID,
    WMV3_VID,
    UNKNOWN_VID
} VIDCODECTYPE;

#define COMPRESSIONID_WMV1  0x31564D57
#define COMPRESSIONID_WMV2  0x32564D57
#define COMPRESSIONID_WMV3  0x33564D57
#define COMPRESSIONID_WVC1  0x31435657

#define COMPRESSIONID_MP43  0x3334504D
#define COMPRESSIONID_MP4S  0x5334504D
#define COMPRESSIONID_MS42  0x3234534D

#define COMPRESSIONID_mp43  0x3334706D
#define COMPRESSIONID_mp4s  0x7334706D
#define COMPRESSIONID_ms42  0x3234736D

/*
 * Create aliases for the codec types supported by the ASF Spec 01.20.30
 *  - See Chapter 11 of spec
 */
#define WMA_SERIES7_9           0x0161
#define WMA9_PRO                0x0162
#define WMA9_LSL                0x0163
#define GSM_AMR_FIXED           0x7A21
#define GSM_AMR_VAR             0x7A22

/* Album ID length */
#define WM_ALBUM_ID_LEN             28
/* Artist ID length */
#define WM_ARTIST_ID_LEN            30
/* Other ID length */
#define WM_OTHER_ID_LEN             18
/* Maximum ID length */
#define WM_MAX_ID_LEN               WM_ARTIST_ID_LEN
/* Maximum stream language ID */
#define MAX_STREAM_LANG_ID          24
/*
 * MPEG Video FourCC codes
 */
typedef enum
{
    FOURCC_UNKNOWN = 0,
    FOURCC_MP4S = 1,
    FOURCC_MS42 = 2,
    FOURCC_MP43 = 3,
    FOURCC_WMV1 = 4,
    FOURCC_WMV2 = 5,
    FOURCC_WMV3 = 6,
    FOURCC_WVC1 = 7    
} VIDEOFOURCCCODEC;


/*
 * Language of an ASF stream
 */
typedef enum
{
    /*
     * unknown language
     */
    UNKNOWN = 0,
    
    /*
     * English  US
     */
    EN_US,
    
    /*
     * Croatian
     */
    HR, 
    
    /*
     *  Hebrew 
     */
    HE, 
    
    /*
     * French
     */
    FR,
    
    /*
     * Japanese
     */
    JA 
} ASFSTREAMLANG;


/*
 * Video encoding profile sued to encode the video stream
 */
typedef enum 
{
    VIDEOENCPROFILE_SIMPLE      = 0,
    VIDEOENCPROFILE_MAIN        = 4,
    VIDEOENCPROFILE_ADVANCED    = 12
} VIDEOENCPROFILE;

/*
 * Bitrate type of an ASF stream
 */
typedef enum 
{
    BITRATE_UNKNOWN             = 0,
    BITRATE_CONSTANT            = 1,
    BITRATE_VARIABLE            = 2
} ASFSTREAMBITRATETYPE;

/*
 * Stream to seek
 */
typedef enum {
    SEEK_AUDIO = 0,
    SEEK_VIDEO = 1,
    SEEK_AUDIO_VIDEO = 2
} SEEKSTREAM;


/*
 * Type of a stream in an ASF file 
 */
typedef enum {
    AUDIO                       = 1,
    VIDEO                       = 2
} ASFSTREAMTYPE;



/* Prototype of the read callback function */
typedef  STATUS (*readCallback)(ASFCONTENTHANDLE asfHandle, 
                         BYTE **pbBuffer,  UINT32 fileOffset, 
                         UINT32 cBytes);
/* Prototype of the write callback function */                        
typedef  STATUS (*writeCallback)(ASFCONTENTHANDLE asfHandle, 
                          BYTE *pbBuffer,  UINT32 fileOffset, 
                          UINT32 cBytes);

/*
 * Structure containing the minimum output protection levels 
 */
typedef struct 
{
     UINT16                      wCompressedDigitaVideo;
     UINT16                      wUnCompressedDigitalVideo;
     UINT16                      wAnalogAudio;
     UINT16                      wCompressedDigitalAudio;
     UINT16                      wUnCompressedDigitalAudio;
} MINOPPROTECTIONLVL;


/*
 * Structure to hold the information about a single metadata
 */
typedef struct 
{
    /*
     * ID of the metadata 
     */
	 UINT32                      dwTagID;
    
    /*
     * Buffer to hold the metadata
     */
	 UINT32                      *pBuffer;
    
    /*
     * length of the buffer
     */
	 UINT32                      cBuffer;
}METADATAOBJ;



/*
 * Structure containing the file operation callbacks
 */
typedef struct 
{
	/*
     * Pointer I/O read function
     */
    readCallback                read;

    /*
     * Pointer I/O write function
     */
    writeCallback               write;
} FILEOPS;

/*
 * Structure containing  the meta information about a media object
 */
typedef struct 
{
    /*
     * Media object number to which the payload belongs. A media 
     * object could be Frame in a video stream 
     */
     UINT32       dwMediaObjNum; 
    
    /*
     * Size of the media object to which this payload belongs to 
     */ 
     UINT32       dwMediaObjSize; 
                    
    /*
     * Media object presentation time 
     */                     
     UINT32       dwMediaObjPresTime; 
    
    /*
     *  Is it compressed object 
     */
     BOOL         fCompressed; 
    
    /*
     *  Is it key frame
     */
     BOOL         fKeyFrame; 
} MEDIAOBJMETAINFO;

// Structure containing wave format specific data
typedef struct {
	 UINT16	wFormatTag;
	 UINT16	cChannels;
	 UINT32	dwSamplesPerSec;
	 UINT32	dwAvgBytesPerSec;
	 UINT16	wBlockAlign;
	 UINT16	wBitsPerSample;
	 UINT16	cbSize;
}WAVEFORMATEX, *PWAVEFORMATEX;

// Structure containing WMA specific Data
typedef struct{
	WAVEFORMATEX		WaveFormatEx;
	 UINT32	dwSamplesPerBlock;
	 UINT16	wEncodeOptions;
	 UINT32	dwSuperBlockAlign;
}WMATYPESPECIFICDATA,*PWMATYPESPECIFICDATA;

/*
 * Structure containing the audio stream specific information 
 */
typedef struct
{
    /*
     * Total number of media objects 
     */
     UINT64       cMediaObjects; 
    
    /*
     *  Total play duration 
     */
     UINT64       qwPlayDuration; 
   
    /*
     * Size of the media object
     */
     UINT32       dwMediaObjSize; 
    
    /*
     * 16-byte Audio stream GUID 
     */
     GUID         audStreamType; 
    
    /*
     * Total length of the type specific data 
     */
     UINT32       dwTypeSpecDataLen; 
    
    /*
     * ID of the stream 
     */
     UINT16       wFlags; 
 
    WMATYPESPECIFICDATA	WMATypeSpecData;

} ASFAUDIOSTREAMINFO;

/*
 * Structure containing the video stream specific information
 */
typedef struct
{
    
     INT32                    lFrameHeight;
    
     INT32                    lFrameWidth;
    
    /*
     * leak rate R rate in bits per second
     */
     UINT32                   dwHrdRate; 
    
    /*
     * buffer size of leaky bucket B in milliseconds
     */
     UINT32                   dwHrdBuffSize; 
    
    /* 
     * MPEG Video FourCC code 
     */
    VIDEOFOURCCCODEC            videoFourCCCodec; 
    
    /*
     * Maximum Media object size
     */
     UINT32                   dwMaxMediaObjSize;
     
    /*
     * Video Encoding Profile 
     */
    VIDEOENCPROFILE             vidEncProfile; 
    
    /*
     * Frame rate- number of frames per sec (fps) 
     */
     UINT32                   dwFrameRate; 
    
    /*
     * Count of bits per pixel 
     */
     UINT16                   cBitPerPixel; 
    
    /*
     * Size of the image in bytes 
     */
     UINT32                   dwImageSize; 
    
    /*
     * Horizontal Pixels Per Meter
     */
     INT32                    cXPelsPerMeter; 
    
    /*
     * Vertical Pixels Per Meter
     */
     INT32                    cYPelsPerMeter; 
    
    /*
     * Colors used count
     */
     UINT32                   cClrUsed; 
    
    /*
     * Important colors count
     */
     UINT32                    cClrImportant; 
    
    /*
     * codec specific information appended to the
     * end of the BITMAPINFOHEADER structure 
     */
//     BYTE			pCodecSpecData[4];
    
     UINT32			dwCodecSpecDataSize;
    
     BYTE			*pCodecSpecData;
    
} ASFVIDEOSTREAMINFO;


/*
 * Minimum information about a stream in an ASF file
 */
typedef struct
{
     BYTE                bStreamId;
    
    /*
     * Type of an ASF stream
     */
    ASFSTREAMTYPE           streamType;
    
    /* 
     * Length of the Stream Language ID in terms of 
     * WORDs (unsigned short) 
     */
     BYTE                bStreamLangIdLen;
    
    /*
     * Language type of the stream
     */
     UINT16               wStreamLangId[MAX_STREAM_LANG_ID]; 
    
     
    ASFSTREAMLANG           streamLangType;
        
    /*
     * If the stream is of Variable bit rate or Constant bit rate 
     */
    ASFSTREAMBITRATETYPE    streamBitrateType; 
    
    /*
     * Bit rate of the stream 
     */
     UINT32               streamBitrate;
    
    /*
     * Whether the stream is encrypted or not
     */
     UINT32               fEncryptedStream;
} MINSTREAMINFO;


/***********************************************************************
 * 
 * ASFParserPBContextInit() - Initializes the ASF parser context for a
 *                             particular media file
 *
 * SYNOPSIS
 *       STATUS ASFParserPBContextInit (
 *          ASFPARSERPBCONTEXT      *pParserContext, 
 *          ASFCONTENTHANDLE        hAsfFile, 
 *          FILEOPS                 *pFileOps
 *      )
 *   
 * INPUT
 *      pParserContext  - Pointer to ASF parser context structure to be
 *                        initialized
 *                         
 *      hAsfFile        - Handle to asf file
 *        
 *      pFileOps        - Pointer to a structure containing file
 *                        operation callbacks 
 *       
 * DESCRIPTION
 *      The ASFParserPBConextInit() parses the header objects of asf 
 *      file and initializes the ASFPARSERPBCONTEXTINTERNAL data 
 *      structures of the parser. The user shall allocate the memory to 
 *      the context structure and pass the address of the structure 
 *      through the ASFParserPBContextInit() call. The parser uses the 
 *      context structure pointer as the reference for the subsequent 
 *      operations on the asf file. The ASFParserPBContextInit () shall 
 *      return appropriate error code while parsing the asf file and
 *      initializing the internal asf context structure.
 *       
 * RETURN
 *      ASF_SUCCESS         - Success. 
 *      ASF_E_INVALIDARG    - Parameter is not valid or is a NULL
 *                            pointer.
 *
 *      ASF_E_BADHEADER     - ASF header is improper.
 *      ASF_E_INVALIDSIZE   - Particular field size is invalid in the
 *                            asf file.
 **********************************************************************/
  STATUS ASFParserContextInit (
    ASFPARSERPBCONTEXT      *pParserContext, 
    ASFCONTENTHANDLE        hAsfFile, 
    FILEOPS                 *pFileOps
 );
 
/***********************************************************************
 * 
 * ASFParserPBContextDeInit() - Initializes the ASF parser context for a
 *                             particular media file
 *
 * SYNOPSIS
 *       STATUS ASFParserPBContextInit (
 *          ASFPARSERPBCONTEXT      *pParserContext, 
 *          ASFAUDIOSTREAMINFO      *pAudioStreamInfo, 
 *          ASFVIDEOSTREAMINFO      *pVideoStreamInfo
 *      )
 *   
 * INPUT
 *      pParserContext  - Pointer to ASF parser context structure to be
 *                        initialized
 *                         
 *      pAudioStreamInfo        - Pointer to the structure containing
 *                        Audio stream information.        
 *      pVideoStreamInfo        - Pointer to the structure containing
 *                        Video stream information.        
 *       
 * DESCRIPTION
 *      The ASFParserPBConextDeInit() free up the resources used by
 *      pParserContext, pAudioStreamInfo, pVideoStreamInfo for extracting 
 *      information from the file. 
 *       
 * RETURN
 *      ASF_SUCCESS         - Success. 
 **********************************************************************/
  STATUS ASFParserContextDeInit (
    ASFPARSERPBCONTEXT          *pContext, 
    ASFAUDIOSTREAMINFO		*pAudioStreamInfo,
    ASFVIDEOSTREAMINFO		*pVideoStreamInfo    
); 

/***********************************************************************
 * 
 * ASFGetNumStreams () - Retrieves the total no. of audio and video 
 *                          streams in an ASF file
 *                       
 * SYNOPSIS
 *       STATUS ASFGetNumStreams (
 *          ASFPARSERPBCONTEXTHANDLE        hAsfPbContext,
 *           BYTE                        *pNumAudStreams,
 *           BYTE                        *pNumVidStreams
 *      )
 * 
 * INPUT
 *      hAsfContext     - Handle to ASF parser context.
 *      pNumAudStreams   Pointer to hold the number of audio streams 
 *                          in the asf content.
 *      pNumVidStreams   Pointer to hold the number of video streams 
 *                          in the asf content. 
 *        
 * DESCRIPTION
 *      The ASFGetNumStreams () API retrieves the number of audio and 
 *      video streams in an ASF file.
 * 
 * RETURN
 *      ASF_SUCCESS                 - on success.
 *      ASF_E_INVALIDARG            - if a parameter is not valid or 
 *                                     is a NULL pointer.
 *
 **********************************************************************/
  STATUS ASFGetNumStreams (
    ASFPARSERPBCONTEXTHANDLE            hAsfPbContext,
     BYTE                            *pNumAudStreams,
     BYTE                            *pNumVidStreams
 );


/***********************************************************************
 * 
 * ASFGetStreamMinInfo() - Retrieves the minimum information about 
 *                      specific streams.
 *
 * SYNOPSIS
 *       STATUS ASFGetStreamMinInfo (
 *          ASFPARSERPBCONTEXTHANDLE    hAsfPbContext,
 *           INT32                    dwStreamType,
 *           BYTE                    cStreams,
 *          MINSTRMINFO                 *pMinStreamInfo[]
 *      )
 * 
 * INPUT
 *      hAsfContext      - Handle to ASF parser context.
 *      dwSteamType      - Type of the steam for which the minimum 
 *                          information to be retrieved.
 *      cStreams         - Number of streams for which the information 
 *                          be retrieved.
 *      pMinStreamInfo[]  Pointer to the array of MINSTRMINFO 
 *                          structure.
 * 
 * DESCRIPTION
 *      The ASFGetStreamMinInfo () API retrieves the minimum 
 *      information of audio or video streams which is useful for the 
 *      application for further operations like complete stream 
 *      information extraction and playback. It retrieves the minimum 
 *      information for the first cStreams in the ASF file.  
 *      The minimum information could be stream id, steam language, 
 *      bit rate and bit rate type (variable or constant). 
 *      Application shall pass the pointer to the array of 
 *      MINSTEARMINFO structure to hold the streams information.
 * 
 * RETURN
 *      ASF_SUCCESS                 - on success.
 *      ASF_E_INVALIDARG            - if a parameter is not valid or 
 *                                     is a NULL pointer.
 *
 **********************************************************************/
  STATUS ASFGetStreamMinInfo (
    ASFPARSERPBCONTEXTHANDLE      hAsfPbContext,
     INT32                      dwStreamType,
     BYTE                      cStreams,
    MINSTREAMINFO                 *pMinStreamInfo[]
 );

/***********************************************************************
 * 
 * ASFGetAudioStreamInfo() - Retrieves the information about specified 
 *                            audio stream specific streams.
 *
 * SYNOPSIS
 *       STATUS ASFGetAudioStreamInfo (
 *          ASFPARSERPBCONTEXTHANDLE                hAsfPbContext,
 *           BYTE                                bStreamId,
 *          ASFAUDIOSTREAMINFO                      *pAudStreamInfo
 *      )
 * 
 * INPUT
 *      hAsfContext    - Handle to ASF parser context.
 *      bSteamId       - Id of the audio stream for which the 
 *                          information to be retrieved.
 *      pAudStreamInfo  Pointer to the ASFAUDIOSTREAMINFO structure.
 * 
 * DESCRIPTION
 *      The ASFGetAudioStreamInfo () API retrieves the complete 
 *      information of audio which is useful for the application to 
 *      build the RCA header. Application can directly dump the whole 
 *      structure to the RCA header without any modification to the 
 *      contents of the structure. 
 * 
 * RETURN
 *      ASF_SUCCESS                 - on success.
 *      ASF_E_INVALIDARG            - if a parameter is not valid or 
 *                                     is a NULL pointer.
 *
 **********************************************************************/
  STATUS ASFGetAudioStreamInfo (
    ASFPARSERPBCONTEXTHANDLE                hAsfPbContext,
     BYTE                                bStreamId,
    ASFAUDIOSTREAMINFO                    *pAudStreamInfo
 );


/***********************************************************************
 * 
 * AFSGetVideoStreamInfo() - Retrieves the information about specified 
 *                              video stream.
 *
 * SYNOPSIS
 *       STATUS ASFGetVideoStreamInfo(
 *          ASFPARSERPBCONTEXTHANDLE        hAsfPbContext,
 *           BYTE                        bStreamId,
 *          ASFVIDEOSTREAMINFO              *pVidStreamInfo
 *      )
 * 
 * INPUT
 *      hAsfPbContext   - Handle to ASF parser context.
 *      bSteamId        - Id of the video stream for which the complete
 *                           information to be retrieved.
 *      pVidStreamInfo   Pointer to the ASFVIDEOSTREAMINFO structure.
 *
 * DESCRIPTION
 *      The ASFGetVideoStreamInfo() API retrieves the information about 
 *      the specified video stream which is useful for the application 
 *      to build the RCV header. Application shall pass the pointer to 
 *      the ASFVIDEOSTREAMINFO structure to hold the video stream 
 *      information.
 * RETURN
 *      ASF_SUCCESS                 - on success.
 *      ASF_E_INVALIDARG            - if a parameter is not valid or 
 *                                     is a NULL pointer.
 *
 **********************************************************************/
  STATUS ASFGetVideoStreamInfo (
    ASFPARSERPBCONTEXTHANDLE            hAsfPbContext,
     BYTE                            bStreamId,
    ASFVIDEOSTREAMINFO                  *pAudStreamInfo
 );


/***********************************************************************
 * 
 * ASFSetActiveStream() - Notifies the asf parser to parse the 
 *                          specified stream Id 
 *
 * SYNOPSIS
 *       STATUS ASFSetActiveStream (
 *          ASFPARSERPBCONTEXTHANDLE    hAsfPbContext, 
 *          ASFSTREAMTYPE               strmType,
 *           BYTE                    bActiveStreamId
 *      )
 *
 * INPUT
 *      hAsfPbContext       - Handle to ASF parser context. 
 *      strmType            - Type of the stream.
 *      bActiveStreamId     - Active audio or video stream Id.
 *
 * DESCRIPTION
 *      The ASFSetActiveStream() API sets the Id audio or video stream 
 *      to parse (active stream). The ASFGetNextMediaObj () API will 
 *      retrieve the media object from the active stream in an asf file.     
 * 
 * RETURN
 *      ASF_SUCCESS                 - on success.
 *      ASF_E_INVALIDARG            - if a parameter is not valid or 
 *                                     is a NULL pointer.
 *
 **********************************************************************/
  STATUS ASFSetActiveStream (
    ASFPARSERPBCONTEXTHANDLE        hAsfPbContext, 
    ASFSTREAMTYPE                   strmType,
     BYTE                        bActiveStreamId
 );



/***********************************************************************
 * 
 * ASFDecryptActiveStream() - Notifies the asf parser to decrypt or not
 *                               the specified stream
 *
 * SYNOPSIS
 *       STATUS ASFDecryptActiveStream (
 *          ASFPARSERPBCONTEXTHANDLE    hAsfPbContext, 
 *          ASFSTREAMTYPE               strmType,
 *           BOOL                     fDecrypt
 *      )
 * 
 * INPUT
 *      hAsfContext         - Handle to ASF parser context. 
 *      strmType            - Type of the stream.
 *      fDecrypt            - To be decrypted or not.
 * 
 * DESCRIPTION
 *      The ASFDecryptActiveStream() API specifies whether the active 
 *      stream to be decrypted or not. The ASFGetNextMediaObj() API 
 *      will retrieve the next media object from the active stream 
 *      after payload decryption of the media object.
 * 
 * RETURN
 *      ASF_SUCCESS                 - on success.
 *      ASF_E_INVALIDARG            - if a parameter is not valid or 
 *                                     is a NULL pointer.
 *
 **********************************************************************/
  STATUS ASFDecryptActiveStream (
    ASFPARSERPBCONTEXTHANDLE        hAsfPbContext, 
    ASFSTREAMTYPE                   strmType,
     BOOL                         fDecrypt
 );



/***********************************************************************
 * 
 * ASFMetaGetASFTag() - Query the ASF header for various possible 
 *                          metadata.
 *
 * SYNOPSIS
 *       STATUS ASFMetaGetASFTag (
 *          ASFPARSERPBCONTEXTHANDLE        hAsfPbContext,
 *          METADATAOBJ                     *pAsfTag[],
 *          UINT16                          cMetadata 
 *      )
 *
 * INPUT
 *      hAsfPbContext   - Handle to ASF parser context.
 *      pAsfTag          pointer to an array of METADATAOBJ structures.
 *      cMetadata        number of elements in the METADATAOBJ 
 *                             structures array.
 *
 * DESCRIPTION
 *      The application shall invoke ASFGetMetaASFTag() to get the 
 *      metadata listed in Table 1. from an asf content. Meta data is 
 *      passed in the metadata node structure. The ASFGetMetaASFTag() 
 *      API internally parses the ASF header objects to retrieve the 
 *      specified metadata request.
 *      The steps inside the function are as follows
 *      1.  From the pAsfTag parameter decide which header objects to 
 *              parse in the ASF header.
 *      2.  Use the objects offset set in the ASFPARSERPBCONTEXT 
 *              structure in the ASFParserPBContextInit() API.
 *      3.  Parse the required header objects and extract the metadata 
 *              from the objects.
 * 
 * RETURN
 *      ASF_SUCCESS             - on success.
 *      ASF_E_INVALIDARG        - if a parameter is not valid or 
 *                                   is a NULL pointer.
 *      ASF_E_METAUNSUPPORTED   - if the Metadata is not supported.
 *      ASF_E_FIELDNOTFOUND     - if the particular field was not 
 *                                             found.
 *      ASF_E_INVALIDSIZE       - due to an invalid size being read.
 * 
 **********************************************************************/
  STATUS ASFMetaGetASFTag (
        ASFPARSERPBCONTEXTHANDLE             hAsfPbContext,
        METADATAOBJ                         *pAsfTag,
         UINT16                           cMetadata 
 );


/***********************************************************************
 * 
 * ASFGetNextMediaObj() - Retrieve next media object from the given 
 *                          stream type.
 *
 * SYNOPSIS
 *       STATUS ASFGetNextMediaObj (
 *          ASFPARSERPBCONTEXTHANDLE        hAsfContext, 
 *           INT32                        strmType,
 *           BYTE                        *pbBuffer,
 *           BYTE                        *pcBuffer,
 *           UINT32                       dwStartOffset,
 *          MEDIAOBJMETAINFO                *pMediaObjMetaInfo
 *      )
 *
 * 
 * INPUT
 *      hAsfContext         - handle to ASF parser context. 
 *      strmType            - Type of the stream (audio or video).
 *      pbBuffer             pointer to the buffer to hold the block 
 *                                      data.
 *      pcBuffer             pointer to length of the pbBuffer.
 *      dwStartOffset       - Offset in the buffer at which the media 
 *                                  object to be copied.
 *      pMediaObjMetaInfo    Pointer to MEDIAOBJMETAINFO structure 
 *                                  containing information about 
 *                                  the media object.
 * 
 * 
 * DESCRIPTION
 *      The ASFGetNextMediaObj() API will retrieve a media object 
 *      from the active stream set by ASFSetActiveStream() API. A media
 *      object might have split across multiple payloads in a single 
 *      data packet or in multiple data packets. The buffer to where 
 *      the data shall be copied should be allocated by the application
 *      before invoking the API. The length of the buffer should be at 
 *      least the size of a media object. If the length of the buffer 
 *      is less than the media object, the API shall return 
 *      ASF_E_BUFINSUFFICIENT error code. 
 *      The parser also returns the metadata of media object. 
 *      The metadata information will help the user build the RCV frame 
 *      header for video stream or he can copy the media object 
 *      information to a RCA file before dumping the actual media 
 *      object data. The payload metadata shall be returned in 
 *      MEDIAOBJMETAINFO structure allocated and managed by the user. 
 *      The payload data shall be decrypted if the file is DRM 
 *      protected and also if the fDecryptAudStream or 
 *      fDecryptVidStream members of the ASFPARSERPBCONTEXT is set.
 * 
 *      Steps inside the function     
 *      Let us consider an example of a media object being split across
 *      three payloads.
 *          1. Return ASF_E_BUFINSUFFICIENT error if the media object 
 *              size is more than the length of the buffer.
 *          2. Copy the payload1 to (pbBuffer + dwStartOffset) and the 
 *              decrypt the payload1.
 *          3. Copy the payload2 to (pbBuffer + dwStartOffset + 
 *              payloadSize1) and the decrypt the payload 2.
 *          4. Copy the payload3 to (pbBuffer + dwStartOffset + 
 *              payloadSize1 + payloadSize2) and the decrypt the 
 *              payload 3.
 *          5. Extract the payload header information from all the 
 *              three payloads and fill the MEDIAOBJMETAINOF structure.
 * 
 * RETURN
 *      ASF_SUCCESS            - on success.
 *      ASF_E_INVALIDARG       - if a parameter is not valid or 
 *                                     is a NULL pointer.
 *      ASF_E_BADPACKETHEADER  - if it finds problems parsing an ASF 
 *                                      packet header.
 *      ASF_E_BADPAYLOADHEADER - if it finds problems parsing an ASF 
 *                                      payload header.
 *      ASF_E_BADDATAHEADER    - if it finds problems parsing an ASF 
 *                                       data object header.
 *      ASF_E_BUFINSUFFICIENT  - if the length of the buffer is less 
 *                                      than the media object.
 *      ASF_E_ENDOFFILE        - if it reaches the end of file.
 *
 **********************************************************************/
 STATUS ASFGetNextMediaObj (
        ASFPARSERPBCONTEXTHANDLE        hAsfContext, 
         INT32                        strmType,
         BYTE                        *pbBuffer,
         UINT32                        *pcBuffer,
         UINT32                       dwStartOffset,
        MEDIAOBJMETAINFO                *pMediaObjMetaInfo
 );
 /***********************************************************************
 * 
 * ASFSeekToMS() - Seek to specified time position in the file.
 *
 * SYNOPSIS
 *       STATUS ASFSeekToMS (
 *          ASFPARSERCONTEXTHANDLE          hAsfContext, 
 *           UINT32                       dwTimeOffsetInMS,
 *           INT32                        streamType
 *      ) 
 *
 * INPUT
 *      hAsfContext                 - Pointer to ASF Parser context 
 *                                       structure for a media file.
 *      dwTimeOffsetInMilliseconds  - Time to seek in milliseconds
 *
 * 
 * DESCRIPTION
 *      The ASFSeekToMS() API will internally move both the audio and 
 *      video data parsing pointer to the specified file offset 
 *      calculated from seek value in milliseconds. The ASFSeekToMS() 
 *      API will internally rounds down to start of the packet which 
 *      includes that time. It internally considers total playback 
 *      duration in milliseconds, per packet duration in milliseconds, 
 *      to calculate the data packet offset to jump to from the given 
 *      dwTimeOffsetInMS parameter. It internally compares the seek to 
 *      millisecond value with the presentation time of the media 
 *      object being present in the data packet at the calculated file 
 *      offset to jump to exact media object.
 *      If the given millisecond offset crosses the total playback time,
 *      the parser sets the currentFileOffset mark to last data packet. 
 *      The ASFSeekToMS() API shall return error code if the user 
 *      tries backwards seeks.
 *
 * RETURN
 *      ASF_SUCCESS         - on success.
 *      ASF_E_INVALIDARG    - if a parameter is not valid or 
 *                                     is a NULL pointer.
 *      ASF_E_FAIL          - if the time position is not able to set.
 *      ASF_E_ENDOFFILE     - if it reaches the end of file. 
 *
 **********************************************************************/
  STATUS ASFSeekToMS (
        ASFPARSERPBCONTEXTHANDLE         hAsfContext, 
         UINT32                        dwTimeOffsetInMS,
         INT32                         streamType       
 );





/****************** DRM Content *********************/
#ifdef ASFDRMDEFINE

#include <drmcommon.h>
#include <drmutilities.h>
#include <drmcrt.h>
#include <drmcontextsizes.h>
#include <drmmanager.h>

/* Rights in the DRM */
#define ASF_RIGHT_PLAYBACK                  0x0001
#define ASF_RIGHT_COLLABORATIVE_PLAY        0x0002
#define ASF_RIGHT_COPY_CD                   0x0004
#define ASF_RIGHT_COPY                      0x0008
#define ASF_RIGHT_COPY_TO_SDMI_DEVICE       0x000C
#define ASF_RIGHT_COPY_TO_NON_SDMI_DEVICE   0x0010
#define ASF_RIGHT_BACKUP                    0x0020
#define ASF_RIGHT_PLAYLISTBURN              0x0040

#endif	//ASFDRMDEFINE
#ifdef ASFDRMDEFINE
typedef enum
{
    /*
     * There is no DRM present on the current file
   	 */
    NO_DRM,
    
    /*
     * JANUS DRM has been used to protect the current file
     */
    JANUS_DRM,
    
    /*
     * PD-DRM has been used to protect the current file
     */
    PD_DRM
} DRMTYPE;
/*
 * Structure containing the DRM context information
 */
typedef struct
{
	/*
	 * Initialized state data of the current DRM session
	 */ 
    DRM_MANAGER_CONTEXT				oDRMContextObj; 
	
	/*
	 * Uninitialized DRM decrypt context structure
	 */
    DRM_MANAGER_DECRYPT_CONTEXT		oDRMDecryptContextObj; 
	/*
	 * Structure to hold the minimum output protection levels 
	 */
    MINOPPROTECTIONLVL				oMinOPLObj; 
	
	/*
	 * DRM Type sued to protect the file
	 */
     INT32						dwDRMtype;
} ASFPARSERDRMCONTEXT;

typedef ASFPARSERDRMCONTEXT*        ASFPARSERDRMCONTEXTHANDLE;
#endif //ASFDRMDEFINE
/***********************************************************************
 * 
 * ASFLicenseEvaluate() - Enumerate the licenses in the license store 
 *                          to render the content.
 *
 * SYNOPSIS
 *       STATUS ASFLicenseEvaluate (
 *          ASFPARSERDRMCONTEXTHANDLE       hDrmContext,
 *          ASFCONTENTHANDLE                hAsfContent, 
 *          FILEOPS                         *pFileOps,
 *           UINT32                       dwRights
 *           UINT16                           cRights
 *      )
 * 
 * INPUT
 *      hAsfDrmContext  Handle to ASFPARSERDRMCONTEXT structure. 
 *      hAsfContent    - Handle to the ASF content
 *      pFileOps       - Pointer to a structure containing file 
 *                          operation callbacks.
 *      dwRights        Containing the single or combination of media 
 *                          rights the caller is requesting.
 *      cRights        - Number of rights 
 *        
 * DESCRIPTION
 *      The application shall invoke ASFLicenseEvaluate () API if the 
 *      media rights to be applied on the DRM protected content. 
 *      The DRM will grant the permission to the application requested 
 *      rights (e.g. Play right, Copy right, Sync right) by processing 
 *      the internal license store. The ASF parser shall be tested only
 *      for the playback right.
 * 
 * RETURN
 *      ASF_SUCCESS                 - on success.
 *      ASF_E_INVALIDARG            - if a parameter is not valid or 
 *                                     is a NULL pointer.
 *      ASF_E_ENCRYPTOBJNOTFOUND    - if an encryption object is not 
 *                                     found in the given asf file.
 *      ASF_E_LICENSENOTFOUND       - if license is not found in 
 *                                          license store.
 *
 **********************************************************************/
#ifdef ASFDRMDEFINE
  STATUS ASFLicenseEvaluate (
    ASFPARSERDRMCONTEXTHANDLE           hDrmContext,
    ASFPARSERPBCONTEXTHANDLE		pContext,
    FILEOPS                             *pFileOps,
     UINT32                           dwRights,
     UINT16                           cRights
 );
#endif //ASFDRMDEFINE
 /***********************************************************************
 * 
 * ASFSetDRMContext() - Sets the DRM context to the ASF parser.
 *                      
 *
 * SYNOPSIS
 *       STATUS ASFSetDRMContext (
 *          ASFPARSERPBCONTEXTHANDLE        hAsfPbContext,
 *          ASFPARSERDRMCONTEXTHANDLE       hDrmContext
 *      )
 * 
 * INPUT
 *      hAsfPbContext  - Handle to the parser playback context
 *      hAsfDrmContext  Handle to ASFPARSERDRMCONTEXT structure. 
 *        
 * DESCRIPTION
 *      It updates the DRM decrypt context pointer and the DRM manager
 *      context pointer members of the ASFPARSERPBCONTEXT structure 
 *      from the ASFPARSERDRMCONTEXT structure.
 *      
 * RETURN
 *      ASF_SUCCESS                 - on success.
 *      ASF_E_INVALIDARG            - if a parameter is not valid or 
 *                                     is a NULL pointer.
 *
 **********************************************************************/\
#ifdef ASFDRMDEFINE
  STATUS ASFSetDRMContext (
    ASFPARSERPBCONTEXTHANDLE            hAsfPbContext,
    ASFPARSERDRMCONTEXTHANDLE           hDrmContext
 );
#endif //ASFDRMDEFINE
/***********************************************************************
 * 
 * ASFGetDecryptASF() - Converts an entire encrypted asf content to 
 *                          decrypted one.
 *
 * SYNOPSIS
 *       STATUS ASFGetDecryptASF (
 *          ASFPARSERCONTEXTHANDLE      hAsfContext, 
 *          ASFCONTENTHANDLE            hDestAsfContent
 *      )
 * 
  * INPUT
 *      hAsfContext    Pointer to ASF Parser context structure for a 
 *                          media file.
 *      hDestAsfFile   Handle to destination asf to where decrypted 
 *                          asf to be dumped.
 * 
 * DESCRIPTION
 *      The application shall invoke ASFGetDecryptASF() API, if it 
 *      needs to convert an entire encrypted asf file to a decrypted 
 *      one. The resulting file is also an asf file except that all 
 *      the payload data is decrypted.
 *      The ASFGetDecryptASF() API modifies the ASF header by 
 *      removing the Content Encryption Object and also Extended 
 *      Content Encryption Object their by modifying the ASF header 
 *      size. It also sets the Encrypted Content Flag in the Stream 
 *      Properties Object to false.
 *
 * RETURN
 *      ASF_SUCCESS            - on success.
 *      ASF_E_INVALIDARG       - if a parameter is not valid or 
 *                                     is a NULL pointer.
 *      ASF_E_BADPACKETHEADER  - if it finds problems parsing an ASF 
 *                                      packet header.
 *      ASF_E_BADPAYLOADHEADER - if it finds problems parsing an ASF 
 *                                      payload header.
 *      ASF_E_FILENOTPROTECTED - if the file is not encrypted.
 *      ASF_E_BADDATAHEADER    - if it finds problems parsing an ASF 
 *
 **********************************************************************/
#ifdef ASFDRMDEFINE
 STATUS ASFGetDecryptASF (
        ASFPARSERPBCONTEXTHANDLE          hAsfContext, 
        ASFCONTENTHANDLE                  hDestAsfContent
 );
#endif // ASFDRMDEFINE
/***********************************************************************
 * 
 * ASFSessionCommit() - Commit to disk all metering and play-count 
 *                          changes in the data store.
 *
 * SYNOPSIS
 *       STATUS ASFSessionCommit (
 *          ASFPARSERPBCONTEXTHANDLE        hAsfPbContext 
 *      )

 * 
 * INPUT
 *      hAsfPbContext      - Handle to ASF parser context.
 * 
 * DESCRIPTION
 *      The ASFSessionCommit() API shall be called by application 
 *      (e. g. Media Player) to commit to disk all metering and 
 *      play-count changes in the data store.
 *      Shall internally call the DRM_MGR_Commit() to commit to disk 
 *      all the metering and play count changes.
 * 
 * RETURN
 *      ASF_SUCCESS                 - on success.
 *      ASF_E_INVALIDARG            - if a parameter is not valid or 
 *                                     is a NULL pointer.
 *
 **********************************************************************/
#ifdef ASFDRMDEFINE 
  STATUS ASFSessionCommit (
        ASFPARSERPBCONTEXTHANDLE        hAsfPbContext 
 );
#endif // ASFDRMDEFINE 

#endif //_ASF_PARSER_H_
