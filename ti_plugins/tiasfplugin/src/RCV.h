/*
 * RCV.h
 *
 * Header file for data Types in ASF Demuxer/Parser plugin 
 * for Gstreamer implementation on DM644x EVM
 *
 * Copyright (C) 2007 Texas Instruments, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#ifndef _RCV_H_
#define _RCV_H_

#include <ASFParser.h>

#define RCV2_SEQUENCE_HEADER_LEN 36
#define RCV2_FRAME_HEADER_LEN 8

#define RCV1_SEQUENCE_HEADER_LEN 20
#define RCV1_FRAME_HEADER_LEN 4

#define VC1_SEQUENCE_HEADER_LEN 21
#define VC1_FRAME_HEADER_LEN 4

STATUS RCV2GenerateSequenceHeader(
			ASFPARSERPBCONTEXTHANDLE pContext,
			ASFVIDEOSTREAMINFO *pAudStreamInfo,
			 BYTE 		*pbBuffer,
			 UINT32		pcBuffer,
			 UINT32		dwStartOffset
);

STATUS RCV2GenerateFrameHeader(
			ASFPARSERPBCONTEXTHANDLE pContext,
			MEDIAOBJMETAINFO *pMediaObjMetaInfo,
			 BYTE 		*pbBuffer,
			 UINT32		pcBuffer,
			 UINT32		dwStartOffset
);

STATUS RCV1GenerateSequenceHeader(
			ASFPARSERPBCONTEXTHANDLE pContext,
			ASFVIDEOSTREAMINFO *pAudStreamInfo,
			 BYTE 		*pbBuffer,
			 UINT32		pcBuffer,
			 UINT32		dwStartOffset
);

STATUS RCV1GenerateFrameHeader(
			ASFPARSERPBCONTEXTHANDLE pContext,
			MEDIAOBJMETAINFO *pMediaObjMetaInfo,
			 BYTE 		*pbBuffer,
			 UINT32		pcBuffer,
			 UINT32		dwStartOffset
);

STATUS VC1GenerateSequenceHeader( 
			ASFPARSERPBCONTEXTHANDLE pContext, 
			ASFVIDEOSTREAMINFO *pVidStreamInfo, 
			 BYTE 		*pbBuffer,
			 UINT32		pcBuffer,
			 UINT32		dwStartOffset
);

STATUS VC1GenerateFrameHeader( 
			ASFPARSERPBCONTEXTHANDLE pContext, 
			MEDIAOBJMETAINFO *pMediaObjMetaInfo,
			 BYTE 		*pbBuffer,
			 UINT32		pcBuffer,
			 UINT32		dwStartOffset
);

STATUS GenerateVideoSequenceHeader( 
			ASFPARSERPBCONTEXTHANDLE pContext, 
			ASFVIDEOSTREAMINFO *pVideoStreamInfo, 
			 BYTE 		*pbBuffer,
			 UINT32		pcBuffer,
			 UINT32		dwSequenceHeaderLen
);

STATUS GenerateVideoFrameHeader( 
			ASFPARSERPBCONTEXTHANDLE pContext, 
			ASFVIDEOSTREAMINFO *pVideoStreamInfo, 
			MEDIAOBJMETAINFO *pMediaObjMetaInfo,
			 BYTE 		*pbBuffer,
			 UINT32		pcBuffer,
			 UINT32		dwFrameHeaderLen,
			 UINT32		dwStartOffset
);

UINT32 GetVideoSequenceHeaderLength( 
			ASFPARSERPBCONTEXTHANDLE pContext, 
			ASFVIDEOSTREAMINFO *pVideoStreamInfo
);

UINT32 GetVideoFrameHeaderLength( 
			ASFPARSERPBCONTEXTHANDLE pContext, 
			ASFVIDEOSTREAMINFO *pVideoStreamInfo
);

#endif //_RCV_H_
