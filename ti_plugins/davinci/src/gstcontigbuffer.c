/* GStreamer
 * Copyright (C) <2008> Tyler Nielsen <tyler@behindtheset.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <string.h>

#include "gstcontigbuffer.h"

static void gst_contigbuffer_init (GTypeInstance * instance, gpointer g_class);
static void gst_contigbuffer_class_init (gpointer g_class, gpointer class_data);
static void gst_contigbuffer_finalize (GstContigBuffer * nbuf);

static GstBufferClass *parent_class;

GType
gst_contigbuffer_get_type (void)
{
  static GType _gst_contigbuffer_type = 0;

  if (G_UNLIKELY (_gst_contigbuffer_type == 0)) {
    static const GTypeInfo contigbuffer_info = {
      sizeof (GstBufferClass),
      NULL,
      NULL,
      gst_contigbuffer_class_init,
      NULL,
      NULL,
      sizeof (GstContigBuffer),
      0,
      gst_contigbuffer_init,
      NULL
    };

    _gst_contigbuffer_type = g_type_register_static (GST_TYPE_BUFFER,
        "GstContigBuffer", &contigbuffer_info, 0);
  }
  return _gst_contigbuffer_type;
}

static void
gst_contigbuffer_class_init (gpointer g_class, gpointer class_data)
{
  GstMiniObjectClass *mo_class = GST_MINI_OBJECT_CLASS (g_class);

  parent_class = g_type_class_peek_parent (g_class);

  mo_class->finalize = (GstMiniObjectFinalizeFunction) gst_contigbuffer_finalize;
}

static void
gst_contigbuffer_init (GTypeInstance * instance, gpointer g_class)
{
}

static void
gst_contigbuffer_finalize (GstContigBuffer * cbuf)
{
  Memory_contigFree(GST_BUFFER_DATA(cbuf), GST_BUFFER_SIZE(cbuf));
}

GstBuffer *
gst_contigbuffer_new (gint32 size)
{
  GstContigBuffer *buf;
  static gboolean _firstTime = TRUE;

  if(_firstTime) {
    CERuntime_init();
    _firstTime = FALSE;
  }

  buf = (GstContigBuffer *) gst_mini_object_new (GST_TYPE_CONTIGBUFFER);
  g_return_val_if_fail(buf != NULL, NULL);

  GST_BUFFER_SIZE(buf) = (size & ~0x01) + 2;
  GST_BUFFER_DATA(buf) = Memory_contigAlloc(GST_BUFFER_SIZE(buf), Memory_DEFAULTALIGNMENT);

  if(GST_BUFFER_DATA(buf) == NULL) {
    //FIXME Memory leak (buf not free)
    return NULL;
  }

  buf->physLocation = Memory_getPhysicalAddress(GST_BUFFER_DATA(buf));

  return GST_BUFFER(buf);
}
