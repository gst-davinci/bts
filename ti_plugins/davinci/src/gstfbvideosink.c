/*
 * Plugin Name : fbvideosink
 * Description : A fbdev Video Sink Plugin for TI Davinci DM644x
 *
 *  test pipelines:
 *  gst-launch-0.10 videotestsrc ! video/x-raw-rgb ! fbvideosink device=/dev/fb0
 *  gst-launch-0.10 videotestsrc ! ffmpegcolorspace ! video/x-raw-rgb ! fbvideosink
 *                  device=/dev/fb0
 *  gst-launch-0.10 videotestsrc ! video/x-raw-yuv ! fbvideosink device=/dev/fb3
 *  gst-launch-0.10 videotestsrc ! video/x-raw-yuv,format=\(fourcc\)UYVY ! fbvideosink
 *                  device=/dev/fb3
 *  :: following YUV formats are supported by fbvideosink
 *   : UYVY
 *   : YUY2
 *   : YVYU
 *  :: ffmpegcolorspace supports only YUY2
 *     videotestsrc supports all of the above
 *
 * Copyright (C) 2007 Texas Instruments, Inc.
 * Copyright (C) 2008 Behind The Set, LLC
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */


#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gstfbvideosink.h"
#include "gstcontigbuffer.h"
#include <stdlib.h>

//temporary define's
#define ENDIANNESS 	G_LITTLE_ENDIAN
#define DEPTH 		16
#define REDMASK		63488
#define GREENMASK	2016
#define BLUEMASK	31

int to_be_displayed = 0;

#include <gst/gstinfo.h>

GST_DEBUG_CATEGORY_STATIC(gst_debug_fbvideosink);
#define GST_CAT_DEFAULT gst_debug_fbvideosink

/* fbimage buffers */
#define GST_TYPE_FBIMAGE_BUFFER (gst_fbimage_buffer_get_type())
#define GST_IS_FBIMAGE_BUFFER(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GST_TYPE_FBIMAGE_BUFFER))
#define GST_FBIMAGE_BUFFER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), GST_TYPE_FBIMAGE_BUFFER, GstFbImageBuffer))

static GstElementDetails gst_fbvideosink_details = {
    "FBDEV Video Sink",
    "Sink/Video",
    "A fbdev based Video Sink",
    "Rishi Bhattacharya <rishi@gstreamer.net>"
};

static GstStaticPadTemplate gst_fbvideosink_sink_template_factory =
    GST_STATIC_PAD_TEMPLATE("sink",
                            GST_PAD_SINK,
                            GST_PAD_ALWAYS,
                            GST_STATIC_CAPS("video/x-raw-yuv, "
                                            "framerate = (fraction) [ 0, MAX ], "
                                            "width = (int) [ 1, MAX ], "
                                            "height = (int) [ 1, MAX ];"
                                            "video/x-raw-rgb, "
                                            "framerate = (fraction) [ 0, MAX ], "
                                            "width = (int) [ 1, MAX ], "
                                            "height = (int) [ 1, MAX ]")
                           );
enum {
    PROP_0,
    PROP_DEVICE
};

static gint gst_fbvideosink_get_format_from_caps(GstFbVideoSink *
        fbvideosink,
        GstCaps * caps);
static GstVideoSinkClass *parent_class = NULL;
static guint counter;

GType gst_fbimage_buffer_get_type(void);
static GstFbImageBuffer *gst_fbvideosink_fbimage_new(GstFbVideoSink *
        fbvideosink,
        GstCaps * caps);
static void gst_fbimage_buffer_free(GstFbImageBuffer * fbimage);
static void gst_fbimage_buffer_finalize(GstFbImageBuffer * fbimage);

/* This function puts a GstFbImage on a FrameBuffer Display */
static void gst_fbvideosink_fbimage_put(GstFbVideoSink * fbvideosink,
                                        GstFbImageBuffer * fbimage)
{
    guint offset;
    GstFbContext *fbcontext;
    g_return_if_fail(GST_IS_FBVIDEOSINK(fbvideosink));
    g_return_if_fail(fbvideosink->fbcontext != NULL);

    fbcontext = fbvideosink->fbcontext;
    g_mutex_lock(fbvideosink->fb_lock);

    GST_LOG("fbvideosink: gst_fbvideosink_fbimage_put BEGIN...");

    // fbcontext->vScreenInfo.yoffset = fbcontext->vScreenInfo.yres * fbcontext->bufferIndex;
    for (offset = 0; offset < fbcontext->noofbuffers; offset++) {
        if (fbcontext->buffers[offset] == GST_BUFFER_DATA(fbimage))
            break;
    }

    if (offset >= fbcontext->noofbuffers) {
        GST_ERROR("PAN FAILED...offset not recognized...");
        g_mutex_unlock(fbvideosink->fb_lock);
        return;
    }

    //      offset = GST_BUFFER_OFFSET(fbimage);// + 1;
    GST_DEBUG("Frame Buffer Offset: %d", offset);
    GST_DEBUG("The Data Pointer of the current Frame: 0x%x",
              GST_BUFFER_DATA(fbimage));

    fbcontext->vScreenInfo.xoffset = 0;
    fbcontext->vScreenInfo.yoffset = D1_MAX_HEIGHT * offset;

    GST_LOG(" The offset value is %d",offset);
    GST_LOG(" The Line length value is %d",fbcontext->fScreenInfo.line_length);
    GST_LOG(" The yres value is %d",fbcontext->vScreenInfo.yres);

    //fbcontext->vScreenInfo.yoffset = fbcontext->vScreenInfo.yres * offset;  // Changed by PB
    //data = fbcontext->data + ( fbcontext->vScreenInfo.yoffset * (fbcontext->fScreenInfo.line_length));

    //do the memcpy into the buffer at the location pointed by data
    GST_DEBUG_OBJECT(fbvideosink, "PANNING THE DISPLAY...");

    //memcpy(data , GST_BUFFER_DATA(fbimage), GST_BUFFER_SIZE(fbimage));
    //PAN the display
    if (ioctl
            (fbcontext->fbdevFd, FBIOPAN_DISPLAY,
             &(fbcontext->vScreenInfo)) == -1) {
        GST_ELEMENT_ERROR(fbvideosink, RESOURCE, WRITE, (NULL),
                          ("PAN DISPLAY Failed"));

        g_mutex_unlock(fbvideosink->fb_lock);
        return;
    }

    //Wait for VSYNC signal from fb device before continuing
    if (ioctl(fbcontext->fbdevFd, FBIO_WAITFORVSYNC, 0) == -1) {
        GST_ELEMENT_ERROR(fbvideosink, RESOURCE, WRITE, (NULL),
                          ("Wait for VSYNC Failed"));
        g_mutex_unlock(fbvideosink->fb_lock);
        return;
    }

    (fbvideosink->framesDisplayed)++;

    GST_LOG(" Frames Displayed is # %d",fbvideosink->framesDisplayed);

    //gst_buffer_unref(fbimage);
    //gst_fbimage_buffer_finalize (fbimage);
    fbcontext->avail[offset] = TRUE;
    g_mutex_unlock(fbvideosink->fb_lock);
    //fbcontext->bufferIndex = (++(fbcontext->bufferIndex)) % fbcontext->noofbuffers;


    GST_LOG("fbvideosink: gst_fbvideosink_fbimage_put END..");
}

static GstFlowReturn gst_fbvideosink_show_frame(GstBaseSink * bsink,
        GstBuffer * buf)
{
    GstFbVideoSink *fbvideosink;
    guint i;
    GstFbImageBuffer *temp,**start_fbimage;
    GstBuffer **start_buf;


    GST_LOG("fbvideosink: gst_fbvideosink_show_frame BEGIN..");

    fbvideosink = GST_FBVIDEOSINK(bsink);
    /* If this buffer has been allocated using our buffer management we simply
       put the fbimage which is in the PRIVATE pointer */

    if (GST_IS_FBIMAGE_BUFFER(buf)) {
        GST_DEBUG("fast put of bufferpool buffer");
        GST_LOG ("fbvideosink: gst_fbvideosink_show_frame: fast put of bufferpool buffer %p", buf);
        gst_fbvideosink_fbimage_put(fbvideosink, GST_FBIMAGE_BUFFER(buf));
    }
    else if (GST_IS_CONTIGBUFFER(buf) && fbvideosink->hRszcopy != RSZCOPY_FAILURE) {
       GST_DEBUG("Hardware resizer activated");
       if (!fbvideosink->fbimage) {
           GST_DEBUG_OBJECT(fbvideosink, "creating our fbimage");

           fbvideosink->fbimage = gst_fbvideosink_fbimage_new(fbvideosink,
                                            GST_BUFFER_CAPS(buf));
           if (!fbvideosink->fbimage)
             goto no_image;
       }
       if (Rszcopy_execute(fbvideosink->hRszcopy, Memory_getPhysicalAddress(GST_BUFFER_DATA(buf)),
             Memory_getPhysicalAddress(GST_BUFFER_DATA(fbvideosink->fbimage))) == RSZCOPY_FAILURE) {
         GST_ERROR("Frame copy using resizer failed");
         fbvideosink->hRszcopy = RSZCOPY_FAILURE;
       }
       gst_fbvideosink_fbimage_put(fbvideosink, fbvideosink->fbimage);
    }
    else {
        GST_DEBUG("slow copy into bufferpool buffer");
        /* Else we have to copy the data into our private image, */
        /* if we have one... */
        if (!fbvideosink->fbimage) {
            GST_DEBUG_OBJECT(fbvideosink, "creating our fbimage");

            fbvideosink->fbimage = gst_fbvideosink_fbimage_new(fbvideosink,
                                   GST_BUFFER_CAPS(buf));
            if (!fbvideosink->fbimage)
                goto no_image;
        }
        GST_DEBUG("Coping data from received buffer to fbvideosinks internal buffer");

        temp = fbvideosink->fbimage;
        start_fbimage = &(fbvideosink->fbimage);
        start_buf = &buf;

        GST_LOG("fbvideosink: show_frame Width is %d... Height is %d",temp->width,temp->height);

        GST_BUFFER_TIMESTAMP(temp) = GST_BUFFER_TIMESTAMP(buf);

        GST_BUFFER_OFFSET(temp) = 0;
        GST_BUFFER_OFFSET(buf) = 0;
        for (i=0; i < temp->height; i++) {
            // GST_LOG("SHOW FRAME:GST_BUFFER_TIMESTAMP:%llu",GST_BUFFER_TIMESTAMP(buf));
            // memcpy(GST_BUFFER_DATA(temp), GST_BUFFER_DATA(buf),MIN(GST_BUFFER_SIZE(buf), fbvideosink->fbimage->size));
            GST_LOG("Copying from %x location to %x location ...%d bytes",GST_BUFFER_DATA(buf),GST_BUFFER_DATA(temp),temp->height*2);
            memcpy(GST_BUFFER_DATA(temp), GST_BUFFER_DATA(buf),temp->width*2);
            GST_BUFFER_DATA(temp) += fbvideosink->fbcontext->fScreenInfo.line_length;
            GST_BUFFER_DATA(buf) += temp->width*2;
        }


        //       GST_BUFFER_TIMESTAMP(temp) = GST_BUFFER_TIMESTAMP(buf);
        //       GST_BUFFER_TIMESTAMP(*start_fbimage) = GST_BUFFER_TIMESTAMP(*start_buf);

        GST_BUFFER_DATA(temp) = GST_BUFFER_DATA(temp) - (fbvideosink->fbcontext->fScreenInfo.line_length * temp->height );
        GST_BUFFER_DATA(buf) = GST_BUFFER_DATA(buf) - (temp->height * temp->width *2);

        GST_LOG("Origin start %x location - Destination start  %x location bytes - Size to free %x bytes",GST_BUFFER_DATA(*start_buf),GST_BUFFER_DATA(*start_fbimage), GST_BUFFER_SIZE(*start_buf));

        to_be_displayed--;

        gst_fbvideosink_fbimage_put(fbvideosink, fbvideosink->fbimage);
    }

    GST_LOG("fbvideosink: gst_fbvideosink_show_frame END..");
    return GST_FLOW_OK;

    /* ERRORS */
no_image: {
        /* No image available. That's very bad ! */
        GST_DEBUG("could not create image");
        GST_ELEMENT_ERROR(fbvideosink, CORE, NEGOTIATION, (NULL),
                          ("Failed creating an FbImage in fbvideosink chain function.")
                         );

        return GST_FLOW_ERROR;
    }
}



/* This function handles GstFbImage creation*/
static GstFbImageBuffer *gst_fbvideosink_fbimage_new(GstFbVideoSink *
        fbvideosink,
        GstCaps * caps)
{
    GstFbImageBuffer *fbimage = NULL;
    GstStructure *structure = NULL;
    GstFbContext *fbcontext;
    guint i;
    gboolean succeeded = FALSE;

    GST_LOG("fbvideosink: gst_fbvideosink_fbimage_new BEGIN..");

    g_return_val_if_fail(GST_IS_FBVIDEOSINK(fbvideosink), NULL);

    fbcontext = fbvideosink->fbcontext;
    fbimage=(GstFbImageBuffer *) gst_mini_object_new(GST_TYPE_FBIMAGE_BUFFER);

    structure = gst_caps_get_structure(caps, 0);

    if (!gst_structure_get_int(structure, "width", &fbimage->width) ||
            !gst_structure_get_int(structure, "height", &fbimage->height)) {
        GST_WARNING("failed getting geometry from caps %"
                    GST_PTR_FORMAT, caps);
    }

    GST_LOG_OBJECT(fbvideosink, "creating %dx%d", fbimage->width,
                   fbimage->height);
    GST_LOG("creating %dx%d", fbimage->width, fbimage->height);

    /*Added by Pratheesh */
    if (!fbcontext->doneonce) {
        fbcontext->doneonce = TRUE;
        //        if (fbimage->height > MAX_FRAME_HEIGHT)
        //    fbcontext->vScreenInfo.yres = MAX_FRAME_HEIGHT;

        // Get variable screen information
        if (ioctl
                (fbcontext->fbdevFd, FBIOGET_VSCREENINFO,
                 &(fbcontext->vScreenInfo)) == -1) {
            g_free(fbcontext);
            GST_ELEMENT_ERROR(fbvideosink, RESOURCE, WRITE, (NULL),
                              ("Could not get variable screen info"));

            return FALSE;
        }

        // Get fixed screen information
        if (ioctl
                (fbcontext->fbdevFd, FBIOGET_FSCREENINFO,
                 &(fbcontext->fScreenInfo)) == -1) {
            g_free(fbcontext);
            GST_ELEMENT_ERROR(fbvideosink, RESOURCE, WRITE, (NULL),
                              ("Could not get fixed screen info"));
            return FALSE;
        }

        if (fbimage->height > D1_MAX_HEIGHT)
            fbcontext->vScreenInfo.yres = D1_MAX_HEIGHT;
        else
            fbcontext->vScreenInfo.yres = fbimage->height;

        fbcontext->vScreenInfo.xres = fbimage->width;
        //       fbcontext->vScreenInfo.yres_virtual =
        //    D1_HEIGHT_PAL * fbcontext->noofbuffers;
        fbcontext->vScreenInfo.yres_virtual = D1_MAX_HEIGHT * fbcontext->noofbuffers;
        fbcontext->vScreenInfo.xres_virtual = fbimage->width;
        //new code for buffer management

        fbcontext->screensize = fbcontext->fScreenInfo.line_length * fbcontext->vScreenInfo.yres;//Added by PB

        for (i = 0; i < fbcontext->noofbuffers; i++) {
            fbcontext->avail[i] = TRUE;
            fbcontext->buffers[i] =
                //  fbcontext->data + (i * D1_MAX_HEIGHT * fbimage->width * 2);
                fbcontext->data + (i * fbcontext->screensize);
            GST_DEBUG("Mapped Frame Buffer Data Pointer: %p, Offset: %d",
                      fbcontext->buffers[i], i);
        }

        GST_LOG("Variable Screen Info is %d",fbcontext->vScreenInfo.yres);
        GST_LOG("Fixed Screen Info Line Length is %d",fbcontext->fScreenInfo.line_length);

        //end
        // Put variable screen information
        if (ioctl
                (fbcontext->fbdevFd, FBIOPUT_VSCREENINFO,
                 &(fbcontext->vScreenInfo)) == -1) {
            g_free(fbcontext);
            GST_ELEMENT_ERROR(fbvideosink, RESOURCE, WRITE, (NULL),
                              ("Could not put variable screen info"));
            return FALSE;
        }

        // Set Y coordinate
        /*
        	   int ypos = (D1_MAX_HEIGHT - fbcontext->vScreenInfo.yres) >> 2;
        	   if (ioctl(fbcontext->fbdevFd, FBIO_SETPOSY, ypos) == -1) {
        	  g_free(fbcontext);
        	   GST_ELEMENT_ERROR(fbvideosink, RESOURCE, WRITE, (NULL),
        	                     ("Could not set SET POSY"));
        	   return FALSE;
                }
        */
        // Set X coordinate
        /*
        	   	 int xpos = (D1_WIDTH - fbcontext->vScreenInfo.xres) >> 1;
        	   if (ioctl(fbcontext->fbdevFd, FBIO_SETPOSX, xpos) == -1) {
        	   g_free(fbcontext);
        	    GST_ELEMENT_ERROR(fbvideosink, RESOURCE, WRITE, (NULL),
                                    ("Could not set SET POSX"));
                   return FALSE;
        	 }
        */

        // Get variable screen information
        if (ioctl
                (fbcontext->fbdevFd, FBIOGET_VSCREENINFO,
                 &(fbcontext->vScreenInfo)) == -1) {
            g_free(fbcontext);
            GST_ELEMENT_ERROR(fbvideosink, RESOURCE, WRITE, (NULL),
                              ("Could not get variable screen info"));
            return FALSE;
        }

        fbcontext->depth = fbcontext->vScreenInfo.red.length +
                           fbcontext->vScreenInfo.green.length +
                           fbcontext->vScreenInfo.blue.length;

        // fbcontext->width = (fbcontext->fScreenInfo.line_length * 8) / fbcontext->bpp;
        fbcontext->width = fbcontext->vScreenInfo.xres;
        fbcontext->height = fbcontext->vScreenInfo.yres;
        fbcontext->widthmm = fbcontext->vScreenInfo.width;
        fbcontext->heightmm = fbcontext->vScreenInfo.height;

        GST_DEBUG_OBJECT(fbvideosink,
                         "Frame Buffer reports %dx%d pixels and %d mm x %d mm",
                         fbcontext->width, fbcontext->height,
                         fbcontext->widthmm, fbcontext->heightmm);
    }


    /*Added by Pratheesh */
    fbimage->im_format =
        gst_fbvideosink_get_format_from_caps(fbvideosink, caps);

    if (!fbimage->im_format) {
        GST_WARNING_OBJECT(fbvideosink,
                           "failed to get format from caps %"
                           GST_PTR_FORMAT, caps);
        goto beach_unlocked;
    }

    fbimage->fbvideosink = gst_object_ref(fbvideosink);
    fbimage->size = fbcontext->screensize;

    GST_LOG("fbImage Size is %d",fbcontext->screensize);

    /* old code
       GST_BUFFER_MALLOCDATA (fbimage) = g_malloc (fbimage->size);
       GST_BUFFER_DATA (fbimage) = GST_BUFFER_MALLOCDATA (fbimage);
       GST_BUFFER_SIZE (fbimage) = fbimage->size;

       succeeded = TRUE;
     */

    //replaced with
    //new code buffer management
    /*
    if (counter == fbcontext->noofbuffers){
        GST_LOG(" no more buffers to allocate, counter = %d = num of buffers",counter);
        return NULL;
    }
    */

    for (i = 0; i < fbcontext->noofbuffers; i++) {
        if (fbcontext->avail[i] == TRUE) {
            GST_BUFFER_DATA(fbimage) = fbcontext->buffers[i];
            GST_BUFFER_SIZE(fbimage) = fbimage->size;
            GST_BUFFER_OFFSET(fbimage) = i;
            fbcontext->avail[i] = FALSE;

            succeeded = TRUE;
            GST_DEBUG
            ("FBVIDEOSINK: Returning Buffer with Data Pointer : %p , and Offset: %d",
             GST_BUFFER_DATA(fbimage), i);
            break;
        }
    }


    //end of new code
    /*
    else
    {
    GST_BUFFER_DATA(fbimage) = fbcontext->buffers[counter];
    GST_BUFFER_SIZE(fbimage) = fbimage->size;
    GST_BUFFER_OFFSET(fbimage) = counter;
    succeeded = TRUE;
    GST_LOG("fbimage new,ptr:%p offset:%d",GST_BUFFER_DATA(fbimage),counter);
    counter++;
    }
    */

beach_unlocked:
    if (!succeeded) {
        GST_ERROR("fbvideosink: ERROR in fbimage_new -- Beach Unlocked");
        gst_fbimage_buffer_free(fbimage);
        fbimage = NULL;
    }

    GST_LOG("fbvideosink: gst_fbvideosink_fbimage_new END..");
    return fbimage;



}

static void gst_fbvideosink_finalize(GObject * object)
{
    GstFbVideoSink *fbvideosink;

    GST_LOG("fbvideosink: gst_fbvideosink_finalize BEGIN..");

    fbvideosink = GST_FBVIDEOSINK(object);

    if (fbvideosink->par) {
        g_free(fbvideosink->par);
        fbvideosink->par = NULL;
    }

    if (fbvideosink->hRszcopy != RSZCOPY_FAILURE) {
        Rszcopy_delete(fbvideosink->hRszcopy);
    }

    G_OBJECT_CLASS(parent_class)->finalize(object);

    GST_LOG("fbvideosink: gst_fbvideosink_finalize END..");

}

static GstFlowReturn gst_fbvideosink_buffer_alloc(GstBaseSink * bsink,
        guint64 offset,
        guint size,
        GstCaps * caps,
        GstBuffer ** buf)
{
    GST_LOG("fbvideosink: gst_fbvideosink_buffer_alloc BEGIN..");
    *buf = gst_contigbuffer_new(size);

    if(*buf == NULL)
    {
        return GST_FLOW_ERROR;
    }

    gst_buffer_set_caps(*buf, caps);

    GST_LOG("fbvideosink: gst_fbvideosink_buffer_alloc END..");

    return GST_FLOW_OK;
}

/* This function destroys a GstFbImage*/
static void gst_fbimage_buffer_destroy(GstFbImageBuffer * fbimage)
{
    GstFbVideoSink *fbvideosink;

    GST_LOG("fbvideosink: gst_fbvideosink_buffer_destroy BEGIN..");

    fbvideosink = fbimage->fbvideosink;
    if (fbvideosink == NULL)
        goto no_sink;

    g_return_if_fail(GST_IS_FBVIDEOSINK(fbvideosink));

    /* If the destroyed image is the current one we destroy our reference too */
    if (fbvideosink->cur_image == fbimage)
        fbvideosink->cur_image = NULL;

    if (GST_BUFFER_DATA(fbimage)) {
        guint offset = GST_BUFFER_OFFSET(fbimage);

        fbvideosink->fbcontext->avail[offset] = TRUE;
//              g_free ((guint8 *)GST_BUFFER_DATA(fbimage));
        (GST_BUFFER_DATA(fbimage)) = NULL;
        (GST_BUFFER_SIZE(fbimage)) = 0;
    }

    fbimage->fbvideosink = NULL;
    gst_object_unref(fbvideosink);

    GST_LOG("fbvideosink: gst_fbvideosink_buffer_destroy END..");

    return;

no_sink: {
        GST_WARNING("no sink found");
        return;
    }
}

static void gst_fbimage_buffer_finalize(GstFbImageBuffer * fbimage)
{
    GstFbVideoSink *fbvideosink;

    GST_LOG("fbvideosink: gst_fbvideosink_buffer_finalize BEGIN..");

    fbvideosink = fbimage->fbvideosink;
    //GstFbContext *fbcontext = fbvideosink->fbcontext;
    if (fbvideosink == NULL)
        goto no_sink;

    /* If our geometry changed we can't reuse that image. */
    if ((fbimage->width != fbvideosink->video_width) ||
            (fbimage->height != fbvideosink->video_height)) {
        GST_DEBUG
        ("destroy image as its size changed %dx%d vs current %dx%d",
         fbimage->width, fbimage->height, fbvideosink->video_width,
         fbvideosink->video_height);
        /*GST_LOG
           ("destroy image as its size changed %dx%d vs current %dx%d",
           fbimage->width, fbimage->height, fbvideosink->video_width,
           fbvideosink->video_height); */
        gst_fbimage_buffer_destroy(fbimage);
    }
    else {
        /* In that case we can reuse the image and add it to our image pool. */
        GST_DEBUG("recycling image in pool:%x pts:%llu",
                  GST_BUFFER_DATA(fbimage),GST_BUFFER_TIMESTAMP(fbimage));
        /* need to increment the refcount again to recycle */
        gst_buffer_ref(GST_BUFFER(fbimage));
        //fbcontext->avail[GST_BUFFER_OFFSET(fbimage)] = TRUE;
    }

    GST_LOG("fbvideosink: gst_fbvideosink_buffer_finalize END..");

    return;

no_sink: {
        GST_WARNING("no sink found");
        return;
    }
}

static void gst_fbimage_buffer_init(GstFbImageBuffer * fbimage,
                                    gpointer g_class)
{
    GST_LOG("fbvideosink: gst_fbvideosink_buffer_init BEGIN..");

    GST_LOG("fbvideosink: gst_fbvideosink_buffer_init END..");
    return;
}

static void gst_fbimage_buffer_class_init(gpointer g_class,
        gpointer class_data)
{
    GstMiniObjectClass *mini_object_class = GST_MINI_OBJECT_CLASS(g_class);

    GST_LOG("fbvideosink: gst_fbvideosink_class_init BEGIN..");

    mini_object_class->finalize =
        (GstMiniObjectFinalizeFunction) gst_fbimage_buffer_finalize;

    GST_LOG("fbvideosink: gst_fbvideosink_class_init END..");

}

GType gst_fbimage_buffer_get_type(void)
{
    static GType _gst_fbimage_buffer_type;

    GST_LOG("fbvideosink: gst_fbimage_buffer_get_type BEGIN..");

    if (G_UNLIKELY(_gst_fbimage_buffer_type == 0)) {
        static const GTypeInfo fbimage_buffer_info = {
            sizeof(GstBufferClass),
            NULL,
            NULL,
            gst_fbimage_buffer_class_init,
            NULL,
            NULL,
            sizeof(GstFbImageBuffer),
            0,
            (GInstanceInitFunc) gst_fbimage_buffer_init,
            NULL
        };
        _gst_fbimage_buffer_type =
            g_type_register_static(GST_TYPE_BUFFER, "GstFbImageBuffer",
                                   &fbimage_buffer_info, 0);
    }

    GST_LOG("fbvideosink: gst_fbimage_buffer_get_type END..");

    return _gst_fbimage_buffer_type;
}

/* This function tries to get a format matching with a given caps in the
   supported list of formats we generated in gst_fbvideosink_get_fb_support */
static gint gst_fbvideosink_get_format_from_caps(GstFbVideoSink *
        fbvideosink,
        GstCaps * caps)
{
    GList *list = NULL;

    GST_LOG("fbvideosink: gst_fbvideosink_get_format_from_caps BEGIN..");

    g_return_val_if_fail(GST_IS_FBVIDEOSINK(fbvideosink), 0);

    list = fbvideosink->fbcontext->formats_list;

    while (list) {
        GstFbImageFormat *format = list->data;

        if (format) {
            GstCaps *icaps = NULL;

            icaps = gst_caps_intersect(caps, format->caps);
            if (!gst_caps_is_empty(icaps)) {
                gst_caps_unref(icaps);
                GST_LOG("fbvideosink: gst_fbvideosink_get_format_from_caps  END..");
                return format->format;
            }
        }
        list = g_list_next(list);
    }

    GST_LOG("fbvideosink: gst_fbvideosink_get_format_from_caps - Return 0 END..");

    return 0;
}

/* This function cleans the Fb context. Closing the Frame Buffer,
	and unrefing the caps for supported formats. */
static void gst_fbvideosink_fbcontext_clear(GstFbVideoSink * fbvideosink)
{
    GList *formats_list;

    GST_LOG("fbvideosink: gst_fbvideosink_fbcontext_clear BEGIN..");

    g_return_if_fail(GST_IS_FBVIDEOSINK(fbvideosink));
    g_return_if_fail(fbvideosink->fbcontext != NULL);

    formats_list = fbvideosink->fbcontext->formats_list;

    while (formats_list) {
        GstFbImageFormat *format = formats_list->data;

        gst_caps_unref(format->caps);
        g_free(format);
        formats_list = g_list_next(formats_list);
    }

    if (fbvideosink->fbcontext->formats_list)
        g_list_free(fbvideosink->fbcontext->formats_list);

    gst_caps_unref(fbvideosink->fbcontext->caps);
    g_free(fbvideosink->fbcontext->par);

    //fbcontext->data will be pointing to the first byte of the frame buffer
    munmap(fbvideosink->fbcontext->data,
           (fbvideosink->fbcontext->screensize) *
           fbvideosink->fbcontext->noofbuffers);
    fbvideosink->fbcontext->data = NULL;
    close(fbvideosink->fbcontext->fbdevFd);
    g_free(fbvideosink->fbcontext);
    fbvideosink->fbcontext = NULL;


    GST_LOG("fbvideosink: gst_fbvideosink_fbcontext_clear END..");

}

static void gst_fbimage_buffer_free(GstFbImageBuffer * fbimage)
{

    GST_LOG("fbvideosink: gst_fbimage_buffer_free BEGIN..");

    /* make sure it is not recycled */
    GST_LOG ("Inside buffer free");
    fbimage->width = -1;
    fbimage->height = -1;
    gst_buffer_unref(GST_BUFFER(fbimage));

    GST_LOG("fbvideosink: gst_fbimage_buffer_free END..");

}

/* This function calculates the pixel aspect ratio based on the properties
 * in the fbcontext structure and stores it there. */
static void gst_fbvideosink_calculate_pixel_aspect_ratio(GstFbContext *
        fbcontext)
{
    gint par[][2] = {
        {1, 1},                 /* regular screen */
        {16, 15},               /* PAL TV */
        {11, 10},               /* 525 line Rec.601 video */
        {54, 59}                /* 625 line Rec.601 video */
    };
    gint i;
    gint index;
    gdouble ratio;
    gdouble delta;

    GST_LOG("fbvideosink: gst_fbvideosink_calculate_pixel_aspect_ratio BEGIN..");

#define DELTA(idx) (ABS (ratio - ((gdouble) par[idx][0] / par[idx][1])))

    /* first calculate the "real" ratio based on the FbContext values;
     * which is the "physical" w/h divided by the w/h in pixels of the display */
    ratio =
        (gdouble) (fbcontext->widthmm * fbcontext->height) /
        (fbcontext->heightmm * fbcontext->width);

    GST_DEBUG("calculated pixel aspect ratio: %f", ratio);
    /* now find the one from par[][2] with the lowest delta to the real one */
    delta = DELTA(0);
    index = 0;

    for (i = 1; i < sizeof(par) / (sizeof(gint) * 2); ++i) {
        gdouble this_delta = DELTA(i);

        if (this_delta < delta) {
            index = i;
            delta = this_delta;
        }
    }

    GST_DEBUG("Decided on index %d (%d/%d)", index, par[index][0],
              par[index][1]);

    g_free(fbcontext->par);
    fbcontext->par = g_new0(GValue, 1);
    g_value_init(fbcontext->par, GST_TYPE_FRACTION);
    gst_value_set_fraction(fbcontext->par, par[index][0], par[index][1]);
    GST_DEBUG("set fbcontext PAR to %d/%d",
              gst_value_get_fraction_numerator(fbcontext->par),
              gst_value_get_fraction_denominator(fbcontext->par)
             );

    GST_LOG("fbvideosink: gst_fbvideosink_calculate_pixel_aspect_ratio END..");

}

/* This function gets the fbdev and global info about it. Everything is
   stored in our object and will be cleaned when the object is disposed. Note
   here that caps for supported format are generated without any window or
   image creation */
static GstFbContext *gst_fbvideosink_fbcontext_get(GstFbVideoSink *
        fbvideosink)
{
    GstFbContext *fbcontext = NULL;
    //guint i;
    //GST_LOG ("Inside fbvideosink_fbcontext_get");

    GST_LOG("fbvideosink: gst_fbvideosink_fbcontext_get BEGIN..");

    g_return_val_if_fail(GST_IS_FBVIDEOSINK(fbvideosink), NULL);

    /* Allocate 1 element of type GstFbContext and initialize to 0's*/
    fbcontext = g_new0(GstFbContext, 1);

    fbcontext->im_format = 0;
    fbcontext->doneonce = FALSE;

    /* Device Driver Open Call for /dev/fb/3 Video Frame Window */
    fbcontext->fbdevFd = open(fbvideosink->device, O_RDWR);

    if (fbcontext->fbdevFd < 0) {
        g_free(fbcontext);
        GST_ELEMENT_ERROR(fbvideosink, RESOURCE, WRITE, (NULL),
                          ("Could not open device: %s",
                           fbvideosink->device));

        return NULL;
    }

    GST_DEBUG_OBJECT(fbvideosink, "Display Device Opened Successfully");

    // Get fixed screen information
    if (ioctl
            (fbcontext->fbdevFd, FBIOGET_FSCREENINFO,
             &(fbcontext->fScreenInfo)) == -1) {
        g_free(fbcontext);
        GST_ELEMENT_ERROR(fbvideosink, RESOURCE, WRITE, (NULL),
                          ("Could not get fixed screen info"));
        return NULL;
    }

    // Get variable screen information
    if (ioctl
            (fbcontext->fbdevFd, FBIOGET_VSCREENINFO,
             &(fbcontext->vScreenInfo)) == -1) {
        g_free(fbcontext);
        GST_ELEMENT_ERROR(fbvideosink, RESOURCE, WRITE, (NULL),
                          ("Could not get variable screen info"));
        return NULL;
    }


    /* Added by PB*/
    fbcontext->vScreenInfo.xres = D1_WIDTH;
    fbcontext->vScreenInfo.yres = D1_HEIGHT;
    fbcontext->vScreenInfo.bits_per_pixel = SCREEN_BPP;

    /* Set video display format */
    if (ioctl(fbcontext->fbdevFd, FBIOPUT_VSCREENINFO, &(fbcontext->vScreenInfo)) == -1) {
        g_free(fbcontext);
        GST_ELEMENT_ERROR(fbvideosink, RESOURCE, WRITE, (NULL),
                          ("Could not SET variable screen info"));
        return NULL;
    }

    if ( fbcontext->vScreenInfo.xres != D1_WIDTH ||
            fbcontext->vScreenInfo.yres != D1_HEIGHT ||
            fbcontext->vScreenInfo.bits_per_pixel != SCREEN_BPP) {
        GST_WARNING("Failed to get the requested screen size: %dx%d at %d bpp",D1_WIDTH,D1_HEIGHT,SCREEN_BPP);
    }


    /* ----------  */


    //find out the buffering that is used (double, triple, quadriple, ...)
    fbcontext->noofbuffers = TRIPLE_BUF;
    //GST_LOG("Number of buffers = %d", fbcontext->noofbuffers);
    fbcontext->bpp = fbcontext->vScreenInfo.bits_per_pixel;
    // Figure out the size of the screen in bytes
    // fbcontext->screensize = D1_WIDTH * D1_MAX_HEIGHT * (fbcontext->bpp / 8);
    fbcontext->screensize = fbcontext->fScreenInfo.line_length * fbcontext->vScreenInfo.yres;//Added by PB

    //GST_LOG("Screen size = %d", fbcontext->screensize);
    // Map the device to memory
    //         fbcontext->data =
    //    (guint8 *) mmap(0, fbcontext->screensize * fbcontext->noofbuffers,
    //                  PROT_READ | PROT_WRITE, MAP_SHARED,
    //                  fbcontext->fbdevFd, 0);

    fbcontext->data =
        (guint8 *) mmap(0, fbcontext->fScreenInfo.line_length * fbcontext->vScreenInfo.yres_virtual,
                        PROT_READ | PROT_WRITE, MAP_SHARED,
                        fbcontext->fbdevFd, 0);

    GST_LOG("I am in get function :....... ");
    GST_LOG("Screen Size here is : ....%d",fbcontext->screensize);

    //      fbcontext->bufferIndex = 1;

    //      GST_LOG("ssize: %d",fbcontext->screensize);

    if ((int) fbcontext->data == -1) {
        g_free(fbcontext);
        GST_ELEMENT_ERROR(fbvideosink, RESOURCE, WRITE, (NULL),
                          ("failed to map framebuffer device to memory"));
        return NULL;
    }

#if 0
    //new code for buffer management
    for (i = 0; i < fbcontext->noofbuffers; i++) {
        fbcontext->avail[i] = TRUE;
        fbcontext->buffers[i] =
            fbcontext->data + (i * fbcontext->screensize);
        GST_DEBUG("Mapped Frame Buffer Data Pointer: %p, Offset: %d",
                  fbcontext->buffers[i], i);
    }
    //end
#endif

    gst_fbvideosink_calculate_pixel_aspect_ratio(fbcontext);
    fbcontext->endianness = ENDIANNESS;

    fbcontext->caps = gst_caps_new_full(gst_structure_new
                                        ("video/x-raw-yuv",
                                         "width", GST_TYPE_INT_RANGE,
                                         176, 720,
                                         "height", GST_TYPE_INT_RANGE,
                                         144, 576,
                                         "framerate",
                                         GST_TYPE_FRACTION_RANGE, 0, 1,
                                         G_MAXINT, 1, "format",
                                         GST_TYPE_FOURCC,
                                         GST_MAKE_FOURCC('U', 'Y', 'V',
                                                         'Y'), NULL),
                                        gst_structure_new
                                        ("video/x-raw-yuv", "width",
                                         G_TYPE_INT, fbcontext->width,
                                         "height", G_TYPE_INT,
                                         fbcontext->height, "framerate",
                                         GST_TYPE_FRACTION_RANGE, 0, 1,
                                         G_MAXINT, 1, "format",
                                         GST_TYPE_FOURCC,
                                         GST_MAKE_FOURCC('Y', 'U', 'Y',
                                                         '2'), NULL),
                                        gst_structure_new
                                        ("video/x-raw-yuv", "width",
                                         G_TYPE_INT, fbcontext->width,
                                         "height", G_TYPE_INT,
                                         fbcontext->height, "framerate",
                                         GST_TYPE_FRACTION_RANGE, 0, 1,
                                         G_MAXINT, 1, "format",
                                         GST_TYPE_FOURCC,
                                         GST_MAKE_FOURCC('Y', 'V', 'Y',
                                                         'U'), NULL),
                                        gst_structure_new
                                        ("video/x-raw-rgb", "endianness",
                                         G_TYPE_INT, fbcontext->endianness,
                                         "depth", G_TYPE_INT,
                                         fbcontext->depth, "bpp",
                                         G_TYPE_INT, fbcontext->bpp,
                                         "blue_mask", G_TYPE_INT, BLUEMASK,
                                         "green_mask", G_TYPE_INT,
                                         GREENMASK, "red_mask", G_TYPE_INT,
                                         REDMASK, "width", G_TYPE_INT,
                                         fbcontext->width, "height",
                                         G_TYPE_INT, fbcontext->height,
                                         "framerate",
                                         GST_TYPE_FRACTION_RANGE, 0, 1,
                                         G_MAXINT, 1, NULL), NULL);

    GstFbImageFormat *format = NULL;
    format = g_new0(GstFbImageFormat, 1);
    if (format) {
        GST_LOG(" I am in UYVY");
        format->format = GST_MAKE_FOURCC('U', 'Y', 'V', 'Y');
        format->caps = gst_caps_new_simple("video/x-raw-yuv",
                                           "width", GST_TYPE_INT_RANGE,
                                           176, 720,
                                           "height", GST_TYPE_INT_RANGE,
                                           144, 576,
                                           "framerate",
                                           GST_TYPE_FRACTION_RANGE, 0, 1,
                                           G_MAXINT, 1, NULL);

        fbcontext->formats_list =
            g_list_append(fbcontext->formats_list, format);
    }

    format = g_new0(GstFbImageFormat, 1);
    if (format) {
        GST_LOG(" I am in YUY2 format");
        format->format = GST_MAKE_FOURCC('Y', 'U', 'Y', '2');
        format->caps = gst_caps_new_simple("video/x-raw-yuv",
                                           "width", G_TYPE_INT,
                                           fbcontext->width, "height",
                                           G_TYPE_INT, fbcontext->height,
                                           "framerate",
                                           GST_TYPE_FRACTION_RANGE, 0, 1,
                                           G_MAXINT, 1, NULL);
        fbcontext->formats_list =
            g_list_append(fbcontext->formats_list, format);
    }

    format = g_new0(GstFbImageFormat, 1);
    if (format) {
        GST_LOG(" I am in YVYU format");
        format->format = GST_MAKE_FOURCC('Y', 'V', 'Y', 'U');
        format->caps = gst_caps_new_simple("video/x-raw-yuv",
                                           "width", G_TYPE_INT,
                                           fbcontext->width, "height",
                                           G_TYPE_INT, fbcontext->height,
                                           "framerate",
                                           GST_TYPE_FRACTION_RANGE, 0, 1,
                                           G_MAXINT, 1, NULL);
        fbcontext->formats_list =
            g_list_append(fbcontext->formats_list, format);
    }

    format = g_new0(GstFbImageFormat, 1);
    if (format) {
        GST_LOG(" I am in RGB format");
        format->format = GST_MAKE_FOURCC('R', 'G', 'B', ' ');
        format->caps = gst_caps_new_simple("video/x-raw-rgb",
                                           "endianness", G_TYPE_INT,
                                           BYTE_ORDER, "depth", G_TYPE_INT,
                                           fbcontext->depth, "bpp",
                                           G_TYPE_INT, fbcontext->bpp,
                                           "width", G_TYPE_INT,
                                           fbcontext->width, "height",
                                           G_TYPE_INT, fbcontext->height,
                                           "framerate",
                                           GST_TYPE_FRACTION_RANGE, 0, 1,
                                           G_MAXINT, 1, NULL);
        fbcontext->formats_list =
            g_list_append(fbcontext->formats_list, format);
    }


    GST_LOG("fbvideosink: gst_fbvideosink_fbcontext_get END..");

    return fbcontext;
}

static GstStateChangeReturn gst_fbvideosink_change_state(GstElement *
        element,
        GstStateChange
        transition)
{
    GstStateChangeReturn ret = GST_STATE_CHANGE_SUCCESS;
    GstFbVideoSink *fbvideosink;

    GST_LOG("fbvideosink: gst_fbvideosink_change_state BEGIN..");

    fbvideosink = GST_FBVIDEOSINK(element);

    switch (transition) {
    case GST_STATE_CHANGE_NULL_TO_READY:
        GST_LOG("fbvideosink: change_state from NULL to READY..");
        fbvideosink->pool_lock = g_mutex_new();
        fbvideosink->fb_lock = g_mutex_new();
        /* Initializing the FbContext */
        if (!fbvideosink->fbcontext
                && !(fbvideosink->fbcontext =
                         gst_fbvideosink_fbcontext_get(fbvideosink)))
            return GST_STATE_CHANGE_FAILURE;
        /* update object's par with calculated one if not set yet */
        if (!fbvideosink->par) {
            fbvideosink->par = g_new0(GValue, 1);
            gst_value_init_and_copy(fbvideosink->par,
                                    fbvideosink->fbcontext->par);
            GST_DEBUG_OBJECT(fbvideosink,
                             "set calculated PAR on object's PAR");
        }
        counter = 0;

        break;
    case GST_STATE_CHANGE_READY_TO_PAUSED:
        GST_LOG("fbvideosink: change_of_state from READY_TO_PAUSED..");
        break;
    case GST_STATE_CHANGE_PAUSED_TO_PLAYING:
        GST_LOG("fbvideosink: change_of_state from PAUSED_TO_PLAYING..");
        break;
    default:
        GST_LOG("fbvideosink: Invalid Change of state requested..");
        break;
    }

    ret =
        GST_ELEMENT_CLASS(parent_class)->change_state(element, transition);
    if (ret == GST_STATE_CHANGE_FAILURE) {
        GST_LOG("fbvideosink: gst_fbvideosink_change_state - Return with failure END..");
        return ret;
    }
    switch (transition) {
    case GST_STATE_CHANGE_PLAYING_TO_PAUSED:
        GST_LOG("fbvideosink: change_of_state from PLAYING_TO_PAUSED..");
        break;
    case GST_STATE_CHANGE_PAUSED_TO_READY:
        GST_LOG("fbvideosink: change_of_state from PAUSED_TO_READY..");
        fbvideosink->fps_n = 0;
        fbvideosink->fps_d = 1;
        GST_VIDEO_SINK_WIDTH(fbvideosink) = 0;
        GST_VIDEO_SINK_HEIGHT(fbvideosink) = 0;
        break;
    case GST_STATE_CHANGE_READY_TO_NULL:
        GST_LOG("fbvideosink: change_of_state from READY_TO_NULL..");
        if (fbvideosink->fbimage) {
            gst_fbimage_buffer_free(fbvideosink->fbimage);
            fbvideosink->fbimage = NULL;
        }

        if (fbvideosink->pool_lock)
            g_mutex_free(fbvideosink->pool_lock);
        if (fbvideosink->fb_lock)
            g_mutex_free(fbvideosink->fb_lock);
        if (fbvideosink->fbcontext) {
            gst_fbvideosink_fbcontext_clear(fbvideosink);
            fbvideosink->fbcontext = NULL;
        }
        break;
    default:
        GST_LOG("fbvideosink: change_of_state INVALID ..");
        break;
    }

    GST_LOG("fbvideosink: gst_fbvideosink_change_state END..");

    return ret;
}

static GstCaps *gst_fbvideosink_getcaps(GstBaseSink * bsink)
{
    GstFbVideoSink *fbvideosink;

    GST_LOG("fbvideosink: gst_fbvideosink_getcaps BEGIN and gst_pad_get_pad_template_caps..");
    fbvideosink = GST_FBVIDEOSINK(bsink);
//    if (fbvideosink->fbcontext)
//        return gst_caps_ref(fbvideosink->fbcontext->caps);

    return gst_caps_copy(gst_pad_get_pad_template_caps
                         (GST_VIDEO_SINK_PAD(fbvideosink)));
}

static gboolean gst_fbvideosink_setcaps(GstBaseSink * bsink,
                                        GstCaps * caps)
{
    GstFbVideoSink *fbvideosink;
    GstStructure *structure;
    GstCaps *intersection;
    guint32 im_format = 0;
    gboolean ret;
    gint video_width, video_height;
    gint video_par_n, video_par_d;      /* video's PAR */
    gint display_par_n, display_par_d;  /* display's PAR */
    GValue display_ratio = { 0, };      /* display w/h ratio */
    const GValue *caps_par;
    const GValue *fps;
    gint num, den;

    GST_LOG("fbvideosink: gst_fbvideosink_setcaps BEGIN..");


    fbvideosink = GST_FBVIDEOSINK(bsink);
    GST_DEBUG_OBJECT(fbvideosink,
                     "In setcaps. Possible caps %" GST_PTR_FORMAT
                     ", setting caps %" GST_PTR_FORMAT,
                     fbvideosink->fbcontext->caps, caps);

    intersection = gst_caps_intersect(fbvideosink->fbcontext->caps, caps);
    GST_DEBUG_OBJECT(fbvideosink, "intersection returned %" GST_PTR_FORMAT,
                     intersection);

    if (gst_caps_is_empty(intersection)) {
        GST_LOG ("Empty");
        return FALSE;
    }

    gst_caps_unref(intersection);

    structure = gst_caps_get_structure(caps, 0);
    ret = gst_structure_get_int(structure, "width", &video_width);
    ret &= gst_structure_get_int(structure, "height", &video_height);
    fps = gst_structure_get_value(structure, "framerate");
    ret &= (fps != NULL);

    if (!ret)
        return FALSE;

    fbvideosink->fps_n = gst_value_get_fraction_numerator(fps);
    fbvideosink->fps_d = gst_value_get_fraction_denominator(fps);

    if (fbvideosink->hRszcopy != RSZCOPY_FAILURE) {
        GST_INFO("Video size %dx%d", video_width, video_height);
        if( Rszcopy_config(fbvideosink->hRszcopy, video_width, video_height, video_width*2, fbvideosink->fbcontext->fScreenInfo.line_length) == RSZCOPY_FAILURE ) {
            GST_WARNING("Unable to config resize copy job");
            Rszcopy_delete(fbvideosink->hRszcopy);
            fbvideosink->hRszcopy = RSZCOPY_FAILURE;
        }
    }

    GST_LOG(" Video Width %d",video_width);
    GST_LOG("Video Height %d",video_height);

    fbvideosink->video_width = video_width;
    fbvideosink->video_height = video_height;
    /* get aspect ratio from caps if it's present, and
     * convert video width and height to a display width and height
     * using wd / hd = wv / hv * PARv / PARd
     * the ratio wd / hd will be stored in display_ratio */
    g_value_init(&display_ratio, GST_TYPE_FRACTION);

    /* get video's PAR */
    caps_par = gst_structure_get_value(structure, "pixel-aspect-ratio");

    GST_LOG("Pixel Aspect Ratio %d",caps_par);

    if (caps_par) {
        video_par_n = gst_value_get_fraction_numerator(caps_par);
        video_par_d = gst_value_get_fraction_denominator(caps_par);
    }
    else {
        video_par_n = 1;
        video_par_d = 1;
    }

    /* get display's PAR */
    if (fbvideosink->par) {
        display_par_n = gst_value_get_fraction_numerator(fbvideosink->par);
        display_par_d =
            gst_value_get_fraction_denominator(fbvideosink->par);
    }
    else {
        display_par_n = 1;
        display_par_d = 1;
    }

    GST_LOG("video par n: %d, display par n: %d, video par d: %d, display par d: %d",
            video_par_n,display_par_n, video_par_d,display_par_d);

    gst_value_set_fraction(&display_ratio,
                           video_width * video_par_n * display_par_d,
                           video_height * video_par_d * display_par_n);

    num = gst_value_get_fraction_numerator(&display_ratio);
    den = gst_value_get_fraction_denominator(&display_ratio);
    GST_DEBUG_OBJECT(fbvideosink,
                     "video width/height: %dx%d, calculated display ratio: %d/%d",
                     video_width, video_height, num, den);
    GST_LOG("video width/height: %dx%d, calculated display ratio: %d/%d",
            video_width, video_height, num, den);

    /* now find a width x height that respects this display ratio.
     * prefer those that have one of w/h the same as the incoming video
     * using wd / hd = num / den
     */

    /* start with same height, because of interlaced video */
    /* check hd / den is an integer scale factor, and scale wd with the PAR */
    if (video_height % den == 0) {
        GST_DEBUG_OBJECT(fbvideosink, "keeping video height");
        GST_VIDEO_SINK_WIDTH(fbvideosink) = video_height * num / den;
        GST_VIDEO_SINK_HEIGHT(fbvideosink) = video_height;
    }
    else if (video_width % num == 0) {
        GST_DEBUG_OBJECT(fbvideosink, "keeping video width");
        GST_VIDEO_SINK_WIDTH(fbvideosink) = video_width;
        GST_VIDEO_SINK_HEIGHT(fbvideosink) = video_width * den / num;
    }
    else {
        GST_DEBUG_OBJECT(fbvideosink,
                         "approximating while keeping video height");
        GST_VIDEO_SINK_WIDTH(fbvideosink) = video_height * num / den;
        GST_VIDEO_SINK_HEIGHT(fbvideosink) = video_height;
    }

    im_format = gst_fbvideosink_get_format_from_caps(fbvideosink, caps);
    if (im_format == 0) {
        //GST_LOG ("Format");
        return FALSE;
    }


    /* We renew our fbimage only if size or format changed;
     * the fbimage is the same size as the video pixel size */
    if ((fbvideosink->fbimage)
            && ((im_format != fbvideosink->fbimage->im_format)
                || (video_width != fbvideosink->fbimage->width)
                || (video_height != fbvideosink->fbimage->height))
       ) {
        GST_DEBUG_OBJECT(fbvideosink,
                         "old format %" GST_FOURCC_FORMAT ", new format %"
                         GST_FOURCC_FORMAT,
                         GST_FOURCC_ARGS(fbvideosink->fbcontext->
                                         im_format),
                         GST_FOURCC_ARGS(im_format));
        GST_DEBUG_OBJECT(fbvideosink, "renewing fbimage");
        gst_buffer_unref(GST_BUFFER(fbvideosink->fbimage));
        fbvideosink->fbimage = NULL;
    }

    fbvideosink->fbcontext->im_format = im_format;

    GST_LOG("fbvideosink: gst_fbvideosink_setcaps END..");

    return TRUE;
}

/*...
	Init and Class Init
...*/

static void gst_fbvideosink_set_property(GObject * object, guint prop_id,
        const GValue * value,
        GParamSpec * pspec)
{
    GstFbVideoSink *fbvideosink;

    GST_LOG("fbvideosink: gst_fbvideosink_set_property BEGIN..");

    fbvideosink = GST_FBVIDEOSINK(object);

    switch (prop_id) {
    case PROP_DEVICE:
        GST_LOG("fbvideosink: Set_property PROP_DEVICE..");
        g_free(fbvideosink->device);
        fbvideosink->device = g_value_dup_string(value);
        break;
    default:
        GST_LOG("fbvideosink: Invalid property ID..");
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
        break;
    }

    GST_LOG("fbvideosink: gst_fbvideosink_set_property END..");


}

static void gst_fbvideosink_get_property(GObject * object, guint prop_id,
        GValue * value,
        GParamSpec * pspec)
{
    GstFbVideoSink *fbvideosink;

    GST_LOG("fbvideosink: gst_fbvideosink_get_property BEGIN..");


    fbvideosink = GST_FBVIDEOSINK(object);

    switch (prop_id) {
    case PROP_DEVICE:
        g_value_set_string(value, fbvideosink->device);
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
        break;
    }

    GST_LOG("fbvideosink: gst_fbvideosink_get_property END..");


}

gboolean gst_fbvideosink_query(GstElement * element, GstQuery * query)
{
    GstStructure *vidp;
    GValue framesDisplayed = { 0, };
    GstFbVideoSink *fbvideosink = GST_FBVIDEOSINK(element);


    GST_LOG("fbvideosink: gst_fbvideosink_query BEGIN..");

    g_value_init(&framesDisplayed, G_TYPE_UINT);
    g_value_set_uint(&framesDisplayed, fbvideosink->framesDisplayed);

    vidp = gst_query_get_structure(query);
    gst_structure_set_value(vidp, "framesDisplayed", &framesDisplayed);
    g_value_unset(&framesDisplayed);


    GST_LOG("fbvideosink: gst_fbvideosink_query END..");


    return TRUE;
}

static void gst_fbvideosink_init(GstFbVideoSink * fbvideosink)
{
    GstElementClass *element_class =
        GST_ELEMENT_CLASS(GST_FBVIDEOSINK_GET_CLASS(fbvideosink));


    GST_LOG("fbvideosink: gst_fbvideosink_init BEGIN..");

    fbvideosink->fbcontext = NULL;
    fbvideosink->fbimage = NULL;
    fbvideosink->cur_image = NULL;
    fbvideosink->framesDisplayed = 0;

    fbvideosink->fps_n = 0;
    fbvideosink->fps_d = 0;
    fbvideosink->video_width = 0;
    fbvideosink->video_height = 0;

    fbvideosink->par = NULL;
    fbvideosink->device = g_strdup(DEFAULT_DEVICE);
    element_class->query = gst_fbvideosink_query;

    fbvideosink->hRszcopy = Rszcopy_create(RSZCOPY_DEFAULTRSZRATE);
    if (fbvideosink->hRszcopy == RSZCOPY_FAILURE) {
      GST_WARNING("Failed to create resize copy job");
    }

    GST_LOG("fbvideosink: gst_fbvideosink_init END..");

}

static void gst_fbvideosink_base_init(gpointer g_class)
{
    GstElementClass *element_class = GST_ELEMENT_CLASS(g_class);

    GST_LOG("fbvideosink: gst_fbvideosink_base_init BEGIN..");

    gst_element_class_set_details(element_class, &gst_fbvideosink_details);
    gst_element_class_add_pad_template(element_class,
                                       gst_static_pad_template_get
                                       (&gst_fbvideosink_sink_template_factory)
                                      );

    GST_LOG("fbvideosink: gst_fbvideosink_base_init END..");

}

static void gst_fbvideosink_class_init(GstFbVideoSinkClass * klass)
{
    GObjectClass *gobject_class;
    GstElementClass *gstelement_class;
    GstBaseSinkClass *gstbasesink_class;

    GST_DEBUG_CATEGORY_INIT(gst_debug_fbvideosink, "fbvideosink", 0,
                            "fbvideosink element");

    GST_LOG("fbvideosink: gst_fbvideosink_class_init BEGIN..");

    gobject_class = (GObjectClass *) klass;
    gstelement_class = (GstElementClass *) klass;
    gstbasesink_class = (GstBaseSinkClass *) klass;

    parent_class = g_type_class_ref(GST_TYPE_VIDEO_SINK);

    gobject_class->set_property = gst_fbvideosink_set_property;
    gobject_class->get_property = gst_fbvideosink_get_property;
    g_object_class_install_property(gobject_class, PROP_DEVICE,
                                    g_param_spec_string("device", "Device",
                                                        "Frame Buffer Device (usually /dev/fb0)",
                                                        DEFAULT_DEVICE,
                                                        G_PARAM_READWRITE)
                                   );

    gobject_class->finalize = gst_fbvideosink_finalize;
    gstelement_class->change_state =
        GST_DEBUG_FUNCPTR(gst_fbvideosink_change_state);

    gstbasesink_class->get_caps =
        GST_DEBUG_FUNCPTR(gst_fbvideosink_getcaps);
    gstbasesink_class->set_caps =
        GST_DEBUG_FUNCPTR(gst_fbvideosink_setcaps);

    gstbasesink_class->buffer_alloc =
        GST_DEBUG_FUNCPTR(gst_fbvideosink_buffer_alloc);
    gstbasesink_class->preroll =
        GST_DEBUG_FUNCPTR(gst_fbvideosink_show_frame);
    gstbasesink_class->render =
        GST_DEBUG_FUNCPTR(gst_fbvideosink_show_frame);


    GST_LOG("fbvideosink: gst_fbvideosink_class_init END..");

}

/*....
	Public Methods
		Object Typing and Creation
...*/
GType gst_fbvideosink_get_type(void)
{
    static GType fbvideosink_type = 0;

    GST_LOG("fbvideosink: gst_fbvideosink_get_type BEGIN..");

    if (!fbvideosink_type) {
        static const GTypeInfo fbvideosink_info = {
            sizeof(GstFbVideoSinkClass),
            gst_fbvideosink_base_init,
            NULL,
            (GClassInitFunc) gst_fbvideosink_class_init,
            NULL,
            NULL,
            sizeof(GstFbVideoSink),
            0,
            (GInstanceInitFunc) gst_fbvideosink_init,
        };

        fbvideosink_type =
            g_type_register_static(GST_TYPE_VIDEO_SINK, "GstFbVideoSink",
                                   &fbvideosink_info, 0);
    }

    GST_LOG("fbvideosink: gst_fbvideosink_get_type END..");

    return fbvideosink_type;
}
