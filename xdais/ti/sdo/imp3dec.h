#ifndef __MP3DEC_H
#define __MP3DEC_H

#include <xdc/std.h>
#include <ti/xdais/ialg.h>
#include <ti/xdais/dm/iauddec.h>


typedef struct IMP3_DynamicParams {
    IAUDDEC_DynamicParams iauddecDynamicparams; /* must be second field of all params structures */
    XDAS_Int32 MonoToStereoCopy; /*for mono streams if this set to 1 mono to stereocopy will happen, if 0 it doesn't happen*/
}IMP3_DynamicParams;

/*
 *  ======== IDECODE_Status ========
 *  This structure defines the parameters that can be changed at runtime
 *  (read/write), and the instance status parameters (read-only).
 */
typedef struct IMP3_Status{
	IAUDDEC_Status	iauddecStatus;
	XDAS_Bool	isValid;	/* are next fields valid? (read-only) */

}IMP3_Status;



#endif
