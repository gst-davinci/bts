/*
 *  Copyright 2006 by Texas Instruments Incorporated.
 *
 *  All rights reserved. Property of Texas Instruments Incorporated.
 *  Restricted rights to use, duplicate or disclose this code are
 *  granted through contract.
 *
 */

/*
 * ======== xdc.h ========
 */

#ifndef XDC__
#define XDC__

#include <stdarg.h>
#include <setjmp.h>

#define __CONC2__(x,y) x ## y
#define __CONC__(x,y) __CONC2__(x,y)

typedef va_list XDC_va_list;
typedef jmp_buf XDC_jmp_buf;

#endif /* XDC__ */






/*
 *  @(#) xdc.rts 1, 0, 0, 0,132 1-2-2006 xdc-m52
*/

