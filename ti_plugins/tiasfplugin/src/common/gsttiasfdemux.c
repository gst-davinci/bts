#include <gst/gstutils.h>
#include <gst/riff/riff-media.h>
#include <gst/riff/riff-read.h>
#include <gst/gst-i18n-plugin.h>
#include <string.h>

#include "gstasfdemux.h"
#include "asfheaders.h"
#include "ASFParser.h"
#include "RCA.h"
#include "RCV.h"

//#define PAD_ALLOC
//#define DEBUG_OUT

#define WMTIME_TO_GSTTIME(time) (long long)((time*1000000L))
#define GSTTIME_TO_WMTIME(time) (time/1000000L)


GST_DEBUG_CATEGORY_STATIC(asf_debug);
#define GST_CAT_DEFAULT asf_debug

PSLSTATUS RCallback(ASFCONTENTHANDLE asfHandle,
                    PSLBYTE ** pbBuffer, PSLUINT32 fileOffset,
                    PSLUINT32 cBytes);
PSLSTATUS WCallback(ASFCONTENTHANDLE asfHandle,
                    PSLBYTE * pbBuffer, PSLUINT32 fileOffset,
                    PSLUINT32 cBytes);

FILEOPS fileOpsObj;
static GstStateChangeReturn gst_asf_demux_change_state(GstElement *
                                                       element,
                                                       GstStateChange
                                                       transition);
static void gst_tidemux_asf_loop(GstPad * pad);
static gboolean gst_tidemux_asf_src_event(GstPad * pad, GstEvent * event);
static gboolean gst_tidemux_asf_sink_event(GstPad * pad, GstEvent * event);
GstCaps *gst_tidemux_asf_sink_getcaps(GstPad * pad);
static gboolean gst_tidemux_asf_sink_setcaps(GstPad * pad, GstCaps * caps);
static gboolean gst_tidemux_asf_sink_activate(GstPad * sinkpad);
static gboolean gst_tidemux_asf_sink_activate_pull(GstPad * sinkpad,
                                                   gboolean active);
static gboolean gst_asf_demux_handle_src_query(GstPad * pad,
                                               GstQuery * query);
static const GstQueryType *gst_asf_demux_get_src_query_types(GstPad * pad);

/* elementfactory information */
static GstElementDetails gst_asf_demux_details = {
    "Asf Parser",
    "Parser/Demuxer",
    "Parses/demuxes the asf file and converts the constituent streams to rca / rcv formats",
    "Pratheesh Gangadhar\nPhanishekhar\nManik Batra\n"
};

static GstStaticPadTemplate gst_asf_demux_sink_template =
GST_STATIC_PAD_TEMPLATE("sink",
                        GST_PAD_SINK,
                        GST_PAD_ALWAYS,
                        GST_STATIC_CAPS("video/x-ms-asf")
    );

GST_BOILERPLATE(GstTIASFDemux, gst_tidemux_asf, GstElement,
                GST_TYPE_ELEMENT);

static GstPadTemplate *videosrctempl;
static GstPadTemplate *audiosrctempl;

static void gst_tidemux_asf_base_init(gpointer g_class)
{
    GstElementClass *element_class = GST_ELEMENT_CLASS(g_class);

    GstCaps *audcaps = gst_riff_create_audio_template_caps(),
        *vidcaps = gst_riff_create_video_template_caps();

    audiosrctempl = gst_pad_template_new("audio_%02d",
                                         GST_PAD_SRC, GST_PAD_SOMETIMES,
                                         audcaps);
    videosrctempl =
        gst_pad_template_new("video_%02d", GST_PAD_SRC, GST_PAD_SOMETIMES,
                             vidcaps);

    gst_element_class_add_pad_template(element_class, audiosrctempl);
    gst_element_class_add_pad_template(element_class, videosrctempl);
    gst_element_class_add_pad_template(element_class,
                                       gst_static_pad_template_get
                                       (&gst_asf_demux_sink_template));

    gst_element_class_set_details(element_class, &gst_asf_demux_details);

    GST_DEBUG_CATEGORY_INIT(asf_debug, "tidemux_asf", 0,
                            "ti asf demuxer element");
}

static void gst_tidemux_asf_class_init(GstTIASFDemuxClass * klass)
{
    GstElementClass *gstelement_class;

    gstelement_class = (GstElementClass *) klass;

    gstelement_class->change_state =
        GST_DEBUG_FUNCPTR(gst_asf_demux_change_state);

}

static void
gst_tidemux_asf_init(GstTIASFDemux * demux, GstTIASFDemuxClass * klass)
{
    fileOpsObj.read = RCallback;
    fileOpsObj.write = WCallback;

    demux->sinkpad =
        gst_pad_new_from_template(gst_static_pad_template_get
                                  (&gst_asf_demux_sink_template), "sink");
    gst_pad_set_activate_function(demux->sinkpad,
                                  GST_DEBUG_FUNCPTR
                                  (gst_tidemux_asf_sink_activate));
    gst_pad_set_activatepull_function(demux->sinkpad,
                                      GST_DEBUG_FUNCPTR
                                      (gst_tidemux_asf_sink_activate_pull));
    gst_pad_set_getcaps_function(demux->sinkpad,
                                 gst_tidemux_asf_sink_getcaps);
    gst_pad_set_setcaps_function(demux->sinkpad,
                                 gst_tidemux_asf_sink_setcaps);

    gst_pad_set_event_function(demux->sinkpad,
                               GST_DEBUG_FUNCPTR
                               (gst_tidemux_asf_sink_event));
    gst_element_add_pad(GST_ELEMENT(demux), demux->sinkpad);

    /* We should zero everything to be on the safe side */
    demux->num_audio_streams = 0;
    demux->num_video_streams = 0;
    demux->num_streams = 0;

    demux->state = GST_ASF_DEMUX_STATE_HEADER;
}

GstCaps *gst_tidemux_asf_sink_getcaps(GstPad * pad)
{
    return gst_caps_copy(gst_pad_get_pad_template_caps(pad));
}

static gboolean gst_tidemux_asf_sink_setcaps(GstPad * pad, GstCaps * caps)
{
    GstStructure *structure;

    const guchar *mime;
    structure = gst_caps_get_structure(caps, 0);
    mime = gst_structure_get_name(structure);

    if (!strcmp("video/x-ms-asf", mime))
        return TRUE;
#ifdef DEBUG_OUT
    printf("mime is not video/x-ms-asf \n");
#endif

    //return gst_pad_set_caps(pad, caps);
    return FALSE;
}

static gboolean gst_tidemux_asf_sink_activate(GstPad * sinkpad)
{
#ifdef DEBUG_OUT
    printf("Activate invoked\n");
#endif
    if (gst_pad_check_pull_range(sinkpad)) {
        return gst_pad_activate_pull(sinkpad, TRUE);
    }
    return FALSE;
};

static gboolean gst_tidemux_asf_sink_activate_pull(GstPad * sinkpad,
                                                   gboolean active)
{
#ifdef DEBUG_OUT
    printf("Activate pull invoked: tidemux_asf\n");
#endif
    if (active) {
        /* if we have a scheduler we can start the task */
        gst_pad_start_task(sinkpad, (GstTaskFunction) gst_tidemux_asf_loop,
                           sinkpad);
    } else {
#ifdef DEBUG_OUT
        printf("Stopping loop task\n");
#endif
        gst_pad_stop_task(sinkpad);
    }

    return TRUE;
};


#define BUF_REQ_SIZE 0

gint gFlag;
GstBuffer *gReadbuf;
gboolean isEos, endAudio, endVideo;
gint64 audio_pts, video_pts, audio_preroll, video_preroll;
guint64 invokeCount;
ASFPARSERPBCONTEXTINTERNAL asfPbContextObj;
ASFAUDIOSTREAMINFO audioStreamInfo;
ASFVIDEOSTREAMINFO videoStreamInfo;
PSLBYTE *rcvSeqHdr, *rcvFrameHdr;
gint frameHeaderLen, sequenceHeaderLen;
guint audBufActualSize, vidBufActualSize;
guint audCodecId, vidCodecId;
PSLBYTE nAudioStreams, nVideoStreams;
guint audMediaObjOffset, vidMediaObjOffset;
#if 0
#define SIZE_OF_BUFFER 1024*512
PSLUINT32 memFileOffset;        //Current file start offset
PSLUINT32 memCBytes;            //Memory cache size
#endif
static void gst_tidemux_asf_loop(GstPad * pad)
{
#ifdef DEBUG_OUT
    int cnt;
#endif
    static
    GstFlowReturn res;
    GstBuffer *audBuf = NULL, *vidBuf = NULL;

    MEDIAOBJMETAINFO mediaObjAudMetaInfo, mediaObjVidMetaInfo;

    FILEOPS fileOpsObj;
    // Audio and Video specific Data 

    MINSTREAMINFO minAudioStreamInfo, minVideoStreamInfo;

    MINSTREAMINFO *minAudioStreamInfoptr, *minVideoStreamInfoptr;
    PSLBYTE rcaSeqHdr[RCA_SEQUENCE_HEADER_LEN],
        rcaFrameHdr[RCA_FRAME_HEADER_LEN];
    PSLSTATUS retStatus = ASF_SUCCESS;

    PSLUINT32 audBufSize, vidBufSize;
    GstCaps *othercaps;
    minAudioStreamInfoptr = &minAudioStreamInfo;
    minVideoStreamInfoptr = &minVideoStreamInfo;

    GstTIASFDemux *demux = GST_TIASF_DEMUX(gst_pad_get_parent(pad));
#ifdef DEBUG_OUT
    invokeCount++;
    printf("Loop invoked... %llu times \n", invokeCount);
#endif
    if (!isEos) {
        fileOpsObj.read = RCallback;
        fileOpsObj.write = WCallback;

        if (!gFlag) {
            // Initialization of ASF parser handle
            retStatus =
                ASFParserContextInit(&asfPbContextObj, demux->sinkpad,
                                     &fileOpsObj);
            gst_segment_init(&demux->segment, GST_FORMAT_TIME);
#ifdef DEBUG_OUT
            if (retStatus != ASF_SUCCESS) {
                printf
                    ("\nsomething wrong with the ContextInit return value: return value = %d\n",
                     retStatus);
                switch (retStatus) {
                case ASF_E_BADASFHEADER:
                    printf("ContextInit: bad asf header\n");
                    break;
                case ASF_E_INVALIDARG:
                    printf("ContextInit: invalid arg\n");
                    break;
                case ASF_E_BUFINSUFFICIENT:
                    printf("ContextInit: Buf Insufficient\n");
                    break;
                case ASF_E_INVALIDOPERATION:
                    printf("ContextInit: Invalid operation\n");
                    break;
                default:
                    printf("ContextInit: Some other error\n");
                }
            }
#endif

            // Getting the number of video and audio streams in the file
            ASFGetNumStreams(&asfPbContextObj, &nAudioStreams,
                             &nVideoStreams);
#ifdef DEBUG_OUT
            printf("nAudioStreams = %d and nVideostreams = %d \n",
                   nAudioStreams, nVideoStreams);
#endif

        }



        if (!gFlag)             //do only once
        {
            if (nAudioStreams) {

                retStatus = ASFGetStreamMinInfo(&asfPbContextObj, AUDIO,
                                                1, &minAudioStreamInfoptr);
#ifdef DEBUG_OUT
                if (retStatus != ASF_SUCCESS) {
                    printf("\n error in ASFGetStreamMinInfo \n");
                }
#endif

                // Set the active stream
                ASFSetActiveStream(&asfPbContextObj, AUDIO,
                                   minAudioStreamInfo.bStreamId);
                ASFGetAudioStreamInfo(&asfPbContextObj,
                                      minAudioStreamInfo.bStreamId,
                                      &audioStreamInfo);
                audCodecId =
                    audioStreamInfo.WMATypeSpecData.WaveFormatEx.
                    wFormatTag;
                if (audCodecId == 0x55) {
                    audMediaObjOffset = 0;
                    audBufActualSize++; //make it positive so that buffer_new_and_alloc is success
                }


                if (audCodecId == 0x161) {
                    retStatus =
                        RCAGenerateSequenceHeader(&asfPbContextObj,
                                                  &audioStreamInfo,
                                                  rcaSeqHdr,
                                                  RCA_SEQUENCE_HEADER_LEN,
                                                  0);
                    if (retStatus != ASF_SUCCESS) {
#ifdef DEBUG_OUT
                        printf(" RCA Sequence header generation error \n");
#endif
                    }
                }
                demux->srcpad[0] = NULL;
                demux->srcpad[0] =
                    gst_pad_new_from_template(audiosrctempl, "audio_00");

                gst_pad_set_query_type_function(demux->srcpad[0],
                                                GST_DEBUG_FUNCPTR
                                                (gst_asf_demux_get_src_query_types));

                gst_pad_set_query_function(demux->srcpad[0],
                                           GST_DEBUG_FUNCPTR
                                           (gst_asf_demux_handle_src_query));

                gst_element_add_pad(GST_ELEMENT(demux), demux->srcpad[0]);
                gst_pad_set_event_function(demux->srcpad[0],
                                           gst_tidemux_asf_src_event);

                gst_segment_init(&demux->segment, GST_FORMAT_TIME);
                gst_pad_push_event(demux->srcpad[0],
                                   gst_event_new_new_segment(FALSE,
                                                             demux->
                                                             segment.rate,
                                                             demux->
                                                             segment.
                                                             format,
                                                             demux->
                                                             segment.start,
                                                             demux->
                                                             segment.
                                                             duration,
                                                             demux->
                                                             segment.
                                                             start));


                if (audCodecId == 0x55) {
#ifdef DEBUG_OUT
                    printf
                        ("\n audCodecId = 0x55: setting mp3 caps on audio srcpad \n");
#endif
                    othercaps =
                        gst_caps_new_simple("audio/mpeg",
                                            "mpegversion", G_TYPE_INT, 1,
                                            "layer", G_TYPE_INT, 3, NULL);

                    gst_pad_set_caps(demux->srcpad[0], othercaps);
                    gst_pad_use_fixed_caps(demux->srcpad[0]);
                    gst_caps_unref(othercaps);
                } else {

                    othercaps =
                        gst_caps_new_simple("audio/x-wma",
                                            "wmaversion", G_TYPE_INT, 3,
                                            NULL);

                    gst_pad_set_caps(demux->srcpad[0], othercaps);
                    gst_pad_use_fixed_caps(demux->srcpad[0]);
                    gst_caps_unref(othercaps);

                }

            }

            if (nVideoStreams) {
                ASFGetStreamMinInfo(&asfPbContextObj, VIDEO,
                                    1, &minVideoStreamInfoptr);
                // Set the active stream
                ASFSetActiveStream(&asfPbContextObj, VIDEO,
                                   minVideoStreamInfo.bStreamId);
                ASFGetVideoStreamInfo(&asfPbContextObj,
                                      minVideoStreamInfo.bStreamId,
                                      &videoStreamInfo);
                vidCodecId = videoStreamInfo.videoFourCCCodec;
                sequenceHeaderLen =
                    GetSequenceHeaderLength(&asfPbContextObj,
                                            &videoStreamInfo);
                //printf ("sequence header length = %d\n", sequenceHeaderLen);
                frameHeaderLen =
                    GetFrameHeaderLength(&asfPbContextObj,
                                         &videoStreamInfo);
                //printf ("frame header length = %d\n", frameHeaderLen);

                rcvSeqHdr = (PSLBYTE *) malloc(sequenceHeaderLen);
                rcvFrameHdr = (PSLBYTE *) malloc(frameHeaderLen);
                GenerateSequenceHeader(&asfPbContextObj,
                                       &videoStreamInfo, rcvSeqHdr,
                                       sequenceHeaderLen,
                                       sequenceHeaderLen);


                demux->srcpad[1] =
                    gst_pad_new_from_template(videosrctempl, "video_00");
                gst_element_add_pad(GST_ELEMENT(demux), demux->srcpad[1]);

                gst_pad_set_query_type_function(demux->srcpad[1],
                                                GST_DEBUG_FUNCPTR
                                                (gst_asf_demux_get_src_query_types));
                gst_pad_set_query_function(demux->srcpad[1],
                                           GST_DEBUG_FUNCPTR
                                           (gst_asf_demux_handle_src_query));

                gst_pad_set_event_function(demux->srcpad[1],
                                           gst_tidemux_asf_src_event);
                if (vidCodecId == 6) {
                    othercaps =
                        gst_caps_new_simple("video/x-vc9",
                                            "wmvversion", G_TYPE_INT, 3,
                                            NULL);
                } else {

                    othercaps = gst_caps_new_simple("video/x-vc1", NULL);

                }



                gst_pad_set_caps(demux->srcpad[1], othercaps);
                gst_pad_use_fixed_caps(demux->srcpad[1]);
                gst_caps_unref(othercaps);

                gst_pad_push_event(demux->srcpad[1],
                                   gst_event_new_new_segment(FALSE,
                                                             demux->
                                                             segment.rate,
                                                             demux->
                                                             segment.
                                                             format,
                                                             demux->
                                                             segment.start,
                                                             demux->
                                                             segment.
                                                             duration,
                                                             demux->
                                                             segment.
                                                             start));

            }
        }

        if (!gFlag) {
            audMediaObjOffset =
                RCA_SEQUENCE_HEADER_LEN + RCA_FRAME_HEADER_LEN;
            vidMediaObjOffset = frameHeaderLen + sequenceHeaderLen;
        } else {
            if (audCodecId == 0x161) {
                audMediaObjOffset = RCA_FRAME_HEADER_LEN;
            }
            vidMediaObjOffset = frameHeaderLen;

        }
        // parse ASF file for metadata and output the same

        if (nAudioStreams && !endAudio
            && (!nVideoStreams || endVideo
                || (long) (audio_pts - video_pts) < 10000L)) {
#ifdef PAD_ALLOC
            res =
                gst_pad_alloc_buffer(demux->srcpad[0],
                                     GST_BUFFER_OFFSET_NONE,
                                     audMediaObjOffset + audBufActualSize,
                                     GST_PAD_CAPS(demux->srcpad[0]),
                                     &audBuf);
            if (res != GST_FLOW_OK) {
#ifdef DEBUG_OUT
                printf
                    ("failed when allocating audio buffer from the audio src pad\n");
#endif
            }
#else
            /* Using Gst_Buffer_new_alloc instead of pad-alloc */

            audBuf =
                gst_buffer_new_and_alloc(audMediaObjOffset +
                                         audBufActualSize);

#endif
            audBufSize = GST_BUFFER_SIZE(audBuf);
            retStatus =
                ASFGetNextMediaObj(&asfPbContextObj, AUDIO,
                                   GST_BUFFER_DATA(audBuf), &audBufSize,
                                   audMediaObjOffset,
                                   &mediaObjAudMetaInfo);
            audio_pts = mediaObjAudMetaInfo.dwMediaObjPresTime;
            if (retStatus != ASF_SUCCESS) {
                gst_buffer_unref(audBuf);       //free the old Gst buffer     
                if (retStatus == ASF_E_BUFINSUFFICIENT) {
#ifdef DEBUG_OUT
                    printf
                        ("ASF_E_BUFINSUFFICIENT : bufsize_given =%d and mediaobjsize = %d  \n",
                         audBufActualSize,
                         mediaObjAudMetaInfo.dwMediaObjSize);
#endif

                    audBufActualSize = mediaObjAudMetaInfo.dwMediaObjSize;

#ifdef PAD_ALLOC
                    res = gst_pad_alloc_buffer(demux->srcpad[0], GST_BUFFER_OFFSET_NONE, audMediaObjOffset + audBufActualSize, GST_PAD_CAPS(demux->srcpad[0]), &audBuf);        //reallocate the Gst Buffer
                    if (res != GST_FLOW_OK) {
#ifdef DEBUG_OUT
                        printf
                            ("failed when allocating audio buffer from the audio src pad\n");
#endif
                    }
#else
                    audBuf =
                        gst_buffer_new_and_alloc(audMediaObjOffset +
                                                 audBufActualSize);
#endif

                    audBufSize = GST_BUFFER_SIZE(audBuf);
                    retStatus =
                        ASFGetNextMediaObj(&asfPbContextObj, AUDIO,
                                           GST_BUFFER_DATA(audBuf),
                                           &audBufSize, audMediaObjOffset,
                                           &mediaObjAudMetaInfo);
                    //audio_pts = mediaObjAudMetaInfo.dwMediaObjPresTime;
                    if (retStatus != ASF_SUCCESS) {
                        gst_buffer_unref(audBuf);       //free the old Gst buffer     
                        printf(" getnextmediaobj failed 2nd time \n");
                    }

                } else {
                    if (retStatus == ASF_E_ENDOFFILE) {
#ifdef DEBUG_OUT
                        printf("AUDIO ASF_E_ENDOFFILE received\n");
#endif
                        endAudio = TRUE;
                        if (endAudio) {
                            //gst_pad_push_event(demux->srcpad[0], gst_event_new_eos());  //send EOS if some other error also
                            if (!nVideoStreams) {
                                isEos = TRUE;
                                goto end;
                            }
                            if (endVideo) {
                                //gst_pad_push_event(demux->srcpad[1], gst_event_new_eos());      //send EOS if some other error also
                                isEos = TRUE;
                                goto end;
                            }
                        }
                    } else {
#ifdef DEBUG_OUT
                        printf
                            ("Some other getnextMediaObj error received = %x\n",
                             retStatus);
#endif
                        endAudio = TRUE;
                        if (endAudio) {
                            //gst_pad_push_event(demux->srcpad[0], gst_event_new_eos());  //send EOS if some other error also
                            if (!nVideoStreams) {
                                isEos = TRUE;
                                goto end;
                            }

                            if (endVideo) {
                                gst_pad_push_event(demux->srcpad[1], gst_event_new_eos());      //send EOS if some other error also
                                isEos = TRUE;
                                goto end;
                            }
                        }
                    }
                }
            }
#ifdef DEBUG_OUT
            printf(" just before  rca call to frame header generation \n");
#endif
            if (retStatus == ASF_SUCCESS) {
#ifdef DEBUG_OUT
                printf(" Parsed audio media object num = %d pts = %ld\n",
                       mediaObjAudMetaInfo.dwMediaObjNum,
                       mediaObjAudMetaInfo.dwMediaObjPresTime);
#endif
                //audio_pts = mediaObjAudMetaInfo.dwMediaObjPresTime;

                if (audCodecId == 0x161) {
                    retStatus =
                        RCAGenerateFrameHeader(&asfPbContextObj,
                                               &mediaObjAudMetaInfo,
                                               rcaFrameHdr,
                                               RCA_FRAME_HEADER_LEN, 0);
                    if (!gFlag) {
#ifdef  DEBUG_OUT
                        printf("\nrcaframehdr print follows \n");
                        for (cnt = 0; cnt < RCA_FRAME_HEADER_LEN; cnt++)
                            printf("%2x", rcaFrameHdr[cnt]);
                        printf("\n");
#endif
                    }
                }
                if (audCodecId == 0x161) {
                    if (!gFlag) {
                        memcpy(GST_BUFFER_DATA(audBuf), rcaSeqHdr,
                               RCA_SEQUENCE_HEADER_LEN);
                        memcpy(GST_BUFFER_DATA(audBuf) +
                               RCA_SEQUENCE_HEADER_LEN, rcaFrameHdr,
                               RCA_FRAME_HEADER_LEN);
                    } else
                        memcpy(GST_BUFFER_DATA(audBuf), rcaFrameHdr, RCA_FRAME_HEADER_LEN);     /*2nd buffer onwards, 
                                                                                                   only frame header needs to be put in front of the data */
                }
                if (!gFlag)
                    audio_preroll =
                        WMTIME_TO_GSTTIME(audio_pts) - 100000000L;
                GST_BUFFER_TIMESTAMP(audBuf) = WMTIME_TO_GSTTIME(audio_pts) - audio_preroll;    /*update the timestamp of the Gst Buffer 
                                                                                                   from the Media Object Info */
                if (audCodecId == 0x55) {
                    othercaps =
                        gst_caps_new_simple("audio/mpeg",
                                            "mpegversion", G_TYPE_INT, 1,
                                            "layer", G_TYPE_INT, 3, NULL);

                    gst_buffer_set_caps(audBuf, othercaps);
                } else {

                    othercaps =
                        gst_caps_new_simple("audio/x-wma",
                                            "wmaversion", G_TYPE_INT, 3,
                                            NULL);

                    gst_buffer_set_caps(audBuf, othercaps);

                }

                res = gst_pad_push(demux->srcpad[0], audBuf);   //push the audio buffer with RCA header to audio src pad
                if (res != GST_FLOW_OK) {
                   endAudio = TRUE;
#ifdef DEBUG_OUT
                    printf(" Audio Pad push failed\n");
                    switch (res) {
                    case GST_FLOW_NOT_LINKED:

                        printf
                            ("\nAudio pad push fail: src pad not linked\n");
                        break;
                    case GST_FLOW_NOT_NEGOTIATED:
                        printf
                            ("\nAudio pad push fail: caps not negotiated\n");
                        break;
                    case GST_FLOW_ERROR:
                        printf("\nAudio pad push fail: GST_FLOW_ERROR\n");
                        break;
                    default:
                        printf
                            ("\nAudio pad push fail: some other error\n");
                    }
#endif
                    if (endAudio) {
                        //gst_pad_push_event(demux->srcpad[0], gst_event_new_eos());  //send EOS if some other error also
                        if (!nVideoStreams) {
                            isEos = TRUE;
                            goto end;
                        }

                        if (endVideo) {
                            //gst_pad_push_event(demux->srcpad[1], gst_event_new_eos());      //send EOS if some other error also
                            isEos = TRUE;
                            goto end;
                        }
                    }
                } else {
#ifdef DEBUG_OUT
                    printf(" Audio Pad push success\n");
#endif
                }
                gst_caps_unref(othercaps);


            }

        }                       //if(nAudioStreams)

        if (nVideoStreams && !endVideo
            && (!nAudioStreams || endAudio
                || (long) (video_pts - audio_pts) < 10000L)) {
#ifdef PAD_ALLOC
            res =
                gst_pad_alloc_buffer(demux->srcpad[1],
                                     GST_BUFFER_OFFSET_NONE,
                                     vidMediaObjOffset + vidBufActualSize,
                                     GST_PAD_CAPS(demux->srcpad[1]),
                                     &vidBuf);
            if (res != GST_FLOW_OK) {
#ifdef DEBUG_OUT
                printf
                    ("failed when allocating video buffer from the video src pad\n");
#endif
            }
#else

            vidBuf =
                gst_buffer_new_and_alloc(vidMediaObjOffset +
                                         vidBufActualSize);

#endif
            vidBufSize = GST_BUFFER_SIZE(vidBuf);
            retStatus =
                ASFGetNextMediaObj(&asfPbContextObj, VIDEO,
                                   GST_BUFFER_DATA(vidBuf), &vidBufSize,
                                   vidMediaObjOffset,
                                   &mediaObjVidMetaInfo);
            video_pts = mediaObjVidMetaInfo.dwMediaObjPresTime;
            if (retStatus != ASF_SUCCESS) {
                gst_buffer_unref(vidBuf);       //free the old Gst buffer     
                if (retStatus == ASF_E_BUFINSUFFICIENT) {
#ifdef DEBUG_OUT
                    printf
                        ("ASF_E_BUFINSUFFICIENT : bufsize_given =%d and mediaobjsize = %d  \n",
                         vidBufActualSize,
                         mediaObjVidMetaInfo.dwMediaObjSize);
#endif
                    vidBufActualSize = mediaObjVidMetaInfo.dwMediaObjSize;
#ifdef PAD_ALLOC
                    res = gst_pad_alloc_buffer(demux->srcpad[1], GST_BUFFER_OFFSET_NONE, vidMediaObjOffset + vidBufActualSize, GST_PAD_CAPS(demux->srcpad[1]), &vidBuf);        //reallocate the Gst Buffer
                    if (res != GST_FLOW_OK) {
#ifdef DEBUG_OUT
                        printf
                            ("failed when allocating video buffer from the video src pad\n");
#endif
                    }
#else

                    vidBuf =
                        gst_buffer_new_and_alloc(vidMediaObjOffset +
                                                 vidBufActualSize);
#endif
                    vidBufSize = GST_BUFFER_SIZE(vidBuf);
                    retStatus =
                        ASFGetNextMediaObj(&asfPbContextObj, VIDEO,
                                           GST_BUFFER_DATA(vidBuf),
                                           &vidBufSize, vidMediaObjOffset,
                                           &mediaObjVidMetaInfo);
                    video_pts = mediaObjVidMetaInfo.dwMediaObjPresTime;
                    if (retStatus != ASF_SUCCESS) {
                        printf(" getnextmediaobj failed 2nd time \n");
                        gst_buffer_unref(vidBuf);       //free the old Gst buffer        
                    }
                } else {
                    if (retStatus == ASF_E_ENDOFFILE) {
#ifdef DEBUG_OUT
                        printf("VIDEO ASF_E_ENDOFFILE received\n");
#endif
                        endVideo = TRUE;
                        if (endVideo) {
                            //gst_pad_push_event(demux->srcpad[1], gst_event_new_eos());  //send EOS if some other error also
                            if (!nAudioStreams) {
                                isEos = TRUE;
                                goto end;
                            }

                            if (endAudio) {
                                //gst_pad_push_event(demux->srcpad[0], gst_event_new_eos());      //send EOS if some other error also
                                isEos = TRUE;
                                goto end;
                            }
                        }
                    } else {
#ifdef DEBUG_OUT
                        printf
                            ("Some other getnextMediaObj error received =%x\n",
                             retStatus);
#endif
                        endVideo = TRUE;
                        if (endVideo) {
                            //gst_pad_push_event(demux->srcpad[1], gst_event_new_eos());  //send EOS if some other error also
                            if (!nAudioStreams) {
                                isEos = TRUE;
                                goto end;
                            }

                            if (endAudio) {
                                //gst_pad_push_event(demux->srcpad[0], gst_event_new_eos());      //send EOS if some other error also
                                isEos = TRUE;
                                goto end;
                            }
                        }
                    }
                }
            }
            if (retStatus == ASF_SUCCESS) {
#ifdef DEBUG_OUT
                printf(" Parsed video media object num = %d, pts = %ld\n",
                       mediaObjVidMetaInfo.dwMediaObjNum,
                       mediaObjVidMetaInfo.dwMediaObjPresTime);
#endif
                video_pts = mediaObjVidMetaInfo.dwMediaObjPresTime;
                GenerateFrameHeader(&asfPbContextObj, &videoStreamInfo,
                                    &mediaObjVidMetaInfo, rcvFrameHdr,
                                    frameHeaderLen, frameHeaderLen, 0);
                if (!gFlag) {
                    memcpy(GST_BUFFER_DATA(vidBuf), rcvSeqHdr,
                           sequenceHeaderLen);
                    memcpy(GST_BUFFER_DATA(vidBuf) +
                           sequenceHeaderLen, rcvFrameHdr, frameHeaderLen);
                } else
                    memcpy(GST_BUFFER_DATA(vidBuf), rcvFrameHdr, frameHeaderLen);       /*2nd buffer onwards, only frame header
                                                                                           needs to be put in front of the data */

                if (!gFlag)
                    video_preroll =
                        WMTIME_TO_GSTTIME(video_pts) - 100000000L;
                GST_BUFFER_TIMESTAMP(vidBuf) =
                    WMTIME_TO_GSTTIME(video_pts) - video_preroll;



                if (vidCodecId == 6) {
                    othercaps =
                        gst_caps_new_simple("video/x-vc9",
                                            "wmvversion", G_TYPE_INT, 3,
                                            NULL);
                } else {

                    othercaps = gst_caps_new_simple("video/x-vc1", NULL);

                }
                gst_buffer_set_caps(vidBuf, othercaps);

                res = gst_pad_push(demux->srcpad[1], vidBuf);   //push the video buffer with RCV header to video src pad
                if (res != GST_FLOW_OK) {
                   endVideo = TRUE;
#ifdef DEBUG_OUT
                    printf("Video Pad push failed\n");
                    switch (res) {
                    case GST_FLOW_NOT_LINKED:
                        nVideoStreams = 0;
                        printf
                            ("\n Video pad push fail: src pad not linked\n");
                        break;
                    case GST_FLOW_NOT_NEGOTIATED:
                        printf
                            ("\nVideo pad push fail: caps not negotiated\n");
                        break;
                    case GST_FLOW_ERROR:
                        printf("\nVideo pad push fail: GST_FLOW_ERROR\n");
                        break;
                    default:
                        printf
                            ("\nVideo pad push fail: some other error\n");
                    }
#endif
                    if (endVideo) {
                        //gst_pad_push_event(demux->srcpad[1], gst_event_new_eos());  //send EOS if some other error also
                        if (!nAudioStreams) {
                            isEos = TRUE;
                            goto end;
                        }

                        if (endAudio) {
                            //gst_pad_push_event(demux->srcpad[0], gst_event_new_eos());      //send EOS if some other error also
                            isEos = TRUE;
                            goto end;
                        }
                    }
                } else {
#ifdef DEBUG_OUT
                    printf("Video Pad push success\n");
#endif

                }
                gst_caps_unref(othercaps);
            }
        }                       //if(nVideoStreams)


        gFlag = 1;              //update the flag for the next audio/video media objects following the first one
    } else {
#ifdef DEBUG_OUT
        printf("pausing asf demux loop\n");
#endif
        if (nAudioStreams)
	   gst_pad_push_event(demux->srcpad[0], gst_event_new_eos()); 
	if (nVideoStreams)   
	   gst_pad_push_event(demux->srcpad[1], gst_event_new_eos()); 
        gst_pad_pause_task(pad);

    }
  end:
    gst_object_unref(demux);
}

#define MAX_STREAMS 2
static GstStateChangeReturn gst_asf_demux_change_state(GstElement *
                                                       element,
                                                       GstStateChange
                                                       transition)
{
    GstTIASFDemux *demux = GST_TIASF_DEMUX(element);
    GstStateChangeReturn ret = GST_STATE_CHANGE_SUCCESS;
    int cnt;
    switch (transition) {
    case GST_STATE_CHANGE_READY_TO_PAUSED:{
            gst_segment_init(&demux->segment, GST_FORMAT_TIME);
            gFlag = 0;
            isEos = FALSE;
            video_pts = 0;
            audio_pts = 0;
            invokeCount = 0;
            endAudio = FALSE;
            endVideo = FALSE;
            gReadbuf = NULL;
            rcvSeqHdr = NULL;
            rcvFrameHdr = NULL;
            audBufActualSize = BUF_REQ_SIZE;
            vidBufActualSize = BUF_REQ_SIZE;
            audMediaObjOffset = 0;
            vidMediaObjOffset = 0;
            memset(&asfPbContextObj, 0, sizeof(asfPbContextObj));
            memset(&videoStreamInfo, 0, sizeof(videoStreamInfo));
            memset(&audioStreamInfo, 0, sizeof(audioStreamInfo));
#if 0
            memFileOffset = 0;
            memCBytes = 0;
#endif
            break;
        }
    default:
        break;
    }

    ret =
        GST_ELEMENT_CLASS(parent_class)->change_state(element, transition);
    if (ret == GST_STATE_CHANGE_FAILURE)
        return ret;

    switch (transition) {

    case GST_STATE_CHANGE_PLAYING_TO_PAUSED:{
#ifdef DEBUG_OUT
            printf("\ntidemux_asf: change state: playing to paused \n");
#endif
            break;
        }

    case GST_STATE_CHANGE_PAUSED_TO_READY:{

#ifdef DEBUG_OUT
            printf("\ntidemux_asf: change state: paused to ready \n");
#endif

            for (cnt = 0; cnt < MAX_STREAMS; cnt++) {
                if (demux->srcpad[cnt]) {
                    gst_element_remove_pad(GST_ELEMENT(demux),
                                           demux->srcpad[cnt]);
                    demux->srcpad[cnt] = NULL;
                }
            }


            gst_segment_init(&demux->segment, GST_FORMAT_UNDEFINED);
            demux->state = GST_ASF_DEMUX_STATE_HEADER;
            if (gReadbuf != NULL) {
                gst_buffer_unref(gReadbuf);     //free the Gst buffer pulled in RCallback
                gReadbuf = NULL;
            }
            if (rcvSeqHdr != NULL)
                free(rcvSeqHdr);
            if (rcvFrameHdr != NULL)
                free(rcvFrameHdr);
            ASFParserContextDeInit(&asfPbContextObj, &audioStreamInfo,
                                   &videoStreamInfo);
            break;
        }
    case GST_STATE_CHANGE_READY_TO_NULL:
#ifdef DEBUG_OUT
        printf("\ntidemux_asf:  state change : ready to null\n");
        break;
#endif
    default:
        break;
    }

    return ret;
}

static gboolean gst_tidemux_asf_sink_event(GstPad * pad, GstEvent * event)
{

    GstTIASFDemux *demux = GST_TIASF_DEMUX(GST_PAD_PARENT(pad));
    gboolean ret = TRUE;

    GST_DEBUG_OBJECT(demux, "Got %s event on sink pad",
                     GST_EVENT_TYPE_NAME(event));
    switch (GST_EVENT_TYPE(event)) {
    case GST_EVENT_NEWSEGMENT:
        {
            ret = gst_pad_event_default(pad, event);
            break;
        }
    case GST_EVENT_FLUSH_START:
        ret = gst_pad_event_default(pad, event);
        break;
    case GST_EVENT_FLUSH_STOP:
        ret = gst_pad_event_default(pad, event);
        break;
    case GST_EVENT_EOS:
        isEos = TRUE;
        ret = gst_pad_event_default(pad, event);
        break;
    default:
        ret = gst_pad_event_default(pad, event);
        break;
    }
    return ret;
}

static gboolean gst_tidemux_asf_src_event(GstPad * pad, GstEvent * event)
{
    GstTIASFDemux *demux = GST_TIASF_DEMUX(GST_PAD_PARENT(pad));
    gboolean ret = TRUE;

    GST_DEBUG_OBJECT(demux, "Got %s event on src pad",
                     GST_EVENT_TYPE_NAME(event));

    ret = gst_pad_push_event(demux->sinkpad, event);
    return ret;
}

#if 0
//Try this with harddisk
PSLSTATUS RCallback(ASFCONTENTHANDLE asfHandle,
                    PSLBYTE ** pbBuffer, PSLUINT32 fileOffset,
                    PSLUINT32 cBytes)
{
    PSLUINT32 rBytes = 0;
    GstFlowReturn res;
    if (memFileOffset <= fileOffset &&
        memFileOffset + memCBytes >= fileOffset + cBytes) {
        *pbBuffer =
            (PSLBYTE *) GST_BUFFER_DATA(gReadbuf) + fileOffset -
            memFileOffset;
        rBytes = cBytes;
    } else {
        memFileOffset = fileOffset;
        if (gReadbuf != NULL) {
            gst_buffer_unref(gReadbuf); //free the Gst buffer pulled in RCallback
            gReadbuf = NULL;
        }
        if ((res = gst_pad_pull_range(asfHandle,
                                      fileOffset,
                                      SIZE_OF_BUFFER,
                                      &gReadbuf)) != GST_FLOW_OK) {
            *pbBuffer = PSLNULL;
#ifdef DEBUG_OUT
            printf("EOF reached\n");
#endif
            return -1;
        }

        *pbBuffer = GST_BUFFER_DATA(gReadbuf);
        rBytes = GST_BUFFER_SIZE(gReadbuf);
        memCBytes = rBytes;
        if (rBytes >= cBytes)
            rBytes = cBytes;
        else
            return 0;

    }

    return rBytes;

}
#else
PSLSTATUS RCallback(ASFCONTENTHANDLE asfHandle,
                    PSLBYTE ** pbBuffer, PSLUINT32 fileOffset,
                    PSLUINT32 cBytes)
{
    GstFlowReturn res;

    if (gReadbuf != NULL) {
        gst_buffer_unref(gReadbuf);     //free the Gst buffer pulled in RCallback
        gReadbuf = NULL;
    }


    if ((res = gst_pad_pull_range(asfHandle,
                                  fileOffset,
                                  cBytes, &gReadbuf)) != GST_FLOW_OK) {
#ifdef DEBUG_OUT
        printf("EOF reached\n");
#endif
        return -1;
    }

    *pbBuffer = GST_BUFFER_DATA(gReadbuf);
    if (cBytes != GST_BUFFER_SIZE(gReadbuf)) {
#ifdef DEBUG_OUT
        printf("bytes requested not equal to bytes actually read\n");
#endif
        return -1;
    }

    return cBytes;
}
#endif
PSLSTATUS WCallback(ASFCONTENTHANDLE asfHandle,
                    PSLBYTE * pbBuffer, PSLUINT32 fileOffset,
                    PSLUINT32 cBytes)
{
    return 1;
}

static const GstQueryType *gst_asf_demux_get_src_query_types(GstPad * pad)
{
    static const GstQueryType types[] = {
        GST_QUERY_DURATION,
        0
    };

    return types;
}

static gboolean
gst_asf_demux_handle_src_query(GstPad * pad, GstQuery * query)
{
    GstTIASFDemux *demux;
    gboolean res = FALSE;
    guint64 playDuration;

    demux = GST_TIASF_DEMUX(gst_pad_get_parent(pad));

    GST_DEBUG("handling %s query",
              gst_query_type_get_name(GST_QUERY_TYPE(query)));

    switch (GST_QUERY_TYPE(query)) {
    case GST_QUERY_DURATION:
        {
            GstFormat format;

            gst_query_parse_duration(query, &format, NULL);

            if (format != GST_FORMAT_TIME) {
                GST_LOG("only support duration queries in TIME format");
                break;
            }
            //GST_OBJECT_LOCK(demux);

            playDuration = (guint64) (asfPbContextObj.qwDuration * 100);
            //printf ("duration = %llu\n", playDuration);
            gst_query_set_duration(query, GST_FORMAT_TIME, playDuration);

            res = TRUE;

            //GST_OBJECT_UNLOCK(demux);
            break;
        }

    default:
        res = gst_pad_query_default(pad, query);
        break;
    }

    gst_object_unref(demux);
    return res;
}
