/*
 * gstfbvideosink.h
 *
 * Header file for Video Display Driver Sink Module for Gstreamer
 * implementation on DM644x device
 *
 * Copyright (C) 2007 Texas Instruments, Inc.
 * Copyright (C) 2008 Behind The Set, LLC
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#ifndef __FBVIDEOSINK_DEMUX_H__
#define __FBVIDEOSINK_DEMUX_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <sys/stat.h>
#include <errno.h>
#include <linux/fb.h>

#include <xdc/std.h>
#include <ti/sdo/ce/video/viddec.h>
#include <ti/sdo/ce/CERuntime.h>
#include <ti/sdo/ce/Engine.h>
#include <ti/sdo/ce/trace/gt.h>
#include <ti/sdo/ce/osal/Memory.h>


#define FBIO_WAITFORVSYNC _IOW('F', 0x20, u_int32_t)
#define FBIO_SETPOSX      _IOW('F', 0x22, u_int32_t)
#define FBIO_SETPOSY      _IOW('F', 0x23, u_int32_t)


#define MAX_NO_OF_CMEM_OUTPUT_BUFFERS 15
#define TRIPLE_BUF   3
#define DOUBLE_BUF   2

#include <gst/gst.h>
#include <gst/video/gstvideosink.h>
#include "rszcopy.h"

G_BEGIN_DECLS
#define GST_TYPE_FBVIDEOSINK          (gst_fbvideosink_get_type())
#define GST_FBVIDEOSINK(obj)          (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_TYPE_FBVIDEOSINK, GstFbVideoSink))
#define GST_FBVIDEOSINK_CLASS(klass)  (G_TYPE_CHECK_CLASS_CAST((klass) , GST_TYPE_FBVIDEOSINK, GstFbVideoSink))
#define GST_FBVIDEOSINK_GET_CLASS(klass)   (G_TYPE_INSTANCE_GET_CLASS((klass),GST_TYPE_FBVIDEOSINK,GstFbVideoSinkClass))
#define GST_IS_FBVIDEOSINK(obj)       (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_TYPE_FBVIDEOSINK))
#define GST_IS_FBVIDEOSINK_CLASS(obj) (G_TYPE_CHECK_CLASS_TYPE((klass) , GST_TYPE_FBVIDEOSINK))
typedef struct _GstFbVideoSink GstFbVideoSink;
typedef struct _GstFbVideoSinkClass GstFbVideoSinkClass;
typedef struct _GstFbContext GstFbContext;
typedef struct _GstFbImageFormat GstFbImageFormat;
typedef struct _GstFbImageBuffer GstFbImageBuffer;
typedef struct _GstFbImageBufferClass GstFbImageBufferClass;

/*
	The Context of the Frame Buffer Device
*/

#define 	DEFAULT_DEVICE	"/dev/fb/3"

#define SCREEN_BPP              16
#define D1_WIDTH            720
#define D1_HEIGHT           480
#define D1_MAX_HEIGHT       576


struct _GstFbContext {
    gint fbdevFd;
    struct fb_var_screeninfo vScreenInfo;
    struct fb_fix_screeninfo fScreenInfo;
    guint8 *data;               //pointer to the mapped memory
    gint screensize;
    gint noofbuffers;           //no of buffers that will be used 1 - single, 2- double....
//      gint    bufferIndex;     //index to the cur buffer
    gint bpp;                   //bits per pixel
    gint depth;                 //color depth
    gint endianness;            //endianess
    gint width;                 //width in px
    gint height;                //height in px
    gint widthmm;               //width in mm
    gint heightmm;              //height in mm
    GValue *par;                //calculated pixel aspect ratio
    GList *formats_list;        //list of formats supported by the frame buffer device
    gint im_format;
    GstCaps *caps;              //capabilities to latch on to the fbdev

    //allocation of hardware buffers
    gboolean avail[10];
    guint8 *buffers[10];
    gboolean doneonce;
};

struct _GstFbImageFormat {
    gint format;
    GstCaps *caps;
};

struct _GstFbImageBuffer {
    GstBuffer buffer;
    GstFbVideoSink *fbvideosink;
    gint width;
    gint height;
    gint im_format;
    size_t size;
};

struct _GstFbVideoSink {

    GstVideoSink videosink;

    GstFbContext *fbcontext;
    GstFbImageBuffer *fbimage;
    GstFbImageBuffer *cur_image;

    gint fps_n;
    gint fps_d;

    /* object-set pixel aspect ratio */
    GValue *par;
    GMutex *fb_lock;
    GMutex *pool_lock;

    /* width and height of the incoming vid frames */
    guint video_width;
    guint video_height;

    Rszcopy_Handle hRszcopy;

    gchar *device;
    guint framesDisplayed;
    guint8 buffers_allocated;
    GMutex *buffer_lock;

};

struct _GstFbVideoSinkClass {
    GstVideoSinkClass parent_class;
};

GType gst_fbvideosink_get_type(void);

G_END_DECLS
#endif
