/*
 *  Copyright 2007 by Texas Instruments Incorporated.
 *
 *  All rights reserved. Property of Texas Instruments Incorporated.
 *  Restricted rights to use, duplicate or disclose this code are
 *  granted through contract.
 *
 */

/*
 *  ======== Thread.h ========
 */

#ifndef ti_sdo_ce_osal_Thread_
#define ti_sdo_ce_osal_Thread_

#ifdef __cplusplus
extern "C" {
#endif

#define Thread_GTNAME "OT"

#define Thread_SUSPENDED   (-666)

#define Thread_MINPRI      1
#define Thread_MAXPRI      15

/*
 *  ======== Thread_Handle ========
 */
typedef struct Thread_Obj *Thread_Handle;

/*
 *  ======== Thread_Attrs ========
 */
typedef struct Thread_Attrs {
    Int         priority;       /* task priority */
    Int         stacksize;      /* size of stack */
    Int         stackseg;       /* segment to allocate stack from */
    Ptr         environ;        /* environment pointer */
    String      name;           /* printable name */
} Thread_Attrs;

/*
 *  ======== Thread_Stat ========
 */
typedef struct Thread_Stat {
    Int         stacksize;
    Int         stackused;
} Thread_Stat;

extern Thread_Attrs Thread_ATTRS;     /* default attributes */

/*
 *  ======== Thread_create ========
 */
extern Thread_Handle Thread_create(Fxn fxn, Thread_Attrs *attrs, ...);

/*
 *  ======== Thread_delete ========
 */
extern Void Thread_delete(Thread_Handle task);

/*
 *  ======== Thread_getpri ========
 */
extern Int Thread_getpri(Thread_Handle task);

/*
 *  ======== Thread_getenv ========
 */
extern Ptr Thread_getenv(Thread_Handle task);

/*
 *  ======== Thread_getname ========
 */
extern String Thread_getname(Thread_Handle task);

/*
 *  ======== Thread_init ========
 */
extern Bool Thread_init(Void);

/*
 *  ======== Thread_exit ========
 */
extern Void Thread_exit(Void);

/*
 *  ======== Thread_join ========
 */
extern Void Thread_join(Thread_Handle task);

/*
 *  ======== Thread_self ========
 */
extern Thread_Handle Thread_self(Void);

/*
 *  ======== Thread_setpri ========
 */
extern Int Thread_setpri(Thread_Handle task, Int newpri);


/*
 *  ======== Thread_stat ========
 */
extern Int Thread_stat(Thread_Handle task, Thread_Stat *buf);

/*
 *  ======== Thread_yield ========
 */
extern Void Thread_yield(Void);


#ifdef __cplusplus
}
#endif /* extern "C" */

#endif
/*
 *  @(#) ti.sdo.ce.osal; 1, 0, 0, 0,171; 4-20-2007 12:58:20; /db/atree/library/trees/ce-e10x/src/
 */

