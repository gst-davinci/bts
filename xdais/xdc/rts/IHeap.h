/*
 *  Copyright 2006 by Texas Instruments Incorporated.
 *
 *  All rights reserved. Property of Texas Instruments Incorporated.
 *  Restricted rights to use, duplicate or disclose this code are
 *  granted through contract.
 *
 */

/*
 *  ======== xdc/rts/IHeap.h ========
 *
 *  DO NOT MODIFY THIS FILE
 *
 *  It is generated from xdc/rts/IHeap.xdc and any changes may be overwritten.
 */

#ifdef xdc_rts_IHeap___EXTERNAL_SYNOPSIS
#error do not compile!!


    #include <xdc/rts/IHeap.h>

    /* -------- module-wide configs -------- */


    /* -------- per-instance configs -------- */

    struct IHeap_Params {
    };


    /* -------- instance creation/deletion -------- */

    IHeap_Instance IHeap_create( const IHeap_Params* __prms, xdc_rts_Error_Block* __eb );
    Void IHeap_delete( IHeap_Instance* __pInst );

    /* -------- per-instance functions -------- */

    Ptr IHeap_alloc( IHeap_Object* __obj, SizeT size, SizeT align, xdc_rts_Error_Block* eb );
    Void IHeap_free( IHeap_Object* __obj, Ptr block, SizeT size );
    Void IHeap_getStats( IHeap_Object* __obj, xdc_rts_Memory_Stats* stats );

    /* -------- abstract convertors -------- */

    IHeap_Module IHeap_Instance__to__Module( IHeap_Instance inst );


#endif /* xdc_rts_IHeap___EXTERNAL_SYNOPSIS */







#ifdef __cplusplus
#define __extern extern "C"
#else
#define __extern extern
#endif

#ifndef xdc_std__include
#ifndef __nested__
#define __nested__
#include <xdc/std.h>
#undef __nested__
#else
#include <xdc/std.h>
#endif
#endif

#ifndef xdc_rts_IHeap__include
#define xdc_rts_IHeap__include

#ifndef xdc_rts__
#include <xdc/rts/package/package.defs.h>
#endif

#ifndef xdc_rts_Memory__include
#ifndef __nested__
#define __nested__
#include <xdc/rts/Memory.h>
#undef __nested__
#else
#include <xdc/rts/Memory.h>
#endif
#endif

#ifndef xdc_rts_IModule__include
#ifndef __nested__
#define __nested__
#include <xdc/rts/IModule.h>
#undef __nested__
#else
#include <xdc/rts/IModule.h>
#endif
#endif

#ifndef xdc_rts_Error__include
#ifndef __nested__
#define __nested__
#include <xdc/rts/Error.h>
#undef __nested__
#else
#include <xdc/rts/Error.h>
#endif
#endif


/* auxiliary definitions */

/* per-instance configs */
struct xdc_rts_IHeap_Params {
    int __size;
};


/* function table */
struct xdc_rts_IHeap_Fxns__ {
    void* __base;
    void* (*__create)(void);
    void (*__delete)(xdc_rts_IHeap_Instance*);
    xdc_Ptr (*alloc)(void*, xdc_SizeT, xdc_SizeT, xdc_rts_Error_Block*);
    xdc_Void (*free)(void*, xdc_Ptr, xdc_SizeT);
    xdc_Void (*getStats)(void*, xdc_rts_Memory_Stats*);
};
__extern void* xdc_rts_IHeap_BASE__;

/* function stubs */
static inline xdc_rts_IHeap___Instance xdc_rts_IHeap_create( xdc_rts_IHeap_Module __mod ) {
    return (xdc_rts_IHeap___Instance) __mod->__create(); }
static inline void xdc_rts_IHeap_delete( xdc_rts_IHeap_Instance* instp ) {
    ((xdc_rts_IHeap___Instance)(*instp))->__fxns->__delete(instp); }
static inline xdc_rts_IHeap_Module xdc_rts_IHeap_Instance__to__Module( xdc_rts_IHeap_Instance inst ) {
    return ((xdc_rts_IHeap___Instance)inst)->__fxns; }
static inline xdc_Ptr xdc_rts_IHeap_alloc( xdc_rts_IHeap_Instance _this, xdc_SizeT size, xdc_SizeT align, xdc_rts_Error_Block* eb ) {
    return ((xdc_rts_IHeap___Instance)_this)->__fxns->alloc((void*)_this, size, align, eb); }
static inline xdc_Void xdc_rts_IHeap_free( xdc_rts_IHeap_Instance _this, xdc_Ptr block, xdc_SizeT size ) {
    ((xdc_rts_IHeap___Instance)_this)->__fxns->free((void*)_this, block, size); }
static inline xdc_Void xdc_rts_IHeap_getStats( xdc_rts_IHeap_Instance _this, xdc_rts_Memory_Stats* stats ) {
    ((xdc_rts_IHeap___Instance)_this)->__fxns->getStats((void*)_this, stats); }

/* function selectors */
typedef xdc_Ptr (*xdc_rts_IHeap_alloc__Fxn)(void*, xdc_SizeT, xdc_SizeT, xdc_rts_Error_Block*);
static inline xdc_rts_IHeap_alloc__Fxn xdc_rts_IHeap_alloc__fxn( xdc_rts_IHeap_Instance _this ) {
    xdc_rts_IHeap___Instance i2 = (xdc_rts_IHeap___Instance)_this;
    return (xdc_rts_IHeap_alloc__Fxn)i2->__fxns->alloc; }
typedef xdc_Void (*xdc_rts_IHeap_free__Fxn)(void*, xdc_Ptr, xdc_SizeT);
static inline xdc_rts_IHeap_free__Fxn xdc_rts_IHeap_free__fxn( xdc_rts_IHeap_Instance _this ) {
    xdc_rts_IHeap___Instance i2 = (xdc_rts_IHeap___Instance)_this;
    return (xdc_rts_IHeap_free__Fxn)i2->__fxns->free; }
typedef xdc_Void (*xdc_rts_IHeap_getStats__Fxn)(void*, xdc_rts_Memory_Stats*);
static inline xdc_rts_IHeap_getStats__Fxn xdc_rts_IHeap_getStats__fxn( xdc_rts_IHeap_Instance _this ) {
    xdc_rts_IHeap___Instance i2 = (xdc_rts_IHeap___Instance)_this;
    return (xdc_rts_IHeap_getStats__Fxn)i2->__fxns->getStats; }

/* C++ wrappers */
#ifdef __cplusplus
namespace xdc_rts { namespace IHeap {
    typedef xdc_rts_IHeap_Instance Instance;
    typedef xdc_rts_IHeap_Params Params;
    struct __Object {
        xdc_rts_IHeap___Object __obj;
        inline xdc_Ptr alloc( xdc_SizeT size, xdc_SizeT align, xdc_rts_Error_Block* eb) { return xdc_rts_IHeap_alloc((xdc_rts_IHeap_Instance)this, size, align, eb); }
        inline xdc_Void free( xdc_Ptr block, xdc_SizeT size) { xdc_rts_IHeap_free((xdc_rts_IHeap_Instance)this, block, size); }
        inline xdc_Void getStats( xdc_rts_Memory_Stats* stats) { xdc_rts_IHeap_getStats((xdc_rts_IHeap_Instance)this, stats); }
    };
}}
using namespace xdc_rts;
#endif

#endif /* xdc_rts_IHeap__include */

/* __prefix alises */
#if !defined(__nested__) && !defined(xdc_rts_IHeap__aliases) && !defined(xdc_rts_IHeap__nolocalnames)
#define xdc_rts_IHeap__aliases
#define IHeap_Instance xdc_rts_IHeap_Instance
#define IHeap_Module xdc_rts_IHeap_Module
#define IHeap_Params xdc_rts_IHeap_Params
#define IHeap_create xdc_rts_IHeap_create
#define IHeap_delete xdc_rts_IHeap_delete
#define IHeap_Instance__to__Module xdc_rts_IHeap_Instance__to__Module
#define IHeap_alloc xdc_rts_IHeap_alloc
#define IHeap_alloc__fxn xdc_rts_IHeap_alloc__fxn
#define IHeap_alloc__Fxn xdc_rts_IHeap_alloc__Fxn
#define IHeap_free xdc_rts_IHeap_free
#define IHeap_free__fxn xdc_rts_IHeap_free__fxn
#define IHeap_free__Fxn xdc_rts_IHeap_free__Fxn
#define IHeap_getStats xdc_rts_IHeap_getStats
#define IHeap_getStats__fxn xdc_rts_IHeap_getStats__fxn
#define IHeap_getStats__Fxn xdc_rts_IHeap_getStats__Fxn
#endif


/*
 *  @(#) xdc.rts 1, 0, 0, 0,132 1-2-2006 xdc-m52
*/

