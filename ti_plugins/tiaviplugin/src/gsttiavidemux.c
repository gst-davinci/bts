/*
 * gsttiavidemux.c
 *
 * Source file for AVI Demuxer plugin for Gstreamer implementation on 
 * DM644x EVM
 *
 * Copyright (C) 2007 Texas Instruments, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#include <gst/gstutils.h>
#include <gst/riff/riff-media.h>
#include <gst/riff/riff-read.h>
#include <gst/gst-i18n-plugin.h>
#include <string.h>

#include "gstavidemux.h"
#include "AVIParser.h"

//#define PAD_ALLOC
//#define DEBUG_OUT

#define WMTIME_TO_GSTTIME(time) (long long)((time*1000000L))
#define GSTTIME_TO_WMTIME(time) (time/1000000L)


GST_DEBUG_CATEGORY_STATIC(avi_debug);
#define GST_CAT_DEFAULT avi_debug

 STATUS RCallback(AVICONTENTHANDLE aviHandle,
                     BYTE ** pbBuffer,  UINT32 fileOffset,
                     UINT32 cBytes);
 STATUS WCallback(AVICONTENTHANDLE aviHandle,
                     BYTE * pbBuffer,  UINT32 fileOffset,
                     UINT32 cBytes);

FILEOPS fileOpsObj;
static GstStateChangeReturn gst_avi_demux_change_state(GstElement *
                                                       element,
                                                       GstStateChange
                                                       transition);
static void gst_tidemux_avi_loop(GstPad * pad);
static gboolean gst_tidemux_avi_src_event(GstPad * pad, GstEvent * event);
static gboolean gst_tidemux_avi_sink_event(GstPad * pad, GstEvent * event);
GstCaps *gst_tidemux_avi_sink_getcaps(GstPad * pad);
static gboolean gst_tidemux_avi_sink_setcaps(GstPad * pad, GstCaps * caps);
static gboolean gst_tidemux_avi_sink_activate(GstPad * sinkpad);
static gboolean gst_tidemux_avi_sink_activate_pull(GstPad * sinkpad,
                                                   gboolean active);
static gboolean gst_avi_demux_handle_src_query(GstPad * pad,
                                               GstQuery * query);
static const GstQueryType *gst_avi_demux_get_src_query_types(GstPad * pad);

/* elementfactory information */
static GstElementDetails gst_avi_demux_details = {
    "Avi Parser",
    "Parser/Demuxer",
    "Parses/demuxes the avi file",
    "Texas Instruments Inc\n"
};

static GstStaticPadTemplate gst_avi_demux_sink_template =
GST_STATIC_PAD_TEMPLATE("sink",
                        GST_PAD_SINK,
                        GST_PAD_ALWAYS,
                        GST_STATIC_CAPS("video/x-msvideo")
    );

GST_BOILERPLATE(GstTIAVIDemux, gst_tidemux_avi, GstElement,
                GST_TYPE_ELEMENT);

static GstPadTemplate *videosrctempl;
static GstPadTemplate *audiosrctempl;

static void gst_tidemux_avi_base_init(gpointer g_class)
{
    GstElementClass *element_class = GST_ELEMENT_CLASS(g_class);

    GstCaps *audcaps = gst_riff_create_audio_template_caps(),
        *vidcaps = gst_riff_create_video_template_caps();

    audiosrctempl = gst_pad_template_new("audio_%02d",
                                         GST_PAD_SRC, GST_PAD_SOMETIMES,
                                         audcaps);
    videosrctempl =
        gst_pad_template_new("video_%02d", GST_PAD_SRC, GST_PAD_SOMETIMES,
                             vidcaps);

    gst_element_class_add_pad_template(element_class, audiosrctempl);
    gst_element_class_add_pad_template(element_class, videosrctempl);
    gst_element_class_add_pad_template(element_class,
                                       gst_static_pad_template_get
                                       (&gst_avi_demux_sink_template));

    gst_element_class_set_details(element_class, &gst_avi_demux_details);

    GST_DEBUG_CATEGORY_INIT(avi_debug, "tidemux_avi", 0,
                            "ti avi demuxer element");
}

static void gst_tidemux_avi_class_init(GstTIAVIDemuxClass * klass)
{
    GstElementClass *gstelement_class;

    gstelement_class = (GstElementClass *) klass;

    gstelement_class->change_state =
        GST_DEBUG_FUNCPTR(gst_avi_demux_change_state);

}

static void
gst_tidemux_avi_init(GstTIAVIDemux * demux, GstTIAVIDemuxClass * klass)
{
    fileOpsObj.read = RCallback;
    fileOpsObj.write = WCallback;

    demux->sinkpad =
        gst_pad_new_from_template(gst_static_pad_template_get
                                  (&gst_avi_demux_sink_template), "sink");
    gst_pad_set_activate_function(demux->sinkpad,
                                  GST_DEBUG_FUNCPTR
                                  (gst_tidemux_avi_sink_activate));
    gst_pad_set_activatepull_function(demux->sinkpad,
                                      GST_DEBUG_FUNCPTR
                                      (gst_tidemux_avi_sink_activate_pull));
    gst_pad_set_getcaps_function(demux->sinkpad,
                                 gst_tidemux_avi_sink_getcaps);
    gst_pad_set_setcaps_function(demux->sinkpad,
                                 gst_tidemux_avi_sink_setcaps);

    gst_pad_set_event_function(demux->sinkpad,
                               GST_DEBUG_FUNCPTR
                               (gst_tidemux_avi_sink_event));
    gst_element_add_pad(GST_ELEMENT(demux), demux->sinkpad);

    /* We should zero everything to be on the safe side */
    demux->num_audio_streams = 0;
    demux->num_video_streams = 0;
    demux->num_streams = 0;
    demux->state = GST_TIAVI_DEMUX_STATE_HEADER;
}

GstCaps *gst_tidemux_avi_sink_getcaps(GstPad * pad)
{
    return gst_caps_copy(gst_pad_get_pad_template_caps(pad));
}

static gboolean gst_tidemux_avi_sink_setcaps(GstPad * pad, GstCaps * caps)
{
    GstStructure *structure;

    const guchar *mime;
    structure = gst_caps_get_structure(caps, 0);
    mime = gst_structure_get_name(structure);

    if (!strcmp("video/x-msvideo", mime))
        return TRUE;
#ifdef DEBUG_OUT
    printf("mime is not video/x-msvideo \n");
#endif

    //return gst_pad_set_caps(pad, caps);
    return FALSE;
}

static gboolean gst_tidemux_avi_sink_activate(GstPad * sinkpad)
{
#ifdef DEBUG_OUT
    printf("Activate invoked\n");
#endif
    if (gst_pad_check_pull_range(sinkpad)) {
        return gst_pad_activate_pull(sinkpad, TRUE);
    }
    return FALSE;
};

static gboolean gst_tidemux_avi_sink_activate_pull(GstPad * sinkpad,
                                                   gboolean active)
{
#ifdef DEBUG_OUT
    printf("Activate pull invoked: tidemux_avi\n");
#endif
    if (active) {
        /* if we have a scheduler we can start the task */
        gst_pad_start_task(sinkpad, (GstTaskFunction) gst_tidemux_avi_loop,
                           sinkpad);
    } else {
#ifdef DEBUG_OUT
        printf("Stopping loop task\n");
#endif
        gst_pad_stop_task(sinkpad);
    }

    return TRUE;
};


#define BUF_REQ_SIZE 0
gint gFlag;
GstBuffer *gReadbuf, *gWritebuf;
gboolean isEos, endAudio, endVideo;
gint64 audio_pts, video_pts, audio_preroll, video_preroll, frame_duration;
AVIPARSERPBCONTEXTINTERNAL aviPbContextObj;
guint64 invokeCount;
guint audBufActualSize, vidBufActualSize;
guint audCodecId;
 BYTE nAudioStreams, nVideoStreams;
guint audMediaObjOffset, vidMediaObjOffset;

static void gst_tidemux_avi_loop(GstPad * pad)
{
    static
    GstFlowReturn res;
    GstBuffer *audBuf = NULL, *vidBuf = NULL;

    MEDIAOBJMETAINFO mediaObjAudMetaInfo, mediaObjVidMetaInfo;

    FILEOPS fileOpsObj;
    // Audio and Video specific Data 
    MINSTREAMINFO minAudioStreamInfo, minVideoStreamInfo;
    AVIAUDIOSTREAMINFO audioStreamInfo;
    AVIVIDEOSTREAMINFO videoStreamInfo;
    MINSTREAMINFO *minAudioStreamInfoptr, *minVideoStreamInfoptr;
     STATUS retStatus = AVI_SUCCESS;
     UINT32 audBufSize, vidBufSize, prevAudBufSize, prevVidBufSize;
    GstCaps *othercaps;
    minAudioStreamInfoptr = &minAudioStreamInfo;
    minVideoStreamInfoptr = &minVideoStreamInfo;

    GstTIAVIDemux *demux = GST_TIAVI_DEMUX(gst_pad_get_parent(pad));

    invokeCount++;
#ifdef DEBUG_OUT
    printf("Loop invoked... %llu times \n", invokeCount);
#endif
    if (!isEos) {
        fileOpsObj.read = RCallback;
        fileOpsObj.write = WCallback;

        if (!gFlag) {
            // Initialization of AVI parser handle
            retStatus =
                AVIParserContextInit(&aviPbContextObj, demux->sinkpad,
                                     &fileOpsObj);
            gst_segment_init(&demux->segment, GST_FORMAT_TIME);
#ifdef DEBUG_OUT
            if (retStatus != AVI_SUCCESS) {
                printf
                    ("\nsomething wrong with the ContextInit return value: return value = %d\n",
                     retStatus);
                switch (retStatus) {
                case AVI_E_BADAVIHEADER:
                    printf("ContextInit: bad avi header\n");
                    break;
                case AVI_E_INVALIDARG:
                    printf("ContextInit: invalid arg\n");
                    break;
                case AVI_E_BUFINSUFFICIENT:
                    printf("ContextInit: Buf Insufficient\n");
                    break;
                case AVI_E_INVALIDOPERATION:
                    printf("ContextInit: Invalid operation\n");
                    break;
                default:
                    printf("ContextInit: Some other error\n");
                }
            }
#endif

            // Getting the number of video and audio streams in the file
            AVIGetNumStreams(&aviPbContextObj, &nAudioStreams,
                             &nVideoStreams);
#ifdef DEBUG_OUT
            printf("nAudioStreams = %d and nVideostreams = %d \n",
                   nAudioStreams, nVideoStreams);
#endif

        }


        if (!gFlag)             //do only once
        {
            if (nAudioStreams) {

                retStatus = AVIGetStreamMinInfo(&aviPbContextObj, AUDIO,
                                                1, &minAudioStreamInfoptr);
#ifdef DEBUG_OUT
                if (retStatus != AVI_SUCCESS) {
                    printf("\n error in AVIGetStreamMinInfo \n");
                }
#endif

                // Set the active stream
                AVISetActiveStream(&aviPbContextObj, AUDIO,
                                   minAudioStreamInfo.bStreamId);
                AVIGetAudioStreamInfo(&aviPbContextObj,
                                      minAudioStreamInfo.bStreamId,
                                      &audioStreamInfo);
                audCodecId =
                    audioStreamInfo.WMATypeSpecData.WaveFormatEx.
                    wFormatTag;
                if (audCodecId == 0x55) {
                    audMediaObjOffset = 0;
                    audBufActualSize++; //make it positive so that buffer_new_and_alloc is success
                }


                demux->srcpad[0] = NULL;
                demux->srcpad[0] =
                    gst_pad_new_from_template(audiosrctempl, "audio_00");

                gst_pad_set_query_type_function(demux->srcpad[0],
                                                GST_DEBUG_FUNCPTR
                                                (gst_avi_demux_get_src_query_types));

                gst_pad_set_query_function(demux->srcpad[0],
                                           GST_DEBUG_FUNCPTR
                                           (gst_avi_demux_handle_src_query));

                gst_element_add_pad(GST_ELEMENT(demux), demux->srcpad[0]);
                gst_pad_set_event_function(demux->srcpad[0],
                                           gst_tidemux_avi_src_event);

                gst_segment_init(&demux->segment, GST_FORMAT_TIME);
                gst_pad_push_event(demux->srcpad[0],
                                   gst_event_new_new_segment(FALSE,
                                                             demux->
                                                             segment.rate,
                                                             demux->
                                                             segment.
                                                             format,
                                                             demux->
                                                             segment.start,
                                                             demux->
                                                             segment.
                                                             duration,
                                                             demux->
                                                             segment.
                                                             start));


                if (audCodecId == 0x55) {
#ifdef DEBUG_OUT
                    printf
                        ("\n audCodecId = 0x55: setting mp3 caps on audio srcpad \n");
#endif
                    othercaps =
                        gst_caps_new_simple("audio/mpeg",
                                            "mpegversion", G_TYPE_INT, 1,
                                            "layer", G_TYPE_INT, 3, NULL);

                    gst_pad_set_caps(demux->srcpad[0], othercaps);
                    gst_pad_use_fixed_caps(demux->srcpad[0]);
                    gst_caps_unref(othercaps);
                } else {

                    othercaps =
                        gst_caps_new_simple("audio/x-wma",
                                            "wmaversion", G_TYPE_INT, 3,
                                            NULL);

                    gst_pad_set_caps(demux->srcpad[0], othercaps);
                    gst_pad_use_fixed_caps(demux->srcpad[0]);
                    gst_caps_unref(othercaps);

                }
            }

            if (nVideoStreams) {
                AVIGetStreamMinInfo(&aviPbContextObj, VIDEO,
                                    1, &minVideoStreamInfoptr);
                // Set the active stream
                AVISetActiveStream(&aviPbContextObj, VIDEO,
                                   minVideoStreamInfo.bStreamId);
                AVIGetVideoStreamInfo(&aviPbContextObj,
                                      minVideoStreamInfo.bStreamId,
                                      &videoStreamInfo);


                frame_duration =
                    (1000000000.0 * videoStreamInfo.dwScale) /
                    (videoStreamInfo.dwRate);

                //printf("frame duration = %lluns\n", frame_duration);


                vidBufActualSize++;     //make it positive so that buffer_new_and_alloc is success

                demux->srcpad[1] =
                    gst_pad_new_from_template(videosrctempl, "video_00");
                gst_pad_set_query_type_function(demux->srcpad[1],
                                                GST_DEBUG_FUNCPTR
                                                (gst_avi_demux_get_src_query_types));

                gst_pad_set_query_function(demux->srcpad[1],
                                           GST_DEBUG_FUNCPTR
                                           (gst_avi_demux_handle_src_query));

                gst_element_add_pad(GST_ELEMENT(demux), demux->srcpad[1]);

                gst_pad_set_event_function(demux->srcpad[1],
                                           gst_tidemux_avi_src_event);

                othercaps =
                    gst_caps_new_simple("video/mpeg",
                                        "mpegversion", G_TYPE_INT, 4,
                                        "systemstream", G_TYPE_BOOLEAN,
                                        FALSE, NULL);

                gst_pad_set_caps(demux->srcpad[1], othercaps);
                gst_pad_use_fixed_caps(demux->srcpad[1]);
                gst_caps_unref(othercaps);

                gst_pad_push_event(demux->srcpad[1],
                                   gst_event_new_new_segment(FALSE,
                                                             demux->
                                                             segment.rate,
                                                             demux->
                                                             segment.
                                                             format,
                                                             demux->
                                                             segment.start,
                                                             demux->
                                                             segment.
                                                             duration,
                                                             demux->
                                                             segment.
                                                             start));

            }
        }
        // parse AVI file for metadata and output the same

        if (nAudioStreams && !endAudio && !(invokeCount & 0x1)) {
#ifdef PAD_ALLOC
            res =
                gst_pad_alloc_buffer(demux->srcpad[0],
                                     GST_BUFFER_OFFSET_NONE,
                                     audMediaObjOffset + audBufActualSize,
                                     GST_PAD_CAPS(demux->srcpad[0]),
                                     &audBuf);
            if (res != GST_FLOW_OK) {
#ifdef DEBUG_OUT
                printf
                    ("failed when allocating audio buffer from the audio src pad\n");
#endif
            }
#else
            /* Using Gst_Buffer_new_alloc instead of pad-alloc */

            audBuf =
                gst_buffer_new_and_alloc(audMediaObjOffset +
                                         audBufActualSize);
#endif
            audBufSize = GST_BUFFER_SIZE(audBuf) - audMediaObjOffset;
            prevAudBufSize = audBufSize;
            retStatus =
                AVIGetNextMediaObj(&aviPbContextObj, AUDIO,
                                   GST_BUFFER_DATA(audBuf), &audBufSize,
                                   audMediaObjOffset,
                                   &mediaObjAudMetaInfo);
            //audio_pts = mediaObjAudMetaInfo.dwMediaObjPresTime;
            if (retStatus != AVI_SUCCESS) {
                gst_buffer_unref(audBuf);       //free the old Gst buffer     
                if (retStatus == AVI_E_BUFINSUFFICIENT) {
#ifdef DEBUG_OUT
                    printf
                        ("AVI_E_BUFINSUFFICIENT : bufsize_given =%d and mediaobjsize = %d  \n",
                         audBufActualSize,
                         mediaObjAudMetaInfo.dwMediaObjSize);
#endif

                    audBufActualSize = mediaObjAudMetaInfo.dwMediaObjSize;


#ifdef PAD_ALLOC
                    res = gst_pad_alloc_buffer(demux->srcpad[0], GST_BUFFER_OFFSET_NONE, audMediaObjOffset + audBufActualSize, GST_PAD_CAPS(demux->srcpad[0]), &audBuf);        //reallocate the Gst Buffer
                    if (res != GST_FLOW_OK) {
#ifdef DEBUG_OUT
                        printf
                            ("failed when allocating audio buffer from the audio src pad\n");
#endif
                    }
#else
                    audBuf =
                        gst_buffer_new_and_alloc(audMediaObjOffset +
                                                 audBufActualSize);

#endif

                    audBufSize =
                        GST_BUFFER_SIZE(audBuf) - audMediaObjOffset;
                    prevAudBufSize = audBufSize;
                    retStatus =
                        AVIGetNextMediaObj(&aviPbContextObj, AUDIO,
                                           GST_BUFFER_DATA(audBuf),
                                           &audBufSize, audMediaObjOffset,
                                           &mediaObjAudMetaInfo);
                    //audio_pts = mediaObjAudMetaInfo.dwMediaObjPresTime;
                    if (retStatus != AVI_SUCCESS) {
                        gst_buffer_unref(audBuf);       //free the old Gst buffer         
#ifdef DEBUG_OUT
                        printf
                            ("AVI_E_BUFINSUFFICIENT 2nd time : bufsize_given =%d and mediaobjsize = %d  \n",
                             audBufSize,
                             mediaObjAudMetaInfo.dwMediaObjSize);
#endif
                    }


                } else {
                    if (retStatus == AVI_E_ENDOFFILE) {
#ifdef DEBUG_OUT
                        printf("AUDIO AVI_E_ENDOFFILE received\n");
#endif
                        endAudio = TRUE;
                        if (endAudio) {
                            gst_pad_push_event(demux->srcpad[0], gst_event_new_eos());  //send EOS if some other error also
                            if (!nVideoStreams) {
                                isEos = TRUE;
                                goto end;
                            }
                            if (endVideo) {
                                gst_pad_push_event(demux->srcpad[1], gst_event_new_eos());      //send EOS if some other error also
                                isEos = TRUE;
                                goto end;
                            }
                        }
                    } else {
#ifdef DEBUG_OUT
                        printf
                            ("Some other getnextMediaObj error received = %x\n",
                             retStatus);
#endif
                        endAudio = TRUE;
                        if (endAudio) {
                            gst_pad_push_event(demux->srcpad[0], gst_event_new_eos());  //send EOS if some other error also
                            if (!nVideoStreams) {
                                isEos = TRUE;
                                goto end;
                            }

                            if (endVideo) {
                                gst_pad_push_event(demux->srcpad[1], gst_event_new_eos());      //send EOS if some other error also
                                isEos = TRUE;
                                goto end;
                            }
                        }
                    }
                }
            }
            if (retStatus == AVI_SUCCESS) {
#ifdef DEBUG_OUT
                printf(" Parsed audio media object num = %d pts = %ld\n",
                       mediaObjAudMetaInfo.dwMediaObjNum,
                       mediaObjAudMetaInfo.dwMediaObjPresTime);
#endif
                //audio_pts = mediaObjAudMetaInfo.dwMediaObjPresTime;


                if (prevAudBufSize > mediaObjAudMetaInfo.dwMediaObjSize) {
                    GST_BUFFER_SIZE(audBuf) -=
                        (prevAudBufSize - audBufSize);
                }
                if (!gFlag)
                    audio_preroll =
                        WMTIME_TO_GSTTIME(audio_pts) - 100000000L;
                GST_BUFFER_TIMESTAMP(audBuf) = WMTIME_TO_GSTTIME(audio_pts) - audio_preroll;    /*update the timestamp of the Gst Buffer 
                                                                                                   from the Media Object Info */

                if (audCodecId == 0x55) {
                    othercaps =
                        gst_caps_new_simple("audio/mpeg",
                                            "mpegversion", G_TYPE_INT, 1,
                                            "layer", G_TYPE_INT, 3, NULL);

                    gst_buffer_set_caps(audBuf, othercaps);
                } else {

                    othercaps =
                        gst_caps_new_simple("audio/x-wma",
                                            "wmaversion", G_TYPE_INT, 3,
                                            NULL);

                    gst_buffer_set_caps(audBuf, othercaps);
                }
                res = gst_pad_push(demux->srcpad[0], audBuf);   //push the audio buffer with RCA header to audio src pad
                if (res != GST_FLOW_OK) {
                    endAudio = TRUE;
#ifdef DEBUG_OUT
                    printf(" Audio Pad push failed\n");
#endif
                    switch (res) {
                    case GST_FLOW_NOT_LINKED:
                        nAudioStreams = 0;
#ifdef DEBUG_OUT
                        printf
                            ("\nAudio pad push fail: src pad not linked\n");
#endif
                        break;
                    case GST_FLOW_NOT_NEGOTIATED:
#ifdef DEBUG_OUT
                        printf
                            ("\nAudio pad push fail: caps not negotiated\n");
#endif
                        break;
                    case GST_FLOW_ERROR:
#ifdef DEBUG_OUT
                        printf("\nAudio pad push fail: GST_FLOW_ERROR\n");
#endif
                        break;
                    default:
#ifdef DEBUG_OUT
                        printf
                            ("\nAudio pad push fail: some other error\n");
#endif
                        break;
                    }
                    if (endAudio) {
                        gst_pad_push_event(demux->srcpad[0], gst_event_new_eos());      //send EOS if some other error also
                        if (!nVideoStreams) {
                            isEos = TRUE;
                            goto end;
                        }

                        if (endVideo) {
                            gst_pad_push_event(demux->srcpad[1], gst_event_new_eos());  //send EOS if some other error also
                            isEos = TRUE;
                            goto end;
                        }
                    }
                } else {
#ifdef DEBUG_OUT
                    printf(" Audio Pad push success\n");
#endif
                }
                gst_caps_unref(othercaps);


            }

        }                       //if(nAudioStreams)

        if (nVideoStreams && !endVideo && (invokeCount & 0x1)) {
#ifdef PAD_ALLOC
            res =
                gst_pad_alloc_buffer(demux->srcpad[1],
                                     GST_BUFFER_OFFSET_NONE,
                                     vidMediaObjOffset + vidBufActualSize,
                                     GST_PAD_CAPS(demux->srcpad[1]),
                                     &vidBuf);
            if (res != GST_FLOW_OK) {
#ifdef DEBUG_OUT
                printf
                    ("failed when allocating video buffer from the video src pad\n");
#endif
            }
#else
            vidBuf =
                gst_buffer_new_and_alloc(vidMediaObjOffset +
                                         vidBufActualSize);
#endif
            vidBufSize = GST_BUFFER_SIZE(vidBuf) - vidMediaObjOffset;
            prevVidBufSize = vidBufSize;
            retStatus =
                AVIGetNextMediaObj(&aviPbContextObj, VIDEO,
                                   GST_BUFFER_DATA(vidBuf), &vidBufSize,
                                   vidMediaObjOffset,
                                   &mediaObjVidMetaInfo);
            if (retStatus != AVI_SUCCESS) {
                gst_buffer_unref(vidBuf);       //free the old Gst buffer     
                if (retStatus == AVI_E_BUFINSUFFICIENT) {
                    vidBufActualSize = mediaObjVidMetaInfo.dwMediaObjSize;

#ifdef PAD_ALLOC
                    res = gst_pad_alloc_buffer(demux->srcpad[1], GST_BUFFER_OFFSET_NONE, vidMediaObjOffset + vidBufActualSize, GST_PAD_CAPS(demux->srcpad[1]), &vidBuf);        //reallocate the Gst Buffer
                    if (res != GST_FLOW_OK) {
#ifdef DEBUG_OUT
                        printf
                            ("failed when allocating video buffer from the video src pad\n");
#endif
                    }
#else

                    vidBuf =
                        gst_buffer_new_and_alloc(vidMediaObjOffset +
                                                 vidBufActualSize);
#endif
                    vidBufSize =
                        GST_BUFFER_SIZE(vidBuf) - vidMediaObjOffset;
                    prevVidBufSize = vidBufSize;
                    retStatus =
                        AVIGetNextMediaObj(&aviPbContextObj, VIDEO,
                                           GST_BUFFER_DATA(vidBuf),
                                           &vidBufSize, vidMediaObjOffset,
                                           &mediaObjVidMetaInfo);
                    if (retStatus != AVI_SUCCESS) {
                        gst_buffer_unref(vidBuf);       //free the old Gst buffer 
#ifdef DEBUG_OUT
                        printf
                            ("AVI_E_BUFINSUFFICIENT 2nd time : bufsize_given =%d and mediaobjsize = %d  \n",
                             audBufSize,
                             mediaObjAudMetaInfo.dwMediaObjSize);
#endif
                    }
                } else {
                    if (retStatus == AVI_E_ENDOFFILE) {
#ifdef DEBUG_OUT
                        printf("VIDEO AVI_E_ENDOFFILE received\n");
#endif
                        endVideo = TRUE;
                        if (endVideo) {
                            gst_pad_push_event(demux->srcpad[1], gst_event_new_eos());  //send EOS if some other error also
                            if (!nAudioStreams) {
                                isEos = TRUE;
                                goto end;
                            }

                            if (endAudio) {
                                gst_pad_push_event(demux->srcpad[0], gst_event_new_eos());      //send EOS if some other error also
                                isEos = TRUE;
                                goto end;
                            }
                        }
                    } else {
#ifdef DEBUG_OUT
                        printf
                            ("Some other getnextMediaObj error received =%x\n",
                             retStatus);
#endif
                        endVideo = TRUE;
                        if (endVideo) {
                            gst_pad_push_event(demux->srcpad[1], gst_event_new_eos());  //send EOS if some other error also
                            if (!nAudioStreams) {
                                isEos = TRUE;
                                goto end;
                            }

                            if (endAudio) {
                                gst_pad_push_event(demux->srcpad[0], gst_event_new_eos());      //send EOS if some other error also
                                isEos = TRUE;
                                goto end;
                            }
                        }
                    }
                }
            }
            if (retStatus == AVI_SUCCESS) {
#ifdef DEBUG_OUT
                printf(" Parsed video media object num = %d, pts = %ld\n",
                       mediaObjVidMetaInfo.dwMediaObjNum,
                       mediaObjVidMetaInfo.dwMediaObjPresTime);
#endif

                prevVidBufSize = vidBufSize;
                if (prevVidBufSize > mediaObjVidMetaInfo.dwMediaObjSize) {

                    GST_BUFFER_SIZE(vidBuf) -=
                        (prevVidBufSize - vidBufSize);
                }
                if (!gFlag)
                    video_preroll =
                        WMTIME_TO_GSTTIME(video_pts) - 100000000L;
                GST_BUFFER_TIMESTAMP(vidBuf) = video_pts;
                video_pts += frame_duration;
                /*printf ("video pts = %llu\n", GST_BUFFER_TIMESTAMP(vidBuf)); */

                othercaps =
                    gst_caps_new_simple("video/mpeg",
                                        "mpegversion", G_TYPE_INT, 4,
                                        "systemstream", G_TYPE_BOOLEAN,
                                        FALSE, NULL);
                gst_buffer_set_caps(vidBuf, othercaps);

                res = gst_pad_push(demux->srcpad[1], vidBuf);   //push the video buffer with RCV header to video src pad
                if (res != GST_FLOW_OK) {
                    endVideo = TRUE;
#ifdef DEBUG_OUT
                    printf("Video Pad push failed\n");
#endif
                    switch (res) {
                    case GST_FLOW_NOT_LINKED:
                        nVideoStreams = 0;
#ifdef DEBUG_OUT
                        printf
                            ("\n Video pad push fail: src pad not linked\n");
#endif
                        break;
                    case GST_FLOW_NOT_NEGOTIATED:
#ifdef DEBUG_OUT
                        printf
                            ("\nVideo pad push fail: caps not negotiated\n");
#endif
                        break;
                    case GST_FLOW_ERROR:
#ifdef DEBUG_OUT
                        printf("\nVideo pad push fail: GST_FLOW_ERROR\n");
#endif
                        break;
                    default:
#ifdef DEBUG_OUT
                        printf
                            ("\nVideo pad push fail: some other error\n");
#endif
                        break;
                    }
                    if (endVideo) {
                        gst_pad_push_event(demux->srcpad[1], gst_event_new_eos());      //send EOS if some other error also
                        if (!nAudioStreams) {
                            isEos = TRUE;
                            goto end;
                        }

                        if (endAudio) {
                            gst_pad_push_event(demux->srcpad[0], gst_event_new_eos());  //send EOS if some other error also
                            isEos = TRUE;
                            goto end;
                        }
                    }
                } else {
#ifdef DEBUG_OUT
                    printf("Video Pad push success\n");
#endif
                }
                gst_caps_unref(othercaps);
            }                   //if(retStatus == AVI_SUCCESS)

        }                       //if(nVideoStreams)

        gFlag = 1;              //update the flag for the next audio/video media objects following the first one
    } else {
#ifdef DEBUG_OUT
        printf("pausing avi demux loop\n");
#endif
#if 0
        if (nAudioStreams)
            gst_pad_push_event(demux->srcpad[0], gst_event_new_eos());
        if (nVideoStreams)
            gst_pad_push_event(demux->srcpad[1], gst_event_new_eos());
#endif
        gst_pad_pause_task(pad);


    }
  end:
    gst_object_unref(demux);
}

#define MAX_STREAMS 2
static GstStateChangeReturn gst_avi_demux_change_state(GstElement *
                                                       element,
                                                       GstStateChange
                                                       transition)
{
    GstTIAVIDemux *demux = GST_TIAVI_DEMUX(element);
    GstStateChangeReturn ret = GST_STATE_CHANGE_SUCCESS;
    int cnt;
    switch (transition) {
    case GST_STATE_CHANGE_READY_TO_PAUSED:{
            gst_segment_init(&demux->segment, GST_FORMAT_TIME);
            gFlag = 0;
            isEos = FALSE;
            video_pts = 0;
            audio_pts = 0;
            invokeCount = 0;
            endAudio = FALSE;
            endVideo = FALSE;
            gReadbuf = NULL;
            gWritebuf = NULL;
            audBufActualSize = BUF_REQ_SIZE;
            vidBufActualSize = BUF_REQ_SIZE;
            audMediaObjOffset = 0;
            vidMediaObjOffset = 0;
            break;
        }
    default:
        break;
    }

    ret =
        GST_ELEMENT_CLASS(parent_class)->change_state(element, transition);
    if (ret == GST_STATE_CHANGE_FAILURE)
        return ret;

    switch (transition) {

    case GST_STATE_CHANGE_PLAYING_TO_PAUSED:{
#ifdef DEBUG_OUT
            printf("\ntidemux_avi: change state: playing to paused \n");
#endif
            break;
        }

    case GST_STATE_CHANGE_PAUSED_TO_READY:{

#ifdef DEBUG_OUT
            printf("\ntidemux_avi: change state: paused to ready \n");
#endif
            for (cnt = 0; cnt < MAX_STREAMS; cnt++) {
                if (demux->srcpad[cnt]) {
                    gst_element_remove_pad(GST_ELEMENT(demux),
                                           demux->srcpad[cnt]);
                    demux->srcpad[cnt] = NULL;
                }
            }


            gst_segment_init(&demux->segment, GST_FORMAT_UNDEFINED);
            demux->state = GST_TIAVI_DEMUX_STATE_HEADER;
            if (gReadbuf != NULL) {
                gst_buffer_unref(gReadbuf);     //free the Gst buffer pulled in RCallback
                gReadbuf = NULL;
            }
            if (gWritebuf != NULL) {
                gst_buffer_unref(gWritebuf);    //free the Gst buffer pulled in RCallback
                gWritebuf = NULL;
            }
            break;
        }
    case GST_STATE_CHANGE_READY_TO_NULL:
#ifdef DEBUG_OUT
        printf("\ntidemux_avi:  state change : ready to null\n");
        break;
#endif
    default:
        break;
    }

    return ret;
}

static gboolean gst_tidemux_avi_sink_event(GstPad * pad, GstEvent * event)
{

    GstTIAVIDemux *demux = GST_TIAVI_DEMUX(GST_PAD_PARENT(pad));
    gboolean ret = TRUE;

    GST_DEBUG_OBJECT(demux, "Got %s event on sink pad",
                     GST_EVENT_TYPE_NAME(event));
#ifdef DEBUG_OUT
    printf("tiavi: Got\" %s\" event on sink pad\n",
           GST_EVENT_TYPE_NAME(event));
#endif
    switch (GST_EVENT_TYPE(event)) {
    case GST_EVENT_NEWSEGMENT:
        {
            ret = gst_pad_event_default(pad, event);
            break;
        }
    case GST_EVENT_FLUSH_START:
        ret = gst_pad_event_default(pad, event);
        break;
    case GST_EVENT_FLUSH_STOP:
        ret = gst_pad_event_default(pad, event);
        break;
    case GST_EVENT_EOS:
        isEos = TRUE;
        ret = gst_pad_event_default(pad, event);
        break;
    default:
        ret = gst_pad_event_default(pad, event);
        break;
    }
    return ret;
}

static gboolean gst_tidemux_avi_src_event(GstPad * pad, GstEvent * event)
{
    GstTIAVIDemux *demux = GST_TIAVI_DEMUX(GST_PAD_PARENT(pad));
    gboolean ret = TRUE;

    GST_DEBUG_OBJECT(demux, "Got %s event on src pad",
                     GST_EVENT_TYPE_NAME(event));

    ret = gst_pad_push_event(demux->sinkpad, event);

    return ret;
}

 STATUS RCallback(AVICONTENTHANDLE aviHandle,
                     BYTE ** pbBuffer,  UINT32 fileOffset,
                     UINT32 cBytes)
{
    GstFlowReturn res;

    if (gReadbuf != NULL) {
        gst_buffer_unref(gReadbuf);     //free the Gst buffer pulled in RCallback
        gReadbuf = NULL;
    }
    if ((res = gst_pad_pull_range(aviHandle,
                                  fileOffset,
                                  cBytes, &gReadbuf)) != GST_FLOW_OK) {
#ifdef DEBUG_OUT
        printf("EOF reached\n");
#endif
        return -1;
    }

    *pbBuffer = GST_BUFFER_DATA(gReadbuf);
    if (cBytes != GST_BUFFER_SIZE(gReadbuf)) {
#ifdef DEBUG_OUT
        printf("bytes requested not equal to bytes actually read\n");
#endif
        return -1;
    }

    return cBytes;
}

 STATUS WCallback(AVICONTENTHANDLE aviHandle,
                     BYTE * pbBuffer,  UINT32 fileOffset,
                     UINT32 cBytes)
{
    GstFlowReturn res;

    if (gWritebuf != NULL) {
        gst_buffer_unref(gWritebuf);    //free the Gst buffer pulled in RCallback
        gWritebuf = NULL;
    }

    if ((res = gst_pad_pull_range(aviHandle,
                                  fileOffset,
                                  cBytes, &gWritebuf)) != GST_FLOW_OK) {
        return -1;
    }

    memcpy(pbBuffer, GST_BUFFER_DATA(gWritebuf),
           GST_BUFFER_SIZE(gWritebuf));
    if (cBytes != GST_BUFFER_SIZE(gWritebuf)) {
#ifdef DEBUG_OUT
        printf
            ("WCallback: bytes requested not equal to bytes actually read\n");
#endif
        return -1;
    }

    return cBytes;
}

static const GstQueryType *gst_avi_demux_get_src_query_types(GstPad * pad)
{
    static const GstQueryType types[] = {
        GST_QUERY_DURATION,
        0
    };

    return types;
}

static gboolean
gst_avi_demux_handle_src_query(GstPad * pad, GstQuery * query)
{
    GstTIAVIDemux *demux;
    gboolean res = FALSE;
    guint64 playDuration;

    demux = GST_TIAVI_DEMUX(gst_pad_get_parent(pad));

    GST_DEBUG("handling %s query",
              gst_query_type_get_name(GST_QUERY_TYPE(query)));

    switch (GST_QUERY_TYPE(query)) {
    case GST_QUERY_DURATION:
        {
            GstFormat format;

            gst_query_parse_duration(query, &format, NULL);

            if (format != GST_FORMAT_TIME) {
                GST_LOG("only support duration queries in TIME format");
                break;
            }
            //GST_OBJECT_LOCK(demux);
            //printf ("duration = %llu\n", aviPbContextObj.qwDuration);
            playDuration = (guint64) (aviPbContextObj.qwDuration * 1000LL);
            gst_query_set_duration(query, GST_FORMAT_TIME, playDuration);

            res = TRUE;

            //GST_OBJECT_UNLOCK(demux);
            break;
        }

    default:
        res = gst_pad_query_default(pad, query);
        break;
    }

    gst_object_unref(demux);
    return res;
}
