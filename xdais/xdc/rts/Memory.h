/*
 *  Copyright 2006 by Texas Instruments Incorporated.
 *
 *  All rights reserved. Property of Texas Instruments Incorporated.
 *  Restricted rights to use, duplicate or disclose this code are
 *  granted through contract.
 *
 */

/*
 *  ======== xdc/rts/Memory.h ========
 *
 *  DO NOT MODIFY THIS FILE
 *
 *  It is generated from xdc/rts/Memory.xdc and any changes may be overwritten.
 */

#ifdef xdc_rts_Memory___EXTERNAL_SYNOPSIS
#error do not compile!!


    #include <xdc/rts/Memory.h>

    /* -------- enums -------- */

    enum Memory_Policy {
        Memory_STATIC_POLICY,
        Memory_CREATE_POLICY,
        Memory_DELETE_POLICY
    };

    /* -------- typedefs -------- */

    typedef Arg Memory_Size;

    /* -------- structs -------- */

    struct Memory_Stats {
        Memory_Size totalSize;
        Memory_Size totalFreeSize;
        Memory_Size largestFreeSize;
        UInt curNumAllocated;
        UInt totalNumAllocated;
    };

    /* -------- module-wide configs -------- */

    xdc_rts_IHeap_Instance Memory_defaultHeapInstance();

    /* -------- module-wide functions -------- */

    Ptr Memory_alloc( xdc_rts_IHeap_Instance heap, SizeT size, SizeT align, xdc_rts_Error_Block* eb );
    Ptr Memory_calloc( xdc_rts_IHeap_Instance heap, SizeT size, SizeT align, xdc_rts_Error_Block* eb );
    Void Memory_free( xdc_rts_IHeap_Instance heap, Ptr block, SizeT size );
    Void Memory_getStats( xdc_rts_IHeap_Instance heap, Memory_Stats* stats );
    Ptr Memory_valloc( xdc_rts_IHeap_Instance heap, SizeT size, SizeT align, Char value, xdc_rts_Error_Block* eb );


#endif /* xdc_rts_Memory___EXTERNAL_SYNOPSIS */



#ifdef xdc_rts_Memory___INTERNAL_SYNOPSIS
#error do not compile!!


    #include "package/internal/Memory.xdc.h"

    /* -------- module-wide state -------- */

    struct Memory_Module__Object {
        xdc_rts_IHeap_Instance *heapTab;
    };

    extern Memory_Module__Object xdc_rts_Memory_module;
    static Memory_Module__Object* const module = &xdc_rts_Memory_module;


#endif /* xdc_rts_Memory___INTERNAL_SYNOPSIS */







#ifdef __cplusplus
#define __extern extern "C"
#else
#define __extern extern
#endif

#ifndef xdc_std__include
#ifndef __nested__
#define __nested__
#include <xdc/std.h>
#undef __nested__
#else
#include <xdc/std.h>
#endif
#endif

#ifndef xdc_rts_Memory__include
#define xdc_rts_Memory__include

#ifndef xdc_rts__
#include <xdc/rts/package/package.defs.h>
#endif

#ifndef xdc_rts_IHeap__include
#ifndef __nested__
#define __nested__
#include <xdc/rts/IHeap.h>
#undef __nested__
#else
#include <xdc/rts/IHeap.h>
#endif
#endif

#ifndef xdc_rts_IModule__include
#ifndef __nested__
#define __nested__
#include <xdc/rts/IModule.h>
#undef __nested__
#else
#include <xdc/rts/IModule.h>
#endif
#endif

#ifndef xdc_rts_Error__include
#ifndef __nested__
#define __nested__
#include <xdc/rts/Error.h>
#undef __nested__
#else
#include <xdc/rts/Error.h>
#endif
#endif


/* auxiliary definitions */
enum xdc_rts_Memory_Policy {
    xdc_rts_Memory_STATIC_POLICY,
    xdc_rts_Memory_CREATE_POLICY,
    xdc_rts_Memory_DELETE_POLICY
};
typedef enum xdc_rts_Memory_Policy xdc_rts_Memory_Policy;
typedef xdc_Arg xdc_rts_Memory_Size;
struct xdc_rts_Memory_Stats {
    xdc_rts_Memory_Size totalSize;
    xdc_rts_Memory_Size totalFreeSize;
    xdc_rts_Memory_Size largestFreeSize;
    xdc_UInt curNumAllocated;
    xdc_UInt totalNumAllocated;
};

/* internal definitions */
typedef xdc_rts_IHeap_Instance __T1_xdc_rts_Memory_Module__Object__heapTab;
typedef xdc_rts_IHeap_Instance *__ARRAY1_xdc_rts_Memory_Module__Object__heapTab;
typedef __ARRAY1_xdc_rts_Memory_Module__Object__heapTab __TA_xdc_rts_Memory_Module__Object__heapTab;
struct xdc_rts_Memory_Module__Object {
    __TA_xdc_rts_Memory_Module__Object__heapTab heapTab;
};
extern xdc_rts_Memory_Module__Object xdc_rts_Memory__module__Object;

/* module-wide configs */
struct xdc_rts_Memory_Config {
    int __size;
    xdc_rts_IHeap_Instance defaultHeapInstance;
};

__extern const xdc_rts_Memory_Config xdc_rts_Memory_CONFIG;

#define xdc_rts_Memory_defaultHeapInstance() (xdc_rts_Memory_CONFIG.defaultHeapInstance)

/* function declarations */
#ifdef xdc_rts_Memory_alloc__inline
#define xdc_rts_Memory_alloc xdc_rts_Memory_alloc__F
static inline xdc_Ptr xdc_rts_Memory_alloc__F( xdc_rts_IHeap_Instance heap, xdc_SizeT size, xdc_SizeT align, xdc_rts_Error_Block* eb );
#elif !defined(xdc_rts_Memory_alloc__macro)
#define xdc_rts_Memory_alloc xdc_rts_Memory_alloc__E
__extern xdc_Ptr xdc_rts_Memory_alloc__E( xdc_rts_IHeap_Instance heap, xdc_SizeT size, xdc_SizeT align, xdc_rts_Error_Block* eb );
__extern xdc_Ptr xdc_rts_Memory_alloc__F( xdc_rts_IHeap_Instance heap, xdc_SizeT size, xdc_SizeT align, xdc_rts_Error_Block* eb );
#endif
#ifdef xdc_rts_Memory_calloc__inline
#define xdc_rts_Memory_calloc xdc_rts_Memory_calloc__F
static inline xdc_Ptr xdc_rts_Memory_calloc__F( xdc_rts_IHeap_Instance heap, xdc_SizeT size, xdc_SizeT align, xdc_rts_Error_Block* eb );
#elif !defined(xdc_rts_Memory_calloc__macro)
#define xdc_rts_Memory_calloc xdc_rts_Memory_calloc__E
__extern xdc_Ptr xdc_rts_Memory_calloc__E( xdc_rts_IHeap_Instance heap, xdc_SizeT size, xdc_SizeT align, xdc_rts_Error_Block* eb );
__extern xdc_Ptr xdc_rts_Memory_calloc__F( xdc_rts_IHeap_Instance heap, xdc_SizeT size, xdc_SizeT align, xdc_rts_Error_Block* eb );
#endif
#ifdef xdc_rts_Memory_free__inline
#define xdc_rts_Memory_free xdc_rts_Memory_free__F
static inline xdc_Void xdc_rts_Memory_free__F( xdc_rts_IHeap_Instance heap, xdc_Ptr block, xdc_SizeT size );
#elif !defined(xdc_rts_Memory_free__macro)
#define xdc_rts_Memory_free xdc_rts_Memory_free__E
__extern xdc_Void xdc_rts_Memory_free__E( xdc_rts_IHeap_Instance heap, xdc_Ptr block, xdc_SizeT size );
__extern xdc_Void xdc_rts_Memory_free__F( xdc_rts_IHeap_Instance heap, xdc_Ptr block, xdc_SizeT size );
#endif
#ifdef xdc_rts_Memory_getStats__inline
#define xdc_rts_Memory_getStats xdc_rts_Memory_getStats__F
static inline xdc_Void xdc_rts_Memory_getStats__F( xdc_rts_IHeap_Instance heap, xdc_rts_Memory_Stats* stats );
#elif !defined(xdc_rts_Memory_getStats__macro)
#define xdc_rts_Memory_getStats xdc_rts_Memory_getStats__E
__extern xdc_Void xdc_rts_Memory_getStats__E( xdc_rts_IHeap_Instance heap, xdc_rts_Memory_Stats* stats );
__extern xdc_Void xdc_rts_Memory_getStats__F( xdc_rts_IHeap_Instance heap, xdc_rts_Memory_Stats* stats );
#endif
#ifdef xdc_rts_Memory_valloc__inline
#define xdc_rts_Memory_valloc xdc_rts_Memory_valloc__F
static inline xdc_Ptr xdc_rts_Memory_valloc__F( xdc_rts_IHeap_Instance heap, xdc_SizeT size, xdc_SizeT align, xdc_Char value, xdc_rts_Error_Block* eb );
#elif !defined(xdc_rts_Memory_valloc__macro)
#define xdc_rts_Memory_valloc xdc_rts_Memory_valloc__E
__extern xdc_Ptr xdc_rts_Memory_valloc__E( xdc_rts_IHeap_Instance heap, xdc_SizeT size, xdc_SizeT align, xdc_Char value, xdc_rts_Error_Block* eb );
__extern xdc_Ptr xdc_rts_Memory_valloc__F( xdc_rts_IHeap_Instance heap, xdc_SizeT size, xdc_SizeT align, xdc_Char value, xdc_rts_Error_Block* eb );
#endif

/* C++ wrappers */
#ifdef __cplusplus
namespace xdc_rts { namespace Memory {
    typedef xdc_rts_Memory_Policy Policy;
    static const Policy STATIC_POLICY = xdc_rts_Memory_STATIC_POLICY;
    static const Policy CREATE_POLICY = xdc_rts_Memory_CREATE_POLICY;
    static const Policy DELETE_POLICY = xdc_rts_Memory_DELETE_POLICY;
    typedef xdc_rts_Memory_Size Size;
    typedef xdc_rts_Memory_Stats Stats;
    typedef xdc_rts_Memory_Module__Object Module__Object;
    static inline xdc_Ptr alloc( xdc_rts_IHeap_Instance heap, xdc_SizeT size, xdc_SizeT align, xdc_rts_Error_Block* eb) { return xdc_rts_Memory_alloc(heap, size, align, eb); }
    static inline xdc_Ptr calloc( xdc_rts_IHeap_Instance heap, xdc_SizeT size, xdc_SizeT align, xdc_rts_Error_Block* eb) { return xdc_rts_Memory_calloc(heap, size, align, eb); }
    static inline xdc_Void free( xdc_rts_IHeap_Instance heap, xdc_Ptr block, xdc_SizeT size) { xdc_rts_Memory_free(heap, block, size); }
    static inline xdc_Void getStats( xdc_rts_IHeap_Instance heap, xdc_rts_Memory_Stats* stats) { xdc_rts_Memory_getStats(heap, stats); }
    static inline xdc_Ptr valloc( xdc_rts_IHeap_Instance heap, xdc_SizeT size, xdc_SizeT align, xdc_Char value, xdc_rts_Error_Block* eb) { return xdc_rts_Memory_valloc(heap, size, align, value, eb); }
}}
using namespace xdc_rts;
#endif

#endif /* xdc_rts_Memory__include */

/* __prefix alises */
#if !defined(__nested__) && !defined(xdc_rts_Memory__aliases) && !defined(xdc_rts_Memory__nolocalnames)
#define xdc_rts_Memory__aliases
#define Memory_Policy xdc_rts_Memory_Policy
#define Memory_Size xdc_rts_Memory_Size
#define Memory_Stats xdc_rts_Memory_Stats
#define Memory_Module__Object xdc_rts_Memory_Module__Object
#define Memory_STATIC_POLICY xdc_rts_Memory_STATIC_POLICY
#define Memory_CREATE_POLICY xdc_rts_Memory_CREATE_POLICY
#define Memory_DELETE_POLICY xdc_rts_Memory_DELETE_POLICY
#define Memory_defaultHeapInstance xdc_rts_Memory_defaultHeapInstance
#define Memory_Config xdc_rts_Memory_Config
#define Memory_CONFIG xdc_rts_Memory_CONFIG
#define Memory_alloc xdc_rts_Memory_alloc
#define Memory_calloc xdc_rts_Memory_calloc
#define Memory_free xdc_rts_Memory_free
#define Memory_getStats xdc_rts_Memory_getStats
#define Memory_valloc xdc_rts_Memory_valloc
#define xdc_Mem_Policy xdc_rts_Memory_Policy
#define xdc_Mem_Size xdc_rts_Memory_Size
#define xdc_Mem_Stats xdc_rts_Memory_Stats
#define xdc_Mem_Module__Object xdc_rts_Memory_Module__Object
#define xdc_Mem_STATIC_POLICY xdc_rts_Memory_STATIC_POLICY
#define xdc_Mem_CREATE_POLICY xdc_rts_Memory_CREATE_POLICY
#define xdc_Mem_DELETE_POLICY xdc_rts_Memory_DELETE_POLICY
#define xdc_Mem_defaultHeapInstance xdc_rts_Memory_defaultHeapInstance
#define xdc_Mem_Config xdc_rts_Memory_Config
#define xdc_Mem_CONFIG xdc_rts_Memory_CONFIG
#define xdc_Mem_alloc xdc_rts_Memory_alloc
#define xdc_Mem_calloc xdc_rts_Memory_calloc
#define xdc_Mem_free xdc_rts_Memory_free
#define xdc_Mem_getStats xdc_rts_Memory_getStats
#define xdc_Mem_valloc xdc_rts_Memory_valloc
#endif


/*
 *  @(#) xdc.rts 1, 0, 0, 0,132 1-2-2006 xdc-m52
*/

