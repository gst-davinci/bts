# MVISTA_INSTALL_DIR should point to your Montavista directory installation
export MVISTA_INSTALL_DIR=/opt/dvevm/mv_pro_4.0/montavista

#FILESYS_DIR should point to NFS shared target root directory
export FILESYS_DIR=/home/user/workdir/filesys

#Location of DVEVM_PATH to point to DVEVM directory
export DVEVM_PATH=/home/user/dvevm_1_20

#Location of the davinci kernel
export KERNEL_SRC=/home/user/linux-davinci

#Location of SED tool in your target root directory
export SED_DIR=$FILESYS_DIR/bin

#Location of Gstreamer compiled binaries
export GSTREAMER_DIR=$FILESYS_DIR/opt/gstreamer

#Location of Gstreamer libraries
export LD_LIBRARY_PATH=$GSTREAMER_DIR/lib

#Location of Gstreamer package config path
export PKG_CONFIG_PATH=$LD_LIBRARY_PATH/pkgconfig

export PATH=$PATH:$MVISTA_INSTALL_DIR/pro/devkit/arm/v5t_le/bin

export TI_PLUGIN_DIR=$PWD
echo $TI_PLUGIN_DIR


export CE_PACKAGES_PATH=$DVEVM_PATH/codec_engine_1_10_01/packages
export XDC_PACKAGES_PATH=$DVEVM_PATH/xdc_2_94/packages
export XDAIS_PACKAGES_PATH=$DVEVM_PATH/xdais_5_10/packages



echo "Please follow these instructions before running the make script"
echo ""
echo "#1. Set MVISTA_INSTALL_DIR to point to your Montavista directory installation"
echo "#2. Set FILESYS_DIR to point to your NFS shared target filesystem"
echo "#3. Set DVEVM_PATH to point to your DVEVM installation directory"
echo "#4. Set KERNEL_SRC to point to your davinci kernel src (must be configured for davinci)"
echo "#5. Create soft links as per 2 commands below logged in as root/superuser"
echo "     a) Command: ln -s $MVISTA_INSTALL_DIR /opt/montavista"
echo "     b) Command: ln -s /bin/sed /opt/montavista/common/bin/sed"
echo""
echo""


export PATH=$PATH:/opt/montavista/common/bin:$CE_PACKAGES_PATH:$XDC_PACKAGES_PATH:$XDAIS_PACKAGES_PATH
cp -fr "$TI_PLUGIN_DIR/xdais" "$GSTREAMER_DIR/include"
cp -fr "$TI_PLUGIN_DIR/system_files_gstreamer" "$FILESYS_DIR/opt"

echo ""
echo "Do you want to compile davinci plugins (adecoder, gdecoder, fbvideosink package.. (y/n)"
read input
if [ "$input" = "y" ];
then

echo "*************************************************"
echo "Building plugins                                 "
echo "*************************************************"


cd "$TI_PLUGIN_DIR/ti_plugins/davinci"
echo "I am in following directory"
echo $PWD
echo ""


CC=arm_v5t_le-gcc ./configure --build=i686-linux --host=arm-linux --prefix=$GSTREAMER_DIR  CFLAGS="-I$FILESYS_DIR/opt/gstreamer/include/xdais -I${KERNEL_SRC}/include" LIBS=-L"$FILESYS_DIR/opt/system_files_gstreamer"
make clean
make install

else

echo "You pressed n or some other key... adecoder gdecoder fbvideosink package is not compiled"

fi

echo ""
echo "Do you want to compile ASF Demuxer (tiasfplugin package.. (y/n)"
read input
if [ "$input" = "y" ];
then

echo "*************************************************"
echo "Building tiasfplugin                             "
echo "*************************************************"

cd "$TI_PLUGIN_DIR/ti_plugins/tiasfplugin"
echo "I am in following directory"
echo $PWD
echo ""

CC=arm_v5t_le-gcc ./configure --build=i686-linux --host=arm-linux --prefix=$GSTREAMER_DIR  LIBS=-L"$FILESYS_DIR/opt/system_files_gstreamer"
make clean
make install

else

echo "You pressed n or some other key... tiasfplugin package is not compiled"

fi

echo ""
echo "Do you want to compile AVI Demuxer (tiaviplugin package.. (y/n)"
read input
if [ "$input" = "y" ];
then

echo "*************************************************"
echo "Building tiaviplugin                             "
echo "*************************************************"

cd "$TI_PLUGIN_DIR/ti_plugins/tiaviplugin"
echo "I am in following directory"
echo $PWD
echo ""

CC=arm_v5t_le-gcc ./configure --build=i686-linux --host=arm-linux --prefix=$GSTREAMER_DIR  LIBS=-L"$FILESYS_DIR/opt/system_files_gstreamer"
make clean
make install

else

echo "You pressed n or some other key... tiaviplugin package is not compiled"

fi

echo "*************************************************"
echo "ALL TI PLUGINS BUILT SUCCESSFULLY                "
echo "*************************************************"
