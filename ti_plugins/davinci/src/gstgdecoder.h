/*
 * Plugin Name : gstgdecoder.h
 * Description : Header file for Video Decoder for for TI Davinci DM644x
 *
 * Copyright (C) 2007 Texas Instruments, Inc.
 * Copyright (C) 2008 Behind The Set, LLC
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
#ifndef __GST_GDECODER_H__
#define __GST_GDECODER_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <pthread.h>
#include <getopt.h>
#include <errno.h>
#include <glib.h>
#include <gst/gst.h>
#include <gst/base/gstadapter.h>

#define 	IDMA3_USEFULLPACKAGEPATH
#define 	MdUns   	UInt16
#define 	SmUns   	UInt8

#include <xdc/std.h>
#include <ti/sdo/ce/video/viddec.h>
#include <ti/sdo/ce/CERuntime.h>
#include <ti/sdo/ce/Engine.h>
#include <ti/sdo/ce/trace/gt.h>
#include <ti/sdo/ce/osal/Memory.h>
#include <ti/sdo/fc/acpy3/idma3.h>

#include <ti/sdo/ivc1dec.h>

#define CLOCK_BASE 9LL
#define CLOCK_FREQ CLOCK_BASE * 10000

#define 	MAX_FRAME_WIDTH  	720
#define 	MAX_FRAME_HEIGHT 	480
#define    PREROLL_TIME 180000000
#define   FRAME_DURATION 33366666       //We can't hardcode this, get it from demuxer

#define    GDECODER_INIT_STATE                        0
#define    GDECODER_PROCESS_STATE               1

/* Begin Declaration */
G_BEGIN_DECLS

#define GST_TYPE_GDECODER	   	     	(gst_gdecoder_get_type())
#define GST_TYPE_GDECODER_ENGINE		(gst_gdecoder_get_engine_type())
#define GST_TYPE_GDECODER_CODEC			(gst_gdecoder_get_codec_type())
#define GST_GDECODER(obj)		     	(G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_GDECODER,GstGDecoder))
#define GST_GDECODER_CLASS(klass)     	(G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_GDECODER,GstGDecoderClass))
#define GST_GDECODER_GET_CLASS(klass) 	(G_TYPE_INSTANCE_GET_CLASS((klass),GST_TYPE_GDECODER,GstGDecoderClass))
#define GST_IS_GDECODER(obj)		    (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_GDECODER))
#define GST_IS_GDECODER_CLASS(obj)	 	(G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_GDECODER))

typedef struct _GstGDecoder GstGDecoder;
typedef struct _GstGDecoderClass GstGDecoderClass;

struct _GstGDecoder {
    GstElement parent;

    GstPad *sinkpad;
    GstPad *srcpad;

    gboolean decReady;
    guint8 codec;
    guint8 engine;
    gchar *engineName;

    gboolean isCodecPropSet;

    XDAS_Int8 *outbuf;
    GstBuffer *inbuf;
    XDM_BufDesc outBufDesc;
    XDM_BufDesc inBufDesc;
    guint32 readOffset;     //File read offset
    guint32 curInBufStartOffset;
    guint64 ptsNoDemux;
    guint32 state;
    void *decoder;
    Engine_Handle ce;
    GstSegment segment;
    GstBuffer *srcbuf;
    gboolean streaming;
    guint32 width;
    guint32 height;
    GSList *inbuf_pool;
    GSList *displaybuf_pool;
    guint64 lastdispts;
    gboolean isRcv;
    gboolean isEof;
    GMutex *pool_lock;
    GMutex *inbuf_lock;
    guint64 bytesConsumed;
    gboolean onlyonce;
    guint32  preroll_buflen;

    gboolean enable_processing;
    GMutex *process_lock;
};

struct _GstGDecoderClass {
    GstElementClass parent_class;
    GstPadTemplate *sink_template;
    GstPadTemplate *src_template;
};

GType gst_gdecoder_get_type(void);
gboolean gst_gdecoder_plugin_init(GstPlugin * plugin);

void gst_gdecoder_reset(GstGDecoder * gdecoder);
void gst_gdecoder_cleanup(GstGDecoder * gdecoder);

G_END_DECLS

#endif                          /* __GST_GDECODER_H__ */

