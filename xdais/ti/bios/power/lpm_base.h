/*
 *  ======== lpm_base.h ========
 *
 */


#if !defined (LPM_BASE_H)
#define LPM_BASE_H


#if defined (__cplusplus)
extern "C" {
#endif /* defined (__cplusplus) */


#define IN
#define OUT
#define INOUT
#if 0
#define EXPORT_API
#endif



/* LPM return status codes */
typedef enum {
    LPM_SOK = 0,        /* success              */
    LPM_EFAIL,          /* general failure      */
    LPM_EBUSY,          /* device is busy       */
} LPM_Status;


/* LPM power states, in ranking order */
typedef enum {
    LPM_HIBERNATE = 0x00402001,
} LPM_PowerState;


/* LPM control operations */
typedef enum {
    LPM_CTRL_CONNECT = 0x10001000,
    LPM_CTRL_DISCONNECT,
    LPM_CTRL_OFF,
    LPM_CTRL_ON,
    LPM_CTRL_RESUME,
    LPM_CTRL_SETPOWERSTATE,
} LPM_Control;


/* *** THIS IS PRIVATE, should be moved to a private header file. */
// Device context states.
typedef enum {
    LPM_COLDBOOT = 0,
    LPM_WARMBOOT
} LPM_Context;



#if defined (__cplusplus)
}
#endif /* defined (__cplusplus) */


#endif /* !defined (LPM_BASE_H) */
