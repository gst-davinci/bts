/*
 *  ======== lpm.h ========
 *
 */


#if !defined (LPM_H)
#define LPM_H


#include <stdio.h>

#include <ti/bios/power/lpm_base.h>



#if defined(__cplusplus)
EXTERN "C" {
#endif


/* Opaque handle to LPM object. */
typedef struct LPM_Object *LPM_Handle;


/* Configuration database types. */
typedef struct LPM_DevObj *LPM_DeviceHandle;

typedef struct {
    char               *name;
    LPM_DeviceHandle   lpm_dev;
} LPM_Cfg;


/* External Power ON/OFF function types. */
typedef void (*LPM_ExtOffFxn)(void *);
typedef void (*LPM_ExtOnFxn)(void *);



/*
 *  ======== LPM_close ========
 *  
 */
LPM_Status LPM_close (IN LPM_Handle hndl);


/*
 *  ======== LPM_connect ========
 *  Open a connection to the transport layer.
 */
LPM_Status LPM_connect (IN LPM_Handle hndl);


/*
 *  ======== LPM_disconnect ========
 *  Open a connection to the transport layer.
 */
LPM_Status LPM_disconnect (IN LPM_Handle hndl);


/*
 *  ======== LPM_exit ========
 *  Do all finalization required by the LPM module. LPM_exit
 *  must be called as part of the module shutdown procedure.
 */
void LPM_exit (void);


/*
 *  ======== LPM_init ========
 *  Do all initialization required by the LPM module. LPM_init
 *  must be called before any of the LPM functions are used.
 */
void LPM_init (void);


/*
 *  ======== LPM_off ========
 *  Turn off the power to the device.
 */
LPM_Status LPM_off (IN LPM_Handle handle);


/*
 *  ======== LPM_on ========
 *  Turn on the power to the device.
 */
LPM_Status LPM_on (IN LPM_Handle handle);


/*
 *  ======== LPM_open ========
 *  Establish a connection with the LPM object.
 */
LPM_Status LPM_open (IN char *name, OUT LPM_Handle *handle);


/*
 *  ======== LPM_regExtPowerOff ========
 *  Register an external power switch OFF function.
 */
LPM_Status LPM_regExtPowerOff (IN LPM_Handle handle, LPM_ExtOffFxn func, void *arg);


/*
 *  ======== LPM_regExtPowerOn ========
 *  Register an external power switch ON function.
 */
LPM_Status LPM_regExtPowerOn (IN LPM_Handle handle, LPM_ExtOnFxn func, void *arg);


/*
 *  ======== LPM_resume ========
 *  Return from hibernate state and resume the previous power state.
 */
LPM_Status LPM_resume (IN LPM_Handle handle);


/*
 *  ======== LPM_setPowerState ========
 *  Transition the device into a new power state.
 */
LPM_Status LPM_setPowerState (IN LPM_Handle handle, IN LPM_PowerState state);



#if defined (__cplusplus)
}
#endif /* defined (__cplusplus) */


#endif /* !defined (LPM_H) */
