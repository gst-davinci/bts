/*
 * gstasf.c
 *
 * Source file for data Types in ASF Demuxer/Parser plugin 
 * for Gstreamer implementation on DM644x EVM
 *
 * Copyright (C) 2007 Texas Instruments, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */


#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gst/gst.h>
#include <gst/riff/riff-read.h>
#include "gst/gst-i18n-plugin.h"

#include "gstasfdemux.h"
/* #include "gstasfmux.h" */


static gboolean
plugin_init (GstPlugin * plugin)
{
#ifdef ENABLE_NLS
  GST_DEBUG ("binding text domain %s to locale dir %s", GETTEXT_PACKAGE,
      LOCALEDIR);
  bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
#endif /* ENABLE_NLS */

  gst_riff_init ();

  if (!gst_element_register (plugin, "tidemux_asf", GST_RANK_SECONDARY,
          GST_TYPE_TIASF_DEMUX)) {
    return FALSE;
  }
/*
  if (!gst_element_register (plugin, "asfmux", GST_RANK_NONE, GST_TYPE_ASFMUX))
    return FALSE;
*/

  return TRUE;
}

GST_PLUGIN_DEFINE (GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    "tidemux_asf",
    "asf parser/demuxer",
    plugin_init, VERSION, "LGPL", "GStreamer", "http://gstreamer.net/")

