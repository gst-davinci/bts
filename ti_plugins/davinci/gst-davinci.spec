%define majorminor  0.10
%define gstreamer   gstreamer09

%define gst_minver  0.9.3

Name:		%{gstreamer}-fbvideosink
Version:	0.9.7
Release:	1.flu
Summary:	GStreamer plug-in for a fbdev based VideoSink

Group:		Applications/Multimedia
License:	Proprietary
URL:		http://www.isp.domain/
Source:		gst-fbvideosink-%{version}.tar.bz2
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	%{gstreamer}-devel >= %{gst_minver}

%description
GStreamer plug-in for a fbdev based VideoSink.

%prep
%setup -q -n gst-fbvideosink-%{version}

%build
%configure
make

%install
rm -rf $RPM_BUILD_ROOT
%makeinstall

# Remove unneeded .la and .a files
rm -rf $RPM_BUILD_ROOT/%{_libdir}/gstreamer-%{majorminor}/*.a
rm -rf $RPM_BUILD_ROOT/%{_libdir}/gstreamer-%{majorminor}/*.la

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc README NEWS
%{_libdir}/gstreamer-%{majorminor}/*.so

%changelog
* Tue Oct 04 2005 Thomas Vander Stichele <thomas at apestaart dot org>
- remove register calls

* Fri Sep 02 2005 Thomas Vander Stichele <thomas at apestaart dot org>
- Clean up spec file
