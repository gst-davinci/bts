/* GStreamer
 * Copyright (C) <2008> Tyler Nielsen <tyler@behindtheset.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GST_CONTIGBUFFER_H__
#define __GST_CONTIGBUFFER_H__

#include <gst/gst.h>

#include <xdc/std.h>
#include <ti/sdo/ce/Engine.h>
#include <ti/sdo/ce/CERuntime.h>
#include <ti/sdo/ce/trace/gt.h>
#include <ti/sdo/ce/osal/Memory.h>

G_BEGIN_DECLS

typedef struct _GstContigBuffer GstContigBuffer;
typedef struct _GstContigAddress GstContigAddress;

#define GST_TYPE_CONTIGBUFFER            (gst_contigbuffer_get_type())
#define GST_IS_CONTIGBUFFER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GST_TYPE_CONTIGBUFFER))
#define GST_CONTIGBUFFER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GST_TYPE_CONTIGBUFFER, GstContigBuffer))

struct _GstContigBuffer {
  GstBuffer buffer;

  guint32 physLocation;
};

/* creating buffers */
GType              gst_contigbuffer_get_type          (void);
GstBuffer*         gst_contigbuffer_new               (gint32 size);

G_END_DECLS

#endif /* __GST_NETBUFFER_H__ */

