#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>

#define ALWAYSERR

#include <asm/arch/davinci_resizer.h>
#include "rszcopy.h"

/* Enables or disables debug output */
#ifdef __DEBUG 
#define DBG(fmt, args...) fprintf(stderr, "Rszcopy Debug: " fmt, ## args)
#else
#define DBG(fmt, args...)
#endif

#if (defined __DEBUG) || (defined ALWAYSERR)
#define ERR(fmt, args...) fprintf(stderr, "Rszcopy Error: " fmt, ## args)
#else
#define ERR(fmt, args...)
#endif

#define RESIZER_DEVICE "/dev/davinci_resizer"
#define FIR_RND_SCALE  256
#define NUM_COEFS      32
#define SCREEN_BPP     16

/******************************************************************************
 * inSizeCalc
 ******************************************************************************/
static int inSizeCalc(int inSize)
{
    int rsz;
    int in = inSize;

    while (1) {
        rsz = ((in - 4) * 256 - 16) / (inSize - 1);

        if (rsz < 256) {
            if (in < inSize) {
                break;
            }
            in++;
        }
        else if (rsz > 256) {
            if (in > inSize) {
                break;
            }
            in--;
        }
        else {
            break;
        }
    }

    return in;
}

/******************************************************************************
 * Rszcopy_config
 ******************************************************************************/
int Rszcopy_config(Rszcopy_Handle hRszcopy,
                   int width, int height, int srcPitch, int dstPitch)
{
    rsz_params_t  params = {
        0,                              /* in_hsize (set at run time) */
        0,                              /* in_vsize (set at run time) */
        0,                              /* in_pitch (set at run time) */
        RSZ_INTYPE_YCBCR422_16BIT,      /* inptyp */
        0,                              /* vert_starting_pixel */
        0,                              /* horz_starting_pixel */
        0,                              /* cbilin */
        RSZ_PIX_FMT_UYVY,               /* pix_fmt */
        0,                              /* out_hsize (set at run time) */
        0,                              /* out_vsize (set at run time) */
        0,                              /* out_pitch (set at run time) */
        0,                              /* hstph */
        0,                              /* vstph */
        {                               /* hfilt_coeffs */
            256, 0, 0, 0, 256, 0, 0, 0, 256, 0, 0, 0, 256, 0, 0, 0,
            256, 0, 0, 0, 256, 0, 0, 0, 256, 0, 0, 0, 256, 0, 0, 0
        },
        {                               /* vfilt_coeffs */
            256, 0, 0, 0, 256, 0, 0, 0, 256, 0, 0, 0, 256, 0, 0, 0,
            256, 0, 0, 0, 256, 0, 0, 0, 256, 0, 0, 0, 256, 0, 0, 0
        },
        {                               /* yenh_params */
            RSZ_YENH_DISABLE,               /* type */
            0,                              /* gain */
            0,                              /* slop */
            0                               /* core */
        }
    };

    DBG("Configuring resizer job to copy image of resolution %dx%d\n",
        width, height);

    /* Set up the rszcopyr job */
    params.in_hsize  = inSizeCalc(width);
    params.in_vsize  = inSizeCalc(height);
    params.in_pitch  = srcPitch;
    params.out_hsize = width;
    params.out_vsize = height;
    params.out_pitch = dstPitch;

    hRszcopy->inSize = srcPitch * params.in_vsize;
    hRszcopy->outSize = dstPitch * params.out_vsize;

    if (ioctl(hRszcopy->rszFd, RSZ_S_PARAM, &params) == -1) {
        ERR("Rszcopy setting parameters failed.\n");
        return RSZCOPY_FAILURE;
    }

    return RSZCOPY_SUCCESS;
}

/******************************************************************************
 * Rszcopy_create
 ******************************************************************************/
Rszcopy_Handle Rszcopy_create(int rszSpeed)
{
    Rszcopy_Handle hRszcopy;

    /* Allocate the rszcopy object */
    hRszcopy = malloc(sizeof(Rszcopy_Object));

    if (hRszcopy == NULL) {
        ERR("Failed to allocate memory space for handle.\n");
        return RSZCOPY_FAILURE;
    }

    /* Open rszcopyr device */
    hRszcopy->rszFd = open(RESIZER_DEVICE, O_RDWR);

    if (hRszcopy->rszFd == -1) {
        ERR("Failed to open %s\n", RESIZER_DEVICE);
        free(hRszcopy);
        return RSZCOPY_FAILURE;
    }

    if (rszSpeed >= 0) {
        if (ioctl(hRszcopy->rszFd, RSZ_S_EXP, &rszSpeed) == -1) {
            ERR("Error calling RSZ_S_EXP on resizer.\n");
            free(hRszcopy);
            return RSZCOPY_FAILURE;
        }
    }

    return hRszcopy;
}

/******************************************************************************
 * Rszcopy_execute
 ******************************************************************************/
int Rszcopy_execute(Rszcopy_Handle hRszcopy, unsigned long srcBuf,
                    unsigned long dstBuf)
{
    rsz_resize_t rsz;
    int          rszError;

    if (dstBuf % 32) {
        ERR("Destination buffer not aligned on 32 bytes\n");
        return RSZCOPY_FAILURE;
    }

    if (srcBuf % 32) {
        ERR("Source buffer not aligned on 32 bytes\n");
        return RSZCOPY_FAILURE;
    }

    rsz.in_buf.index     = -1;
    rsz.in_buf.buf_type  = RSZ_BUF_IN;
    rsz.in_buf.offset    = srcBuf;
    rsz.in_buf.size      = hRszcopy->inSize;

    rsz.out_buf.index    = -1;
    rsz.out_buf.buf_type = RSZ_BUF_OUT;
    rsz.out_buf.offset   = dstBuf;
    rsz.out_buf.size     = hRszcopy->outSize;

    do {
        rszError = ioctl(hRszcopy->rszFd, RSZ_RESIZE, &rsz);
    } while (rszError == -1 && errno == EAGAIN);

    if (rszError == -1) {
        ERR("Failed to execute resize job\n");
        return RSZCOPY_FAILURE;
    }

    return RSZCOPY_SUCCESS;
}

/******************************************************************************
 * Rszcopy_delete
 ******************************************************************************/
void Rszcopy_delete(Rszcopy_Handle hRszcopy)
{
    if (hRszcopy != NULL) {
        close(hRszcopy->rszFd);
        free(hRszcopy);
    }
}
