/* 
 *  Copyright 2007
 *  Texas Instruments Incorporated
 *
 *  All rights reserved.  Property of Texas Instruments Incorporated
 *  Restricted rights to use, duplicate or disclose this code are
 *  granted through contract.
 * 
 */
/*
 *  ======== Memory.h ========
 */
/**
 *  @file       Memory.h
 *
 *  @brief      The Codec Engine OSAL Memory interface.  Provides the user
 *              an OS-independent Memory abstraction.
 */
/**
 *  @defgroup   ti_sdo_ce_osal_Memory     Codec Engine OSAL - Memory
 */

#ifndef ti_sdo_ce_osal_Memory_
#define ti_sdo_ce_osal_Memory_

#ifdef __cplusplus
extern "C" {
#endif

/** @ingroup    ti_sdo_ce_osal_MEMORY */
/*@{*/

/*
 *  ======== Memory_DEFAULTALIGNMENT ========
 */
#define Memory_DEFAULTALIGNMENT ((UInt)(-1))

/*
 *  ======== Memory_GTNAME ========
 */
#define Memory_GTNAME "OM"

/** @cond INTERNAL */

/**
 *  @brief      Statistics structure for a memory section.
 */
typedef struct Memory_Stat {

    /* allocation statistics applicable to BIOS only */
    UInt size;       /**< Original size of segment. */
    UInt used;       /**< Number of bytes used in segment. */
    UInt length;     /**< Length of largest contiguous block. */

} Memory_Stat;


/*
 *  ======== Memory_alloc ========
 */
/**
 *  @brief      Heap memory allocation.
 *
 *  @param[in]  size    Number of bytes to allocate.
 *
 *  @retval     NULL    The memory request failed.
 *  @retval     non-NULL The pointer to a buffer containing the requested
 *                      memory.
 *
 *  @note       On some operating systems, which provide further memory
 *              allocation flexibility than simply the size of the
 *              buffer (e.g. alignment, segment, etc), the
 *              implementation will use default values for these
 *              fields.  If your memory requirements require some
 *              constraints, consider other allocation APIs.
 *
 *  @sa         Memory_free(), Memory_stat(), Memory_contigAlloc() and
 *              Memory_segAlloc().
 */
extern Ptr Memory_alloc(UInt size);

/** @endcond */

/*
 *  ======== Memory_cacheInv ========
 */
/**
 *  @brief      Invalidate a range of cache.
 *
 *  @param[in]  addr            Address of the beginning of the buffer
 *                               to invalidate.
 *  @param[in]  sizeInBytes     Size of the buffer to invalidate.
 *
 *  @remarks    This is typically called by a skeleton implementor after
 *              it has received a shared memory buffer from
 *              another processor, and before it invokes an algorithm's
 *              interface which will operate on that shared memory buffer.
 *
 *  @sa         Memory_cacheWbInv()
 */
extern Void Memory_cacheInv(Ptr addr, Int sizeInBytes);

/*
 *  ======== Memory_cacheWbInv ========
 */
/**
 *  @brief      Write back and invalidate cache (flush).
 *
 *  @param[in]  addr            Address of the beginning of the buffer
 *                              to writeback and invalidate.
 *  @param[in]  sizeInBytes     Size of the buffer to writeback invalidate.
 *
 *  @remarks    This is typically called by a skeleton implementor after
 *              it has invoked an algorithm's interface which operated on
 *              a shared memory buffer and before it returns those buffers
 *              to the remote processor's control.
 *
 *  @sa         Memory_cacheInv()
 */
extern Void Memory_cacheWbInv(Ptr addr, Int sizeInBytes);


/*
 *  ======== Memory_contigAlloc ========
 */
/**
 *  @brief      Allocate physically contiguous blocks of memory.
 *
 *  @param[in]  size    Size of the buffer to allocate.
 *  @param[in]  align   Alignment of the buffer; must be divisible by a power
 *                      of two.
 *
 *  @pre        @c align must be divisible by a power of two.
 *
 *  @retval     non-NULL Address of a physically contiguous buffer.
 *  @retval     NULL    Unable to obtain the requested buffer.
 *
 *  @remarks    This is commonly called by an application wishing to obtain
 *              a physically contiguous buffer which it must share with
 *              another processor.
 *
 *  @remarks    For portability reasons, applications commonly use
 *              this API even in environments which do not have virtual memory
 *              or an MMU (e.g. a single processor, DSP/BIOS based system).
 *              Doing so makes them portable to environments which <i>do</i>
 *              have to provide physically contiguous memory to remote
 *              processors.
 *
 *  @sa         Memory_contigFree().
 */
extern Ptr Memory_contigAlloc(UInt size, UInt align);

/*
 *  ======== Memory_contigFree ========
 */
/**
 *  @brief      Free memory allocated by Memory_contigAlloc()
 *
 *  @param[in]  addr    Address of a buffer allocated by Memory_contigAlloc()
 *  @param[in]  size    Size of the buffer to free.
 *
 *  @retval     TRUE    The buffer was freed and the memory pointed to by
 *                      @c addr is no longer valid.
 *  @retval     FALSE   The buffer couldn't be freed.
 *
 *  @pre        @c addr must be a valid address returned by
 *              Memory_contigAlloc().
 *
 *  @pre        @c size must be equivalent to the @c size passed in during the
 *              Memory_contigAlloc() allocation.
 *
 *  @sa         Memory_contigAlloc().
 */
extern Bool Memory_contigFree(Ptr addr, UInt size);


/** @cond INTERNAL */

/*
 *  ======== Memory_contigStat ========
 */
/**
 *  @brief      Obtain the memory usage statistics of the contiguous
 *              memory allocator.
 *
 *  @param[out] statbuf Buffer to fill with statistics data.
 *
 *  @retval     TRUE    The statistics were captured, and @c statbuf has been
 *                      filled.
 *  @retval     FALSE   The statistics could not be obtained.
 *
 *  @pre        @c statbuf must be a pointer to memory of size
 *              <tt>sizeof(#Memory_Stat)</tt>.
 *
 *  @sa         Memory_contigAlloc() and Memory_contigFree().
 */
extern Bool Memory_contigStat(Memory_Stat *statbuf);


/*
 *  ======== Memory_free ========
 */
/**
 *  @brief      Free memory allocated with Memory_alloc().
 *
 *  @param[in]  addr    Address of a buffer allocated by Memory_alloc().
 *  @param[in]  size    Size of the buffer to free.
 *
 *  @retval     TRUE    The buffer was freed and the memory pointed to by
 *                      @c addr is no longer valid.
 *  @retval     FALSE   The buffer couldn't be freed.
 *
 *  @pre        @c addr must be a valid address returned by
 *              Memory_alloc().
 *
 *  @pre        @c size must be equivalent to the @c size passed in during the
 *              Memory_alloc() allocation.
 *
 *  @sa         Memory_alloc(), Memory_stat(), Memory_contigAlloc(), and
 *              Memory_segAlloc().
 */
extern Bool Memory_free(Ptr addr, UInt size);

/** @endcond  */

/*
 *  ======== Memory_getBufferPhysicalAddress ========
 */
/**
 *  @brief      Converts application virtual address to a physical address.
 *
 *  This API also checks verifies that the buffer is really contiguous.
 *
 *  @param[in]  virtualAddress  Address of a buffer.
 *  @param[in]  sizeInBytes     Size of the buffer.
 *  @param[out] isContiguous    Optional flag indicating whether the buffer
 *                              was physically contiguous or not.
 *
 *  @retval     0               Failure, the physical address could not
 *                              be obtained.
 *  @retval     non-zero        The physical address of the buffer.
 *
 *  @remarks    @c isContiguous is an optional parameter, and can be NULL if
 *              the caller doesn't want the results of this check.
 *              If @c isContiguous is NULL but the buffer is not contiguous,
 *              error trace will be generated.
 *
 *  @remarks    If a physically discontiguous buffer is provided in
 *              @c virtualAddress, zero will be returned.  And if
 *              @c isContiguous is non-null, it will be set to FALSE.
 *
 *  @remarks    This is typically called by a stub implementation to
 *              translate an application-side virtual address into a physical
 *              address usable by a remote skeleton and algorithm.
 *
 *  @remarks    To enable portable code, this is safe to call on systems
 *              without virtual memory.  In those cases, the returned
 *              physical buffer will be the same as the provided
 *              "virtual" buffer.
 *
 *  @sa         Memory_getBufferVirtualAddress().
 */
extern UInt32 Memory_getBufferPhysicalAddress(Ptr virtualAddress,
    Int sizeInBytes, Bool *isContiguous);

/*
 *  ======== Memory_getBufferVirtualAddress ========
 */
/**
 *  @brief      Obtains the virtual address of a physically contiguous
 *              buffer.
 *
 *  @param[in]  physicalAddress Physical address of a buffer.
 *  @param[in]  sizeInBytes     Size of the buffer.
 *
 *  @retval     NULL            Failure, the virtual address could not
 *                              be obtained.
 *  @retval     non-zero        The virtual address of the buffer.
 *
 *  @remarks    If @c virtualAddress, was not acquired by
 *              Memory_getBufferPhysicalAddress(), no attempt is made to map the
 *              physically contiguous buffer into a the application's
 *              virtual memory space.
 *
 *  @remarks    This is typically called by a stub implementation to
 *              translate a buffer provided by a remote skeleton and/or
 *              algorithm into an application-usable, virtual address.
 *
 *  @remarks    To enable portable code, this is safe to call on systems
 *              without virtual memory.  In those cases, the returned
 *              "virtual" buffer will be the same as the provided
 *              physical buffer.
 *
 *  @sa         Memory_getBufferPhysicalAddress().
 */
extern Ptr Memory_getBufferVirtualAddress(
               UInt32 physicalAddress, Int sizeInBytes);

/** @cond INTERNAL */

/*
 *  ======== Memory_getPhysicalAddress ========
 *  Convert user virtual address to physical address
 *  (returns the exact same value everywhere other than on Linux)
 *  This function is deprecated -- everyone should be calling the one above.
 */
extern UInt32 Memory_getPhysicalAddress(Ptr virtualAddress);


/*
 *  ======== Memory_init ========
 */
extern Bool Memory_init(Void);

/*
 *  ======== Memory_exit ========
 */
extern Void Memory_exit(Void);

/*
 *  ======== Memory_segAlloc ========
 */
/**
 *  @brief      Allocate memory from a specific segment.
 *
 *  @param[in]  segid   Segment ID to allocate the buffer from.
 *  @param[in]  size    Size of the buffer to allocate.
 *  @param[in]  align   Alignment of the buffer, must be divisible by a power
 *                      of two.
 *
 *  @retval     non-NULL Address of a physically contiguous buffer.
 *  @retval     NULL    Unable to obtain the requested buffer.
 *
 *  @sa         Memory_segFree(), Memory_segStat(), Memory_alloc(), and
 *              Memory_contigAlloc().
 */
extern Ptr Memory_segAlloc(Int segid, UInt size, UInt align);

/*
 *  ======== Memory_segFree ========
 */
/**
 *  @brief      Free memory allocated with Memory_segAlloc().
 *
 *  @param[in]  segid   Segment the buffer to free was allocated from.
 *  @param[in]  addr    Address of a buffer allocated by Memory_segAlloc().
 *  @param[in]  size    Size of the buffer to free.
 *
 *  @retval     TRUE    The buffer was freed and the memory pointed to by
 *                      @c addr is no longer valid.
 *  @retval     FALSE   The buffer couldn't be freed.
 *
 *  @pre        @c segid must be equivalent to the @c segid passed in during the
 *              Memory_segAlloc() allocation.
 *
 *  @pre        @c addr must be a valid address returned by
 *              Memory_segAlloc().
 *
 *  @pre        @c size must be equivalent to the @c size passed in during the
 *              Memory_segAlloc() allocation.
 *
 *  @sa         Memory_segAlloc(), Memory_segStat(), Memory_contigAlloc(), and
 *              Memory_alloc().
 */
extern Bool Memory_segFree(Int segid, Ptr addr, UInt size);

/*
 *  ======== Memory_segStat ========
 */
/**
 *  @brief      Obtain the memory usage statistics of a given memory segment
 *              allocator.
 *
 *  @param[in]  segid   Segment ID to obtain statistics information for.
 *  @param[out] statbuf Buffer to fill with statistics data.
 *
 *  @retval     TRUE    The statistics were captured, and @c statbuf has been
 *                      filled.
 *  @retval     FALSE   The statistics could not be obtained.
 *
 *  @pre        @c statbuf must be a pointer to memory of size
 *              <tt>sizeof(#Memory_Stat)</tt>.
 *
 *  @sa         Memory_segAlloc(), and Memory_segFree().
 */
extern Bool Memory_segStat(Int segid, Memory_Stat *statbuf);

/** @endcond */

/*@}*/  /* ingroup */

#ifdef __cplusplus
}
#endif

#endif
/*
 *  @(#) ti.sdo.ce.osal; 1, 0, 0, 0,171; 4-20-2007 12:58:20; /db/atree/library/trees/ce-e10x/src/
 */

