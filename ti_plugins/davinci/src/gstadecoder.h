/*
 * gstadecoder.h
 *
 * Header File for Audio Decoder plugin for Gstreamer
 *
 * Copyright (C) 2007 Texas Instruments, Inc.
 * Copyright (C) 2008 Behind The Set, LLC
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#ifndef __GST_ADECODER_H__
#define __GST_ADECODER_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <pthread.h>
#include <getopt.h>
#include <errno.h>
#include <string.h>

#include <xdc/std.h>
#include <ti/sdo/ce/audio/auddec.h>
#include <ti/sdo/ce/CERuntime.h>
#include <ti/sdo/ce/Engine.h>
#include <ti/sdo/ce/trace/gt.h>
#include <ti/sdo/ce/osal/Memory.h>
//#include <ti/sdo/fc/acpy3/idma3.h>
//#include <ti/sdo/imp3dec.h>
//#include <ti/sdo/iwma.h>
// #include <ti/sdo/loadstuff.h>
//#include <ti/sdo/iaacdec.h>

#include <gst/gst.h>
#include <gst/base/gstadapter.h>


#define INTERLEAVED
//#define	MIN_INBUF_SIZE					4096
#define INPUT_BUFFER_SIZE 				500000
#define WMA_HEADER_LENGTH				70

#define	DEFAULT_NO_OF_OUTPUT_CHANNELS 	2

#define NANO_SEC_PER_SEC				(unsigned long long)1000000000

#define TIMESTAMP_ENABLE

G_BEGIN_DECLS

#define GST_TYPE_ADECODER	   	     	(gst_adecoder_get_type())
#define GST_TYPE_ADECODER_ENGINE		(gst_adecoder_get_engine_type())
#define GST_TYPE_ADECODER_CODEC			(gst_adecoder_get_codec_type())
#define GST_ADECODER(obj)		     	(G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_ADECODER,GstADecoder))
#define GST_ADECODER_CLASS(klass)     	(G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_ADECODER,GstADecoderClass))
#define GST_ADECODER_GET_CLASS(klass) 	(G_TYPE_INSTANCE_GET_CLASS((klass),GST_TYPE_ADECODER,GstADecoderClass))
#define GST_IS_ADECODER(obj)		    (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_ADECODER))
#define GST_IS_ADECODER_CLASS(obj)	 	(G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_ADECODER))

typedef struct _GstADecoder GstADecoder;
typedef struct _GstADecoderClass GstADecoderClass;

struct _GstADecoder {
    GstElement parent;
    GstPad *sinkpad;
    GstPad *srcpad;
    gboolean initialized;
    guint8 codec;
    XDAS_Int8 *outbuf[2];
    XDAS_Int8 *inbuf;
    XDAS_Int8 *inbuf_start_addr;
    XDM_BufDesc outBufDesc;
    XDM_BufDesc inBufDesc;
    IAUDDEC_DynamicParams *DyParams;
    IAUDDEC_Status *Status;
    IAUDDEC_InArgs *InArgs;
    IAUDDEC_OutArgs *OutArgs;
    guint32 readOffset;//File read offset
    XDAS_Int32 inBufSize;
    XDAS_Int32 outBufSize[2];
    gboolean firstFrame;
    AUDDEC_Handle codec_handle;
    Engine_Handle ce;
    GstSegment segment;
    gboolean   isInPushMode;
    gint32 bytes_not_decoded;
    guint32 bytesConsumed;
    guint32 frameCount;
    guint8 engine;
    gchar *engineName;
    gboolean isEof;
    gboolean isEos;
    gboolean isEnginePropSet;
    gboolean isCodecPropSet;
    guint64 initTimeStamp;
    unsigned long long frame_time;
    gboolean enable_processing;
    pthread_mutex_t *start_decode;
    gboolean isStopped;
    gboolean isPaused;
};

struct _GstADecoderClass {
    GstElementClass parent_class;

GstPadTemplate *sink_template;
    GstPadTemplate *src_template;
};

GType gst_adecoder_get_type(void);
gboolean gst_adecoder_plugin_init(GstPlugin * plugin);

G_END_DECLS

#endif                          /* __GST_ADECODER_H__ */
