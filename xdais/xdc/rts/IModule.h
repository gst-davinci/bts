/*
 *  Copyright 2006 by Texas Instruments Incorporated.
 *
 *  All rights reserved. Property of Texas Instruments Incorporated.
 *  Restricted rights to use, duplicate or disclose this code are
 *  granted through contract.
 *
 */

/*
 *  ======== xdc/rts/IModule.h ========
 *
 *  DO NOT MODIFY THIS FILE
 *
 *  It is generated from xdc/rts/IModule.xdc and any changes may be overwritten.
 */

#ifdef xdc_rts_IModule___EXTERNAL_SYNOPSIS
#error do not compile!!


    #include <xdc/rts/IModule.h>

    /* -------- module-wide configs -------- */



#endif /* xdc_rts_IModule___EXTERNAL_SYNOPSIS */







#ifdef __cplusplus
#define __extern extern "C"
#else
#define __extern extern
#endif

#ifndef xdc_std__include
#ifndef __nested__
#define __nested__
#include <xdc/std.h>
#undef __nested__
#else
#include <xdc/std.h>
#endif
#endif

#ifndef xdc_rts_IModule__include
#define xdc_rts_IModule__include

#ifndef xdc_rts__
#include <xdc/rts/package/package.defs.h>
#endif

#ifndef xdc_rts_IHeap__include
#ifndef __nested__
#define __nested__
#include <xdc/rts/IHeap.h>
#undef __nested__
#else
#include <xdc/rts/IHeap.h>
#endif
#endif


/* auxiliary definitions */

/* function table */
struct xdc_rts_IModule_Fxns__ {
    void* __base;
    void* __create;
    void* __delete;
};
__extern void* xdc_rts_IModule_BASE__;

/* function stubs */

/* function selectors */

/* C++ wrappers */
#ifdef __cplusplus
namespace xdc_rts { namespace IModule {
}}
using namespace xdc_rts;
#endif

#endif /* xdc_rts_IModule__include */

/* __prefix alises */
#if !defined(__nested__) && !defined(xdc_rts_IModule__aliases) && !defined(xdc_rts_IModule__nolocalnames)
#define xdc_rts_IModule__aliases
#define IModule_Module xdc_rts_IModule_Module
#endif


/*
 *  @(#) xdc.rts 1, 0, 0, 0,132 1-2-2006 xdc-m52
*/

