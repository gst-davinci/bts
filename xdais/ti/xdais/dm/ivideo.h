/* 
 *  Copyright 2006
 *  Texas Instruments Incorporated
 *
 *  All rights reserved.  Property of Texas Instruments Incorporated
 *  Restricted rights to use, duplicate or disclose this code are
 *  granted through contract.
 * 
 */

/**
 *  @file       ivideo.h
 *
 *  @brief      This header defines all types, constants, enums, and functions
 *              that are common across the various video codecs.
 *
 *  @version    0.5
 */
/**
 *  @defgroup   IVIDEO    xDM Video Interface
 *
 *  This is the xDM video interface shared between the various codecs.
 */

#ifndef IVIDEO_
#define IVIDEO_

#ifdef __cplusplus
extern "C" {
#endif

/** @ingroup    IVIDEO */
/*@{*/

/**
 *  @brief      Video frame skip features for video decoder.
 */
typedef enum {
    IVIDEO_NO_SKIP = 0,     /**< Do not skip the current frame. */
    IVIDEO_SKIP_P,          /**< Skip forward inter coded frame. */
    IVIDEO_SKIP_B,          /**< Skip bi-directional inter coded frame. */
    IVIDEO_SKIP_I           /**< Skip intra coded frame. */
} IVIDEO_FrameSkip;

/**
 *  @brief      xDM supported frame types for video.
 */
typedef enum {
    IVIDEO_I_FRAME = 0,     /**< Intra coded frame. */
    IVIDEO_P_FRAME,         /**< Forward inter coded frame. */
    IVIDEO_B_FRAME,         /**< Bi-directional inter coded frame. */
    IVIDEO_IDR_FRAME        /**< Intra coded frame that can be used for
                             *   refreshing video content.
                             */
} IVIDEO_FrameType;

/**
 *  @brief      xDM supported video content types.
 */
typedef enum {
    IVIDEO_PROGRESSIVE = 0, /**< Progressive video content. */
    IVIDEO_INTERLACED       /**< Interlaced video content. */
} IVIDEO_ContentType;

/**
 *  @brief      xDM supported video rate control presets.
 */
typedef enum {
    IVIDEO_LOW_DELAY = 1,   /**< CBR rate control for video conferencing. */
    IVIDEO_STORAGE,         /**< VBR rate control for local storage (DVD)
                             *   recording.
                             */
    IVIDEO_TWOPASS,         /**< Two pass rate control for non real time
                             *   applications.
                             */
    IVIDEO_NONE,
    IVIDEO_USER_DEFINED     /**< User defined configuration using advanced
                             *   parameters.
                             */
} IVIDEO_RateControlPreset;

/**
 *  @brief      xDM supported frame skipping modes for video.
 */
typedef enum {
    IVIDEO_FRAME_ENCODED = 0,
    IVIDEO_FRAME_SKIPPED
} IVIDEO_SkipMode;

/**
 *  @brief      Buffer descriptor for input and output buffers.
 */
typedef struct IVIDEO_BufDesc {
    XDAS_Int32  numBufs;    /**< Number of buffers. */
    XDAS_Int32  width;      /**< Added width of a video frame. */
    XDAS_Int8  *bufs[XDM_MAX_IO_BUFFERS];  /**< Pointer to vector
                                            *   containing buffer addresses.
                                            */
    XDAS_Int32  bufSizes[XDM_MAX_IO_BUFFERS]; /**< Size of each buffer
                                               *   in 8-bit bytes.
                                               */

} IVIDEO_BufDesc;


#ifdef __cplusplus
}
#endif

/*@}*/  /* ingroup */

#endif  /* IVIDEO_ */
/*
 *  @(#) ti.xdais.dm 1, 0, 0, 0,20 4-20-2006 dais-f06
*/

