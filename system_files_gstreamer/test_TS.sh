export LD_LIBRARY_PATH=.:/opt/system_files_gstreamer:/opt/gstreamer/lib
export GST_PLUGIN_PATH=/opt/gstreamer/lib/gstreamer-0.10/
export PATH=$PATH:/opt/gstreamer/bin

cat /dev/zero > /dev/fb/2

gst-launch-0.10 filesrc location=$1 ! flutsdemux name=t ! queue max-size-buffers=60 ! gdecoder Codec=0 ! fbvideosink device=/dev/fb/3 t. ! queue max-size-buffers=180 ! adecoder Engine=0 ! osssink 
