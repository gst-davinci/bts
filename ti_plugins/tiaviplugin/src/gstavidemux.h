/*
 * gstavidemux.h
 *
 * Header file for AVI Muxer/Parser plugin for Gstreamer implementation on 
 * DM644x EVM
 *
 * Copyright (C) 2007 Texas Instruments, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */



#ifndef __TIAVI_DEMUX_H__
#define __TIAVI_DEMUX_H__

#include <gst/gst.h>
#include <gst/base/gstadapter.h>

G_BEGIN_DECLS
  
#define GST_TYPE_TIAVI_DEMUX \
  (gst_tidemux_avi_get_type())
#define GST_TIAVI_DEMUX(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_TIAVI_DEMUX,GstTIAVIDemux))
#define GST_TIAVI_DEMUX_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_TIAVI_DEMUX,GstTIAVIDemuxClass))
#define GST_IS_TIAVI_DEMUX(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_TIAVI_DEMUX))
#define GST_IS_TIAVI_DEMUX_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_TIAVI_DEMUX))

typedef struct _GstTIAVIDemux GstTIAVIDemux;
typedef struct _GstTIAVIDemuxClass GstTIAVIDemuxClass;

typedef struct
{
  GstPad     *pad;
  guint16     id;
  guint32     frag_offset;
  guint32     sequence;
  guint64     delay;
  guint64     first_pts;
  guint64     last_pts;
  GstBuffer  *payload;

  gboolean    need_newsegment;  /* do we need to send a new-segment event? */

  /* video-only */
  guint64     last_buffer_timestamp;  /* timestamp of last buffer sent out */
  gboolean    is_video;
  gboolean    fps_known;
  GstBuffer  *cache;

  GstCaps    *caps;

  GstTagList *pending_tags;
} avi_stream_context;

typedef enum {
  GST_TIAVI_DEMUX_STATE_HEADER,
  GST_TIAVI_DEMUX_STATE_DATA,
  GST_TIAVI_DEMUX_STATE_EOS
} GstTIAviDemuxState;

#define GST_TIAVI_DEMUX_NUM_VIDEO_PADS   16
#define GST_TIAVI_DEMUX_NUM_AUDIO_PADS   32
#define GST_TIAVI_DEMUX_NUM_STREAMS      32
#define GST_TIAVI_DEMUX_NUM_STREAM_IDS  127

struct _GstTIAVIDemux {
  GstElement 	     element;

  GstPad            *sinkpad;
  GstPad            *srcpad[GST_TIAVI_DEMUX_NUM_STREAMS];
  GstAdapter        *adapter;
  GstTagList        *taglist;
  GstTIAviDemuxState   state;
  
  /* The number of bytes needed for the next parsing unit. Set by
   * parsing functions when they return TIAVI_FLOW_NEED_MORE_DATA.
   * if not set after an TIAVI_FLOW_NEED_MORE_DATA, this indicates 
   * that we are parsing broken data and want to parse beyond an
   * object or packet boundary. */ 
  guint              bytes_needed;

  guint64            data_offset;  /* byte offset where packets start    */
  guint64            data_size;    /* total size of packet data in bytes */
  guint64            num_packets;  /* total number of data packets       */
  guint64            packet;       /* current packet                     */

  /* bitrates are unused at the moment */
  guint32              bitrate[GST_TIAVI_DEMUX_NUM_STREAM_IDS];

  gchar              **languages;
  guint                num_languages;

  GSList              *ext_stream_props;

  guint32              num_audio_streams;
  guint32              num_video_streams;
  guint32              num_streams;
  avi_stream_context   stream[GST_TIAVI_DEMUX_NUM_STREAMS];

  guint32              packet_size; /* -1 if not fixed or not known */
  guint32              timestamp;   /* in milliseconds              */
  guint64              play_time;

  guint64              preroll;
  guint64              pts;

  /* expected byte offset of next buffer to be received by chain
   * function. Used to calculate the current byte offset into the
   * file from the adapter state and the data parser state */
  gint64               next_byte_offset;

  
  GstSegment           segment; /* configured play segment */

  /* Descrambler settings */
  guint8               span;
  guint16              ds_packet_size;
  guint16              ds_chunk_size;
  guint16              ds_data_size;

  /* for debugging only */
  gchar               *objpath;
};

struct _GstTIAVIDemuxClass {
  GstElementClass parent_class;
};

GType gst_tidemux_avi_get_type (void);

G_END_DECLS

#endif /* __TIAVI_DEMUX_H__ */
