#
#  ======== makefile =======
#  GNUmake-based makefile for linuxonly speech sample app.
#
#! Revision History
#! ================
#! 2005-Oct-06 cring:  Better documentation and default values.
#

# This file simulates the build for a generic Linux application that
# happens to use Codec Engine. This makefile has special sections to
# accomodate inclusion of the codec engine, while the rest is standard.
# Codec Engine-specific additions are marked with the [CE] tag.

# [CE] define EXAMPLES_ROOTDIR to point to root of <CE/examples> directory
EXAMPLES_ROOTDIR := /home/user/dvevm_1_20/codec_engine_1_10_01/examples

# [CE] include the file that defines paths to XDC packages and XDC tools
include $(EXAMPLES_ROOTDIR)/xdcpaths.mak

# [CE] add the examples directory itself to the list of paths to packages
XDC_PATH := $(EXAMPLES_ROOTDIR);$(XDC_PATH)

# [CE] include the makefile that rus XDC configuration step for our
# program configuration script.
#
# Input:
# XDC_CFGFILE: location of the program configuration script (if in different
#              directory, include the relative path to the file)
# Implicit input: XDC_ROOT and XDC_PATH defined by the xdcpaths.mak above
#
# Output:
# XDC_FLAGS:   additional compiler flags that must be added to existing 
#              CFLAGS or CPPFLAGS
# XDC_CFILE:   name of the XDC-generated C file; usually does not need to be
#              used explicitly, the existing .c->.o rules will take care of it
# XDC_OFILE:   name of object file produced by compiling XDC-generated C file;
#              must be linked with the user's application
# XDC_LFILE:   list of Codec Engine libraries that must be supplied to the
#              linker
# Implicit output: rule that generates .c file from the program configuration
#              script (XDC_CFGFILE); rule to generate dummy package one dir.
#              level below the script; rule that cleans generated files
XDC_CFGFILE = ./libce.cfg
include       ./buildutils/xdccfg_linuxarm.mak

# [CE] Augment the standard $(CPPFLAGS) variable, adding the
# $(XDC_FLAGS) variable, defined by the file above, to it.
CPPFLAGS += $(XDC_FLAGS)


# "normal" makefile settings and rules follow, with some additions for CE
# This app consists of a codec-engine-using app.c file.

%.o : %.c
	$(CC) -c -fPIC $(CPPFLAGS) $(CFLAGS) -o $@ $<

# [EDIT THIS] path to the MVArm compiler 
CC=/opt/montavista/pro/devkit/arm/v5t_le/armv5tl-montavista-linuxeabi/bin/gcc \
    -g -Wall -Os

# [CE] link configured CE-using app and all CE libraries it requires (but no others)
# into a shared library file, configured_CE_obj.so which
# includes a compiled XDC-generated $(XDC_CFILE) and link list file $(XDC_LFILE).
# The link list file contains names only of Codec Engine libraries required by
# the CE-using app
libce.so: libce.o $(XDC_OFILE)
	$(CC) -fPIC -shared -lpthread -o $@ $^ $(XDC_LFILE)  

all: libce.so

# clean rule; must be a :: rule because CE's xdccfg.mak defines clean::, too
clean::
	rm -rf *.o *.so 

