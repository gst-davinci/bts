/*
//============================================================================
//
//    FILE NAME : ih264mpvdec.h
//
//    ALGORITHM : H264MPVDEC
//
//    VENDOR    : TI
//
//    TARGET DSP: C64x
//
//    PURPOSE   : IH264MPVDEC Interface Header
//
//============================================================================
*/
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2006 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */
#ifndef IH264MPVDEC_
#define IH264MPVDEC_

#include <xdc/std.h>
#include <ti/xdais/ialg.h>
#include <ti/xdais/dm/ividdec.h>

//Sreni: Added this because of adding sHrdParm_t structure
#define MAXCPBCNT 32

typedef enum profile{
    PROFILE_INVALID=-1,       //!<-1: Used to indicate unsupported profile
    BASELINE=0,              //!< 0: BASELINE PROFILE
    MAIN    =1,              //!< 1: MAIN PROFILE
    EXTENDED=2,              //!< 2: EXTENDED PROFILE
    MAX_PROFILES_TYPES=3     //!< Maximum Number of Profile == 3
}eH264MPVDEC_TI_Profile;


/*!
 *****************************************************************************
 * \enum  eLevelIdc
 * \brief Level Enumerators as per Annex A.
 *
 * This enumerator describes possible values of syntax element level_idc
 * which is equal to 10 times the value of level number
 *****************************************************************************/
typedef enum levelIdc{
  LEVEL_1 = 10, //!<Level 1.0
  LEVEL_1_1,    //!<Level 1.1
  LEVEL_1_2,    //!<Level 1.2
  LEVEL_1_3,    //!<Level 1.3
  LEVEL_2 = 20, //!<Level 2.0
  LEVEL_2_1,    //!<Level 2.1
  LEVEL_2_2,    //!<Level 2.2
  LEVEL_3 = 30, //!<Level 3.0
  LEVEL_3_1,    //!<Level 3.1
  LEVEL_3_2,    //!<Level 3.2
  LEVEL_4 = 40, //!<Level 4.0
  LEVEL_4_1,    //!<Level 4.1
  LEVEL_4_2,    //!<Level 4.2
  LEVEL_5 = 50, //!<Level 5.0
  LEVEL_5_1,    //!<Level 5.1
  LEVEL_INVALID = 0xFF, //!< Indicates Invalid level
  MAX_NUM_LEVELS = 15  //!< Size of Level Array
}eH264MPVDEC_TI_LevelIdc;

typedef struct  hrd_parm {
	unsigned int  cpb_cnt;                                          //!< ue(v)
	unsigned int  bit_rate_scale;                                   //!< u(4)
	unsigned int  cpb_size_scale;                                   //!< u(4)
	unsigned int  bit_rate_value [MAXCPBCNT];                       //!< ue(v)
	unsigned int  cpb_size_value[MAXCPBCNT];                        //!< ue(v)
	unsigned int  vbr_cbr_flag[MAXCPBCNT];                          //!< u(1)
	unsigned int  initial_cpb_removal_delay_length_minus1;          //!< u(5)
	unsigned int  cpb_removal_delay_length_minus1;                  //!< u(5)
	unsigned int  dpb_output_delay_length_minus1;                   //!< u(5)
	unsigned int  time_offset_length;                               //!< u(5)
} sHrdParm_t;

typedef struct  vui_seq_parm {
    unsigned int  parsed_flag;
  unsigned int  aspect_ratio_info_present_flag;                    //!< u(1)
	unsigned int  aspect_ratio_idc;                                  //!< u(8)
	unsigned int  sar_width;                                         //!< u(16)
	unsigned int  sar_height;                                        //!< u(16)
  unsigned int  overscan_info_present_flag;                        //!< u(1)
	unsigned int  overscan_appropriate_flag;                         //!< u(1)
  unsigned int  video_signal_type_present_flag;                    //!< u(1)
	unsigned int  video_format;                                      //!< u(3)
	unsigned int  video_full_range_flag;                             //!< u(1)
	unsigned int  colour_description_present_flag;                   //!< u(1)
	unsigned int  colour_primaries;                                  //!< u(8)
	unsigned int  transfer_characteristics;                          //!< u(8)
	unsigned int  matrix_coefficients;                               //!< u(8)
  unsigned int  chroma_location_info_present_flag;                 //!< u(1)
	unsigned int  chroma_sample_loc_type_top_field;                  //!< ue(v)
	unsigned int  chroma_sample_loc_type_bottom_field;               //!< ue(v)
  unsigned int  timing_info_present_flag;                          //!< u(1)
	unsigned int  num_units_in_tick;                                 //!< u(32)
	unsigned int  time_scale;                                        //!< u(32)
	unsigned int  fixed_frame_rate_flag;                             //!< u(1)
  unsigned int  nal_hrd_parameters_present_flag;                   //!< u(1)
	sHrdParm_t nal_hrd_parameters;                              //!< hrd_paramters_t
  unsigned int  vcl_hrd_parameters_present_flag;                   //!< u(1)
	sHrdParm_t vcl_hrd_parameters;                              //!< hrd_paramters_t
	unsigned int  low_delay_hrd_flag;                                //!< u(1)
  unsigned int  pic_struct_present_flag;                           //!< u(1)
  unsigned int  bitstream_restriction_flag;                        //!< u(1)
	unsigned int  motion_vectors_over_pic_boundaries_flag;           //!< u(1)
	unsigned int  max_bytes_per_pic_denom;                           //!< ue(v)
	unsigned int  max_bits_per_mb_denom;                             //!< ue(v)
	unsigned int  log2_max_mv_length_vertical;                       //!< ue(v)
	unsigned int  log2_max_mv_length_horizontal;                     //!< ue(v)
	unsigned int  num_reorder_frames;                                //!< ue(v)
	unsigned int  max_dec_frame_buffering;                           //!< ue(v)
} sVSP_t;


//#ifdef SEI_SUPPORT

typedef struct prog_refinement_start{
    unsigned int parsed_flag;
    unsigned int progressive_refinement_id;
    unsigned int num_refinement_steps_minus1;
} sProgRefineStart_t;

typedef struct prog_refinement_end{
    unsigned int parsed_flag;
    unsigned int progressive_refinement_id;
} sProgRefineEnd_t;

typedef struct recovery_point_info {
  unsigned int  parsed_flag;
  unsigned int  recovery_frame_cnt;
  unsigned char   exact_match_flag;
  unsigned char   broken_link_flag;
  unsigned char   changing_slice_group_idc;
} sRecoveryPointInfo_t;

typedef struct picture_timing_SEI{
   unsigned int parsed_flag;
   unsigned int cpb_removal_delay;
   unsigned int dpb_output_delay;
   unsigned int pic_struct;
   unsigned int clock_timestamp_flag;
   unsigned int ct_type;
   unsigned int nuit_field_based_flag;
   unsigned int counting_type;
   unsigned int full_timestamp_flag;
   unsigned int discontinuity_flag;
   unsigned int cnt_dropped_flag;
   unsigned int n_frames;
   unsigned int seconds_value;
   unsigned int minutes_value;
   unsigned int hours_value;
   unsigned int time_offset;
} sPictureTiming_t;

typedef struct frame_freeze_repetetion{
   unsigned int parsed_flag;
   unsigned int full_frame_freeze_repetition_period;
} sFullFrameFreezeRepetition_t;

typedef struct frame_freeze_release{
   unsigned int parsed_flag;
   unsigned char  full_frame_freeze_release_flag;
} sFullFrameFreezeRelease_t;

typedef struct sei_messages {
  unsigned int parsed_flag;
  sFullFrameFreezeRepetition_t frame_freeze_repetition;
  sFullFrameFreezeRelease_t frame_freeze_release;
  sProgRefineStart_t prog_refine_start;
  sProgRefineEnd_t   prog_refine_end;
  sRecoveryPointInfo_t recovery_pt_info;
  sPictureTiming_t   pic_timing;
} sSeiMessages_t;

//#endif /* SEI_SUPPORT */

typedef struct  sei_vui_parm {
   unsigned int parsed_flag;
   sVSP_t vui_params;
   sSeiMessages_t sei_messages;
} sSeiVuiParams_t;




/*
 *  ======== IH264MPVDEC_Handle ========
 *  This handle is used to reference all H264MPVDEC instance objects
 */
typedef struct IH264MPVDEC_Obj *IH264MPVDEC_Handle;

/*
 *  ======== IH264MPVDEC_Obj ========
 *  This structure must be the first field of all H264MPVDEC instance objects
 */
typedef struct IH264MPVDEC_Obj {
    struct IH264MPVDEC_Fxns *fxns;
} IH264MPVDEC_Obj;


//! H264MPVDEC_TI_picture structure type
typedef enum pic_struct {
  IH264MPVDEC_TOP_FIELD = 0,  //!< 0: top field
  IH264MPVDEC_BOT_FIELD,      //!< 1: bottom field
  IH264MPVDEC_FRAME,          //!< 2: frame
  IH264MPVDEC_MAXSTRUCT       //!< 3: size of this type array
} ePicStruct_t;

/*
 *  ======== IH264MPVDEC_Params ========
 *  This structure defines the creation parameters for all H264MPVDEC objects
 */
typedef struct IH264MPVDEC_Params {
    IVIDDEC_Params viddecParams; //Should be the first argument

} IH264MPVDEC_Params;

/*
 *  ======== IH264MPVDEC_Status ========
 *  Status structure defines the parameters that can be changed or read
 *  during real-time operation of the alogrithm.
 */
typedef struct IH264MPVDEC_Status {
    IVIDDEC_Status viddecStatus;   // Should be the first argument
	  eH264MPVDEC_TI_Profile profile;     // Profile of the bitstream
	  eH264MPVDEC_TI_LevelIdc level;             // Level number of the Bitstream
	  XDAS_Int32  Qp;                // Frame Quantization parameter

    //Added the following to capture VUI params including those for BT.601 color
	//params
#if 0
    unsigned int  vui_parameters_present_flag;                      //!< u(1)
    sVSP_t        vui_parameters;
#endif

//#ifdef EXTEND_STATUS
    /* Ramesh 20 Sep 2006. Adding some fields to extended params status
     * structure on customer request.
     * For erroneous FMO bitstreams, this field may not be so enlightening.
     */
    XDAS_UInt32 last_decoded_mb_addr;
    XDAS_UInt32 slice_header_frame_num;
    XDAS_UInt32 full_frame_decoded;
    XDAS_Int32 poc_num;

//#endif /* EXTEND_STATUS */



} IH264MPVDEC_Status;

/*
// ===========================================================================
// IH264MPVDEC_DynamicParams
//
// This structure defines the run time parameters for all H264DEC objects
*/
typedef struct IH264MPVDEC_DynamicParams {
    IVIDDEC_DynamicParams viddecDynamicParams; //Should be the first argument
} IH264MPVDEC_DynamicParams;

/*
// ===========================================================================
// IH264MPVDEC_InArgs
//
// This structure provides the Input parameters for H.264 Decode call
*/
typedef struct IH264MPVDEC_InArgs {
	IVIDDEC_InArgs viddecInArgs; //Should be the first argument
//#ifdef ENHANCED_FRAME_DISPLAY
    XDAS_Int32 maxDisplayDelay;
//#endif /*ENHANCED_FRAME_DISPLAY*/

//#ifdef SEI_SUPPORT
    XDAS_Int32       Sei_Vui_parse_flag;
    sSeiVuiParams_t *SeiVui_buffer_ptr;
//#endif SEI_SUPPORT

} IH264MPVDEC_InArgs;

/*
// ===========================================================================
// IH264MPVDEC_OutArgs
//
// This structure provides the Output parameters for H.264 Decode call
*/
typedef struct IH264MPVDEC_OutArgs {
	IVIDDEC_OutArgs viddecOutArgs; //Should be the first argument
  ePicStruct_t  pict_struct;

//#ifdef SKIP_SUPPORT
  XDAS_Int32 display_frame_skip_flag;
//#endif /* SKIP_SUPPORT */
//#ifdef SEI_SUPPORT
  sSeiVuiParams_t *SeiVui_buffer_ptr;
//#endif SEI_SUPPORT


} IH264MPVDEC_OutArgs;

/*
 *  ======== IH264MPVDEC_Cmd ========
 *  The Cmd enumeration defines the control commands for the H264MPVDEC
 *  control method.
 */

typedef IVIDDEC_Cmd IH264MPVDEC_Cmd;
/*
// ===========================================================================
// control method commands
*/
#define IH264MPVDEC_GETSTATUS      XDM_GETSTATUS
#define IH264MPVDEC_SETPARAMS      XDM_SETPARAMS
#define IH264MPVDEC_RESET          XDM_RESET
#define IH264MPVDEC_SETDEFAULT     XDM_SETDEFAULT
#define IH264MPVDEC_FLUSH          XDM_FLUSH
#define IH264MPVDEC_GETBUFINFO     XDM_GETBUFINFO



typedef struct IH264MPVDEC_Fxns {

  IVIDDEC_Fxns ividdec;

} IH264MPVDEC_Fxns;


/*
 *  ======== IH264MPVDEC_PARAMS ========
 *  Default parameter values for H264MPVDEC instance objects
 */
extern IH264MPVDEC_Params IH264MPVDEC_PARAMS;

/*******Added for IMCOP memory management during Hibernate**********/
typedef enum
{
	IMCOP_SAVE = 256,
	IMCOP_RESTORE,
	IMCOP_SET_TO_ZERO
}IMCOP_CONTROL_CMD;



#endif    /* IH264MPVDEC_ */

/* ======================================================================== */
/* End of file : ih264mpvdec.h                                              */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2006 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */

