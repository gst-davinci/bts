/* 
 *  Copyright 2006
 *  Texas Instruments Incorporated
 *
 *  All rights reserved.  Property of Texas Instruments Incorporated
 *  Restricted rights to use, duplicate or disclose this code are
 *  granted through contract.
 * 
 */

/**
 *  @file       xdm.h
 *
 *  @brief      This header defines all types, constants, and functions
 *              shared across the various xDM classes of algorithms.
 *
 *  @version    0.4
 */
/**
 *  @defgroup   XDM       xDM Interface
 *
 *  This is the xDM interface.
 */

#ifndef XDM_
#define XDM_

#include <ti/xdais/ialg.h>
#include <ti/xdais/xdas.h>

#ifdef __cplusplus
extern "C" {
#endif

/** @ingroup    XDM */
/*@{*/

#define XDM_EOK                 IALG_EOK    /**< Success. */
#define XDM_EFAIL               IALG_EFAIL  /**< General failure. */
#define XDM_ERUNTIME            -2          /**< General runtime failure. */

#define XDM_MAX_IO_BUFFERS      16          /**< Max I/O Buffers */

/**
 *  @brief      Buffer descriptor for input and output buffers.
 */
typedef struct XDM_BufDesc {
    XDAS_Int8   **bufs;    /**< Pointer to a vector containing buffer
                            *   addresses.
                            */
    XDAS_Int32   numBufs;  /**< Number of buffers. */
    XDAS_Int32  *bufSizes; /**< Size of each buffer in 8-bit bytes. */
} XDM_BufDesc;

/**
 *  @brief      Buffer information descriptor for input and output buffers.
 */
typedef struct XDM_AlgBufInfo {
    XDAS_Int32 minNumInBufs;       /**< Minimum number of input buffers. */
    XDAS_Int32 minNumOutBufs;      /**< Minimum number of output buffers. */
    XDAS_Int32 minInBufSize[XDM_MAX_IO_BUFFERS];  /**< Minimum size, in 8-bit
                                    * bytes, required for each input buffer.
                                    */
    XDAS_Int32 minOutBufSize[XDM_MAX_IO_BUFFERS]; /**< Minimum size, in 8-bit
                                    * bytes, required for each output buffer.
                                    */
} XDM_AlgBufInfo;


/**
 *  @brief      Standard control commands that must be implemented by
 *              xDM compliant multimedia algorithms.
 *
 *  @remarks    Any control ID extension in IMOD interface should start
 *              from 256 onward.  The ID range from 0 to 255 is
 *              reserved.
 */
typedef enum {
    XDM_GETSTATUS = 0,     /**< Query algorithm to fill status structure. */
    XDM_SETPARAMS,         /**< Set run time dynamic parameters. */
    XDM_RESET,             /**< Reset the algorithm.  All fields in the
                            *   internal data structures are reset and all
                            *   internal buffers are flushed.
                            */
    XDM_SETDEFAULT,        /**< Initialize all fields in the param
                            *   structure to their default values.  The
                            *   application can change specific
                            *   parameters using XDM_SETPARAMS.
                            */
    XDM_FLUSH,             /**< Handle end of stream conditions.  This
                            *   command forces the algorithm to output
                            *   data without additional input.  The
                            *   recommended sequence is to call the
                            *   control() function (with XDM_FLUSH)
                            *   followed repeated calls to the
                            *   process() function until it returns an
                            *   error.
                            */
    XDM_GETBUFINFO         /**< Query algorithm instance regarding its
                            *   properties of input and output
                            *   buffers.
                            */
} XDM_CmdId;

/**
 *  @brief      Extended error enumeration for xDM compliant encoders
 *              and decoders.
 *
 *  @remarks    The value of each enum is the bit which is set.
 *
 *  @remarks    Bits 32-16 and bit 8 are reserved.  Bits 7-0 are codec and
 *              implementation specific.
 *
 *  @remarks    Algorithm will set multiple bits to 1 based on conditions.
 *              e.g. it will set bits #XDM_FATALERROR (fatal) and
 *              #XDM_UNSUPPORTEDPARAM (unsupported params) in case
 *              of unsupported run time parameters.
 *
 *  @remarks    Some errors are applicable to decoders only.
 */
typedef enum {
    XDM_APPLIEDCONCEALMENT = 9,  /**< Bit 9 - Applied concealment. */
    XDM_INSUFFICIENTDATA = 10,   /**< Bit 10 - Insufficient input data. */
    XDM_CORRUPTEDDATA = 11,      /**< Bit 11 - Data problem/corruption. */
    XDM_CORRUPTEDHEADER = 12,    /**< Bit 12 - Header problem/corruption. */
    XDM_UNSUPPORTEDINPUT = 13,   /**< Bit 13 - Unsupported feature/parameter
                                  *   in input.
                                  */
    XDM_UNSUPPORTEDPARAM = 14,   /**< Bit 14 - Unsupported input parameter or
                                  *   configuration.
                                  */
    XDM_FATALERROR = 15          /**< Bit 15 - Fatal error (stop the codec).
                                  *   If there is an error and this
                                  *   bit is not set, the error is a
                                  *   recoverable one.
                                  */
} XDM_ErrorBit;

/** Check for fatal error */
#define XDM_ISFATALERROR(x)         (((x) >> XDM_FATALERROR) & 0x1)
/** Check for unsupported parameter */
#define XDM_ISUNSUPPORTEDPARAM(x)   (((x) >> XDM_UNSUPPORTEDPARAM) & 0x1)
/** Check for unsupported input */
#define XDM_ISUNSUPPORTEDINPUT(x)   (((x) >> XDM_UNSUPPORTEDINPUT) & 0x1)
/** Check for corrupted header */
#define XDM_ISCORRUPTEDHEADER(x)    (((x) >> XDM_CORRUPTEDHEADER) & 0x1)
/** Check for corrupted data */
#define XDM_ISCORRUPTEDDATA(x)      (((x) >> XDM_CORRUPTEDDATA) & 0x1)
/** Check for insufficient data */
#define XDM_ISINSUFFICIENTDATA(x)   (((x) >> XDM_INSUFFICIENTDATA) & 0x1)
/** Check for applied concealment */
#define XDM_ISAPPLIEDCONCEALMENT(x) (((x) >> XDM_APPLIEDCONCEALMENT) & 0x1)

/** Set fatal error bit */
#define XDM_SETFATALERROR(x)         ((x) |= (0x1 << XDM_FATALERROR))
/** Set unsupported parameter bit */
#define XDM_SETUNSUPPORTEDPARAM(x)   ((x) |= (0x1 << XDM_UNSUPPORTEDPARAM))
/** Set unsupported input bit */
#define XDM_SETUNSUPPORTEDINPUT(x)   ((x) |= (0x1 << XDM_UNSUPPORTEDINPUT))
/** Set corrupted header bit */
#define XDM_SETCORRUPTEDHEADER(x)    ((x) |= (0x1 << XDM_CORRUPTEDHEADER))
/** Set corrupted data bit */
#define XDM_SETCORRUPTEDDATA(x)      ((x) |= (0x1 << XDM_CORRUPTEDDATA))
/** Set insufficient data bit */
#define XDM_SETINSUFFICIENTDATA(x)   ((x) |= (0x1 << XDM_INSUFFICIENTDATA))
/** Set applied concealment bit */
#define XDM_SETAPPLIEDCONCEALMENT(x) ((x) |= (0x1 << XDM_APPLIEDCONCEALMENT))

/**
 *  @brief      Endianness of data
 */
typedef enum {
    XDM_BYTE = 1,           /**< Big endian stream. */
    XDM_LE_16,              /**< 16 bit little endian stream. */
    XDM_LE_32               /**< 32 bit little endian stream. */
} XDM_DataFormat;

/**
 *  @brief      Encoding presets.
 */
typedef enum {
    XDM_DEFAULT = 0,        /**< Default setting of encoder.  See
                             *   codec specific documentation for its
                             *   encoding behaviour.
                             */
    XDM_HIGH_QUALITY,       /**< High quality encoding. */
    XDM_HIGH_SPEED,         /**< High speed encoding. */
    XDM_USER_DEFINED        /**< User defined configuration, using
                             *   advanced parameters.
                             */
} XDM_EncodingPreset;

/**
 *  @brief      Decode entire access unit or only header.
 */
typedef enum {
    XDM_DECODE_AU = 0,      /**< Decode entire access unit, including all
                             *   the headers.
                             */
    XDM_PARSE_HEADER        /**< Decode only header. */
} XDM_DecMode;

/**
 *  @brief      Encode entire access unit or only header
 */
typedef enum {
    XDM_ENCODE_AU = 0,
    XDM_GENERATE_HEADER
} XDM_EncMode;

/**
 *  @brief      Chroma formats
 */
typedef enum {
    XDM_YUV_420P = 1,          /**< YUV 4:2:0 planer. */
    XDM_YUV_422P,              /**< YUV 4:2:2 planer. */
    XDM_YUV_422IBE,            /**< YUV 4:2:2 interleaved (big endian). */
    XDM_YUV_422ILE,            /**< YUV 4:2:2 interleaved (little endian). */
    XDM_YUV_444P,              /**< YUV 4:4:4 planer. */
    XDM_YUV_411P,              /**< YUV 4:1:1 planer. */
    XDM_GRAY,                  /**< Gray format. */
    XDM_RGB                    /**< RGB color format. */
} XDM_ChromaFormat;

#ifdef __cplusplus
}
#endif

/*@}*/  /* ingroup */

#endif  /* XDM_ */
/*
 *  @(#) ti.xdais.dm 1, 0, 0, 0,20 4-20-2006 dais-f06
*/

