/*
 * dataTypes.h
 *
 * Header file for data Types used in AVI Muxer/Parser plugin
 *
 * Copyright (C) 2007 Texas Instruments, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#ifndef _DATATYPES_H_
#define _DATATYPES_H_


typedef unsigned char BYTE;

typedef short INT16;
typedef unsigned short UINT16;

typedef long INT32;
typedef unsigned long  UINT32;

typedef long long  INT64;
typedef unsigned long long  UINT64;

typedef UINT32  BOOL;

typedef void  VOID;
typedef VOID*  HANDLE;

typedef char  CHAR;
typedef CHAR*  STR;

typedef UINT16  WCHAR;
typedef WCHAR*  WSTR;

#ifdef  _64BIT_PORTABILITY
typedef  UINT64  PARAM;
#else
typedef  UINT32  PARAM;
#endif

typedef  UINT64  LPARAM;

typedef struct struct_GUID
{
     UINT32               Data1;
     UINT16               Data2;
     UINT16               Data3;
     BYTE                 Data4[8];
}GUID;

/*
 *    Error Handling
 *
 */

typedef  INT32  STATUS;

#define  SUCCEEDED(_ps)  (0 <= (_ps))
#define  FAILED(_ps)     (0 > (_ps))

//#define  TRUE  1
//#define  FALSE 0
//#define  NULL  0

#endif  /* _DATATYPES_H_ */

