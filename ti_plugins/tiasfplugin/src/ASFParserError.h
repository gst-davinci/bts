/*
 * ASFParserError.h
 *
 * Header file for data Types in ASF Demuxer/Parser plugin 
 * for Gstreamer implementation on DM644x EVM
 *
 * Copyright (C) 2007 Texas Instruments, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#ifndef _ASF_PARSER_ERROR_H_
#define _ASF_PARSER_ERROR_H_

#include <dataTypes.h>

#define ASF_SEVERITY_SUCCESS                0uL
#define ASF_SEVERITY_ERROR                  1uL
#define ASF_S_BASECODE                      0xC000
#define ASF_E_BASECODE                      0xC000

#define MAKE_ASF_RESULT(sev, code) \
            ((STATUS) (((unsigned long)(sev)<<31) | \
                                ((unsigned long)(code))) )
                                
/* ASF parsing errors */
#define ASF_SUCCESS             MAKE_ASF_RESULT( ASF_SEVERITY_SUCCESS,\
                                                 ASF_S_BASECODE + 3000 )
#define ASF_E_BADASFHEADER      MAKE_ASF_RESULT( ASF_SEVERITY_ERROR,\
                                                 ASF_E_BASECODE + 4000 )
#define ASF_E_BADPACKETHEADER   MAKE_ASF_RESULT( ASF_SEVERITY_ERROR,\
                                                 ASF_E_BASECODE + 4001 )
#define ASF_E_BADPAYLOADHEADER  MAKE_ASF_RESULT( ASF_SEVERITY_ERROR,\
                                                 ASF_E_BASECODE + 4002 )
#define ASF_E_BADDATAHEADER     MAKE_ASF_RESULT( ASF_SEVERITY_ERROR,\
                                                 ASF_E_BASECODE + 4003 )
#define ASF_E_INVALIDOPERATION  MAKE_ASF_RESULT( ASF_SEVERITY_ERROR,\
                                                 ASF_E_BASECODE + 4004 )
#define ASF_E_INVALIDARG        MAKE_ASF_RESULT( ASF_SEVERITY_ERROR,\
                                                 ASF_E_BASECODE + 4005 )
#define ASF_E_ENCRYPTOBJNOTFOUND MAKE_ASF_RESULT( ASF_SEVERITY_ERROR,\
                                                 ASF_E_BASECODE + 4006 )
#define ASF_E_LICENSENOTFOUND   MAKE_ASF_RESULT( ASF_SEVERITY_ERROR,\
                                                 ASF_E_BASECODE + 4007 )
#define ASF_E_FILENOTDRMPROTECTED  MAKE_ASF_RESULT( ASF_SEVERITY_ERROR,\
                                                 ASF_E_BASECODE + 4008 )
#define ASF_E_BUFINSUFFICIENT    MAKE_ASF_RESULT( ASF_SEVERITY_ERROR,\
                                                 ASF_E_BASECODE + 4009 )
#define ASF_E_ENDOFFILE          MAKE_ASF_RESULT( ASF_SEVERITY_ERROR,\
                                                 ASF_E_BASECODE + 4010 )
#endif //_ASF_PARSER_ERROR_H_
