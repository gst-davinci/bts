/** ==================================================================
* @file loadstuff.h
*
* @path $(codecs_dev)\WMA_Decoder\AlgSource\GoldenC\Include\configurable\WMA_specific
*
* @desc This File contains macros for loading bitstream data.
*
* =====================================================================
* Copyright (c) Texas Instruments Inc 2003, 2004
*
* Use of this software is controlled by the terms and conditions found
* in the license agreement under which this software has been supplied
=====================================================================*/


//*@@@+++@@@@******************************************************************
//
// Microsoft Windows Media
// Copyright (C) Microsoft Corporation. All rights reserved.
//
//*@@@---@@@@******************************************************************
//+-------------------------------------------------------------------------
//
//  Microsoft Windows Media
//
//  Copyright (C) Microsoft Corporation, 1999 - 1999
//
//  File:       loadStuff.h
//
//--------------------------------------------------------------------------

#ifndef LOADSTUFF_H
#define LOADSTUFF_H

/**** Robustness: START OF BLOCK ****/

#ifndef UNALIGNED
#define UNALIGNED
#endif


/****************************************************************************/
#ifndef _BOOL_DEFINED
#define _BOOL_DEFINED
typedef long BOOL;
#endif /* _BOOL_DEFINED */

#ifndef _U8_DEFINED
#define _U8_DEFINED
typedef unsigned char   U8;
#endif /* _U8_DEFINED */

#ifndef _WORD_DEFINED
#define _WORD_DEFINED
typedef unsigned short  WORD;
#endif /* _WORD_DEFINED */

#ifndef _DWORD_DEFINED
#define _DWORD_DEFINED
typedef unsigned int    DWORD; 
#endif /* _DWORD_DEFINED */

#ifndef _QWORD_DEFINED
#define _QWORD_DEFINED
typedef PACKED struct tQWORD
{
    DWORD   dwLo;
    DWORD   dwHi;

}   QWORD;
#endif /* _QWORD_DEFINED */
#ifndef GUID_DEFINED
#define GUID_DEFINED

typedef struct {          // size is 16
    tWMA_U32  Data1;
    tWMA_U16 Data2;
    tWMA_U16 Data3;
    tWMA_U8  Data4[8];
} GUID;

#endif // !GUID_DEFINED
#define WAVE_FORMAT_WMAUDIO3  0x0162
#define WAVE_FORMAT_WMAUDIO2  0x0161
#define WAVE_FORMAT_MSAUDIO1  0x0160
#define WAVE_FORMAT_WMAUDIO_LOSSLESS  0x0163

#ifdef _MSC_VER
#define LITTLE_ENDIAN
#endif

#ifdef LITTLE_ENDIAN
#if 1// C64X
#define GetUnalignedWord( pb, w )   (w) = (WORD)(*pb) + ((WORD) *(pb+1) << 8); 
#else
#define GetUnalignedWord( pb, w )   (w) = *(UNALIGNED WORD*)(pb); 
#endif // C64X

#if 1 // C64X
#define GetUnalignedDword( pb, dw ) (dw) = (DWORD) *pb + ((DWORD) *(pb+1) <<8) \
                                           + ((DWORD) *(pb+2) <<16) \
                                           + ((DWORD) *(pb+3) <<24);
#else
#define GetUnalignedDword( pb, dw ) (dw) = *(UNALIGNED DWORD*)(pb);
#endif // C64X

#if 1 // C64X
#define GetUnalignedQword( pb, qw ) \
                 GetUnalignedDword( pb, (qw).dwLo ); \
                 GetUnalignedDword( (pb + 4), (qw).dwHi );
#else
#define GetUnalignedQword( pb, qw ) (qw) = *(UNALIGNED QWORD*)(pb);
#endif // C64X

#define GetUnalignedDouble( pb, d ) (d) = *(UNALIGNED Double*)(pb);

#else

#define GetUnalignedWord( pb, w ) \
            (w) = ((WORD) *(pb + 1) << 8) + *pb;

#define GetUnalignedDword( pb, dw ) \
            (dw) = ((DWORD) *(pb + 3) << 24) + \
                   ((DWORD) *(pb + 2) << 16) + \
                   ((DWORD) *(pb + 1) << 8) + *pb;

#define GetUnalignedQword( pb, qw ) \
            GetUnalignedDword( pb, (qw).dwLo ); \
            GetUnalignedDword( (pb + 4), (qw).dwHi );

#define GetUnalignedDouble( pb, d ) (d) = *(UNALIGNED Double*)(pb);

#endif

#define GetUnalignedWordEx( pb, w )     GetUnalignedWord( pb, w ); (pb) += sizeof(WORD);
#define GetUnalignedDwordEx( pb, dw )   GetUnalignedDword( pb, dw ); (pb) += sizeof(DWORD);
#define GetUnalignedQwordEx( pb, qw )   GetUnalignedQword( pb, qw ); (pb) += sizeof(QWORD);
#define GetUnalignedDoubleEx( pb, d )   GetUnalignedDouble( pb, d ); (pb) += sizeof(Double);

#define LoadBYTE( b, p )    b = *(U8 *)(p);  (p) += sizeof( U8 )

#define LoadWORD( w, p )    GetUnalignedWordEx( p, w )
#define LoadDWORD( dw, p )  GetUnalignedDwordEx( p, dw )
#define LoadQWORD( qw, p )  GetUnalignedQwordEx( p, qw )

#define LoadGUID( g, p ) \
        { \
            LoadDWORD( (g).Data1, p ); \
            LoadWORD( (g).Data2, p ); \
            LoadWORD( (g).Data3, p ); \
            LoadBYTE( (g).Data4[0], p ); \
            LoadBYTE( (g).Data4[1], p ); \
            LoadBYTE( (g).Data4[2], p ); \
            LoadBYTE( (g).Data4[3], p ); \
            LoadBYTE( (g).Data4[4], p ); \
            LoadBYTE( (g).Data4[5], p ); \
            LoadBYTE( (g).Data4[6], p ); \
            LoadBYTE( (g).Data4[7], p ); \
        }

#endif  // LOADSTUFF_H
