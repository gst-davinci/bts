/*
 *  Copyright 2006 by Texas Instruments Incorporated.
 *
 *  All rights reserved. Property of Texas Instruments Incorporated.
 *  Restricted rights to use, duplicate or disclose this code are
 *  granted through contract.
 *
 */

/*
 *  ======== xdc/rts/Error.h ========
 *
 *  DO NOT MODIFY THIS FILE
 *
 *  It is generated from xdc/rts/Error.xdc and any changes may be overwritten.
 */

#ifdef xdc_rts_Error___EXTERNAL_SYNOPSIS
#error do not compile!!


    #include <xdc/rts/Error.h>

    /* -------- enums -------- */

    enum Error_Policy {
        Error_TERMINATE,
        Error_UNWIND
    };

    /* -------- structs -------- */

    struct Error_Block {
        Int _code;
        String _msg;
        Error_Block* _prev;
    };

    /* -------- module-wide configs -------- */

    Error_Policy Error_policy();
    Void (*Error_raiseHook())(Error_Block*);

    /* -------- module-wide functions -------- */

    Bool Error_check( Error_Block* eb );
    Void Error_clear( Error_Block* eb );
    Void Error_enter( Error_Block* eb, Error_Block* prev );
    Int Error_getCode( Error_Block* eb );
    String Error_getMsg( Error_Block* eb );
    Void Error_leave( Error_Block* eb );
    Void Error_raise( Error_Block* eb, Int code, String msg );


#endif /* xdc_rts_Error___EXTERNAL_SYNOPSIS */



#ifdef xdc_rts_Error___INTERNAL_SYNOPSIS
#error do not compile!!


    #include "package/internal/Error.xdc.h"


#endif /* xdc_rts_Error___INTERNAL_SYNOPSIS */







#ifdef __cplusplus
#define __extern extern "C"
#else
#define __extern extern
#endif

#ifndef xdc_std__include
#ifndef __nested__
#define __nested__
#include <xdc/std.h>
#undef __nested__
#else
#include <xdc/std.h>
#endif
#endif

#ifndef xdc_rts_Error__include
#define xdc_rts_Error__include

#ifndef xdc_rts__
#include <xdc/rts/package/package.defs.h>
#endif

#ifndef xdc_rts_IModule__include
#ifndef __nested__
#define __nested__
#include <xdc/rts/IModule.h>
#undef __nested__
#else
#include <xdc/rts/IModule.h>
#endif
#endif


/* auxiliary definitions */
struct xdc_rts_Error_Block {
    xdc_Int _code;
    xdc_String _msg;
    xdc_rts_Error_Block* _prev;
};
enum xdc_rts_Error_Policy {
    xdc_rts_Error_TERMINATE,
    xdc_rts_Error_UNWIND
};
typedef enum xdc_rts_Error_Policy xdc_rts_Error_Policy;

/* internal definitions */

/* module-wide configs */
struct xdc_rts_Error_Config {
    int __size;
    xdc_rts_Error_Policy policy;
    xdc_Void (*raiseHook)(xdc_rts_Error_Block*);
};

__extern const xdc_rts_Error_Config xdc_rts_Error_CONFIG;

#define xdc_rts_Error_policy() (xdc_rts_Error_CONFIG.policy)
#define xdc_rts_Error_raiseHook() (xdc_rts_Error_CONFIG.raiseHook)

/* function declarations */
#ifdef xdc_rts_Error_check__inline
#define xdc_rts_Error_check xdc_rts_Error_check__F
static inline xdc_Bool xdc_rts_Error_check__F( xdc_rts_Error_Block* eb );
#elif !defined(xdc_rts_Error_check__macro)
#define xdc_rts_Error_check xdc_rts_Error_check__E
__extern xdc_Bool xdc_rts_Error_check__E( xdc_rts_Error_Block* eb );
__extern xdc_Bool xdc_rts_Error_check__F( xdc_rts_Error_Block* eb );
#endif
#ifdef xdc_rts_Error_clear__inline
#define xdc_rts_Error_clear xdc_rts_Error_clear__F
static inline xdc_Void xdc_rts_Error_clear__F( xdc_rts_Error_Block* eb );
#elif !defined(xdc_rts_Error_clear__macro)
#define xdc_rts_Error_clear xdc_rts_Error_clear__E
__extern xdc_Void xdc_rts_Error_clear__E( xdc_rts_Error_Block* eb );
__extern xdc_Void xdc_rts_Error_clear__F( xdc_rts_Error_Block* eb );
#endif
#ifdef xdc_rts_Error_getCode__inline
#define xdc_rts_Error_getCode xdc_rts_Error_getCode__F
static inline xdc_Int xdc_rts_Error_getCode__F( xdc_rts_Error_Block* eb );
#elif !defined(xdc_rts_Error_getCode__macro)
#define xdc_rts_Error_getCode xdc_rts_Error_getCode__E
__extern xdc_Int xdc_rts_Error_getCode__E( xdc_rts_Error_Block* eb );
__extern xdc_Int xdc_rts_Error_getCode__F( xdc_rts_Error_Block* eb );
#endif
#ifdef xdc_rts_Error_getMsg__inline
#define xdc_rts_Error_getMsg xdc_rts_Error_getMsg__F
static inline xdc_String xdc_rts_Error_getMsg__F( xdc_rts_Error_Block* eb );
#elif !defined(xdc_rts_Error_getMsg__macro)
#define xdc_rts_Error_getMsg xdc_rts_Error_getMsg__E
__extern xdc_String xdc_rts_Error_getMsg__E( xdc_rts_Error_Block* eb );
__extern xdc_String xdc_rts_Error_getMsg__F( xdc_rts_Error_Block* eb );
#endif
#ifdef xdc_rts_Error_leave__inline
#define xdc_rts_Error_leave xdc_rts_Error_leave__F
static inline xdc_Void xdc_rts_Error_leave__F( xdc_rts_Error_Block* eb );
#elif !defined(xdc_rts_Error_leave__macro)
#define xdc_rts_Error_leave xdc_rts_Error_leave__E
__extern xdc_Void xdc_rts_Error_leave__E( xdc_rts_Error_Block* eb );
__extern xdc_Void xdc_rts_Error_leave__F( xdc_rts_Error_Block* eb );
#endif
#ifdef xdc_rts_Error_enter__inline
#define xdc_rts_Error_enter xdc_rts_Error_enter__F
static inline xdc_Void xdc_rts_Error_enter__F( xdc_rts_Error_Block* eb, xdc_rts_Error_Block* prev );
#elif !defined(xdc_rts_Error_enter__macro)
#define xdc_rts_Error_enter xdc_rts_Error_enter__E
__extern xdc_Void xdc_rts_Error_enter__E( xdc_rts_Error_Block* eb, xdc_rts_Error_Block* prev );
__extern xdc_Void xdc_rts_Error_enter__F( xdc_rts_Error_Block* eb, xdc_rts_Error_Block* prev );
#endif
#ifdef xdc_rts_Error_raise__inline
#define xdc_rts_Error_raise xdc_rts_Error_raise__F
static inline xdc_Void xdc_rts_Error_raise__F( xdc_rts_Error_Block* eb, xdc_Int code, xdc_String msg );
#elif !defined(xdc_rts_Error_raise__macro)
#define xdc_rts_Error_raise xdc_rts_Error_raise__E
__extern xdc_Void xdc_rts_Error_raise__E( xdc_rts_Error_Block* eb, xdc_Int code, xdc_String msg );
__extern xdc_Void xdc_rts_Error_raise__F( xdc_rts_Error_Block* eb, xdc_Int code, xdc_String msg );
#endif

/* C++ wrappers */
#ifdef __cplusplus
namespace xdc_rts { namespace Error {
    typedef xdc_rts_Error_Block Block;
    typedef xdc_rts_Error_Policy Policy;
    static const Policy TERMINATE = xdc_rts_Error_TERMINATE;
    static const Policy UNWIND = xdc_rts_Error_UNWIND;
    static inline xdc_Bool check( xdc_rts_Error_Block* eb) { return xdc_rts_Error_check(eb); }
    static inline xdc_Void clear( xdc_rts_Error_Block* eb) { xdc_rts_Error_clear(eb); }
    static inline xdc_Int getCode( xdc_rts_Error_Block* eb) { return xdc_rts_Error_getCode(eb); }
    static inline xdc_String getMsg( xdc_rts_Error_Block* eb) { return xdc_rts_Error_getMsg(eb); }
    static inline xdc_Void leave( xdc_rts_Error_Block* eb) { xdc_rts_Error_leave(eb); }
    static inline xdc_Void enter( xdc_rts_Error_Block* eb, xdc_rts_Error_Block* prev) { xdc_rts_Error_enter(eb, prev); }
    static inline xdc_Void raise( xdc_rts_Error_Block* eb, xdc_Int code, xdc_String msg) { xdc_rts_Error_raise(eb, code, msg); }
}}
using namespace xdc_rts;
#endif

#endif /* xdc_rts_Error__include */

/* __prefix alises */
#if !defined(__nested__) && !defined(xdc_rts_Error__aliases) && !defined(xdc_rts_Error__nolocalnames)
#define xdc_rts_Error__aliases
#define Error_Block xdc_rts_Error_Block
#define Error_Policy xdc_rts_Error_Policy
#define Error_TERMINATE xdc_rts_Error_TERMINATE
#define Error_UNWIND xdc_rts_Error_UNWIND
#define Error_policy xdc_rts_Error_policy
#define Error_raiseHook xdc_rts_Error_raiseHook
#define Error_Config xdc_rts_Error_Config
#define Error_CONFIG xdc_rts_Error_CONFIG
#define Error_check xdc_rts_Error_check
#define Error_clear xdc_rts_Error_clear
#define Error_getCode xdc_rts_Error_getCode
#define Error_getMsg xdc_rts_Error_getMsg
#define Error_leave xdc_rts_Error_leave
#define Error_enter xdc_rts_Error_enter
#define Error_raise xdc_rts_Error_raise
#endif


/*
 *  @(#) xdc.rts 1, 0, 0, 0,132 1-2-2006 xdc-m52
*/

