/*
 * AVIParserError.h
 *
 * Header file for AVI Parser Error used in AVI Muxer/Parser plugin
 *
 * Copyright (C) 2007 Texas Instruments, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */


#ifndef _AVI_PARSER_ERROR_H_
#define _AVI_PARSER_ERROR_H_

#include <dataTypes.h>

#define AVI_SEVERITY_SUCCESS                0uL
#define AVI_SEVERITY_ERROR                  1uL
#define AVI_S_BASECODE                      0xC000
#define AVI_E_BASECODE                      0xC000

#define MAKE_AVI_RESULT(sev, code) \
            ((STATUS) (((unsigned long)(sev)<<31) | \
                                ((unsigned long)(code))) )
                                
/* AVI parsing errors */
#define AVI_SUCCESS             MAKE_AVI_RESULT( AVI_SEVERITY_SUCCESS,\
                                                 AVI_S_BASECODE + 3000 )
#define AVI_E_BADAVIHEADER      MAKE_AVI_RESULT( AVI_SEVERITY_ERROR,\
                                                 AVI_E_BASECODE + 4000 )
#define AVI_E_BADPACKETHEADER   MAKE_AVI_RESULT( AVI_SEVERITY_ERROR,\
                                                 AVI_E_BASECODE + 4001 )
#define AVI_E_BADPAYLOADHEADER  MAKE_AVI_RESULT( AVI_SEVERITY_ERROR,\
                                                 AVI_E_BASECODE + 4002 )
#define AVI_E_BADDATAHEADER     MAKE_AVI_RESULT( AVI_SEVERITY_ERROR,\
                                                 AVI_E_BASECODE + 4003 )
#define AVI_E_INVALIDOPERATION  MAKE_AVI_RESULT( AVI_SEVERITY_ERROR,\
                                                 AVI_E_BASECODE + 4004 )
#define AVI_E_INVALIDARG        MAKE_AVI_RESULT( AVI_SEVERITY_ERROR,\
                                                 AVI_E_BASECODE + 4005 )
#define AVI_E_ENCRYPTOBJNOTFOUND MAKE_AVI_RESULT( AVI_SEVERITY_ERROR,\
                                                 AVI_E_BASECODE + 4006 )
#define AVI_E_LICENSENOTFOUND   MAKE_AVI_RESULT( AVI_SEVERITY_ERROR,\
                                                 AVI_E_BASECODE + 4007 )
#define AVI_E_FILENOTDRMPROTECTED  MAKE_AVI_RESULT( AVI_SEVERITY_ERROR,\
                                                 AVI_E_BASECODE + 4008 )
#define AVI_E_BUFINSUFFICIENT    MAKE_AVI_RESULT( AVI_SEVERITY_ERROR,\
                                                 AVI_E_BASECODE + 4009 )
#define AVI_E_ENDOFFILE          MAKE_AVI_RESULT( AVI_SEVERITY_ERROR,\
                                                 AVI_E_BASECODE + 4010 )
#endif //_AVI_PARSER_ERROR_H_
