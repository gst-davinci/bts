/*
 * Plugin Name : adecoder
 * Description : A Generic (ARM/DSP based) Audio Decoder for for TI
 *               Davinci DM644x
 *
 * Copyright (C) 2007 Texas Instruments, Inc.
 * Copyright (C) 2008 Behind The Set, LLC
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */



#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gstadecoder.h"

typedef enum {
    ST_AUDIO_ARM_MP3,
    ST_AUDIO_AAC,
    ST_AUDIO_WMA,
    ST_AUDIO_DSP_MP3,
}CODECS;

typedef enum {
    PROP_0,			//prop_id should be greater than 0.
    ARG_ENGINE_NAME,
    ARG_ENGINE_OPEN,
    ARG_CODEC_NAME,
    ARG_ENABLE_PROCESSING,
}PROPERTIES;

typedef enum {
    H264ENGINE,
    MPEG4ENGINE,
    WMENGINE,
    MP3ENGINE
}ENGINE;

GST_DEBUG_CATEGORY_STATIC(gstadecoder_debug);
#define GST_CAT_DEFAULT (gstadecoder_debug)

/* Declarations */
static GType gst_adecoder_get_engine_type(void);
static GType gst_adecoder_get_codec_type(void);
static gboolean gst_adecoder_query(GstElement *element, GstQuery *query);
static void gst_adecoder_base_init(GstADecoderClass * klass);
static void gst_adecoder_class_init(GstADecoderClass * klass);
static void gst_adecoder_init(GstADecoder * adecoder);
static GstFlowReturn gst_adecoder_chain(GstPad * pad, GstBuffer * buffer);
static GstStateChangeReturn gst_adecoder_change_state(GstElement * element,GstStateChange transition);
static gboolean gst_adecoder_decoder_initialize(GstADecoder * adecoder,GstBuffer * input_buffer);
static GstFlowReturn gst_adecoder_send_data(GstADecoder * adecoder,GstBuffer * buffer);
static GstCaps *gst_adecoder_sink_getcaps(GstPad * pad);
static GstCaps *gst_adecoder_src_getcaps(GstPad * pad);
static gboolean gst_adecoder_sink_setcaps(GstPad * pad, GstCaps * caps);
static gboolean gst_adecoder_src_event(GstPad * pad, GstEvent * event);
static gboolean gst_adecoder_sink_event(GstPad * pad, GstEvent * event);
static gboolean gst_adecoder_sink_activate(GstPad * sinkpad);
static gboolean gst_adecoder_sink_activate_pull(GstPad * sinkpad,gboolean active);
static void gst_adecoder_loop(GstPad * pad);
static void block_to_interleaved(XDAS_Int16 *src,XDAS_Int8 *dest,guint32 frameLen);
static GstElementClass *parent_class = NULL;

#ifdef PROFILE
static unsigned long timeDiffMs(struct timespec *pTs1, struct timespec *pTs2);
#endif
static gboolean gst_adecoder_loop_get_data(GstADecoder *adecoder);
static gboolean gst_adecoder_chain_get_data(GstADecoder *adecoder,GstBuffer * input_buffer);
static gboolean gst_adecoder_process_data(GstADecoder *adecoder);
static void gst_adecoder_initialize_osssink(GstADecoder *adecoder);
static gboolean gst_adecoder_output_data(GstADecoder *adecoder);

static void gst_adecoder_initialize_handle(GstADecoder * adecoder);
static gboolean gst_adecoder_open_engine(GstADecoder * adecoder);
static gboolean gst_adecoder_create_codec(GstADecoder * adecoder,GstBuffer * input_buffer);
static gboolean gst_adecoder_initialize_codec(GstADecoder * adecoder);
static void gst_adecoder_decode_remaining(GstADecoder * adecoder);

static void gst_adecoder_set_property (GObject * object, guint prop_id, const GValue * value, GParamSpec * pspec);


#define translate_DspCommError(x) AVPLAYER_APP_DSPCOMM

/* elementfactory information */

static GstElementDetails adecoder_details = {
    "A Generic Audio Decoder",
    "Codec/Decoder/Audio",
    "Decodes Various Audio Formats using the Codec Engine",
    "Yashwant Vijayakumar"
};

static GstStaticPadTemplate sink_template = GST_STATIC_PAD_TEMPLATE("sink",
        GST_PAD_SINK,
        GST_PAD_ALWAYS,
        GST_STATIC_CAPS
        ("audio/mpeg, "
         "mpegversion = (int) { 1, 2, 4 }, "
         "layer = (int) { 1, 2, 3 }; "
         "audio/x-wma, "
         "wmaversion = (int) { 1, 2, 3 }, "
         "rate = (int) { 8000, 11025, 12000, 16000, 22050, 24000, 32000, 44100, 48000 }, "
         "channels = (int) [ 1, 2 ]"));

static GstStaticPadTemplate src_template = GST_STATIC_PAD_TEMPLATE("src",
        GST_PAD_SRC,
        GST_PAD_ALWAYS,
        GST_STATIC_CAPS
        ("audio/x-raw-int, "
         "endianness = (int) " G_STRINGIFY (G_BYTE_ORDER) ", "
         "signed = (boolean)true, "
         "width = (int) 16, depth = (int) 16, "
         "rate = (int) {8000, 11025, 12000, 16000, 22050, 24000, 32000, 44100, 48000 }, "
         "channels = (int) [ 1, 2 ]"));

GType gst_adecoder_get_type(void)
{

    static GType adecoder_type = 0;

    if (!adecoder_type) {
        static const GTypeInfo adecoder_info = {
            sizeof(GstADecoderClass),
            (GBaseInitFunc) gst_adecoder_base_init,
            NULL,
            (GClassInitFunc) gst_adecoder_class_init,
            NULL,
            NULL,
            sizeof(GstADecoder),
            0,
            (GInstanceInitFunc) gst_adecoder_init,
        };

        adecoder_type = g_type_register_static(GST_TYPE_ELEMENT, "GstADecoder",&adecoder_info, 0);
        GST_DEBUG_CATEGORY_INIT(gstadecoder_debug,"adecoder",0,"Generic Audio Decoder Element for the ARM");
    }

    return adecoder_type;
}

static GType gst_adecoder_get_engine_type(void)
{
    static GType adecoder_engine_type = 0;

    if (!adecoder_engine_type) {
        static GEnumValue engine_types[] = {
            { H264ENGINE, "H264Engine","When running H264 or standalone AAC"},
            { MPEG4ENGINE,"MPEG4Engine","When running MPEG4"},
            { WMENGINE,"WMEngine","When running WMV or standalone WMA"},
            { MP3ENGINE,"MP3Engine","When running standalone MP3"},
            { 0,NULL,NULL }
        };

        adecoder_engine_type = g_enum_register_static("AdecoderEngineName",engine_types);
    }

    return adecoder_engine_type;
}

static GType gst_adecoder_get_codec_type(void)
{
    static GType adecoder_codec_type = 0;

    if (!adecoder_codec_type) {
        static GEnumValue codec_types[] = {
            { ST_AUDIO_ARM_MP3, "MP3","When running MP3 Decoder"},
            { ST_AUDIO_WMA,"WMA","When running WMA Decoder"},
            { ST_AUDIO_AAC,"AAC","When running AAC Decoder"},
            { ST_AUDIO_DSP_MP3, "MP3","When running MP3 Decoder"},
            { 0,NULL,NULL }
        };

        adecoder_codec_type = g_enum_register_static("AdecoderCodecName",codec_types);
    }

    return adecoder_codec_type;
}

static GstCaps *gst_adecoder_sink_getcaps(GstPad * pad)
{
    GST_LOG("Sink getcaps invoked");

    return gst_caps_copy(gst_pad_get_pad_template_caps(pad));
}

static GstCaps *gst_adecoder_src_getcaps(GstPad * pad)
{
    GST_LOG("Src getcaps invoked");

    return gst_caps_copy(gst_pad_get_pad_template_caps(pad));
}

static gboolean gst_adecoder_sink_setcaps(GstPad * pad, GstCaps * caps)
{
    GstADecoder *adecoder;
    GstStructure *structure;
    GstCaps *intersection;
    const gchar *mime;

    GST_LOG("Inside set caps...");

    adecoder = GST_ADECODER(GST_PAD_PARENT(pad));

    intersection = gst_caps_intersect(gst_pad_get_pad_template_caps(pad), caps);

    if (gst_caps_is_empty(intersection)) {
        GST_LOG(" Caps empty...");
        return FALSE;
    }

    gst_caps_unref(intersection);

    structure = gst_caps_get_structure(caps, 0);

    mime = gst_structure_get_name(structure);

    if (!strcmp("audio/mpeg", mime)) {
        int version = 0;

        gst_structure_get_int(structure, "mpegversion", &version);

        if (!version) {
            return FALSE;
        }

        if (version == 1) {				//version 1  -> mp3
            adecoder->codec = ST_AUDIO_DSP_MP3;
            GST_INFO("Audio is MP3");
        }
        else {							//version 2 or 4 -> aac
            adecoder->codec = ST_AUDIO_AAC;
            GST_INFO("Audio is AAC");
        }
    }
    else if (!strcmp("audio/x-wma",mime)) {
        adecoder->codec = ST_AUDIO_WMA;
        GST_INFO("Audio is WMA");
    }
    else {
        GST_WARNING(" Unknown mime type...");
        return FALSE;
    }

    adecoder->isCodecPropSet = TRUE;
    return TRUE;

}

static void gst_adecoder_base_init(GstADecoderClass * klass)

{
    GstElementClass *element_class = GST_ELEMENT_CLASS(klass);

    klass->sink_template = gst_static_pad_template_get(&sink_template);
    klass->src_template = gst_static_pad_template_get(&src_template);

    gst_element_class_add_pad_template(element_class, klass->src_template);
    gst_element_class_add_pad_template(element_class,klass->sink_template);
    gst_element_class_set_details(element_class, &adecoder_details);
}

static void gst_adecoder_class_init(GstADecoderClass * klass)
{
    GstElementClass *gstelement_class = (GstElementClass *) klass;
    GObjectClass *object_class = (GObjectClass *)(klass);

    parent_class = g_type_class_ref(GST_TYPE_ELEMENT);

    gstelement_class->change_state = gst_adecoder_change_state;

    object_class->set_property = gst_adecoder_set_property;

    g_object_class_install_property(object_class,ARG_ENGINE_NAME,g_param_spec_enum ("Engine","Engine","Name of Engine to open"
                                    ,GST_TYPE_ADECODER_ENGINE,0,G_PARAM_WRITABLE));

    g_object_class_install_property(object_class,ARG_ENGINE_OPEN,g_param_spec_string ("EngineOverride","EngineOverride","Force a specific engine to open"
                                    ,"",G_PARAM_WRITABLE));

    g_object_class_install_property(object_class,ARG_CODEC_NAME,g_param_spec_enum ("Codec","Codec","Name of Codec to run"
                                    ,GST_TYPE_ADECODER_CODEC,0,G_PARAM_WRITABLE));

    g_object_class_install_property(object_class,ARG_ENABLE_PROCESSING,g_param_spec_boolean ("Enable_Processing","Enable_Processing","Used during dsp on/off"
                                    ,TRUE,G_PARAM_WRITABLE));
}

static void gst_adecoder_set_property (GObject * object, guint prop_id, const GValue * value, GParamSpec * pspec)
{
    GstADecoder *adecoder = GST_ADECODER(object);

    switch (prop_id) {
    case ARG_ENGINE_NAME:
        adecoder->engine = g_value_get_enum(value);
        adecoder->isEnginePropSet = TRUE;
        GST_LOG("Engine handle set to value %x",adecoder->ce);
        break;

    case ARG_ENGINE_OPEN:
        if(adecoder->engineName)
            free(adecoder->engineName);
        adecoder->engineName = g_value_dup_string(value);
        GST_LOG("Engine name set to value %s",adecoder->engineName);
        break;

    case ARG_CODEC_NAME:
        adecoder->codec = g_value_get_enum(value);
        adecoder->isCodecPropSet = TRUE;
        GST_LOG("Codec set to enum value %d",adecoder->codec);
        break;

    case ARG_ENABLE_PROCESSING:
        if (adecoder->enable_processing != g_value_get_boolean(value)) {
            adecoder->enable_processing = g_value_get_boolean(value);

            if (adecoder->enable_processing == TRUE) {
                GST_LOG("Enable_processing");
                pthread_mutex_unlock(adecoder->start_decode);
            }
            else {
                GST_LOG("Disable_processing");
                //pthread_mutex_lock(adecoder->start_decode);
            }
        }
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }

    //gst_object_unref(adecoder); //getting seg.fault when unref is done.
}


static void gst_adecoder_init(GstADecoder * adecoder)

{
    GstADecoderClass *klass = GST_ADECODER_GET_CLASS(adecoder);
    GstElementClass *element_class = GST_ELEMENT_CLASS(klass);

    adecoder->sinkpad = gst_pad_new_from_template(klass->sink_template, "sink");

    gst_pad_set_activate_function(adecoder->sinkpad,GST_DEBUG_FUNCPTR(gst_adecoder_sink_activate));
    gst_pad_set_activatepull_function(adecoder->sinkpad,GST_DEBUG_FUNCPTR(gst_adecoder_sink_activate_pull));
    gst_pad_set_event_function(adecoder->sinkpad,GST_DEBUG_FUNCPTR(gst_adecoder_sink_event));
    gst_pad_set_chain_function(adecoder->sinkpad, gst_adecoder_chain);
    gst_pad_set_getcaps_function(adecoder->sinkpad,gst_adecoder_sink_getcaps);
    gst_pad_set_setcaps_function(adecoder->sinkpad,gst_adecoder_sink_setcaps);

    gst_element_add_pad(GST_ELEMENT(adecoder), adecoder->sinkpad);

    adecoder->srcpad = gst_pad_new_from_template(klass->src_template, "src");

    gst_pad_set_event_function(adecoder->srcpad,GST_DEBUG_FUNCPTR(gst_adecoder_src_event));
    gst_pad_set_getcaps_function(adecoder->srcpad,gst_adecoder_src_getcaps);

    gst_element_add_pad(GST_ELEMENT(adecoder), adecoder->srcpad);

    element_class->query = gst_adecoder_query;

    gst_adecoder_initialize_handle(adecoder);
}


static gboolean gst_adecoder_query(GstElement *element, GstQuery *query)
{
    GstStructure *query_struct;
    GValue bitRate = {0};
    GValue dspLoad = { 0, };

    GstADecoder *adecoder = GST_ADECODER(element);

    g_value_init(&bitRate,G_TYPE_UINT);
    g_value_set_uint(&bitRate,adecoder->Status->bitRate);

    g_value_init(&dspLoad, G_TYPE_INT);
    g_value_set_int(&dspLoad, Engine_getCpuLoad(adecoder->ce));

    query_struct = gst_query_get_structure(query);

    gst_structure_set_value(query_struct,"bitRate",&bitRate);
    gst_structure_set_value(query_struct, "dspCpuLoad", &dspLoad);
    g_value_unset(&bitRate);
    g_value_unset(&dspLoad);

    gst_object_unref(adecoder);

    return TRUE;
}

static void gst_adecoder_initialize_handle(GstADecoder * adecoder)
{
    adecoder->firstFrame = TRUE;
    adecoder->inbuf = adecoder->inbuf_start_addr = NULL;
    adecoder->outbuf[0] = NULL;
    adecoder->outbuf[1] = NULL;
    adecoder->readOffset = 0;
    adecoder->codec_handle = NULL;
    adecoder->ce = NULL;
    adecoder->bytes_not_decoded = 0;
    adecoder->bytesConsumed = 0;
    adecoder->frameCount = 0;
    adecoder->isEof = FALSE;
    adecoder->isEos = FALSE;
    adecoder->codec = 0;
    adecoder->engine = 0;
    adecoder->isEnginePropSet = FALSE;
    adecoder->isCodecPropSet = FALSE;
    adecoder->isPaused = FALSE;
    adecoder->isStopped = FALSE;
    adecoder->frame_time = 0;
    adecoder->enable_processing = TRUE;
    adecoder->start_decode = malloc(sizeof(pthread_mutex_t));
    pthread_mutex_init(adecoder->start_decode, NULL);
    pthread_mutex_lock(adecoder->start_decode);
}

static gboolean gst_adecoder_sink_activate(GstPad * sinkpad)
{
    GstADecoder *adecoder = GST_ADECODER(gst_pad_get_parent(sinkpad));

    GST_LOG("Sink Activate invoked");

    if (gst_pad_check_pull_range(sinkpad)) {
        adecoder->isInPushMode = FALSE;

        gst_object_unref(adecoder);

        return gst_pad_activate_pull(sinkpad, TRUE);
    }

    else {
        GST_LOG("pull_range not supported on sinkpad");
        GST_LOG("Running In Push Mode");

        adecoder->isInPushMode = TRUE;

        gst_object_unref(adecoder);

        return gst_pad_activate_push(sinkpad, TRUE);
    }
}

static gboolean gst_adecoder_sink_activate_pull(GstPad * sinkpad, gboolean active)
{
    GstADecoder *adecoder = GST_ADECODER(gst_pad_get_parent(sinkpad));

    GST_LOG("Activate pull invoked");

    if (active)
        gst_pad_start_task(sinkpad, (GstTaskFunction) gst_adecoder_loop,sinkpad);/* if we have a scheduler we can start the task */
    else
        gst_pad_stop_task(sinkpad);

    gst_object_unref(adecoder);

    return TRUE;
}

static void gst_adecoder_loop(GstPad * pad)
{
    GstADecoder *adecoder = GST_ADECODER(gst_pad_get_parent(pad));

    if (adecoder->isEos == FALSE && adecoder->isStopped == FALSE) {

        if (adecoder->initialized == FALSE) {
            if ((adecoder->initialized = gst_adecoder_decoder_initialize(adecoder,NULL)) == FALSE) {
                GST_LOG("Could not initialize");
                gst_object_unref(adecoder);
                return;
            }
        }

        if (gst_adecoder_loop_get_data(adecoder) == FALSE) {
            gst_object_unref(adecoder);
            return;
        }

        if (adecoder->enable_processing == FALSE) {
            GST_LOG("Waiting on lock");
            pthread_mutex_lock(adecoder->start_decode);
            GST_LOG("Lock succeeded");
        }

        if (adecoder->isPaused == TRUE) {
            gst_object_unref(adecoder);
            return;
        }

        if (gst_adecoder_process_data(adecoder) == FALSE) {
            gst_object_unref(adecoder);
            return;
        }

        if (adecoder->firstFrame == TRUE)
            gst_adecoder_initialize_osssink(adecoder);

        gst_adecoder_output_data(adecoder);
    }

    gst_object_unref(adecoder);
}

static gboolean gst_adecoder_loop_get_data(GstADecoder *adecoder)
{
    GstBuffer *buf = NULL;

    if (adecoder->bytes_not_decoded < adecoder->inBufSize && adecoder->isEof == FALSE) {
        if (gst_pad_pull_range(adecoder->sinkpad,adecoder->readOffset,INPUT_BUFFER_SIZE - adecoder->bytes_not_decoded,&buf) != GST_FLOW_OK) {
            GST_LOG("Pad pull range failed");

            gst_pad_push_event(adecoder->srcpad, gst_event_new_eos());

            return FALSE;
        }

        if (GST_BUFFER_SIZE(buf) < INPUT_BUFFER_SIZE - adecoder->bytes_not_decoded)
            adecoder->isEof = TRUE;

        memmove(adecoder->inbuf_start_addr,adecoder->inbuf,adecoder->bytes_not_decoded);

        adecoder->inbuf = adecoder->inbuf_start_addr;

        memcpy((guint8 *) ((guint32) adecoder->inbuf + adecoder->bytes_not_decoded),GST_BUFFER_DATA(buf), GST_BUFFER_SIZE(buf));

        adecoder->bytes_not_decoded += GST_BUFFER_SIZE(buf);

        adecoder->readOffset += GST_BUFFER_SIZE(buf);

        gst_buffer_unref(buf);

    }

    if (adecoder->bytes_not_decoded <= 10) {
        GST_LOG("No more data to decode....Sending EOS event to terminate...");
        GST_LOG("Bytes not decoded:%d",adecoder->bytes_not_decoded);

        gst_pad_push_event(adecoder->srcpad, gst_event_new_eos());

        adecoder->isEos = TRUE;

        return FALSE;
    }

    return TRUE;
}

static GstFlowReturn gst_adecoder_chain(GstPad * pad,GstBuffer * input_buffer)
{
    GstADecoder *adecoder = GST_ADECODER(gst_pad_get_parent(pad));

    if (adecoder->initialized == FALSE) {

        if ((adecoder->initialized = gst_adecoder_decoder_initialize(adecoder,input_buffer)) == FALSE) {
            GST_LOG("Could not initialize");
            gst_object_unref(adecoder);
            gst_buffer_unref(input_buffer);
            return GST_FLOW_ERROR;
        }

        adecoder->initTimeStamp = GST_BUFFER_TIMESTAMP(input_buffer);
    }

    GST_LOG("Buffer size from Demuxer:%d",GST_BUFFER_SIZE(input_buffer));

    if (gst_adecoder_chain_get_data(adecoder,input_buffer) == FALSE) {
        gst_object_unref(adecoder);
        return GST_FLOW_OK;
    }

    while (adecoder->bytes_not_decoded > adecoder->inBufSize && adecoder->isStopped == FALSE) {
        if (adecoder->enable_processing == FALSE) {
            GST_LOG("Waiting on lock");
            pthread_mutex_lock(adecoder->start_decode);
            GST_LOG("Lock succeeded");
        }

        if ( gst_adecoder_process_data(adecoder) == FALSE) {
            gst_object_unref(adecoder);

            return GST_FLOW_OK;
        }

        if (adecoder->firstFrame == TRUE)
            gst_adecoder_initialize_osssink(adecoder);

        if (gst_adecoder_output_data(adecoder) == FALSE) {
            gst_object_unref(adecoder);
            return GST_FLOW_ERROR;
        }
    }

    gst_object_unref(adecoder);

    return GST_FLOW_OK;
}

static gboolean gst_adecoder_chain_get_data(GstADecoder *adecoder,GstBuffer * buffer)
{
    guint32 buf_size = GST_BUFFER_SIZE(buffer);

    if (adecoder->inbuf != adecoder->inbuf_start_addr) {
        memmove(adecoder->inbuf_start_addr,adecoder->inbuf,adecoder->bytes_not_decoded);
        adecoder->inbuf = adecoder->inbuf_start_addr;
    }

    if (adecoder->bytes_not_decoded + buf_size >= INPUT_BUFFER_SIZE) {
        GST_LOG("Push mode:Not enough memory to save the buffer....current size:%d",adecoder->bytes_not_decoded);
        gst_buffer_unref(buffer);
        return FALSE;
    }

    memcpy((guint8 *) ((guint32) adecoder->inbuf + adecoder->bytes_not_decoded),(char *)(GST_BUFFER_DATA(buffer)),buf_size);

    gst_buffer_unref(buffer);

    adecoder->bytes_not_decoded += buf_size;

    if (adecoder->bytes_not_decoded < adecoder->inBufSize)
        return FALSE;

    return TRUE;
}


static gboolean gst_adecoder_process_data(GstADecoder *adecoder)

{
    GST_LOG("Before calling AUDDEC_process");

    int status;

    status = AUDDEC_process(adecoder->codec_handle,&(adecoder->inBufDesc),&(adecoder->outBufDesc),adecoder->InArgs,adecoder->OutArgs);

    adecoder->inbuf += adecoder->OutArgs->bytesConsumed;
    adecoder->bytes_not_decoded -= adecoder->OutArgs->bytesConsumed;

    if (status == AUDDEC_EOK) {
        GST_LOG("AUDDEC_process returned successfully,bytesConsumed:%d",adecoder->OutArgs->bytesConsumed);

        AUDDEC_control(adecoder->codec_handle,XDM_GETSTATUS,adecoder->DyParams,adecoder->Status);

        adecoder->frameCount++;

        GST_LOG("FrameCount:%u FrameLen:%d SampleRate:%d BitsPerSample:%d BitRate:%d %s notDecoded:%d",adecoder->frameCount,
                adecoder->Status->frameLen,adecoder->Status->sampleRate,adecoder->Status->outputBitsPerSample,adecoder->Status->bitRate,
                (adecoder->Status->numChannels == IAUDIO_MONO)?"IAUDIO_MONO":"IAUDIO_STEREO", adecoder->bytes_not_decoded);

        return TRUE;
    }
    else {
        GST_LOG("AUDDEC_process returned with failure");
        return FALSE;
    }
}

static void gst_adecoder_initialize_osssink(GstADecoder *adecoder)
{
    GstCaps *othercaps, *getcaps;
    GstStructure *structure;
    gint rate;
    othercaps = gst_caps_new_simple ("audio/x-raw-int",
                                     "endianness", G_TYPE_INT, G_BYTE_ORDER,
                                     "signed", G_TYPE_BOOLEAN, TRUE,
                                     "width", G_TYPE_INT, 16,
                                     "depth", G_TYPE_INT, 16,
                                     "rate", G_TYPE_INT,adecoder->Status->sampleRate,
                                     "channels", G_TYPE_INT,DEFAULT_NO_OF_OUTPUT_CHANNELS, NULL);

    if (gst_pad_set_caps(adecoder->srcpad, othercaps) == FALSE) {
        GST_LOG("Initialize osssink failed");
    }

    gst_pad_use_fixed_caps(adecoder->srcpad);

    getcaps = gst_pad_get_caps(adecoder->srcpad);

    structure = gst_caps_get_structure(getcaps, 0);

    gst_structure_get_int(structure,"rate",&rate);

    GST_LOG("Sampling Rate:%d",rate);

    gst_caps_unref(othercaps);

    if (adecoder->isInPushMode == FALSE) {
        gst_pad_push_event(adecoder->srcpad,
                           gst_event_new_new_segment(	FALSE,
                                                      adecoder->segment.rate,
                                                      adecoder->segment.format,
                                                      adecoder->segment.start,
                                                      adecoder->segment.duration,
                                                      adecoder->segment.start));
    }

    if (adecoder->firstFrame == TRUE)
        adecoder->firstFrame = FALSE;
}

static gboolean gst_adecoder_output_data(GstADecoder *adecoder)
{
    GstFlowReturn ret;
    GstBuffer *outbuf[2];
    guint32 frameLen = adecoder->Status->frameLen;
    guint32 bitsPerSample = adecoder->Status->outputBitsPerSample;
    guint8 numChannels = adecoder->Status->numChannels;

    if (frameLen != 0) {
        int i;

        for ( i = 0; (i < adecoder->outBufDesc.numBufs);i++) {
            ret = gst_pad_alloc_buffer(adecoder->srcpad, GST_BUFFER_OFFSET_NONE,frameLen * DEFAULT_NO_OF_OUTPUT_CHANNELS * bitsPerSample / 8
                                       ,GST_PAD_CAPS(adecoder->srcpad),&(outbuf[i]));


            if (ret != GST_FLOW_OK) {
                GST_LOG("failed when allocating a %ld bytes buffer",frameLen * DEFAULT_NO_OF_OUTPUT_CHANNELS * bitsPerSample / 8);

                if ( (adecoder->isInPushMode == FALSE) && (adecoder->isPaused == FALSE) )
                    gst_pad_pause_task(adecoder->sinkpad);

                return FALSE;
            }

        }

        for ( i = 0;(i < adecoder->outBufDesc.numBufs);i++) {
            if (adecoder->codec == ST_AUDIO_WMA && numChannels == IAUDIO_MONO) //WMA Decoder is not replicating data in case of MONO.
            {
                memcpy((char *)((unsigned)(adecoder->outbuf[i]) + frameLen * bitsPerSample / 8),
                       (char *)(adecoder->outbuf[i]),frameLen * bitsPerSample / 8);

                block_to_interleaved((XDAS_Int16 *)(adecoder->outbuf[i]),(XDAS_Int8 *)GST_BUFFER_DATA(outbuf[i]),frameLen);

            }
            else {
#ifndef INTERLEAVED
                block_to_interleaved((XDAS_Int16 *)(adecoder->outbuf[i]),(XDAS_Int8 *)GST_BUFFER_DATA(outbuf[i]),frameLen);
#endif
                memcpy((char *)GST_BUFFER_DATA(outbuf[i]),(char *)adecoder->outbuf[i],
                       frameLen * DEFAULT_NO_OF_OUTPUT_CHANNELS * bitsPerSample / 8);
            }

            if (gst_adecoder_send_data(adecoder,outbuf[i]) != GST_FLOW_OK) {
                GST_LOG("Send data to osssink failed");
                return FALSE;
            }
        }
    }

    return TRUE;
}


static gboolean gst_adecoder_decoder_initialize(GstADecoder * adecoder,GstBuffer * input_buffer)
{
    GST_LOG ("Entering gst_adecoder_decoder_initialize...");

    if (gst_adecoder_open_engine(adecoder) == FALSE)
        return FALSE;

    if (gst_adecoder_create_codec(adecoder,input_buffer) == FALSE)
        return FALSE;

    if (gst_adecoder_initialize_codec(adecoder) == FALSE)
        return FALSE;

    if (adecoder->isInPushMode == FALSE)
        gst_segment_init(&adecoder->segment, GST_FORMAT_TIME);

    GST_LOG ("Exiting gst_adecoder_decoder_initialize...");

    return TRUE;
}

static gboolean gst_adecoder_open_engine(GstADecoder * adecoder)
{
    CERuntime_init();
    //GT_set("*=01234567,CE-1");

    if (adecoder->engineName) {
        //Engine override is set
        GST_LOG ("Opening %s Engine ...", adecoder->engineName);
        adecoder->ce = Engine_open(adecoder->engineName, NULL, NULL);
    }
    else if (adecoder->isEnginePropSet == TRUE) {
        switch (adecoder->engine) {
        case H264ENGINE:
            GST_LOG ("Opening H264 Engine...");
            adecoder->ce = Engine_open("H264Engine", NULL, NULL);
            break;

        case MPEG4ENGINE:
            GST_LOG ("Opening MPEG4 Engine...");
            adecoder->ce = Engine_open("MPEG4Engine", NULL, NULL);
            break;

        case WMENGINE:
            GST_LOG ("Opening WM Engine...");
            adecoder->ce = Engine_open("WMEngine", NULL, NULL);
            break;

        case MP3ENGINE:
            GST_LOG ("Opening MP3 Engine...");
            adecoder->ce = Engine_open("MP3Engine", NULL, NULL);
            break;
        }
    }
    else if (adecoder->isCodecPropSet == TRUE) {	//standalone case
        switch (adecoder->codec) {
        case ST_AUDIO_AAC:
            GST_LOG ("Opening MPEG4 Engine...");
            adecoder->ce = Engine_open("MPEG4Engine", NULL, NULL);
            break;

        case ST_AUDIO_WMA:
            GST_LOG ("Opening WM Engine...");
            adecoder->ce = Engine_open("WMEngine", NULL, NULL);
            break;

        case ST_AUDIO_DSP_MP3:
            GST_LOG ("Opening MP3 Engine...");
            adecoder->ce = Engine_open("MP3Engine", NULL, NULL);
            break;
        }
    }
    else {
        GST_LOG ("Both properties not Set.....Opening H264 Engine by default..");
        adecoder->ce = Engine_open("H264Engine", NULL, NULL);
    }

    if ( adecoder->ce == NULL) {
        GST_LOG("Engine_open failed");
        return FALSE;
    }

    return TRUE;
}

static gboolean gst_adecoder_create_codec(GstADecoder * adecoder,GstBuffer * input_buffer)
{
    IAUDDEC_Params audParams;
    AUDDEC_Handle audDec;

    switch (adecoder->codec) {
    case ST_AUDIO_WMA: {
        GST_LOG ("Creating WMA Codec...");

        audParams.size            =  sizeof(IAUDDEC_Params);
        audParams.maxSampleRate	=  48000;
        audParams.maxBitrate	=  384000;
        audParams.maxNoOfCh	=  IAUDIO_STEREO;
        audParams.dataEndianness	=  XDM_BYTE;

        audDec = AUDDEC_create(adecoder->ce, "wma9dec", &audParams);

        if (audDec == NULL) {
            GST_LOG("Engine handle:%x",adecoder->ce);
            GST_LOG("AUDDEC_create failed");
            return FALSE;
        }
    }
    break;

    case ST_AUDIO_AAC: {
        GST_LOG ("Creating AAC Codec...");

        audParams.size             =  sizeof(IAUDDEC_Params);
        audParams.maxSampleRate    =  48000;
        audParams.maxBitrate       =  448000;
        audParams.maxNoOfCh	       =  IAUDIO_STEREO;
        audParams.dataEndianness   =  XDM_BYTE;

        audDec = AUDDEC_create(adecoder->ce, "aachedec", &audParams);

        if (audDec == NULL) {
            GST_LOG("AUDDEC_create failed");
            return FALSE;
        }
    }
    break;

    case ST_AUDIO_DSP_MP3: {
        GST_LOG ("Creating DSP MP3 Codec...");

        audParams.size             =  sizeof(IAUDDEC_Params);
        audParams.maxSampleRate    =  48000;
        audParams.maxBitrate       =  448000;
        audParams.maxNoOfCh	       =  IAUDIO_STEREO;
        audParams.dataEndianness   =  XDM_BYTE;

        audDec = AUDDEC_create(adecoder->ce, "mp3dec", &audParams);

        if (audDec == NULL) {
            GST_LOG("AUDDEC_create failed");
            return FALSE;
        }
    }
    break;

    default:
        return FALSE;
    }
    adecoder->codec_handle = audDec;

    GST_LOG ("Exiting Create Codec...");
    return TRUE;
}

static gboolean gst_adecoder_initialize_codec(GstADecoder * adecoder)
{
    IAUDDEC_DynamicParams *audDynamicParams = NULL;
    IAUDDEC_Status *audStatus = NULL;
    IAUDDEC_InArgs *InArgs = NULL;
    IAUDDEC_OutArgs *OutArgs = NULL;
    XDAS_Int32 ret2,ret3,ret4;

    GST_LOG ("Entering gst_adecoder_initialize_codec...");

    switch (adecoder->codec) {
    case ST_AUDIO_WMA: {
        InArgs = (IAUDDEC_InArgs *)malloc(sizeof(IAUDDEC_InArgs));
        InArgs->size =  sizeof(IAUDDEC_InArgs);

        OutArgs = (IAUDDEC_OutArgs *)malloc(sizeof(IAUDDEC_OutArgs));
        OutArgs->size = sizeof(IAUDDEC_OutArgs);

        audStatus = (IAUDDEC_Status *)malloc(sizeof(IAUDDEC_Status));
        audStatus->size =	sizeof(IAUDDEC_Status);

        audDynamicParams = (IAUDDEC_DynamicParams *)malloc(sizeof(IAUDDEC_DynamicParams));
        audDynamicParams->size = sizeof(IAUDDEC_DynamicParams);
    }

    break;

    case ST_AUDIO_AAC: {
        audStatus = (IAUDDEC_Status *)malloc(sizeof(IAUDDEC_Status));
        audStatus->size =	sizeof(IAUDDEC_Status);
    }
    break;
    }

    if (InArgs == NULL) {
        InArgs = (IAUDDEC_InArgs *)malloc(sizeof(IAUDDEC_InArgs));
        InArgs->size = sizeof(IAUDDEC_InArgs);
    }

    if (audDynamicParams == NULL) {
        audDynamicParams = (IAUDDEC_DynamicParams *)malloc(sizeof(IAUDDEC_DynamicParams));
        audDynamicParams->size = sizeof(IAUDDEC_DynamicParams);
    }

    if (OutArgs == NULL) {
        OutArgs	= (IAUDDEC_OutArgs *)malloc(sizeof(IAUDDEC_OutArgs));
        OutArgs->size = sizeof(IAUDDEC_OutArgs);
    }

    if (audStatus == NULL) {
        audStatus	= (IAUDDEC_Status *)malloc(sizeof(IAUDDEC_Status));
        audStatus->size = sizeof(IAUDDEC_Status);
    }

    GST_LOG("handle:%x audDynamicParams:%x Status:%x",adecoder->codec_handle,audDynamicParams,audStatus);

    // ret1 = AUDDEC_control(adecoder->codec_handle,XDM_RESET,audDynamicParams,audStatus);

    //	if (ret1 != AUDDEC_EOK)
    //{
    //  GST_LOG("AUDDEC_control() 1 returned failure in gst_adecoder_initialize_codec()");
    //return FALSE;
    //}


    ret2 = AUDDEC_control(adecoder->codec_handle,XDM_SETDEFAULT,audDynamicParams,audStatus);


#ifdef INTERLEAVED
    audDynamicParams->outputFormat = IAUDIO_INTERLEAVED;
#else
    audDynamicParams->outputFormat = IAUDIO_BLOCK;
#endif

    ret3 = AUDDEC_control(adecoder->codec_handle,XDM_SETPARAMS, audDynamicParams,audStatus);
    ret4 = AUDDEC_control(adecoder->codec_handle,XDM_GETBUFINFO, audDynamicParams, audStatus);

    if (ret2!= AUDDEC_EOK) {
        GST_LOG("AUDDEC_control() 2 returned failure in gst_adecoder_initialize_codec()");

        return FALSE;
    }

    //	if (ret1 != AUDDEC_EOK || ret2!= AUDDEC_EOK || ret3 != AUDDEC_EOK || ret4 != AUDDEC_EOK)
    if (ret3 != AUDDEC_EOK) {
        GST_LOG("AUDDEC_control() 3 returned failure in gst_adecoder_initialize_codec()");

        return FALSE;
    }

    if (ret4 != AUDDEC_EOK) {

        GST_LOG("AUDDEC_control() 4 returned failure in gst_adecoder_initialize_codec()");

        return FALSE;
    }



    adecoder->inBufSize = audStatus->bufInfo.minInBufSize[0];

    GST_LOG("Input buffer size = %ld",audStatus->bufInfo.minInBufSize[0]);

    adecoder->inbuf_start_addr = adecoder->inbuf = (XDAS_Int8 *) Memory_contigAlloc(INPUT_BUFFER_SIZE,Memory_DEFAULTALIGNMENT);

    if (adecoder->inbuf == NULL) {
        GST_LOG("Failed to allocate input buffer using CMEM");

        return FALSE;
    }

    int i;

    for (i = 0;i < audStatus->bufInfo.minNumOutBufs;i++) {
        adecoder->outBufSize[i] = audStatus->bufInfo.minOutBufSize[i];

        adecoder->outbuf[i] = (XDAS_Int8 *) Memory_contigAlloc(adecoder->outBufSize[i],Memory_DEFAULTALIGNMENT);

        if (adecoder->outbuf[i] == NULL) {
            GST_LOG("Failed to allocate output buffer using CMEM");

            return FALSE;
        }
    }

    adecoder->outBufDesc.bufSizes = adecoder->outBufSize;
    adecoder->outBufDesc.numBufs = audStatus->bufInfo.minNumOutBufs;
    adecoder->inBufDesc.bufSizes = &adecoder->inBufSize;
    adecoder->inBufDesc.numBufs = 1;
    adecoder->outBufDesc.bufs = adecoder->outbuf;
    adecoder->inBufDesc.bufs = &(adecoder->inbuf);
    InArgs->numBytes = adecoder->inBufSize;
    adecoder->DyParams = audDynamicParams;
    adecoder->Status = audStatus;
    adecoder->InArgs = InArgs;
    adecoder->OutArgs = OutArgs;

    GST_LOG ("Exiting gst_adecoder_initialize_codec...");

    return TRUE;
}


static void block_to_interleaved(XDAS_Int16 *src,XDAS_Int8 *dest,guint32 frameLen)
{
    short i,j;
    unsigned short c,c1,c2;

    for (i = 0,j = 0;i < frameLen;i++,j += 4) {
        /* Left Channel */
        c  = src[i];
        c1 = (c >> 8) & 0xff;
        c2 = c & 0xff;
        dest[j] = c2;
        dest[j+1] = c1;

        /* Right Channel  */
        c    = src[i+frameLen];
        c1 = (c >> 8) & 0xff;
        c2 = c & 0xff;
        dest[j+2] = c2;
        dest[j+3] = c1;
    }
}

#ifdef PROFILE
static unsigned long timeDiffMs(struct timespec *pTs1, struct timespec *pTs2)
{
    unsigned long  delta = 0;

    pTs1->tv_nsec /= 1000;
    pTs2->tv_nsec /= 1000;

    delta  = pTs2->tv_nsec > pTs1->tv_nsec ?
             (pTs2->tv_sec - pTs1->tv_sec) * 1000000 + (pTs2->tv_nsec - pTs1->tv_nsec) :
             (pTs2->tv_sec - pTs1->tv_sec - 1) * 1000000 + (1000000 - pTs1->tv_nsec + pTs2->tv_nsec);

    return (delta);
}
#endif

static GstFlowReturn gst_adecoder_send_data(GstADecoder * adecoder,GstBuffer * buffer)
{
#ifdef TIMESTAMP_ENABLE
    if (adecoder->isInPushMode == TRUE) {	//No timestamps in case of Pull mode / Standalone playback.
        if (adecoder->frame_time == 0 && adecoder->initTimeStamp != 0)
            adecoder->frame_time = adecoder->initTimeStamp;
        else
            adecoder->frame_time += adecoder->Status->frameLen * NANO_SEC_PER_SEC / adecoder->Status->sampleRate;

        GST_BUFFER_TIMESTAMP(buffer) = adecoder->frame_time;
        GST_LOG("Timestamp value:%llu",GST_BUFFER_TIMESTAMP(buffer));
    }
#endif

    return gst_pad_push(adecoder->srcpad, buffer);
}

static gboolean gst_adecoder_sink_event(GstPad * pad, GstEvent * event)
{
    GstADecoder *adecoder = GST_ADECODER(GST_PAD_PARENT(pad));

    gboolean ret = TRUE;

    GST_LOG("Got %s event on sink pad", GST_EVENT_TYPE_NAME(event));

    switch (GST_EVENT_TYPE(event)) {
    case GST_EVENT_NEWSEGMENT:
        ret = gst_pad_event_default(pad, event);
        break;

    case GST_EVENT_FLUSH_START:
        ret = gst_pad_event_default(pad, event);
        break;

    case GST_EVENT_FLUSH_STOP:
        ret = gst_pad_event_default(pad, event);
        break;

    case GST_EVENT_EOS:
        gst_adecoder_decode_remaining(adecoder);
        ret = gst_pad_event_default(pad, event);
        break;

    default:
        ret = gst_pad_event_default(pad, event);
        break;
    }

    return ret;
}

static void gst_adecoder_decode_remaining(GstADecoder * adecoder)
{
    while (adecoder->bytes_not_decoded > 10 && adecoder->isStopped == FALSE) {
        if (adecoder->enable_processing == FALSE)
            pthread_mutex_lock(adecoder->start_decode);

        gst_adecoder_process_data(adecoder);

        if (gst_adecoder_output_data(adecoder) == FALSE)
            break;
    }

    GST_LOG("bytes_not_decoded:%d",adecoder->bytes_not_decoded);
}

static gboolean gst_adecoder_src_event(GstPad * pad, GstEvent * event)
{
    gboolean res;
    GstADecoder *adecoder;

    adecoder = GST_ADECODER(GST_PAD_PARENT(pad));

    if (adecoder->codec_handle == NULL)
        goto no_decoder;

    res = gst_pad_push_event(adecoder->sinkpad, event);

    return res;

no_decoder:
    {
        GST_DEBUG_OBJECT(adecoder, "no decoder, cannot handle event");
        gst_event_unref(event);
        return FALSE;
    }
}

static GstStateChangeReturn gst_adecoder_change_state(GstElement * element, GstStateChange transition)
{
    GstStateChangeReturn result;
    GstADecoder *adecoder = GST_ADECODER(element);

    switch (transition) {
    case GST_STATE_CHANGE_NULL_TO_READY:
        GST_DEBUG_OBJECT(adecoder, "State Changed from NULL_TO_READY");
        break;

    case GST_STATE_CHANGE_READY_TO_PAUSED:
        GST_DEBUG_OBJECT(adecoder, "State Changed from READY_TO_PAUSED");
        break;

    case GST_STATE_CHANGE_PAUSED_TO_PLAYING:
        adecoder->isPaused = FALSE;
        break;

    default:
        break;
    }

    result = GST_ELEMENT_CLASS(parent_class)->change_state(element, transition);

    switch (transition) {
    case GST_STATE_CHANGE_PLAYING_TO_PAUSED:
        adecoder->isPaused = TRUE;
        GST_LOG("playing to paused");
        break;

    case GST_STATE_CHANGE_PAUSED_TO_READY:
        adecoder->isStopped = TRUE;
        GST_LOG("paused to ready");
        break;

    case GST_STATE_CHANGE_READY_TO_NULL:
        GST_LOG("Ready to Null");
        free(adecoder->DyParams);
        free(adecoder->Status);
        free(adecoder->InArgs);
        free(adecoder->OutArgs);
        pthread_mutex_unlock(adecoder->start_decode);
        free(adecoder->start_decode);

        if (adecoder->inbuf != NULL)
            Memory_contigFree((XDAS_Int8 *) (adecoder->inbuf_start_addr),INPUT_BUFFER_SIZE);

        if (adecoder->outbuf[0] != NULL)
            Memory_contigFree((XDAS_Int8 *) (adecoder->outbuf[0]),adecoder->outBufSize[0]);

        if (adecoder->outbuf[1] != NULL)
            Memory_contigFree((XDAS_Int8 *) (adecoder->outbuf[1]),adecoder->outBufSize[1]);

        if (adecoder->codec_handle)
            AUDDEC_delete(adecoder->codec_handle);
        if (adecoder->ce)
            Engine_close(adecoder->ce);
        gst_adecoder_initialize_handle(adecoder);
        break;

    default:
        break;
    }

    return result;
}
