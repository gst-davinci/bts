#ifndef xdc_rts__
#define xdc_rts__


/*
 * ======== interface xdc.rts.IModule ========
 */

typedef struct xdc_rts_IModule_Fxns__ xdc_rts_IModule_Fxns__;
typedef const xdc_rts_IModule_Fxns__* xdc_rts_IModule_Module;
typedef struct xdc_rts_IModule_Config xdc_rts_IModule_Config;

/*
 * ======== module xdc.rts.Error ========
 */

typedef struct xdc_rts_Error_Block xdc_rts_Error_Block;
typedef struct xdc_rts_Error_Config xdc_rts_Error_Config;

/*
 * ======== module xdc.rts.Memory ========
 */

typedef struct xdc_rts_Memory_Stats xdc_rts_Memory_Stats;
typedef struct xdc_rts_Memory_Module__Object xdc_rts_Memory_Module__Object;
typedef struct xdc_rts_Memory_Config xdc_rts_Memory_Config;

/*
 * ======== interface xdc.rts.IHeap ========
 */

typedef struct xdc_rts_IHeap_Fxns__ xdc_rts_IHeap_Fxns__;
typedef const xdc_rts_IHeap_Fxns__* xdc_rts_IHeap_Module;
typedef struct xdc_rts_IHeap_Config xdc_rts_IHeap_Config;
typedef struct xdc_rts_IHeap_Params xdc_rts_IHeap_Params;
typedef struct xdc_rts_IHeap_Instance__Object xdc_rts_IHeap_Instance__Object;
#ifdef __cplusplus
namespace xdc_rts { namespace IHeap { struct __Object; }}
typedef struct xdc_rts_IHeap___Object { xdc_rts_IHeap_Fxns__* __fxns; } * xdc_rts_IHeap___Instance;
typedef xdc_rts::IHeap::__Object* xdc_rts_IHeap_Instance;
#else
typedef struct xdc_rts_IHeap___Object { xdc_rts_IHeap_Fxns__* __fxns; } * xdc_rts_IHeap_Instance;
typedef xdc_rts_IHeap_Instance xdc_rts_IHeap___Instance;
#endif


/*
 * ======== module xdc.rts.HeapMin ========
 */

typedef struct xdc_rts_HeapMin_Instance__Object xdc_rts_HeapMin_Instance__Object;
typedef struct xdc_rts_HeapMin_Fxns__ xdc_rts_HeapMin_Fxns__;
typedef const xdc_rts_HeapMin_Fxns__* xdc_rts_HeapMin_Module;
typedef struct xdc_rts_HeapMin_Config xdc_rts_HeapMin_Config;
typedef struct xdc_rts_HeapMin_Params xdc_rts_HeapMin_Params;
#ifdef __cplusplus
namespace xdc_rts { namespace HeapMin { struct Object; }}
typedef xdc_rts::HeapMin::Object xdc_rts_HeapMin_Object;
typedef xdc_rts_HeapMin_Object* xdc_rts_HeapMin___Instance;
typedef xdc_rts_HeapMin_Object* xdc_rts_HeapMin_Instance;
#else
typedef struct xdc_rts_HeapMin_Instance__Object xdc_rts_HeapMin_Object;
typedef xdc_rts_HeapMin_Object* xdc_rts_HeapMin___Instance;
typedef xdc_rts_HeapMin___Instance xdc_rts_HeapMin_Instance;
#endif


/*
 * ======== module xdc.rts.HeapStd ========
 */

typedef struct xdc_rts_HeapStd_Instance__Object xdc_rts_HeapStd_Instance__Object;
typedef struct xdc_rts_HeapStd_Fxns__ xdc_rts_HeapStd_Fxns__;
typedef const xdc_rts_HeapStd_Fxns__* xdc_rts_HeapStd_Module;
typedef struct xdc_rts_HeapStd_Config xdc_rts_HeapStd_Config;
typedef struct xdc_rts_HeapStd_Params xdc_rts_HeapStd_Params;
#ifdef __cplusplus
namespace xdc_rts { namespace HeapStd { struct Object; }}
typedef xdc_rts::HeapStd::Object xdc_rts_HeapStd_Object;
typedef xdc_rts_HeapStd_Object* xdc_rts_HeapStd___Instance;
typedef xdc_rts_HeapStd_Object* xdc_rts_HeapStd_Instance;
#else
typedef struct xdc_rts_HeapStd_Instance__Object xdc_rts_HeapStd_Object;
typedef xdc_rts_HeapStd_Object* xdc_rts_HeapStd___Instance;
typedef xdc_rts_HeapStd___Instance xdc_rts_HeapStd_Instance;
#endif


/*
 * ======== interface xdc.rts.ISystemProvider ========
 */

typedef struct xdc_rts_ISystemProvider_Fxns__ xdc_rts_ISystemProvider_Fxns__;
typedef const xdc_rts_ISystemProvider_Fxns__* xdc_rts_ISystemProvider_Module;
typedef struct xdc_rts_ISystemProvider_Config xdc_rts_ISystemProvider_Config;

/*
 * ======== module xdc.rts.System ========
 */

typedef struct xdc_rts_System_Fxns__ xdc_rts_System_Fxns__;
typedef const xdc_rts_System_Fxns__* xdc_rts_System_Module;
typedef struct xdc_rts_System_Config xdc_rts_System_Config;

/*
 * ======== module xdc.rts.SysMin ========
 */

typedef struct xdc_rts_SysMin_Fxns__ xdc_rts_SysMin_Fxns__;
typedef const xdc_rts_SysMin_Fxns__* xdc_rts_SysMin_Module;
typedef struct xdc_rts_SysMin_Config xdc_rts_SysMin_Config;

/*
 * ======== module xdc.rts.SysStd ========
 */

typedef struct xdc_rts_SysStd_Fxns__ xdc_rts_SysStd_Fxns__;
typedef const xdc_rts_SysStd_Fxns__* xdc_rts_SysStd_Module;
typedef struct xdc_rts_SysStd_Config xdc_rts_SysStd_Config;

/*
 * ======== module xdc.rts.SysNull ========
 */

typedef struct xdc_rts_SysNull_Fxns__ xdc_rts_SysNull_Fxns__;
typedef const xdc_rts_SysNull_Fxns__* xdc_rts_SysNull_Module;
typedef struct xdc_rts_SysNull_Config xdc_rts_SysNull_Config;

/*
 * ======== module xdc.rts._SysProxy ========
 */

typedef struct xdc_rts__SysProxy_Fxns__ xdc_rts__SysProxy_Fxns__;
typedef const xdc_rts__SysProxy_Fxns__* xdc_rts__SysProxy_Module;
typedef struct xdc_rts__SysProxy_Config xdc_rts__SysProxy_Config;
#endif /* xdc_rts__ */ 
